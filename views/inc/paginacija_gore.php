<div id="paginacijaGore" style="float: right;  line-height: 27px; height:27px; border: 0px solid green;">
    <?php
    if (isset($brStrana)) {
        echo "<div id='paginacijaGoreStrane' class='pagination' style='float:left;'>";
        if ($trenutna >= 1) {
            echo "<a title='Strana {$trenutna}' href='javascript:void(0)' onclick='promeniStranu(" . ($trenutna - 1) . ")'>&#171; Prethodna</a>";
        } else {
            //echo "<span class='disabled'>&#171; Prethodna</span>";
        }
        for ($i = 0; $i < $brStrana; $i++) {
            if ($i == $trenutna) {
                echo "<span class='current'>" . ($i + 1) . "</span>";
            } else if ($i + 1 == $trenutna || $i + 2 == $trenutna) {
                echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='promeniStranu({$i})'>" . ($i + 1) . "</a>";
            } else if ($i == $trenutna - 3) {
                //stampanje prve strane
                echo "<a title='Strana 1' href='javascript:void(0)' onclick='promeniStranu(0)'>1</a>";
                echo " ... ";
            } else if ($i == $trenutna + 3) {
                echo " ... ";
                //stampanje poslednje strane
                echo "<a title='Poslednja strana' href='javascript:void(0)' onclick='promeniStranu(" . ($brStrana - 1) . ")'>{$brStrana}</a>";
                break;
            } else if ($i - 1 == $trenutna || $i - 2 == $trenutna) {
                echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='promeniStranu({$i})'>" . ($i + 1) . "</a>";
            }
        }//for
        if ($trenutna < $brStrana - 1) {
            echo "<a title='Strana " . ($trenutna + 2) . "' href='javascript:void(0)' onclick='promeniStranu(" . ($trenutna + 1) . ")' class='next'>Sledeća &#187;</a>";
        } else {
            //echo "<span class='disabled'>Sledeća &#187;</span>";
        }
        echo "</div>";
        ?>                                           
        <div style='font-size:10px;float: left;'>&nbsp;Idi na 
            <input size='2' type='text' id='idiNaStranu0' onkeyup="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" onkeydown="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')">&nbsp;
            <input type='button' onclick="idiNaStranu($('#idiNaStranu0').val())" style='font-size:11px;' value='stranu'>
        </div>
        <?php
    }
    ?>                
</div><!--  paginacijaGore -->