
<?php $time = microtime(true); ?>
<!DOCTYPE html>
<html>


    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>


        <script src="script/funkcije.js"></script>

        <style>

            td
            {          
                padding: 3px 6px 3px 6px;   

                text-align:center ;
                vertical-align: middle;
            }

            #header {
                width: 100%; 
                height: 80px; 

                /*background-color: ghostwhite;*/
                /*border: 1px solid black; */

                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);

            }

            #header_in{                
                width: 960px; 
                height: 80px; 
                /* border: 1px solid black; */
                margin: 0 auto;
            }

            #header2{                
                width: 100%; 
                height: 18px;                 
                margin: 0 auto;                
                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);

            }

            #footer2{                
                width: 100%;
                height: 100px;
                margin-top: 10px;
                float: left;

                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #footer_in{
                width: 960px; 
                height: 50px; 
                margin: 0 auto;
            }


            #naslov {
                font-size: 33px;
                float: left;               
                font-weight: 700;
                font-style: oblique;
                /*font-style: italic;*/
                padding: 15px 0 0 5px;     
                /*border: 1px solid black;*/
            }

            #naslov a{
                color: gray;                
                text-decoration: none;                
            }

            #naslov a:hover{
                color: lightgray;                
            }

            nav{
                width: 100%;
                border: 1px solid lightcyan; 
                background-color: steelblue;
                background-color: lightsteelblue;
                background-color: lightgrey;
                text-align: center;                               
                height: 30px;
                line-height: 30px;                
                /* background-color: beige;                  */
            }

            nav a{                                 
                text-decoration: none; 
                /*color: black;*/
                color: white;

                margin-left: 15px;
                margin-right:15px;
                font-size: 18px;  
                font-weight: 700;
                /* padding: 0 20px 0 20px;*/
            }

            nav a:hover {
                color: grey;                                
            }

            /*Ovde se nalaze sve kolone*/
            #content{
                /* position: relative;*/
                margin: 0px auto;    
                margin-top: 10px;
                width: 960px; 
                height: auto;
                display: block;
                /*float: left;*/
                /*  height: 1100px;                 */
                /*position: */

                /*  background-color: whitesmoke; */
                /* box-shadow: 1px 1px silver; */

            }

            #kontejner{
                width: 960px; 
                height: 1000px; 
                /* border: 1px solid silver; */
                margin: 0 auto; 
                /*  background-color: whitesmoke; */
                box-shadow: 1px 1px silver; 
                border-radius: 0px;    
            }



            #center_con{                
                width: 650px; 
                height:800px;
                float: left; 
                margin-left: 5px; 
                border: 0px solid black; 
                background-color: wheat;
                margin-top: 5px;
            }

            #center_con_cen{
                width: 960px; 
                height:800px;
                background-color: white;
                float: left;   
                border: 1px solid silver;         
                /*margin-top: 5px;*/
                margin: 5px;
            }

            #center_con_small
            {
                /* margin-top: 10px;*/
                /* width: 960px;*/

                width: 660px;
                float: left;
                padding: 50px;
                /*margin: 0px auto;*/                
                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }


            #left_con{
                width: 160px; 
                height: auto; float: left;
                background-color: #fff;
                margin : 0 10px 0 10px;

                padding: 10px;

                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #left_con p{
                padding: 10px;
            }

            #right_con{
                width: 150px; height:auto; float: right;


                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }



            .text {
                border: 0px solid black;                
                /*centrira na serd strane*/
                margin: 0 auto;
                /*width: 650px;*/
                font-size: 14px;                
                /* padding: 20px;  */
            }

            #loginCon {
                width: 960px; 

                /*height: auto; */
                /* border: 1px solid black; */
                height: 18px;
                line-height: 18px;   
                margin: 0 auto;
            }

            #login2{

                float: right; 
                padding: 0px 3px 0px 3px; 
                font-size: 13px; 
                font-weight: 600;                                         
            }

            #login2 a{
                text-decoration: none;
                color: black;
            }

            #login2 a:hover {
                color: gray;                
            }


            #login{
                float: right; 
                padding: 10px 15px 0px 0px; 
                font-size: 14px; 
                font-weight: 600;                
            }

            #login a{
                text-decoration: none;
                color: black;
            }

            #login a:hover {
                color: gray;                
            }


        </style>
    </head>
    <body>
        <!--<div style="width: 960px; height: 1000px; border: 1px solid silver; margin: 0 auto; background-color: whitesmoke; box-shadow: 1px 1px silver; border-radius: 0px;">-->
        <!--<div id="kontejner">-->
        <header id="header" >        
            <div id="header_in">
                <div id="naslov">
                    <a href="#">
                        Ankete online
                    </a>
                </div>
                <div id="login">
                    <!-- <a href>prijavi se</a>-->
                    <a href>Prijava</a> | <a href>Registracija</a>
                </div>
            </div>

        </header>
        <nav>
            <a href="#">Ankete</a>
            <a href="#">Moje ankete</a>
            <a href="#">Statistika</a>
            <a href="#">Kontakt</a>
        </nav>
        <div id="header2">
            <div id="loginCon">
                <div id="login2">
                    <!-- <a href>prijavi se</a>-->
                    <a href>Prijava</a> | <a href>Registracija</a>
                </div>
            </div>
        </div>

        <!-- <div style="float:left;width: 100%">-->
        <div id="content">