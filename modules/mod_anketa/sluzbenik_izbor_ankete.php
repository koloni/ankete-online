<script type="text/javascript" src="script/json2.js"></script>
<script>

    function copy0(elem)
    {                               
        var target = document.getElementById ("tbodyIzabraniIspitanik");
        target.appendChild ( elem.parentNode.parentNode );
    }

    function copy(elem)       
    {   
               
        var tr=elem.parentNode.parentNode;

        //brišem link izaberi
        elem.innerHTML="";                  
        
        //dodajemo link izbaci, koji vraća tr 
        // elem.innerHTML="<a href='javascript: void(0)' onclick='izbaci(this)'>Izbaci";
                         
        //var divPremestiU="tbodyRezultatPretrageIspitanik";
        //elem.parentNode.innerHTML="<a href='javascript: void(0)' onclick='premesti(this)'>Izbaci";  
        //setVrednostCelije("<a href='javascript: void(0)' onclick='premesti(this, 'rezultat_pretrage_ispitanik')'>Izbaci",5, 'tblIspitanik'); 
         
        
        var target = document.getElementById ("tbodyIzabraniIspitanik");
        //var target = document.getElementById('tblIspitanik').getElementsByTagName('tbody');
        
        //brišemo prethodnu vrendost ako je bila, jer ne želimo da izaberemo dva ispitanika
        //target.innerHTML="";
        
        //dohvatamo prethodnu izabranu vresnot 'trIzabraniIspitanik' i vraćamo je u tabelu prerage
        var trIzabraniIspitanik=target.getElementsByTagName('tr');
        //alert(trIzabraniIspitanik.length);
        //anko tbody nema ni jenog reda vratiće nulu za dužinu niza
        if(trIzabraniIspitanik.length>0){         
            var divTbodyRezultatPretrage=document.getElementById('tbodyRezultatPretrageIspitanik');
            setVrednostCelije("<a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a>",5, 'tblIspitanik');
            divTbodyRezultatPretrage.appendChild(trIzabraniIspitanik[0]);
        }
        //ipak ovo radi move
        //<tbody> dodajemo <tr> kao rezultat pretrage 
        target.appendChild (tr);
        setVrednostCelije("<a href='javascript: void(0)' onclick='premesti(this)'>Izbaci</a>",5, 'tblIspitanik');
                
        //pravimo novi red, i kopiramo red koji je rezultat pretrage
        // var tr=document.createElement('tr');        
        //tr.innerHTML=(elem.parentNode.parentNode).innerHTML;
        //target.appendChild(tr);
        
        //cloneNode nije radio, ali treba ponovo proveriti
        //var trKopija=document.cloneNode(elem.parentNode.parentNode);
        //target.appendChild (trKopija);
        
        //brišem tabelu pretrage
        //document.getElementById('tblRezultatPretrage').innerHTML="";                  
        
        //brisemo textBox pretretrage
        document.frmPretraga.kljucna_rec.value="";
                  
        //prikazivanje pretrage anketa
        document.getElementById('pretragaAnketa').style.display="block";                
        
        //prikazujemo div sa tabelom tblIspitanik
        //document.getElementById('izabraniIspitanik').style.display="block";
        document.getElementById('izabraniIspitanik').style.display="table";
        
        //ako je pretraga ankete izvršena, a mi izabiramo ili menjamo ispitanika, osveži rezultate pretrage anketa, zbog linkova nastavi, popuni, i popunjena
        if(document.frmPretragaAnketa.kljucna_rec.value.length>=1){         
            pronadjiAnketu(document.frmPretragaAnketa.kljucna_rec);
        }
    }//copy
    
    function copy2(elem)       
    {             
        var tr=elem.parentNode.parentNode;
        var target = document.getElementById ("tbodyIzabraniIspitanik");
        //ne radi
        //var target = document.getElementById('tblIspitanik').getElementsByTagName('tbody');
        
        //brišemo prethodnu vrendost ako je bila, jer ne želimo da izaberemo dva ispitanika
        target.innerHTML="";
        
        //ipak ovo radi move
        //<tbody> dodajemo <tr> kao rezultat pretrage 
        //target.appendChild (elem.parentNode.parentNode );
        
        //pravimo novi red, i kopiramo red koji je rezultat pretrage
        var trKopija=document.createElement('tr');        
        trKopija.innerHTML=(tr).innerHTML;
               
        //moramo prvo da dodamo red, da bi mogli damenjamo vrenost ćelije
        target.appendChild(trKopija);
             
        //ovde menja izaberi link u Izbaci, odustao od izbaci(this)
        //setVrednostCelije("<a href='javascript: void(0)' onclick='izbaci(this)'>Izbaci",5, 'tblIspitanik');         
        setVrednostCelije("<a href='javascript: void(0)' onclick='premesti(this)'>Izbaci</a>",5, 'tblIspitanik'); 
        
        //Ovo nije radilo!
        //var trKopija=document.cloneNode(elem.parentNode.parentNode);
        //target.appendChild (trKopija);
        
        //brišem tabelu pretrage
        //document.getElementById('tblRezultatPretrage').innerHTML="";
        //brisemo textBox pretretrage
        //document.frmPretraga.kljucna_rec.value="";
                  
        //prikazivanje pretrage anketa
        document.getElementById('pretragaAnketa').style.display="block";                
        
        //prikazujemo div sa tabelom tblIspitanik
        document.getElementById('izabraniIspitanik').style.display="table";
    }//copy2
    
    function izbaci(elem){        
        var tr=elem.parentNode.parentNode;
        //tr.parentNode.removeChild(tr);
        tr.innerHTML="";       
        document.getElementById('izabraniIspitanik').style.display="none";
    }
    
    function premesti(elem){ 
        
        var tr=elem.parentNode.parentNode;                 
        var target=document.getElementById('tbodyRezultatPretrageIspitanik');          
        setVrednostCelije("<a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a>",5, 'tblIspitanik');
        target.appendChild(tr);
        //alert(document.getElementById('tbodyIzabraniIspitanik').innerHTML);
               
        
        //može i ovako, ali je monogo više koda
        /*
        var target = document.getElementById ("tbodyIzabraniIspitanik");
        var trIzabraniIspitanik=target.getElementsByTagName('tr');
        //alert(divIzabraniIspitanik.length);
        //anko tbody nema ni jenog reda vratiće nulu za dužinu niza
        if(divIzabraniIspitanik.length>0){         
            var divTbodyRezultatPretrage=document.getElementById('tbodyRezultatPretrageIspitanik');
            setVrednostCelije("<a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a>",5, 'tblIspitanik');
            divTbodyRezultatPretrage.appendChild(divIzabraniIspitanik[0]);
        }
         */
        
        document.getElementById('izabraniIspitanik').style.display="none";
        
        //ako je pretraga ankete izvršena, a mi izabiramo ili menjamo ispitanika, osveži rezultate pretrage anketa, zbog linkova nastavi, popuni, i popunjena
        if(document.frmPretragaAnketa.kljucna_rec.value.length>=1){         
            pronadjiAnketu(document.frmPretragaAnketa.kljucna_rec);
        }
    }//premesti
    
    //nalazi korisnike i briše prethodni rezultat pretrage, i izabranog ispitanika, 
    function pronadjiKorisnika(kljucnaRec) {        
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {                                   
                var jsonKorisnici=xmlhttp.responseText;                              
                var rezultat="";
                                
                //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa
                //if(jsonAnkete.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                if(jsonKorisnici.replace(/\r\n|\r|\n/, "")=='false' && kljucnaRec.value.length>=1){                   
                    document.getElementById('rezultat_pretrage_ispitanik').style.display='none';
                    document.getElementById('nemaRezIspitanik').style.display='block';
                    document.getElementById('nemaRezIspitanik').innerHTML="Nema rezultata pretrage!";
                    //rezultat="Nema rezultata pretrage!"
                }else if(kljucnaRec.value.length>=1) {
                    var arrKorisnici = JSON.parse(jsonKorisnici);   
                    //prikazi div razultat pretrage
                    document.getElementById('rezultat_pretrage_ispitanik').style.display='table';
                    
                    document.getElementById('nemaRezIspitanik').innerHTML="";
     
                    if(arrKorisnici.length>=1){
 
                        /*for(i=0; i<arrAnkete.length; i++){
                            rezultat+="<a href=''>"+arrKorisnici[i].firstName+" "+arrKorisnici[i].lastName+"</a><br/>";
                        }*/                        
                        for(i=0; i<arrKorisnici.length; i++){                                                                
                            rezultat+="<tr id='tr"+arrKorisnici[i].idKorisnik+"'>"+
                                "<td style='display: none;'>"+arrKorisnici[i].idKorisnik+"</td>"+
                                "<td>"+arrKorisnici[i].firstName+"</td>"+
                                "<td>"+arrKorisnici[i].lastName+"</td>"+
                                "<td name='ispitanik'>"+arrKorisnici[i].username+"</td>"+
                                "<td>"+arrKorisnici[i].email+"</td>"+                               
                                "<td><a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a></td>"+ 
                                "</tr>"       
                        }//for
                        //"<td><a href='#' onclick='izaberiKorisnika('', '', '')'>Izaberi</a></td>"+
                    } else {                                                                                                                                       
                        rezultat+="<tr id='tr"+arrKorisnici.idKorisnik+"'>"+
                            "<td style='display: none;'>"+arrKorisnici.idKorisnik+"</td>"+
                            "<td>"+arrKorisnici.firstName+"</td>"+
                            "<td>"+arrKorisnici.lastName+"</td>"+
                            "<td>"+arrKorisnici.username+"</td>"+
                            "<td>"+arrKorisnici.email+"</td>"+
                            "<td><a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a></td>"+ 
                            "</tr>" 
                       
                        // rezultat="<a href=''>"+arrAnkete.firstName+" "+arrKorisnici.lastName+"</a><br/>";  
                        //}else if(jsonAnkete.toString()=='false' && kljucnaRec.value.length>1){
              
                    }
                }else{
                    document.getElementById('rezultat_pretrage_ispitanik').style.display='none';
                    document.getElementById('nemaRezIspitanik').innerHTML="";
                }                     
                
                document.getElementById('tbodyRezultatPretrageIspitanik').innerHTML=rezultat;  
                //document.getElementById('rezultat_pretrage_ispitanik').innerHTML=rezultat; 
                //document.getElementById('rezultat_pretrage_ispitanik').appendChild(rezultat);
                
                // document.getElementById('rezultat_pretrage_ispitanik').innerHTML=xmlhttp.responseText;
            }
        }
                
        //alert("index.php?idPitanje="+x);
        
        xmlhttp.open("GET","index.php?p=52&filterPretrage="+document.frmPretraga.filterPretrage.value+"&kljucna_rec="+kljucnaRec.value);                        
    
        xmlhttp.send();
    }//pronadjiKorisnika
    
    function pronadjiAnketu(kljucnaRec) {        
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {                                   
                var jsonAnkete=xmlhttp.responseText;      
                //alert(jsonAnkete);
                var rezultat="";
                // alert(jsonAnkete);         
                //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
                //if(jsonAnkete.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                if(jsonAnkete.replace(/\r\n|\r|\n/, "")=='false' && kljucnaRec.value.length>=1){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>"                  
                }else if(kljucnaRec.value.length>=1) {
                    var arrAnkete = JSON.parse(jsonAnkete);                                    
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 650px;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Naziv</th>"+
                        "<th scope='col'>Datum počekta</th>"+
                        "<th scope='col'>Datum isteka</th>"+
                        "<th scope='col'>Broj pitanja</th>"+                                                   
                        "<th scope='col'></th>"+ 
                        "</tr>"+
                        "</thead>"+
                        "</tbody>";                        
                    if(arrAnkete.length>=1){
                        /*for(i=0; i<arrAnkete.length; i++){
                            rezultat+="<a href=''>"+arrKorisnici[i].firstName+" "+arrKorisnici[i].lastName+"</a><br/>";
                        }*/ 
        
                        var ispitanik=getVrednostCelije(0, 'tblIspitanik');
                        for(i=0; i<arrAnkete.length; i++){                            
                            rezultat+="<tr id='tr"+arrAnkete[i].idAnketa+"'>"+
                                "<td>"+arrAnkete[i].naziv+"</td>"+
                                "<td>"+arrAnkete[i].datumPocetka+"</td>"+
                                "<td>"+arrAnkete[i].datumZavrsetka+"</td>"+
                                "<td>"+arrAnkete[i].ukupnoPitanja+"</td>";
                            if(arrAnkete[i].isPopunjen==0){
                                rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrAnkete[i].idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Nastavi</a></td>";
                            }else if(arrAnkete[i].isPopunjen==1){
                                rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete[i].idAnketa+"'>Popunjena</a></td>";
                                //mora ovakva porvera, jer ako ispitanik nije popunjavao anketu, ili nije izabran ispitanik, arrAnkete[i].isPopunjen vratiće undefined
                            }else if(ispitanik!=null) {
                                rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrAnkete[i].idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Popuni anketu</a></td>";
                            }
                            
                            //rezultat+="<td><a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a></td>"+ 
                            rezultat+="</tr>";
                        }//for
                        //"<td><a href='#' onclick='izaberiKorisnika('', '', '')'>Izaberi</a></td>"+
                    } else {                                                                                                                                       
                        rezultat+="<tr id='tr"+arrAnkete.idAnketa+"'>"+
                            "<td>"+arrAnkete.naziv+"</td>"+
                            "<td>"+arrAnkete.datumPocetka+"</td>"+
                            "<td>"+arrAnkete.datumZavrsetka+"</td>"+
                            "<td>"+arrAnkete.ukupnoPitanja+"</td>";
                        if(arrAnkete.isPopunjen==0){
                            rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrAnkete.idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Nastavi</a></td>";
                        }else if(arrAnkete.isPopunjen==1){
                            rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete.idAnketa+"'>Popunjena</a></td>";
                        }else {
                            rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrAnkete.idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Popuni anketu</a></td>";
                        }                              
                        rezultat+="</tr>"+
                            "</toby></table>";
                       
                        // rezultat="<a href=''>"+arrAnkete.firstName+" "+arrKorisnici.lastName+"</a><br/>";  
                        //}else if(jsonAnkete.toString()=='false' && kljucnaRec.value.length>1){
              
                    }
                }                                                                                                                                                    
                
                document.getElementById('rezultat_pretrage_anketa').innerHTML=rezultat;                                                                                                  
                // document.getElementById('rezultat_pretrage_anketa').innerHTML=xmlhttp.responseText;
            }
        }  
        
        //izbor pretrage
        if(getVrednostCelije(0, 'tblIspitanik')===null){
            xmlhttp.open("GET","index.php?p=54&filterPretrage="+document.frmPretraga.filterPretrage.value+"&kljucna_rec="+kljucnaRec.value);                        
        }else{
            xmlhttp.open("GET","index.php?p=54&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"&filterPretrage="+document.frmPretraga.filterPretrage.value+"&kljucna_rec="+kljucnaRec.value);                        
        }
        
    
        xmlhttp.send();
    }//pronadjiAnketu
    
    /*
                         echo "<tr>";
                            echo "<td>{$a['naziv']}</td>";
                            echo "<td>" . substr($a['datumPocetka'], 0, 10) . "</td>";
                            echo "<td>" . substr($a['datumZavrsetka'], 0, 10) . "</td>";
                            //echo "<td>{$a['datumZavrsetka']}</td>";                            
                            echo "<td>" . ($a['isPersonalizovana'] == 1 ? 'Da' : 'Ne') . "</td>";
                            echo "<td>{$a['ukupnoPitanja']}</td>";
                            if (isset($a['isPopunjen']) && $a['isPopunjen'] == 0) {
                                echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Nastavi</a></td>";
                            } else if (!isset($a['isPopunjen'])) {
                                echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa={$a['idAnketa']}'>Popuni anketu</a></td>";
                            } else {
                                echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}'>Popunjena</a></td>";
                            }
                            //echo "<td><a href='edit_anketa.php?idAnketa={$a['idAnketa']}'>izmeni</a></td>";
                            echo "</tr>";
    
     */
    

    function getVrednostCelije(index, tableId){
        var table = document.getElementById(tableId),
        cells = table.getElementsByTagName('td');
        if(cells.length>0) {
            return cells[index].innerHTML;
        }else{
            return null;
        }
    }
    
    function setVrednostCelije(value, index, tableId){
        var table = document.getElementById(tableId),
        cells = table.getElementsByTagName('td');
        cells[index].innerHTML=value;
    }
</script>
<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 660px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    ?>
    <div class="naslov">Popunjavanje anketa</div>

    <div style="float:left; width: 95%; height: 1px; border-top:  1px dotted #666; margin-top: 20px; margin-bottom: 10px;"></div>

    <div id="pretragaIspitanika">
        <div class="naslov">Izbor ispitanika</div>
        <form name="frmPretraga">
            <input type="hidden" name="ispitanik">
            <table>
                <tr>
                    <td>Filter pretrage:</td>
                    <td>
                        <select name="filterPretrage">
                            <option value='1'> Ime i/ili Prezime </option>
                            <option value='2'> Korisničko ime  </option>
                            <option value='3'> E-mail adresa  </option>
                        </select>
                    <td>
                </tr>
                <tr>
                    <td>Pretraga korisnika:</td>
                    <td><input value="" onkeyup="pronadjiKorisnika(document.frmPretraga.kljucna_rec)" type="text" name="kljucna_rec" id="pretraga"><td>
                </tr>
            </table>
        </form>        
    </div>


    <div id="rezultat_pretrage_ispitanik" style="display: none; margin: 0 auto; padding-top: 15px;">
        <div style='float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;'></div>
        <div style='font-style: italic;'>Rezultat pretrage</div>
        <table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 600px;'>
            <thead>
                <tr>
                    <th style='display: none;' scope='col'>Ime</th>
                    <th scope='col'>Ime</th>
                    <th scope='col'>Prezime</th>
                    <th scope='col'>Korisničko ime</th>
                    <th scope='col'>E-mail adresa</th>                                                  
                    <th scope='col'></th>
                </tr>
            </thead>
            <tbody id="tbodyRezultatPretrageIspitanik"></tbody>
        </table>
    </div>
    <div id="nemaRezIspitanik" style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'></div>

    <br/>
    <br/>
    <div id="izabraniIspitanik" style="display: none; margin: 0 auto;">
        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>
        <span style="font-style: italic;">Izabrani ispitanik</span>         
        <table border='0px' id='tblIspitanik' class='hor-minimalist-b0' style='width: 550px;'>
            <thead>
                <tr>
                    <th style="display: none;" scope='col'></th>
                    <th scope='col'>Ime</th>
                    <th scope='col'>Prezime</th>
                    <th scope='col'>Korisničko ime</th>
                    <th scope='col'>E-mail adresa</th>
                    <th scope='col'></th>
                <tr>
            </thead>
            <tbody id="tbodyIzabraniIspitanik"></tbody>
        </table>
    </div>
    <div style="float:left; width: 95%; height: 1px; border-top:  1px dotted #666; margin-top: 20px; margin-bottom: 10px;"></div>

    <div id="pretragaAnketa" style="display: none;">
        <div class="naslov">Izbor ankete</div>
        <form name="frmPretragaAnketa">
            <table><!--
                <tr>
                    <td>Filter pretrage:</td>
                    <td>
                        <select name="filterPretrage">
                            <option value='1'> Ime i/ili Prezime </option>
                            <option value='2'> Korisničko ime  </option>
                            <option value='3'> E-mail adresa  </option>
                        </select>
                    <td>
                </tr>-->
                <tr>
                    <td>Pretraga anketa:</td>
                    <td><input value="" onkeyup="pronadjiAnketu(document.frmPretragaAnketa.kljucna_rec)" type="text" name="kljucna_rec" id="pretraga"><td>
                </tr>
            </table>

        </form>        
    </div>
    <div id="rezultat_pretrage_anketa"></div>





</div>


