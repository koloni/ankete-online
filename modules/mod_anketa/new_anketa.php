<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmNewAnketa").validate({
            rules: {
                naziv: {
                    required: true,                    
                    naziv:true,
                    minlength: 3,
                    maxlength: 100
                },
                datumPocetka: {
                    required: true                    
                }, 
                datumIsteka: {
                    required: true                 
                },
                brPitanjaPoStrani: {
                    required:true,
                    max:30                    
                }
            },
            messages: {
                naziv: {
                    required: "'Naziv ankete' je obavezan!",                   
                    minlength: $.format("Naziv mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera za 'Naziv' je: {0}")
                },
                datumPocetka: {
                    required: "Datum početka je obavezan!"                  
                },
                datumIsteka: {
                    required: "Datum isteka obavezan!!"                                                                                
                },
                brPitanjaPoStrani: {
                    required: "Broj pitanja po strani je obavezan",
                    max: $.format("Maksimalno pitanja po strani je {0}")
                }
            }
        });
    });
       
   
    //uključuje proveru hidden polja
    $.validator.setDefaults({ ignore: '' });
    
    
    $.validator.addMethod("alphabetWithOneSpace",
    function(value, element) {                
        //ovde dozvoljava jedan ' ' ili jednu '_'
        //return /^([a-zA-Z]+(_[a-zA-Z]+)*)(\s([a-zA-Z]+(_[a-zA-Z]+)*))*$/.test(value);
        
        return /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/.test(value);
    },
    "Dozvoljena su samo slova, sa maksimum jednim razmakom u serdini!"
);
            
    $.validator.addMethod("naziv",
    function(value, element) {                 
        //return /^(([A-Z]|[a-џ]|[0-9])|(\s|\.|\!|\?|(\,){0,1}))+$/.test(value);
        
        //return /^((([A-Z]|[a-џ]|[0-9]))|((\s|\.|\!|\?|(\,){0,1})))+$/.test(value);
        
        
        //razmank moze da se ponavlja najviše jednom u sredini rečenice
        //return /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/.test(value);
        
        //razmank moze da se ponavlja najviše jednom u sredini i na kraju rečenice
        //return /^([a-zA-Z]+)(\s?([a-zA-Z]))+(\s?|([a-zA-Z])*)$/.test(value);
        
        //razmak i ostali karakteri mogu da se ponavljanju najveše dvaput u seredini i jenim na kraju rečenice
        //return /^([a-zA-Z]+)(((\s|\.|\!|\,|\?){0,2})([a-zA-Z]))+(\s?|\.?|\!?|\,?|\??|([a-zA-Z])*)$/.test(value);
        
        //razmak i ostali karakteri mogu da se ponavljanju najveše dvaput u seredini i na kraju rečenice
        //return /^([a-žA-Ž]+)(((\s|\.|\!|\,|\?){0,2})([a-žA-Ž]))+((\s|\.|\!|\,|\?){0,2}|([a-žA-Ž])*)$/.test(value);
        
        return /^([a-џA-Z]+)(((\s|\.|\!|\,|\?){0,2})([a-џA-Z]))+((\s|\.|\!|\,|\?){0,2}|([a-џA-Z])*)$/.test(value);
    },
    /*"Dozvoljena su slova, sa maksimum 2 specijalna karaktera u nizu u serdini i na kraju račenice<br>"*/
    "Neisravan unos!"
);  
    
    $.validator.addMethod("alphabet",
    function(value, element) {                 
        return /^[A-Za-z]+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
    $.validator.addMethod("razlicitoo",
    function(value, element) {                 
        return /^[A-Za-z]+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
    /*ako su vrednosti iste vraća true*/ 
    jQuery.validator.addMethod("razlicito", function(value, element, param) {
        
        var target = $(param);
        return this.optional(element) || value != target.val();
    }, "'Morate uneti različite vrenosti'!"
);
    
</script>

<div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
    <div class="naslov">Pravljenje ankete</div>
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    include_once MODULES_DIR . '/mod_menu/menu_dubina.php';
    ?>

    <form name="frmNewAnketa" id="frmNewAnketa" method="POST" action="">
        <div class="anketa" style="margin: 0 auto; display: table; min-width: 650px;">
            <div class="kolona_levo">                
                <div>Naziv ankete:</div>
                <!-- <div>Vreme trajanja:</div>-->
                <div>Datum početka:</div>
                <div>Datum isteka:</div>
                <div>Personalizovana:</div>                    
                <div>Broj pitanja po strani:</div>            
                <div>Redosled prikazivanja pitanja:</div>
                <div></div>
            </div>

            <div class="kolona">               
                <div>                               
                    <input type="text" name="naziv" style='width:250px;'>                
                </div>
               <!--<div>od <input type="date" name="datumPocetka"> do <input type="date" name="datumIsteka"></div>-->
                <!--<div><input type="date" name="datumPocetka"></div>
                    <div><input type="date" name="datumIsteka"></div>     -->
                <div><span style="font-style: italic;"id="datumPocetkaTekst">izaberite datum </span><button type="reset" id="btnDatumPocetka">...</button><input type="hidden" name="datumPocetka" id="datumPocetka" /></div>                
                <div><span style="font-style: italic;" id="datumIstekaTekst">izaberite datum </span><button type="reset" id="btnDatumIsteka">...</button><input type="hidden" name="datumIsteka" id="datumIsteka" /></div>                                                              
                <div>
                    <input type="radio" name="personalizovana" value="true" id="pers_true"><label for="pers_true">da</label>
                    <input type="radio" name="personalizovana" value="false" id="pers_false"  checked><label for="pers_false" >ne</label>
                </div>
                <div><input type="text" name="brPitanjaPoStrani" size="5" onkeyup="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" onkeydown="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')"></div>                

                <!-- onkeydown="this.value=this.value.replace(/[^\d+]/,'')" morao sam da dodam o keydown je ako se drzi dugme, moze pogrešno da se ukuca, onkeyup brise samo poslednje-->

                <!--
                različito mora da stoji u uglastim zgradama []
                
                za sve cele brojeve >=1
                this.value.replace(/^(0|[^\d+])|([^\d+])$/,'')" 
                
                //morao sam da dodam i za +, jer \d prihvata i +
                this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')
                -->

                <!--
                <div>
                    <input type="radio" name="redosledPitanja" value="0" id="poredu" checked><label for="poredu">po redosledu unosa</label> 
                    <input type="radio" name="redosledPitanja" value="1" id="rucno"><label for="rucno">ručno izabran</label>
                    <input type="radio" name="redosledPitanja" value="2" id="slucajno"><label for="slucajno">slučajan izbor</label>                    
                </div>   
                -->

                <div>
                    <select name="redosledPitanja">
                        <option value="0">po redosledu unosa</option>
                        <option value="1">ručno izabran</option>
                        <option value="2">slučajan izbor</option>
                    </select>                    
                </div>


                <div><input style="padding: 3px 10px; margin-right: 5px;" type="submit" value="Napravi anketu" /> <input style="padding: 3px 10px;" type="reset" value="Poništi"/></div>                    
            </div>
        </div>
        <!--
        <div style="height: 30px; width: 100%; border: 1px solid aquamarine;">
            <div style="border: 1px solid #ff7c7c;"><button style="margin: 0 auto;">napravi anketu</button></div>
        </div>
        -->
    </form>

            <!--<tr>                
                <td colspan="2"><label for="naziv" class="error"></label></td>
            </tr>-->
    <!--
    <form name="frmNewAnketa" id="frmNewAnketa" action="index.php?p=9" method="post"> 
        <table border="1px" class="tableForm">   
            <tr>
                <td>Naziv:</td>
                <td><input type="text" name="naziv" style='width:250px;'></td>
            </tr>
            <tr>
                <td colspan="2"><label for="naziv" class="error"></label></td>
            </tr>
            <tr>
                <td>Datum početka: </td>
                <td>
                    <input type="hidden" name="datumPocetka" id="datumPocetka" /><span style="font-style: italic;"id="datumPocetkaTekst">izaberite datum </span><button type="reset" id="btnDatumPocetka">...</button>
                </td>
            </tr>
            <tr>                    
                <td>Datum isteka: </td>               
                <td>
                    <input type="hidden" name="datumIsteka" id="datumIsteka" /><span style="font-style: italic;" id="datumIstekaTekst">izaberite datum </span><button type="reset" id="btnDatumIsteka">...</button>
                </td>
            </tr>  
            <tr>                    
                <td>Personalizovana: </td>
                <td>
                    <input type="radio" name="personalizovana" value="true" id="pers_true"><label for="pers_true">da</label>
                    <input type="radio" name="personalizovana" value="false" id="pers_false"  checked><label for="pers_false" >ne</label>
                </td>
            </tr>

            <tr>                    
                <td>Broj pitanja po strani: </td>
                <td><input type="text" name="brPitanjaPoStrani" size="5" onkeyup="this.value=this.value.replace(/^(0|[^\d+])|([^\d+])$/,'')" onkeydown="this.value=this.value.replace(/^(0|[^\d+])|([^\d+])$/,'')"></td>
            </tr>
            <tr>                    
                <td>Redosled prikazivanja pitanja: </td>
                <td>
                    <input type="radio" name="redosledPitanja" value="0" id="poredu" checked><label for="poredu">po redosledu unosa</label> 
                    <input type="radio" name="redosledPitanja" value="1" id="rucno"><label for="rucno">ručno izabran</label>
                    <input type="radio" name="redosledPitanja" value="2" id="slucajno"><label for="slucajno">slučajan izbor</label>
                </td>
            </tr>       
        </table>
    </form>



    <style type="text/css">
        table.empty{
            /*  width:350px;*/
            /*  border-collapse: collapse;*/
            /*  empty-cells:hide;*/
        }
        td.normal{
            border-style:solid;
            border-width:1px;
            border-color: blue;
        }   
        td.empty{      
            /*style:'display=none'*/
        }
    </style>

    <form style="padding:0;margin:0;">      
        <table border="1px;">

            <tr>

                <th>Title one</th>
                <th>Title two</th>
            </tr>
            <tr>

                <td class="normal">value</td>
                <td class="normal">value</td>
            </tr>
            <tr>

                <td colspan="2"><label for="naziv" class="error"></label></td>

            </tr>
            <tr>

                <td class="normal">value</td>
                <td class="normal">value</td>
            </tr>        
        </table>  
    </form>
    -->
</div>

<script type="text/javascript">    
    Calendar.setup({
        inputField     :    "datumPocetka",      // id of the input field        
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        displayArea    :    "datumPocetkaTekst",
        daFormat       :    "%B %e, %Y",//"%B %e, %Y %I:%M %p" format of the displayed date
        showsTime      :    false,            // will display a time selector
        button         :    "btnDatumPocetka",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
    Calendar.setup({
        inputField     :    "datumIsteka",      // id of the input field        
        ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
        displayArea    :    "datumIstekaTekst",
        daFormat       :    "%B %e, %Y",//"%B %e, %Y %I:%M %p" format of the displayed date
        showsTime      :    false,            // will display a time selector
        button         :    "btnDatumIsteka",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
</script>
