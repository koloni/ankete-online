<script>
    var trenutna=0;
    var limit=<?php echo $limit; ?>;
    var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
    var brojStrana=<?php echo $brStrana; ?>;
    
    function idiNaStranu(strana) {      
        strana-=1;
        if(strana<0) {
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            return;
        }else if(strana>=brojStrana) {
            getPitanjaInfoAjaxJson(brojStrana-1);    
            return;
        }        
        getPitanjaInfoAjaxJson(strana);    
    }//idiNaStranu
    
    function promeniStranu(strana){       
        getPitanjaInfoAjaxJson(strana);
    }//promeniStranu
    
    var anketa=<?php echo $idAnketa; ?>;
    function getPitanjaInfoAjaxJson(strana){       
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {   
                var jsonRezultat=xmlhttp.responseText;   
                //alert(jsonRezultat);
                var rezultat="";   
             
                if(jsonRezultat=='\nfalse'){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                    ukupnoZapisa=0;
                    brojStrana=0;
                }else  {        
                    //alert(jsonRezultat);
                    var json=JSON.parse(jsonRezultat); 
                    //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    var arrRezultat = json.ankete;                                   
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Pitanja</th>"+                        
                        "<th scope='col'>Dat. kreiranja</th>"+                        
                        "<th scope='col'></th>"+ 
                        "</tr>"+
                        "</thead>"+
                        "<tbody>";                        
                    if(arrRezultat.length>=1){ 
                        for(i=0; i<arrRezultat.length; i++){                            
                            rezultat+="<tr id='"+arrRezultat[i].idPitanje+"'>"+
                                "<td>"+arrRezultat[i].tekst+"</td>"+
                                "<td>"+arrRezultat[i].datumKreiranja+"</td>"+ 
                                "<td style='text-align: right; width: 55px;'><a class='link' href='javascript: void(0);' onclick='dodajPitanje("+arrRezultat[i].idPitanje+", "+anketa+");'>dodaj </a></td>";
                        }//for                    
                    } else {                                                                                                                                       
                        rezultat+="<tr id='"+arrRezultat.idPitanje+"'>"+
                            "<td>"+arrRezultat.tekst+"</td>"+
                            "<td>"+arrRezultat.datumKreiranja+"</td>"+ 
                            "<td style='text-align: right; width: 55px;'><a class='link' href='javascript: void(0);' onclick='dodajPitanje("+arrRezultat.idPitanje+", "+anketa+");'>dodaj </a></td>";
                    }
                    rezultat+="</tr>"+
                        "</toby></table>";
                }                                                                                                                                                    
                    
                if(ukupnoZapisa>=1){
                    document.getElementById('sadrzaj').innerHTML=rezultat;                             
                    brojStrana=Math.ceil(ukupnoZapisa / limit);
                    odstampajPaginaciju(strana);
                }else {
                    //document.getElementById('sadrzaj').innerHTML="";
                    document.getElementById('sadrzaj').innerHTML=rezultat;
                    odstampajPaginaciju(0);
                }
            }//if
        }//onreadystatechange
        
        //uzima izbranu vrstu sortiranja
        var e = document.getElementById("orderby");
        var orderby = e.options[e.selectedIndex].value;     
        //limt prikaza po strani
        e = document.getElementById("limit");
        limit = e.options[e.selectedIndex].value;
        
        var kljucnaRec=document.frmPretraga.kljucna_rec.value;
        
        xmlhttp.open("GET","index.php?p=22&anketa="+anketa+"&ajaxJson&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby, true);    
        
        xmlhttp.send();
    }//getActiveAnketeInfoAjaxJson

</script>

<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 750px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($pitanja)) {
        ?>    
        <div class="naslov">Dodavanje pitanja</div>

        <div style="float:left; width: 100%; height: 0px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:0px dotted grey; width: 99%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; margin: 5px 5px 0px 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: right; border: 0px solid #bdf;">Sortiraj po:<br/> 

                    <select id="orderby" name="orderby" onchange="promeniStranu(0)">
                        <option value="tekstAsc" <?php echo ($orderby === 'tekstAsc' ? 'selected' : ''); ?>>Nazivu: A - Z</option>
                        <option value="tekstDesc" <?php echo ($orderby === 'tekstDesc' ? 'selected' : ''); ?>>Nazivu: Z - A</option>
                        <option value="datumKreiranjaAsc" <?php echo ($orderby === 'datumKreiranjaAsc' ? 'selected' : ''); ?>>Dat. kreiranja: rastuće</option>
                        <option value="datumKreiranjaDesc" <?php echo ($orderby === 'datumKreiranjaDesc' ? 'selected' : ''); ?>>Dat. kreiranja: opadajuće</option>
                    </select>
                </div>

                <div style="float: left; border: 0px solid #bdf;">
                    <form name="frmPretraga">
                        Pretraga po tekstu pitanja:<br/>
                        <input value="" onkeyup="promeniStranu(0)" type="text" name="kljucna_rec" id="pretraga" style="width:137px;">
                    </form>
                </div>
            </div>

            <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%;">  
                <div style="border: 0px solid red; float: left; width: 30%; height: 27px; line-height: 27px;">
                    <span>Prikaži: </span>
                    <select id="limit" onchange="promeniStranu(0)">
                        <option value="2">2</option>
                        <option value="5">5</option>
                        <option value="10" selected>10</option>
                        <option value="20">20</option>                        
                    </select>                    
                </div>

                <?php
                include_once 'modules/mod_paginacija/paginacija_gore.php';
                ?>

            </div>
        </div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%">            
            <table class="hor-minimalist-b0" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope='col'>Pitanja</th>
                        <th scope='col'>Dat. kreiranja</th>  
                        <!-- ne trebaju mi strelice na linku, tako da je dodata nepostojeća slika> -->
                        <th scope='col'></th>
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (count($pitanja) != count($pitanja, 1)) {
                        foreach ($pitanja as $p) {
                            ?>
                            <tr id="<?php echo $p['idPitanje']; ?>">                            
                                <?php
                                echo "<td>{$p['tekst']}</td>";
                                echo "<td>{$p['datumKreiranja']}</td>";
                                ?> <td style="text-align: right; width: 55px;"><a class="link" href='javascript: void(0);' onclick='dodajPitanje(<?php echo "{$p['idPitanje']}, {$idAnketa}"; ?>);'>dodaj </a></td>
                                <?php
                                echo "</tr>";
                            }
                        } else {
                            ?>
                        <tr id="<?php echo $pitanje['idPitanje']; ?>">  
                            <?php
                            echo "<td>{$pitanja['tekst']}</td>";
                            echo "<td>{$pitanja['datumKreiranja']}</td>";
                            ?> <td style="text-align: right; width: 55px;"><a class="link" href='javascript: void(0);' onclick='dodajPitanje(<?php echo "{$pitanje['idPitanje']}, {$idAnketa}"; ?>);'>dodaj </a></td>
                            <?php
                            echo "</tr>";
                        }
                        ?>            
                </tbody>
            </table>
        </div>       

        <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">

            <?php
            include_once 'modules/mod_paginacija/paginacija_dole.php';
            ?>

            <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
                Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> pitanja
            </div>

        </div>

    <?php }
    ?>
</div>