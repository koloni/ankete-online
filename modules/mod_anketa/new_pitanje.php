<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmNewPitanje").validate({
            rules: {                
                tekst:{
                    required: true,                    
                    minlength: 3,
                    maxlength: 200
                    
                },
                'odgovori[]': {
                    required: true,                    
                    minlength: 1,
                    maxlength: 100
                }
                ,            
                'potpitanja[]': {
                    required: true,                    
                    minlength: 1,
                    maxlength: 100
                
                }},
            messages: {
                tekst:{
                    required: "Naslov pitanja je obavezan!",                    
                    minlength: $.format("Naslov pitanja mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")                    
                },
                'odgovori[]': {
                    required: "Svi <b>odgovori</b> moraju biti popunjeni!",                    
                    minlength: $.format("Odgovor mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                'potpitanja[]': {
                    required: "Sva <b>potpitanja</b> moraju biti popunjena!",                    
                    minlength: $.format("Potpitanje mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                }
            }
        });
                
    });
    
                        

    
    
    $.validator.addMethod("alphabetWithOneSpace",
    function(value, element) {                
        //ovde dozvoljava jedan ' ' ili jednu '_'
        //return /^([a-zA-Z]+(_[a-zA-Z]+)*)(\s([a-zA-Z]+(_[a-zA-Z]+)*))*$/.test(value);
        
        return /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/.test(value);
    },
    "Dozvoljena su samo slova, sa jednim razmakom!"
);
    
    $.validator.addMethod("alphabet",
    function(value, element) {                 
        return /^[A-Za-z]+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
</script>


<div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
    <div class="naslov">Pravljenje pitanja</div>
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
    <form name="frmNewPitanje" id="frmNewPitanje" action="" method="POST">            
        <div class = 'edit_pitanje' id = 'vrstaPitanja'>
            <!--<div style="float: left; padding: 5px;">-->
            <div class="edit_pitanje_kolona_levo">
                <div>Tip pitanja: </div>
            </div>

            <div class="edit_pitanje_kolona_desno">
                <div> 
                    <select onchange='showPitanje(this.value);' name = 'idVrstaOdgovora' > 
                        <option value='0'> - - - Izaberite tip pitanja - - - </option>
                        <?php
                        foreach ($vrstaOdgovora as $vo) {
                            echo "<option value='{$vo['idVrstaOdgovora']}'>{$vo['naziv']}</option>";
                        }
                        ?>               
                    </select>
                </div>
            </div>
        </div>

        <div class = 'edit_pitanje' id = 'pitanje' style='display: none;'>
            <div style="width: 100%;"><label style="padding-left: 220px;" for="tekst" class="error"></label></div>

            <div class = 'edit_pitanje_kolona_levo'>                      
                <div>Tekst pitanja: </div>       
            </div>

            <div class = 'edit_pitanje_kolona_desno' style="width: 400px;">
                <input name='idAnketa' type='hidden' value='<?php echo $idAnketa; ?>'>
                <div><input  type = 'text' name='tekst' style="width: 100%"/></div>                 
                <!--<div><button onclick = 'addOdgovor(this.parentNode);'>dodaj odgovor</button></div>-->

            </div>
        </div>

        <div class = 'edit_pitanje' id = 'podnaslov' style='display: none;'>
            <div style="width: 100%;"><label style="padding-left: 220px;" for="tekst" class="error"></label></div>

            <div class = 'edit_pitanje_kolona_levo'>                      
                <div>Podnaslov: </div>       
            </div>

            <div class = 'edit_pitanje_kolona_desno' style="width: 370px;">                
                <div><input  type = 'text' name='podnaslov' style="width: 100%"/></div>                                 
            </div>
        </div>



        <div class = 'edit_pitanje' id = 'potpitanja' style='display: none;'>
            <div style="width: 100%;"><label style="padding-left: 220px;" for="potpitanja[]" class="error"></label></div>

            <div class = 'edit_pitanje_kolona_levo'> 
                <div>Potpitanja:</div>
            </div>
            <div class = 'edit_pitanje_kolona_desno' style="width: 385px;"> 
                <div><a href='javascript:void(0);' onclick = "addPotpitanje(this.parentNode)"><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj potpitanje</a></div>
            </div>
        </div>

        <div class = 'edit_pitanje' id = 'odgovori' style='display: none;'>
            <div style="width: 100%;"><label style="padding-left: 220px;" for="odgovori[]" class="error"></label></div>

            <!--<div style = "border: 1px solid black; height:auto;">-->
            <div class = 'edit_pitanje_kolona_levo'>
                <div>Ogovori: </div>
            </div>

            <div class = 'edit_pitanje_kolona_desno' style="width: 350px;">  
                <div>
                    <input style="width: 85%;" type = 'text' name = 'odgovori[]' value=''>
                    <a href="javascript:void(0);" onclick = 'removeOdgovor(this.parentNode)'><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png' ></a>
                </div>


                <div><a href="javascript:void(0);" onclick = "addOdgovor(this.parentNode)"><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj novi odgovor</a></div>
            </div>
        </div>


        <div class = 'edit_pitanje' id = 'nesto_drugo' style='display: none;'>                                      
            <div class="edit_pitanje_kolona_levo"></div>
            <div class="edit_pitanje_kolona_desno">           
                <input type="checkbox" name="nestoDrugo" value="1" id="nestoDrugo" onchange="document.getElementById('nestoDrugoTekst').style.display=='none'?document.getElementById('nestoDrugoTekst').style.display='block':document.getElementById('nestoDrugoTekst').style.display='none';"/><label for="nestoDrugo">Dozvoli jedan slobodan unos teksta</label>                                   
                <div id="nestoDrugoTekst" style="padding:0 0 20px 20px; display: none;">Tekst labele slobodnog unosa:                                                                               
                                <select onchange='' name = 'idOdgovorNestoDrugo' >                                     
                                    <?php
                                     echo "<option value='{$odgovorNestoDrugo[0]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[0]['tekst']}</option>";
                                    for ($i=1; $i<count($odgovorNestoDrugo); $i++) {
                                        echo "<option value='{$odgovorNestoDrugo[$i]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[$i]['tekst']}</option>";
                                    }
                                    ?>               
                                </select><br/>                               
                </div>
            </div>              
        </div>

        <div class = 'edit_pitanje' id = 'obavezno' style='display: none;'>
            <div class = 'edit_pitanje_kolona_levo'>        
                <!--<div>Obavezno: </div>                   -->
            </div>

            <div class = 'edit_pitanje_kolona_desno' style="width: 400px;">  
                <input type="checkbox" name="isObavezno" value="1" id="isObavezno"/><label for="isObavezno">Obavezan odgovor</label>
                <!-- <div> 
                     <select name = 'isObavezno'> 
                         <option value='1' selected >Da</option>
                         <option value='0' >Ne</option>
                     </select>
                 </div>                   
                -->
            </div>
        </div>

        <div class = 'edit_pitanje' id = 'kontrole' style='display: none;'>

            <div class="edit_pitanje_kolona_levo">           
            </div>
            <div class="edit_pitanje_kolona_desno">           
                <input style="width: 135px; height: 27px; margin-right: 10px;" type = 'submit' value = 'Sačuvaj Pitanje'/>                     
                <button style="width: 125px; height: 27px;" onclick='javascript:history.back();'>Odustani</button>
                 <!--<input style = "float:left;" type = 'button' onclick = '' value = 'Odustani'/>-->
            </div>
        </div>

        <!--</div>-->
        <!--
        <div style = "width: 380px; float: left;border: 1px solid red;">
        <div style = "margin: 0 auto;">
        <input style = "float:left;width: 100px;" type = 'button' onclick = '' value = 'Sačuvaj'/>
        <input style = "float:right;width: 100px;" type = 'button' onclick = '' value = 'Obriši'/>
        </div>
        </div>
        --> 
    </form>

</div>
