<?php
include_once MODULES_DIR . "/mod_message/message.php";
if (isset($ankete)) {
    ?>     
    <table id="active_ankete" class="hor-minimalist-b0" style="width: 100%;">
        <thead>
            <tr>
                <th scope='col'>Naziv</th>
                <th scope='col'>Dat. početka</th>
                <th scope='col'>Dat. isteka</th>
                <th scope='col'>Personalizovana</th>
                <th scope='col'>Broj pitanja.</th> 
                <th> </th>
            </tr>
        </thead>
        <tbody>            
            <?php
            if (count($ankete) != count($ankete, 1)) {
                foreach ($ankete as $a) {
                    echo "<tr>";
                    echo "<td>{$a['naziv']}</td>";
                    echo "<td>" . substr($a['datumPocetka'], 0, 10) . "</td>";
                    echo "<td>" . substr($a['datumZavrsetka'], 0, 10) . "</td>";
                    //echo "<td>{$a['datumZavrsetka']}</td>";                            
                    echo "<td>" . ($a['isPersonalizovana'] == 0 ? 'Ne' : 'Da') . "</td>";
                    echo "<td>{$a['ukupnoPitanja']}</td>";
                    if (isset($a['isPopunjen']) && $a['isPopunjen'] == 0) {
                        echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Nastavi</a></td>";
                    } else if (!isset($a['isPopunjen'])) {
                        echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Popuni anketu</a></td>";
                    } else {
                        echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}'>Popunjena</a></td>";
                    }
                    //echo "<td><a href='edit_anketa.php?idAnketa={$a['idAnketa']}'>izmeni</a></td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr>";
                echo "<td>{$ankete['naziv']}</td>";
                echo "<td>" . substr($ankete['datumPocetka'], 0, 10) . "</td>";
                echo "<td>" . substr($ankete['datumZavrsetka'], 0, 10) . "</td>";
                //echo "<td>{$a['datumZavrsetka']}</td>";
                echo "<td>" . ($ankete['isPersonalizovana'] == 0 ? 'Ne' : 'Da') . "</td>";
                echo "<td>{$ankete['ukupnoPitanja']}</td>";
                if (isset($ankete['isPopunjen']) && $ankete['isPopunjen'] == 0) {
                    echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$ankete['idAnketa']}'>Nastavi</a></td>";
                } else if (!isset($ankete['isPopunjen'])) {
                    echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&anketa={$ankete['idAnketa']}'>Popuni anketu</a></td>";
                } else {
                    echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$ankete['idAnketa']}'>Popunjena</a></td>";
                }
                //echo "<td><a href='edit_anketa.php?idAnketa={$a['idAnketa']}'>izmeni</a></td>";
                echo "</tr>";
            }
            ?>            
        </tbody>
    </table>

<?php } ?>