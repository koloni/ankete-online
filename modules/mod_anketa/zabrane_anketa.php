<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="Stylesheet"></link>
<style type="text/css">
    #someDiv
    {
        width:200px;
        height:250px;   
        background:rgb(33,177,60);
        margin:5px;
        margin-left:0px;
        display:none;
    }

    .visiblediv
    {
        visibility:visible;
    }
    .hidden
    {
        display:none;
    }
    #loading-div-background  
    {
        display:none;
        position:fixed;
        top:0;
        left:0;
        /* background:black;*/
        background:white;
        width:100%;
        height:100%;
        z-index:1000;
    } 

    #loading-div
    {
        width: 300px;
        height: 200px;
        background-color: white;
        text-align:center;
        position:absolute;
        left: 50%;
        top: 50%;
        margin-left:-150px;
        margin-top: -100px;
        z-index:1001;
    }
    #loading-div h2
    {
        font-family:Segoe UI,Calibri,Tahoma;
        text-decoration:none;
    }

</style>

<script type="text/javascript">
    $(document).ready(function () {
        //$("#loading-div-background").css({ opacity: 0.8 });
        $("#loading-div-background").css({ opacity: 0.3 });
            
    });

    function ShowProgressAnimation() {            
        $("#loading-div-background").show();
    } 

    function HideProgressAnimation() { 
        $("#loading-div-background").hide();
    }
</script>

<script>
    var trenutna=0;
    var limit=<?php echo $limit; ?>;
    var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
    var brojStrana=<?php echo $brStrana; ?>;
    
    function idiNaStranu(strana) {      
        strana-=1;
        if(strana<0) {
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            return;
        }else if(strana>=brojStrana) {
            getAnketeInfoAjaxJson(brojStrana-1);    
            return;
        }        
        getAnketeInfoAjaxJson(strana);    
    }//idiNaStranu
    
    function promeniStranu(strana){       
        getAnketeInfoAjaxJson(strana);
    }//promeniStranu
    
    /*
    function odstampajPaginaciju(str){
        if(brojStrana>=1) {    
            trenutna=str;          
            var strane="";//"<div class='pagination' style='float:left;'>";
            //prethodna strana
            if(trenutna >= 1) {  
                strane+="<a title='Strana "+(trenutna)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna-1)+")'>&#171; Prethodna</a>"                        
            }else{
                //strane+="<span class='disabled'>&#171; Prethodna</span>"
            }   
                    
            for (i = 0; i < brojStrana; i++) {
                if (i == str) {
                    strane+="<span class='current'>"+ (i + 1) +"</span>"; 
                    //štampanje dve sa desne strane
                } else if (i+1 == str || i+2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>"; 
                }else if (i == str-3) {
                    //štampanje prve strane
                    strane+="<a title='Strana 1' href='javascript:void(0)' onclick='promeniStranu(0)'>1</a>";
                    strane+=" ... ";                
                }else if (i == str+3) {
                    strane+=" ... ";
                    //štampanje poslednje strane
                    strane+="<a title='Poslednja strana' href='javascript:void(0)' onclick='promeniStranu("+(brojStrana-1)+")'>"+ (brojStrana) +"</a>";
                    break;
                    //štampanje dve sa leve strane
                }else if (i-1 == str || i-2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>";                                                        
                }            
            }//for
            //sledeća strana
            if (trenutna < brojStrana - 1) {                                
                strane+="<a title='Strana "+(trenutna+2)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna+1)+")' class='next'>Sledeća &#187;</a>";                        
            }else{
                //strane+="<span class='disabled'>Sledeća &#187;</span>";
            }
        
            //strane+="</div>";
            //odustao sam od stampanja jer imam dva iput type text
            //strane+="<div style='font-size:10px;float:left;'> Idi na <input size='2' type='text'>&nbsp;<input type='button' style='font-size:11px;' value='stranu'></div>"             
             
            document.getElementById("paginacijaGore").style.display='block';
            document.getElementById("paginacijaDole").style.display='block';
            document.getElementById("paginacijaGoreStrane").innerHTML=strane;
            document.getElementById("paginacijaDoleStrane").innerHTML=strane; 
            //poništi tekst
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            //$('#idiNaStranu1').value="";
            //$("#idiNaStranu1").html("");
        
            var pocetniZapis=trenutna*limit;
            var zapisDo=(pocetniZapis+limit*1);
            if(zapisDo>ukupnoZapisa){
                zapisDo=ukupnoZapisa;
            }
       
            var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa+" pitanja";
            document.getElementById("prikazanoZapisa").innerHTML=prikazanoZapisa;
        }else {
            document.getElementById("paginacijaGore").style.display='none';
            document.getElementById("paginacijaDole").style.display='none';                  
            document.getElementById("prikazanoZapisa").innerHTML="";
            //document.getElementById("paginacijaGore").innerHTML="";        
            //document.getElementById("paginacijaDole").innerHTML=""; 
        }       
    }//odstampajPaginaciju
     */
    
    function getAnketeInfoAjaxJson(strana){       
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {   
                var jsonRezultat=xmlhttp.responseText;   
                //alert(jsonRezultat);
                var rezultat="";   
                //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
                //if(jsonRezultat.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                //if(jsonRezultat.replace(/\r\n|\r|\n/, "")=='false'){                
                if(jsonRezultat=='\nfalse'){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                    ukupnoZapisa=0;
                    brojStrana=0;
                }else  {        
                    //alert(jsonRezultat);
                    var json=JSON.parse(jsonRezultat); 
                    //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    var arrRezultat = json.ankete;                                   
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Naziv</th>"+
                        "<th scope='col'>Dat. početka</th>"+                                                                                    
                        "<th scope='col'>Dat. isteka</th>"+ 
                        "<th scope='col'>Stanje</th>"+ 
                        "<th scope='col'>Aktiviraj</th>"+                        
                        "<th scope='col'>Blokiraj</th>"+ 
                        "</tr>"+
                        "</thead>"+
                        "<tbody>";                        
                    if(arrRezultat.length>=1){ 
                        for(i=0; i<arrRezultat.length; i++){                            
                            rezultat+="<tr id='tr"+arrRezultat[i].idAnketa+"'>"+
                                "<td>"+arrRezultat[i].naziv+"</td>"+
                                "<td>"+arrRezultat[i].datumPocetka+"</td>"+
                                "<td>"+arrRezultat[i].datumZavrsetka+"</td>";                                
                            if(arrRezultat[i].stanje==1){                    
                                rezultat+="<td><span style='color: green;'>"+arrRezultat[i].stanjeAnkete+"</span></td>"+
                                    "<td><button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+arrRezultat[i].idAnketa+", 1)'>aktiviraj</button></td>"+
                                    "<td><button style='font-weight:600; color: red;' onclick='changeStateAnketa("+arrRezultat[i].idAnketa+", -1)'>blokiraj</button></td>";
                            }else if (arrRezultat[i].stanje==-1){
                                rezultat+="<td><span style='color: red;'>"+arrRezultat[i].stanjeAnkete+"</span></td>"+
                                    "<td><button style='font-weight:600; color: green;' onclick='changeStateAnketa("+arrRezultat[i].idAnketa+", 1)'>aktiviraj</button></td>"+
                                    "<td><button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+arrRezultat[i].idAnketa+", -1)'>blokiraj</button></td>";
                            }  
                            rezultat+="</tr>";
                        }//for                    
                    } else {                                                                                                                                       
                        rezultat+="<tr id='tr"+arrRezultat.idAnketa+"'>"+
                            "<td>"+arrRezultat.naziv+"</td>"+
                            "<td>"+arrRezultat.datumPocetka+"</td>"+
                            "<td>"+arrRezultat.datumZavrsetka+"</td>";                            
                        if(arrRezultat.stanje==1){
                            "<td><span style='color: green;'>"+arrRezultat.stanjeAnkete+"</span></td>"+
                                "<td><button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+arrRezultat.idAnketa+", 1)'>aktiviraj</button></td>"+
                                "<td><button style='font-weight:600; color: red;' onclick='changeStateAnketa("+arrRezultat.idAnketa+", -1)'>blokiraj</button></td>";
                        }else if (arrRezultat.stanje==-1){
                            rezultat+="<td><span style='color: red;'>"+arrRezultat.stanjeAnkete+"</span></td>"+
                                "<td><button style='font-weight:600; color: green;' onclick='changeStateAnketa("+arrRezultat.idAnketa+", 1)'>aktiviraj</button></td>"+
                                "<td><button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+arrRezultat.idAnketa+", -1)'>blokiraj</button></td>";
                        } 
                        rezultat+="</tr>";
                    }
                    rezultat+="</toby></table>";
                }                                                                                                                                                    
                    
                if(ukupnoZapisa>=1){
                    document.getElementById('sadrzaj').innerHTML=rezultat;                             
                    brojStrana=Math.ceil(ukupnoZapisa / limit);
                    odstampajPaginaciju(strana);
                }else {
                    //document.getElementById('sadrzaj').innerHTML="";
                    document.getElementById('sadrzaj').innerHTML=rezultat;
                    odstampajPaginaciju(0);
                }           
            }//if           
        }//onreadystatechange
        
        //uzima izbranu vrstu sortiranja
        var e = document.getElementById("orderby");
        var orderby = e.options[e.selectedIndex].value;     
        //limt prikaza po strani
        e = document.getElementById("limit");
        limit = e.options[e.selectedIndex].value;
        
        var kljucnaRec=document.frmPretraga.kljucna_rec.value;
        
        xmlhttp.open("GET","index.php?p=60&ajaxJson&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby, true);    
        
        xmlhttp.send();
    }//getActiveAnketeInfoAjaxJson

</script>

<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 750px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($ankete)) {
        ?>    
        <div id="loading-div-background">
            <div class="ui-corner-all" id="loading-div">
                <img alt="Loading.." src="http://localhost/ankete/images/loading.gif" style="height: 30px; margin: 30px;" />     
                <br />
                <h2 style="color: black; font-weight: normal;">Molimo sačekajte....</h2>              
            </div>
        </div>

        <div class="naslov">Zabrana anketa</div>

        <div style="float:left; width: 100%; height: 0px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:0px dotted grey; width: 99%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; margin: 5px 5px 0px 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: right; border: 0px solid #bdf;">Sortiraj po:<br/> 

                    <select id="orderby" name="orderby" onchange="promeniStranu(0)" style="font-size: 12px;">
                        <option value="nazivAsc" <?php echo ($orderby === 'nazivAsc' ? 'selected' : ''); ?>>Nazivu: A - Z</option>
                        <option value="nazivDesc" <?php echo ($orderby === 'nazivDesc' ? 'selected' : ''); ?>>Nazivu: Z - A</option>
                        <option value="datumPocetkaAsc" <?php echo ($orderby === 'datumPocetkaAsc' ? 'selected' : ''); ?>>Dat. početka: rastuće</option>
                        <option value="datumPocetkaDesc" <?php echo ($orderby === 'datumPocetkaDesc' ? 'selected' : ''); ?>>Dat. početka: opadajuće</option>
                        <option value="datumIstekaAsc" <?php echo ($orderby === 'datumIstekaAsc' ? 'selected' : ''); ?>>Dat. isteka: rastuće</option>
                        <option value="datumIstekaDesc" <?php echo ($orderby === 'datumIstekaDesc' ? 'selected' : ''); ?>>Dat. isteka: opadajuće</option>
                    </select>
                </div>

                <div style="float: left; border: 0px solid #bdf;">
                    <form name="frmPretraga">
                        Pretraga po nazivu ankete:<br/>
                        <input value="" onkeyup="promeniStranu(0)" type="text" name="kljucna_rec" id="pretraga" style="width:137px;">
                    </form>
                </div>
            </div>

            <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%;">  
                <div style="border: 0px solid red; float: left; width: 30%; height: 27px; line-height: 27px;">
                    <span>Prikaži: </span>
                    <select id="limit" onchange="promeniStranu(0)" style="font-size: 12px;">
                        <option value="2">2</option>
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>                        
                    </select>                    
                </div>

                <?php
                include_once 'modules/mod_paginacija/paginacija_gore.php';
                ?>

            </div>
        </div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%">            
            <table class="hor-minimalist-b0" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope='col'>Naziv</th>
                        <th scope='col'>Dat. kreiranja</th>                          
                        <th scope='col'>Dat. isteka</th>
                        <th scope='col'>Stanje</th>
                        <th scope='col'>Aktiviraj</th>
                        <th scope='col'>Blokiraj</th>
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (count($ankete) != count($ankete, 1)) {
                        foreach ($ankete as $a) {
                            ?>
                            <tr id="tr<?php echo $a['idAnketa']; ?>">
                                <?php
                                echo "<td>{$a['naziv']}</td>";
                                echo "<td>" . substr($a['datumPocetka'], 0, 10) . "</td>";
                                echo "<td>" . substr($a['datumZavrsetka'], 0, 10) . "</td>";
                                if ($a['stanje'] == -1) {
                                    ?>
                                    <td><span style="color:red;">Blokirana</span></td>
                                    <td><button style='font-weight:600; color: green;' onclick="changeStateAnketa(<?php echo "{$a['idAnketa']}, 1"; ?>)">aktiviraj</button></td>                            
                                    <td><button disabled style='font-weight:600; color: gray;' onclick="changeStateAnketa(<?php echo "{$a['idAnketa']}, -1"; ?>)">blokiraj</button></td>                        
                                    <?php
                                } else if ($a['stanje'] == 1) {
                                    ?>
                                    <td><span style="color: green;">Aktivna</span></td>
                                    <td><button disabled style='font-weight:600; color: gray;' onclick="changeStateAnketa(<?php echo "{$a['idAnketa']}, 1"; ?>)">aktiviraj</button></td>                            
                                    <td><button style='font-weight:600; color: red;' onclick="changeStateAnketa(<?php echo "{$a['idAnketa']}, -1"; ?>)">blokiraj</button></td>                        
                                    <?php
                                }
                                echo "</tr>";
                            }
                        } else {
                            ?>
                        <tr id="tr<?php echo $ankete['idAnketa']; ?>">                              
                            <?php
                            echo "<td>{$ankete['naziv']}</td>";
                            echo "<td>" . substr($ankete['datumPocetka'], 0, 10) . "</td>";
                            echo "<td>" . substr($ankete['datumZavrsetka'], 0, 10) . "</td>";
                            if ($ankete['stanje'] == -1) {
                                ?>
                                <td><button style='font-weight:600; color: green;' onclick="changeStateAnketa(<?php echo "{$ankete['idAnketa']}, 1"; ?>)">aktiviraj</button></td>                            
                                <td><button disabled style='font-weight:600; color: gray;' onclick="changeStateAnketa(<?php echo "{$ankete['idAnketa']}, -1"; ?>)">blokiraj</button></td>                        
                                <?php
                            } else if ($ankete['stanje'] == 1) {
                                ?>
                                <td><button disabled style='font-weight:600; color: gray;' onclick="changeStateAnketa(<?php echo "{$ankete['idAnketa']}, 1"; ?>)">aktiviraj</button></td>                            
                                <td><button style='font-weight:600; color: red;' onclick="changeStateAnketa(<?php echo "{$ankete['idAnketa']}, -1"; ?>)">blokiraj</button></td>                        
                                <?php
                            }
                            echo "</tr>";
                        }
                        ?>            
                </tbody>
            </table>
        </div>       

        <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">

            <?php
            include_once 'modules/mod_paginacija/paginacija_dole.php';
            ?>

            <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
                Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> anketa
            </div>

        </div>

    <?php }
    ?>
</div>