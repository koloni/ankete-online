<?php
include_once MODULES_DIR . "/mod_message/message.php";
if (isset($pitanja) && is_array($pitanja)) {
    ?>
    <div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
        <div class="naslov">Dodavanje pitanja</div>
        <table class="hor-minimalist-b" summary="Dodavanje pitanja" style="font-size: 13px;">
            <tbody>

                <?php
                if (count($pitanja) != count($pitanja, 1)) {
                    foreach ($pitanja as $p) {
                        ?>
                        <!--
                         <tr>
                             <td><?php echo $p['tekst'] ?></td>
                             <td style="text-align: right; width: 55px;" class="link" onclick='dodajPitanje(this.parentNode, <?php echo "{$idAnketa}, {$p['idPitanje']}"; ?>);'><a href='javascript: void(0);'>dodaj </a></td>
                         </tr>  
                        -->
                        <tr id="<?php echo $p['idPitanje']; ?>">
                            <td><?php echo $p['tekst'] ?></td>
                            <td style="text-align: right; width: 55px;"><a class="link" href='javascript: void(0);' onclick='dodajPitanje(<?php echo "{$p['idPitanje']}, {$idAnketa}"; ?>);'>dodaj </a></td>
                        </tr>  
                        <?php
                    }
                } else {
                    ?>
                    <tr id="<?php echo $pitanja['idPitanje']; ?>">
                        <td><?php echo $pitanja['tekst'] ?></td>
                        <td style="text-align: right; width: 55px;"><a class="link" href='javascript: void(0);' onclick='dodajPitanje(<?php echo "{$pitanja['idPitanje']}, {$idAnketa}"; ?>);'>dodaj </a></td>
                    </tr>   
                <? }
                ?>  
            </tbody>
        </table>

    </div>

<?php } ?>

