<?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
<?php
if (isset($ankete) && is_array($ankete)) {
    ?>
    <div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
        <div class="naslov">Brisanje anketa</div>
        <table id="active_ankete" class="hor-minimalist-b">
            <thead>     
                <tr>
                    <th scope='col'>Naziv</th>
                    <th scope='col'>Dat. početka</th>
                    <th scope='col'>Dat. isteka</th>
                    <th scope='col'>Anonimna</th>
                    <th scope='col'>Ukupno pitanja</th>
                   <!-- <th scope='col'>Br. pit. po strani</th>-->
                    <th scope='col'>Stanje</th>
                    <!--<th scope='col'>Redosled pitanja</th>                    -->
                    <th style="padding-left: 0px; background-image: none;">Obriši</th>               
                </tr>        
            </thead>
            <tbody>            
                <?php
                if (count($ankete) != count($ankete, 1)) {
                    foreach ($ankete as $a) {
                        ?>
                        <tr id="<?php echo $a['idAnketa']; ?>">
                            <td><?php echo $a['naziv'] ?></td>
                            <td><?php echo substr($a['datumPocetka'], 0, 10); ?></td>
                            <td><?php echo substr($a['datumZavrsetka'], 0, 10); ?></td>                  
                            <td><?php echo $a['isPersonalizovana'] == 0 ? 'Da' : 'Ne'; ?></td>
                            <?php
                            if ($a['ukupnoPitanja'] == 0) {
                                echo "<td style='color: red;'>{$a['ukupnoPitanja']}</td>";
                            } else {
                                echo "<td>{$a['ukupnoPitanja']}</td>";
                            }
                            ?>                        
                            <!--<td><?php //echo $a['brPitanjaPoStrani'];  ?></td>-->
                            <td><?php echo $a['stanjeAnkete']; ?></td> 
                            <!--<td><?php //echo $a['redosledPitanjaOpis'];   ?></td>                        -->
                            <td>
                                <!--<a style="font-size: 12px; color: red; font-weight:700;" href='index.php?p=33&anketa=<?php echo $a['idAnketa']; ?>'>Obriši</a>-->   

            <!--<a style="font-size: 12px; color: red; font-weight:700;" href="javascript:void(0)" onclick="removeAnketa(<?php echo $a['idAnketa']; ?>)">Obriši</a>-->
                                <a href="javascript:removeAnketa(<?php echo $a['idAnketa']; ?>)" class="ask"><img src="images/trash.png" alt="" title="" border="0" /></a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr id="<?php echo $ankete['idAnketa']; ?>">
                        <td><?php echo $ankete['naziv'] ?></td>
                        <td><?php echo substr($ankete['datumPocetka'], 0, 10); ?></td>
                        <td><?php echo substr($ankete['datumZavrsetka'], 0, 10); ?></td>                  
                        <td><?php echo $ankete['isPersonalizovana'] == 0 ? 'Da' : 'Ne'; ?></td>
                        <?php
                        if ($ankete['ukupnoPitanja'] == 0) {
                            echo "<td style='color: red;'>{$ankete['ukupnoPitanja']}</td>";
                        } else {
                            echo "<td>{$ankete['ukupnoPitanja']}</td>";
                        }
                        ?>                        
                        <!--<td><?php //echo $ankete['brPitanjaPoStrani'];  ?></td>-->
                        <td><?php echo $ankete['stanjeAnkete']; ?></td>
                       <!-- <td><?php //echo $ankete['redosledPitanjaOpis'];    ?></td>-->
                        <td>                            
                            <!--<a style="font-size: 12px; color: red; font-weight:700;" href="javascript:void(0)" onclick="removeAnketa(<?php echo $ankete['idAnketa']; ?>)">Obriši</a>  -->
                            <a href="javascript:removeAnketa(<?php echo $ankete['idAnketa']; ?>)" class="ask"><img src="images/trash.png" alt="" title="" border="0" /></a>
                        </td>                        
                    </tr>
                <?php }
                ?>         
            </tbody>
        </table>
    </div>
    <?php
}
