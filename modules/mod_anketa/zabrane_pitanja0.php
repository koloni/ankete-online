<script>
    var trenutna=0;
    var limit=<?php echo $limit; ?>;
    var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
    var brojStrana=<?php echo $brStrana; ?>;
    
    function idiNaStranu(strana) {      
        strana-=1;
        if(strana<0) {
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            return;
        }else if(strana>=brojStrana) {
            getActiveAnketeInfoAjaxJson(brojStrana-1);    
            return;
        }        
        getActiveAnketeInfoAjaxJson(strana);    
    }//idiNaStranu
    
    function odstampajPaginaciju(){
           if(brojStrana>=1) {    
            trenutna=str;          
            var strane="";//"<div class='pagination' style='float:left;'>";
            //prethodna strana
            if(trenutna >= 1) {  
                strane+="<a title='Strana "+(trenutna)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna-1)+")'>&#171; Prethodna</a>"                        
            }else{
                //strane+="<span class='disabled'>&#171; Prethodna</span>"
            }   
                    
            for (i = 0; i < brojStrana; i++) {
                if (i == str) {
                    strane+="<span class='current'>"+ (i + 1) +"</span>"; 
                    //štampanje dve sa desne strane
                } else if (i+1 == str || i+2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>"; 
                }else if (i == str-3) {
                    //štampanje prve strane
                    strane+="<a title='Strana 1' href='javascript:void(0)' onclick='promeniStranu(0)'>1</a>";
                    strane+=" ... ";                
                }else if (i == str+3) {
                    strane+=" ... ";
                    //štampanje poslednje strane
                    strane+="<a title='Poslednja strana' href='javascript:void(0)' onclick='promeniStranu("+(brojStrana-1)+")'>"+ (brojStrana) +"</a>";
                    break;
                    //štampanje dve sa leve strane
                }else if (i-1 == str || i-2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>";                                                        
                }            
            }//for
            //sledeća strana
            if (trenutna < brojStrana - 1) {                                
                strane+="<a title='Strana "+(trenutna+2)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna+1)+")' class='next'>Sledeća &#187;</a>";                        
            }else{
                //strane+="<span class='disabled'>Sledeća &#187;</span>";
            }
        
            //strane+="</div>";
            //odustao sam od stampanja jer imam dva iput type text
            //strane+="<div style='font-size:10px;float:left;'> Idi na <input size='2' type='text'>&nbsp;<input type='button' style='font-size:11px;' value='stranu'></div>"             
             
            document.getElementById("paginacijaGore").style.display='block';
            document.getElementById("paginacijaDole").style.display='block';
            document.getElementById("paginacijaGoreStrane").innerHTML=strane;
            document.getElementById("paginacijaDoleStrane").innerHTML=strane; 
            //poništi tekst
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            //$('#idiNaStranu1').value="";
            //$("#idiNaStranu1").html("");
        
            var pocetniZapis=trenutna*limit;
            var zapisDo=(pocetniZapis+limit*1);
            if(zapisDo>ukupnoZapisa){
                zapisDo=ukupnoZapisa;
            }
       
            var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa+" pitanja";
            document.getElementById("prikazanoZapisa").innerHTML=prikazanoZapisa;
        }else {
            document.getElementById("paginacijaGore").style.display='none';
            document.getElementById("paginacijaDole").style.display='none';                  
            document.getElementById("prikazanoZapisa").innerHTML="";
            //document.getElementById("paginacijaGore").innerHTML="";        
            //document.getElementById("paginacijaDole").innerHTML=""; 
        }       
    }//odstampajPaginaciju
    
    function promeniStranu(str){ 
        if(brojStrana>=1) {    
            trenutna=str;          
            var strane="";//"<div class='pagination' style='float:left;'>";
            //prethodna strana
            if(trenutna >= 1) {  
                strane+="<a title='Strana "+(trenutna)+"' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson("+(trenutna-1)+")'>&#171; Prethodna</a>"                        
            }else{
                //strane+="<span class='disabled'>&#171; Prethodna</span>"
            }   
                    
            for (i = 0; i < brojStrana; i++) {
                if (i == str) {
                    strane+="<span class='current'>"+ (i + 1) +"</span>"; 
                    //štampanje dve sa desne strane
                } else if (i+1 == str || i+2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson("+i+")'>"+ (i + 1) +"</a>"; 
                }else if (i == str-3) {
                    //štampanje prve strane
                    strane+="<a title='Strana 1' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(0)'>1</a>";
                    strane+=" ... ";                
                }else if (i == str+3) {
                    strane+=" ... ";
                    //štampanje poslednje strane
                    strane+="<a title='Poslednja strana' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson("+(brojStrana-1)+")'>"+ (brojStrana) +"</a>";
                    break;
                    //štampanje dve sa leve strane
                }else if (i-1 == str || i-2==str) {
                    strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson("+i+")'>"+ (i + 1) +"</a>";                                                        
                }            
            }//for
            //sledeća strana
            if (trenutna < brojStrana - 1) {                                
                strane+="<a title='Strana "+(trenutna+2)+"' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson("+(trenutna+1)+")' class='next'>Sledeća &#187;</a>";                        
            }else{
                //strane+="<span class='disabled'>Sledeća &#187;</span>";
            }
        
            //strane+="</div>";
            //odustao sam od stampanja jer imam dva iput type text
            //strane+="<div style='font-size:10px;float:left;'> Idi na <input size='2' type='text'>&nbsp;<input type='button' style='font-size:11px;' value='stranu'></div>"             
             
            document.getElementById("paginacijaGore").style.display='block';
            document.getElementById("paginacijaDole").style.display='block';
            document.getElementById("paginacijaGoreStrane").innerHTML=strane;
            document.getElementById("paginacijaDoleStrane").innerHTML=strane; 
            //poništi tekst
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            //$('#idiNaStranu1').value="";
            //$("#idiNaStranu1").html("");
        
            var pocetniZapis=trenutna*limit;
            var zapisDo=(pocetniZapis+limit*1);
            if(zapisDo>ukupnoZapisa){
                zapisDo=ukupnoZapisa;
            }
       
            var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa+" pitanja";
            document.getElementById("prikazanoZapisa").innerHTML=prikazanoZapisa;
        }else {
            document.getElementById("paginacijaGore").style.display='none';
            document.getElementById("paginacijaDole").style.display='none';                  
            document.getElementById("prikazanoZapisa").innerHTML="";
            //document.getElementById("paginacijaGore").innerHTML="";        
            //document.getElementById("paginacijaDole").innerHTML=""; 
        }       
    }//promeniStranu

    function getActiveAnketeInfoAjaxJson(strana){       
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {   
                var jsonAnkete=xmlhttp.responseText;   
                //alert(jsonAnkete);
                var rezultat="";   
                //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
                //if(jsonAnkete.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                //if(jsonAnkete.replace(/\r\n|\r|\n/, "")=='false'){                
                if(jsonAnkete=='\nfalse'){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                    ukupnoZapisa=0;
                    brojStrana=0;
                }else  {        
                    //alert(jsonAnkete);
                    var json=JSON.parse(jsonAnkete); 
                    //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    var arrAnkete = json.ankete;                                   
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Pitanje</th>"+
                        "<th scope='col'>Dat. kreiranja</th>"+                                                            
                        "<th scope='col'>Zabrani</th>"+ 
                        "</tr>"+
                        "</thead>"+
                        "<tbody>";                        
                    if(arrAnkete.length>=1){ 
                        for(i=0; i<arrAnkete.length; i++){                            
                            rezultat+="<tr id=''>"+
                                "<td>"+arrAnkete[i].tekst+"</td>"+
                                "<td>"+arrAnkete[i].datumKreiranja+"</td>"+
                                "<td>zabrani</td>";
                        }//for                    
                    } else {                                                                                                                                       
                        rezultat+="<tr id=''>"+
                            "<td>"+arrAnkete.tekst+"</td>"+
                            "<td>"+arrAnkete.datumKreiranja+"</td>"+
                            "<td>zabrani</td>";
                    }
                    rezultat+="</tr>"+
                        "</toby></table>";
                }                                                                                                                                                    
                    
                if(ukupnoZapisa>=1){
                    document.getElementById('sadrzaj').innerHTML=rezultat;                             
                    brojStrana=Math.ceil(ukupnoZapisa / limit);
                    promeniStranu(strana);            
                }else {
                    //document.getElementById('sadrzaj').innerHTML="";
                    document.getElementById('sadrzaj').innerHTML=rezultat;
                    promeniStranu(0);
                }
            }//if
        }//onreadystatechange
        
        //uzima izbranu vrstu sortiranja
        var e = document.getElementById("orderby");
        var orderby = e.options[e.selectedIndex].value;     
        //limt prikaza po strani
        e = document.getElementById("limit");
        limit = e.options[e.selectedIndex].value;
        
        var kljucnaRec=document.frmPretraga.kljucna_rec.value;
        
        xmlhttp.open("GET","index.php?p=62&ajaxJson&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby, true);    
        
        xmlhttp.send();
    }//getActiveAnketeInfoAjaxJson

</script>

<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 750px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($pitanja)) {
        ?>    
        <div class="naslov">Zabrana pitanja</div>

        <div style="float:left; width: 100%; height: 0px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:0px dotted grey; width: 99%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; margin: 5px 5px 0px 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: right; border: 0px solid #bdf;">Sortiraj po:<br/> 

                    <select id="orderby" name="orderby" onchange="getActiveAnketeInfoAjaxJson(0)">
                        <option value="tekstAsc" <?php echo ($orderby === 'tekstAsc' ? 'selected' : ''); ?>>Nazivu: A - Z</option>
                        <option value="tekstDesc" <?php echo ($orderby === 'tekstDesc' ? 'selected' : ''); ?>>Nazivu: Z - A</option>
                        <option value="datumKreiranjaAsc" <?php echo ($orderby === 'datumKreiranjaAsc' ? 'selected' : ''); ?>>Dat. kreiranja: rastuće</option>
                        <option value="datumKreiranjaDesc" <?php echo ($orderby === 'datumKreiranjaDesc' ? 'selected' : ''); ?>>Dat. kreiranja: opadajuće</option>
                    </select>
                </div>

                <div style="float: left; border: 0px solid #bdf;">
                    <form name="frmPretraga">
                        Pretraga po tekstu pitanja:<br/>
                        <input value="" onkeyup="getActiveAnketeInfoAjaxJson(0)" type="text" name="kljucna_rec" id="pretraga" style="width:137px;">
                    </form>
                </div>
            </div>

            <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%;">  
                <div style="border: 0px solid red; float: left; width: 30%; height: 27px; line-height: 27px;">
                    <span>Prikaži: </span>
                    <select id="limit" onchange="getActiveAnketeInfoAjaxJson(0)">
                        <option value="2">2</option>
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>                        
                    </select>                    
                </div>

                <div id="paginacijaGore" style="float: right;  line-height: 27px; height:27px; border: 0px solid green;">
                    <?php
                    if (isset($brStrana)) {
                        //onclick = "alert($('page0').value)"
                        echo "<div id='paginacijaGoreStrane' class='pagination' style='float:left;'>";
                        if ($trenutna >= 1) {
                            echo "<a title='Strana {$trenutna}' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($trenutna - 1) . ")'>&#171; Prethodna</a>";
                        } else {
                            //echo "<span class='disabled'>&#171; Prethodna</span>";
                        }
                        for ($i = 0; $i < $brStrana; $i++) {
                            if ($i == $trenutna) {
                                echo "<span class='current'>" . ($i + 1) . "</span>";
                            } else if ($i + 1 == $trenutna || $i + 2 == $trenutna) {
                                echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson({$i})'>" . ($i + 1) . "</a>";
                            } else if ($i == $trenutna - 3) {
                                //stampanje prve strane
                                echo "<a title='Strana 1' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(0)'>1</a>";
                                echo " ... ";
                            } else if ($i == $trenutna + 3) {
                                echo " ... ";
                                //stampanje poslednje strane
                                echo "<a title='Poslednja strana' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($brStrana - 1) . ")'>{$brStrana}</a>";
                                break;
                            } else if ($i - 1 == $trenutna || $i - 2 == $trenutna) {
                                echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson({$i})'>" . ($i + 1) . "</a>";
                            }
                        }//for
                        if ($trenutna < $brStrana - 1) {
                            echo "<a title='Strana " . ($trenutna + 2) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($trenutna + 1) . ")' class='next'>Sledeća &#187;</a>";
                        } else {
                            //echo "<span class='disabled'>Sledeća &#187;</span>";
                        }
                        ?>                        
                    </div>
                    <div style='font-size:10px;float: left;'>&nbsp;Idi na 
                        <input size='2' type='text' id='idiNaStranu0' onkeyup="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" onkeydown="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')">&nbsp;
                        <input type='button' onclick="idiNaStranu($('#idiNaStranu0').val())" style='font-size:11px;' value='stranu'>
                    </div>
                    <?php
                }
                ?>                
            </div><!--  paginacijaGore -->
        </div>
    </div>

    <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

    <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%">            
        <table class="hor-minimalist-b0" style="width: 100%;">
            <thead>
                <tr>
                    <th scope='col'>Naziv</th>
                    <th scope='col'>Dat. kreiranja</th>  
                    <!-- ne trebaju mi strelice na linku, tako da je dodata nepostojeća slika> -->
                    <th scope='col'>Zabrani</th>
                </tr>
            </thead>
            <tbody>            
                <?php
                if (count($pitanja) != count($pitanja, 1)) {
                    foreach ($pitanja as $p) {
                        echo "<tr>";
                        echo "<td>{$p['tekst']}</td>";
                        echo "<td>{$p['datumKreiranja']}</td>";
                        echo "<td>zabrani</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td>{$pitanja['tekst']}</td>";
                    echo "<td>{$pitanja['datumKreiranja']}</td>";
                    echo "<td>zabrani</td>";
                    echo "</tr>";
                }
                ?>            
            </tbody>
        </table>
    </div>       

    <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">
        <div id="paginacijaDole" style="float: right; line-height: 27px; height:27px; border: 0px solid green;">
            <?php
            if (isset($brStrana)) {
                echo "<div id='paginacijaDoleStrane' class='pagination' style='float:left;'>";
                if ($trenutna >= 1) {
                    echo "<a title='Strana {$trenutna}' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($trenutna - 1) . ")'>&#171; Prethodna</a>";
                } else {
                    //echo "<span class='disabled'>&#171; Prethodna</span>";
                }
                for ($i = 0; $i < $brStrana; $i++) {
                    if ($i == $trenutna) {
                        echo "<span class='current'>" . ($i + 1) . "</span>";
                    } else if ($i + 1 == $trenutna || $i + 2 == $trenutna) {
                        echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson({$i})'>" . ($i + 1) . "</a>";
                    } else if ($i == $trenutna - 3) {
                        //stampanje prve strane
                        echo "<a title='Strana 1' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(0)'>1</a>";
                        echo " ... ";
                    } else if ($i == $trenutna + 3) {
                        echo " ... ";
                        //stampanje poslednje strane
                        echo "<a title='Poslednja strana' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($brStrana - 1) . ")'>{$brStrana}</a>";
                        break;
                    } else if ($i - 1 == $trenutna || $i - 2 == $trenutna) {
                        echo "<a title='Strana " . ($i + 1) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson({$i})'>" . ($i + 1) . "</a>";
                    }
                }//for
                if ($trenutna < $brStrana - 1) {
                    echo "<a title='Strana " . ($trenutna + 2) . "' href='javascript:void(0)' onclick='getActiveAnketeInfoAjaxJson(" . ($trenutna + 1) . ")' class='next'>Sledeća &#187;</a>";
                } else {
                    //echo "<span class='disabled'>Sledeća &#187;</span>";
                }
                ?>
            </div>
            <div style='font-size:10px;float: left;'>&nbsp;Idi na                
                <input size='2' type='text' id='idiNaStranu1' onkeyup="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" onkeydown="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')">&nbsp;
                <input type='button' onclick="idiNaStranu($('#idiNaStranu1').val())" style='font-size:11px;' value='stranu'>
            </div>
            <?php
        }
        ?>
    </div><!--  paginacijaDole -->

    <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
        Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> pitanja
    </div>

    </div>

<?php }
?>
</div>