<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 650px;" >
    <div class="naslov">Azuriranje ankete</div>
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($anketa)) {
        ?>
        <form name="frmAnketa" method="POST" action="index?p=30">
            <div class="anketa">
                <div class="kolona_levo">
                    <div>Naziv ankete:</div>
                    <div>Datum početka:</div>
                    <div>Datum isteka:</div>
                    <div>Personalizovana:</div>                    
                    <div>Broj pitanja po strani:</div>            
                    <div>Redosled prikazivanja pitanja:</div>
                    <div></div>
                </div>
                <div class="kolona">
                    <div><input type="text" name="naziv" size="30" value="<?php echo $anketa['naziv'] ?>"></div>
                   <!--<div>od <input type="date" name="datumPocetka"> do <input type="date" name="datumIsteka"></div>-->
                    <div><input type="date" name="datumPocetka" value="<?php echo substr($anketa['datumPocetka'], 0, 10) ?>"></div>
                    <div><input type="date" name="datumIsteka" value="<?php echo substr($anketa['datumZavrsetka'], 0, 10) ?>"></div>
                    <div>
                        <input type="radio" name="personalizovana" value="true" id="pers_true" <?php echo $anketa['isPersonalizovana']['da'] ?>><label for="pers_true">da</label> 
                        <input type="radio" name="personalizovana" value="false" id="pers_false" <?php echo $anketa['isPersonalizovana']['ne'] ?>><label for="pers_false">ne</label>
                    </div>
                    <div><input type="text" name="brPitanjaPoStrani" size="5" onkeydown="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" onkeyup="this.value=this.value.replace(/^(\+|0|[^\d+])|(\+|[^\d+])$/,'')" value="<?php echo $anketa['brPitanjaPoStrani'] ?>"></div>
                    <!--
                      <div>                       
                          <input type="radio" name="redosledPitanja" value="0" id="poredu" <?php echo $anketa['idRedosledPitanja']['poredu']; ?> />
                          <label for="poredu">po redosledu unosa</label> 
                          <input type="radio" name="redosledPitanja" value="1" id="rucno" <?php echo $anketa['idRedosledPitanja']['rucno']; ?> />
                          <label for="rucno">ručno izabran</label>
                          <input type="radio" name="redosledPitanja" value="2" id="slucajno" <?php echo $anketa['idRedosledPitanja']['slucajno']; ?> />
                          <label for="slucajno">slučajan izbor</label>
                      </div> 
                    -->
                    <div>
                        <select name="redosledPitanja">
                            <option <?php echo $anketa['idRedosledPitanja']['poredu']; ?>  value="0">po redosledu unosa</option>
                            <option <?php echo $anketa['idRedosledPitanja']['rucno']; ?>  value="1">ručno izabran</option>
                            <option <?php echo $anketa['idRedosledPitanja']['slucajno']; ?>  value="2">slučajan izbor</option>
                        </select>                    
                    </div>

                    <div>
                        <input style="height: 28px;" type="submit" value="Sačuvaj izmene"/>
                        <input style="height: 28px;" type="button" value="Odustani"onclick='javascript:history.back();'/>
                        <!--<input style="padding: 3px 10px;" type="reset" value="Odustani"/>-->


                    </div>                    
                </div>
            </div>
            <!--
            <div style="height: 30px; width: 100%; border: 1px solid aquamarine;">
                <div style="border: 1px solid #ff7c7c;"><button style="margin: 0 auto;">napravi anketu</button></div>
            </div>
            -->
        </form>
    <?php } ?>
</div>