<script>
    /*
    <!--
//<select onchange="sorttable.innerSortFunction.apply(document.getElementById(this.value), [])">
   <select id="trigger-link">
   <option value="nazivAsc">Nazivu: A - Z</option>
   <option value="nazivDesc">Nazivu: Z - A</option>
   <option value="datumPocetkaAsc">Dat. početka: rasuće</option>
   <option value="datumPocetkaDesc" selected>Dat. početka: opadajuće</option>
   <option value="datumIstekaAsc">Dat. isteka: rasuće</option>
   <option value="datumIstekaDesc">Dat. isteka: opadajuće</option>
</select>-->

     */
       $(document).ready(function() { 
           $("table").tablesorter(); 
           $("#trigger-link").click(function() { 
               // set sorting column and direction, this will sort on the first and third column the column index starts at zero 
               //var sorting = [[0,0],[3,0]];         
               var sorting="";
               var vrednost=$(this).val();
               if(vrednost=="nazivAsc") {
                   sorting = [[0,0]];     
               }else if(vrednost=="nazivDesc") {
                   sorting = [[0,1]];   
               }else if(vrednost=="datumPocetkaAsc") {
                   sorting = [[1,0]];   
               }else if(vrednost=="datumPocetkaDesc") {
                   sorting = [[1,1]];   
               }else if(vrednost=="datumIstekaAsc") {
                   sorting = [[2,0]];   
               }else if(vrednost=="datumIstekaDesc") {
                   sorting = [[2,1]];   
               }
                        
               // sort on the first column 
               $("table").trigger("sorton",[sorting]); 
               // return false to stop default link action 
               return false; 
           }); 
       });

       var trenutna=0;
       var limit=<?php echo $limit; ?>;
       var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
       var brojStrana=<?php echo $brStrana; ?>;
 
       function idiNaStranu(strana) {      
           strana-=1;
           if(strana<0) {
               document.getElementById('idiNaStranu0').value="";
               document.getElementById('idiNaStranu1').value="";
               return;
           }else if(strana>=brojStrana) {
               getActiveAnketeInfoAjaxJson(brojStrana-1);    
               return;
           }        
           getActiveAnketeInfoAjaxJson(strana);    
       }//idiNaStranu
    
       function promeniStranu(strana){       
           getActiveAnketeInfoAjaxJson(strana);
       }//promeniStranu
 

       function getActiveAnketeInfoAjaxJson(strana){       
           var xmlhttp;
           if (window.XMLHttpRequest)
           {// code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp=new XMLHttpRequest();
           }
           else
           {// code for IE6, IE5
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function()
           {
               if (xmlhttp.readyState==4 && xmlhttp.status==200)
               {   
                   var jsonRezultat=xmlhttp.responseText;                              
                   var rezultat="";   
                   //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
                   //if(jsonRezultat.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                   //if(jsonRezultat.replace(/\r\n|\r|\n/, "")=='false'){                
                   if(jsonRezultat=='\nfalse'){                
                       rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                       ukupnoZapisa=0;
                       brojStrana=0;
                   }else  {        
                       //alert(jsonRezultat);
                       var json=JSON.parse(jsonRezultat); 
                       //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                       ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                       var arrRezultat = json.ankete;                                   
                       rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                           "<thead>"+
                           "<tr>"+
                           "<th scope='col'>Naziv</th>"+
                           "<th scope='col'>Dat. počekta</th>"+
                           "<th scope='col'>Dat. isteka</th>"+
                           "<th scope='col'>Personalizovana</th>"+
                           "<th scope='col'>Broj pitanja</th>"+                                                   
                           "<th scope='col'></th>"+ 
                           "</tr>"+
                           "</thead>"+
                           "<tbody>";                        
                       if(arrRezultat.length>=1){ 
                           for(i=0; i<arrRezultat.length; i++){                            
                               rezultat+="<tr id='tr"+arrRezultat[i].idAnketa+"'>"+
                                   "<td>"+arrRezultat[i].naziv+"</td>"+
                                   "<td>"+arrRezultat[i].datumPocetka+"</td>"+
                                   "<td>"+arrRezultat[i].datumZavrsetka+"</td>"+
                                   "<td>"+(arrRezultat[i].isPersonalizovana==1?'Da':'Ne')+"</td>"+
                                   "<td>"+arrRezultat[i].ukupnoPitanja+"</td>";
                               if(arrRezultat[i].isPopunjen==0){
                                   rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrRezultat[i].idAnketa+"'>Nastavi</a></td>";
                               }else if(arrRezultat[i].isPopunjen==1){
                                   rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrRezultat[i].idAnketa+"'>Popunjena</a></td>";                           
                               }else {
                                   rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrRezultat[i].idAnketa+"'>Popuni anketu</a></td>";
                               } 
                               rezultat+="</tr>";
                           }//for                    
                       } else {                                                                                                                                       
                           rezultat+="<tr id='tr"+arrRezultat.idAnketa+"'>"+
                               "<td>"+arrRezultat.naziv+"</td>"+
                               "<td>"+arrRezultat.datumPocetka+"</td>"+
                               "<td>"+arrRezultat.datumZavrsetka+"</td>"+
                               "<td>"+(arrRezultat.isPersonalizovana==1?'Da':'Ne')+"</td>"+
                               "<td>"+arrRezultat.ukupnoPitanja+"</td>";
                           if(arrRezultat.isPopunjen==0){
                               rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrRezultat.idAnketa+"'>Nastavi</a></td>";
                           }else if(arrRezultat.isPopunjen==1){
                               rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrRezultat.idAnketa+"'>Popunjena</a></td>";
                           }else {
                               rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrRezultat.idAnketa+"'>Popuni anketu</a></td>";
                           }                                                      
                           rezultat+="</tr>";                           
                           /*
                        if (isset($a['isPopunjen']) && $a['isPopunjen'] == 0) {
                            echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Nastavi</a></td>";
                        } else if (!isset($a['isPopunjen'])) {
                            echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa={$a['idAnketa']}'>Popuni anketu</a></td>";
                        } else {
                            echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}'>Popunjena</a></td>";
                        }
                            */ 
                       }
                       rezultat+="</toby></table>";
                   }                                                                                                                                                    
                    
                   if(ukupnoZapisa>=1){
                       document.getElementById('sadrzaj').innerHTML=rezultat;                             
                       brojStrana=Math.ceil(ukupnoZapisa / limit);
                       odstampajPaginaciju(strana);
                   }else {
                       //document.getElementById('sadrzaj').innerHTML="";
                       document.getElementById('sadrzaj').innerHTML=rezultat;
                       odstampajPaginaciju(0);
                   }
               }//if
           }//onreadystatechange
        
           //uzima izbranu vrstu sortiranja
           var e = document.getElementById("orderby");
           var orderby = e.options[e.selectedIndex].value; 
           //limt prikaza po strani
           e = document.getElementById("limit");
           limit = e.options[e.selectedIndex].value;
        
           var kljucnaRec=document.frmPretraga.kljucna_rec.value;
    
           //xmlhttp.open("GET","index.php?p=2&ajax&str="+strana+"&brZapisa="+brZapisa+"&orderby="+orderby,true);
           xmlhttp.open("GET","index.php?p=2&ajaxJson&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby, true);    
    
           //xmlhttp.open("GET","index.php?p=2&orderby="+orderBy,true);    
           xmlhttp.send();
       }//getActiveAnketeInfoAjaxJson

       //Vraća sortiranje aktivne ankete
       function getActiveAnketeInfoAjax(strana, brStrana){       
           var xmlhttp;
           if (window.XMLHttpRequest)
           {// code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp=new XMLHttpRequest();
           }
           else
           {// code for IE6, IE5
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function()
           {
               if (xmlhttp.readyState==4 && xmlhttp.status==200)
               {   
                   //alert(xmlhttp.responseText);                       
                   //var tabela=document.getElementById('active_ankete');            
                   //divSadrzaj.removeChild(tabela);

                   document.getElementById('sadrzaj').innerHTML=xmlhttp.responseText;
                        
                   promeniStranu(strana, brStrana);            
               }
           }
        
           //uzima izbranu vrstu sortiranja
           var e = document.getElementById("orderby");
           var orderby = e.options[e.selectedIndex].value; 
           //limt prikaza po strani
           e = document.getElementById("limit");
           var limit = e.options[e.selectedIndex].value;
        
           var kljucnaRec=document.frmPretraga.kljucna_rec.value;
    
           //xmlhttp.open("GET","index.php?p=2&ajax&str="+strana+"&brZapisa="+brZapisa+"&orderby="+orderby,true);
           xmlhttp.open("GET","index.php?p=2&ajax&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby,true);
        
           //xmlhttp.open("GET","index.php?p=2&orderby="+orderBy,true);    
           xmlhttp.send();
       }//getActiveAnketeInfoAjax

       function pronadjiAnketu(kljucnaRec) {        
           var xmlhttp;
           if (window.XMLHttpRequest)
           {// code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp=new XMLHttpRequest();
           }
           else
           {// code for IE6, IE5
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function()
           {                
               var jsonAnkete=xmlhttp.responseText;                              
               var rezultat="";
               // alert(jsonRezultat);         
               //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
               //if(jsonRezultat.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
               if(jsonAnkete.replace(/\r\n|\r|\n/, "")=='false' && kljucnaRec.value.length>=1){                
                   rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>"                  
               }else if(kljucnaRec.value.length>=1) {
                   var arrAnkete = JSON.parse(jsonAnkete);                                    
                   rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 650px;'>"+
                       "<thead>"+
                       "<tr>"+
                       "<th scope='col'>Naziv</th>"+
                       "<th scope='col'>Dat. počekta</th>"+
                       "<th scope='col'>Dat. isteka</th>"+
                       "<th scope='col'>Broj pitanja</th>"+                                                   
                       "<th scope='col'></th>"+ 
                       "</tr>"+
                       "</thead>"+
                       "</tbody>";                        
                   if(arrAnkete.length>=1){
                       /*for(i=0; i<arrRezultat.length; i++){
                            rezultat+="<a href=''>"+arrKorisnici[i].firstName+" "+arrKorisnici[i].lastName+"</a><br/>";
                        }*/         
                       //var ispitanik=getVrednostCelije(0, 'tblIspitanik');
                       var ispitanik=null;
                       for(i=0; i<arrAnkete.length; i++){                            
                           rezultat+="<tr id='tr"+arrAnkete[i].idAnketa+"'>"+
                               "<td>"+arrAnkete[i].naziv+"</td>"+
                               "<td>"+arrAnkete[i].datumPocetka+"</td>"+
                               "<td>"+arrAnkete[i].datumZavrsetka+"</td>"+
                               "<td>"+arrAnkete[i].ukupnoPitanja+"</td>";
                           if(arrAnkete[i].isPopunjen==0){
                               rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrAnkete[i].idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Nastavi</a></td>";
                           }else if(arrAnkete[i].isPopunjen==1){
                               rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete[i].idAnketa+"'>Popunjena</a></td>";
                               //mora ovakva porvera, jer ako ispitanik nije popunjavao anketu, ili nije izabran ispitanik, arrRezultat[i].isPopunjen vratiće undefined
                           }else if(ispitanik!=null) {
                               rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrAnkete[i].idAnketa+"&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"'>Popuni anketu</a></td>";
                           }                            
                           //rezultat+="<td><a href='javascript: void(0)' onclick='copy(this)'>Izaberi</a></td>"+ 
                           rezultat+="</tr>";
                       }//for
                       //"<td><a href='#' onclick='izaberiKorisnika('', '', '')'>Izaberi</a></td>"+
                   } else {                                                                                                                                       
                       rezultat+="<tr id='tr"+arrAnkete.idAnketa+"'>"+
                           "<td>"+arrAnkete.naziv+"</td>"+
                           "<td>"+arrAnkete.datumPocetka+"</td>"+
                           "<td>"+arrAnkete.datumZavrsetka+"</td>"+
                           "<td>"+arrAnkete.ukupnoPitanja+"</td>";
                       if(arrAnkete.isPopunjen==0){
                           rezultat+="<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa="+arrAnkete.idAnketa+"'>Nastavi</a></td>";
                       }else if(arrAnkete.isPopunjen==1){
                           rezultat+="<td><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete.idAnketa+"'>Popunjena</a></td>";
                       }else {
                           rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa="+arrAnkete.idAnketa+"'>Popuni anketu</a></td>";
                       }                                                                                                      
                       /*
                                    if (isset($a['isPopunjen']) && $a['isPopunjen'] == 0) {
                                echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Nastavi</a></td>";
                            } else if (!isset($a['isPopunjen'])) {
                                echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa={$a['idAnketa']}'>Popuni anketu</a></td>";
                            } else {
                                echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}'>Popunjena</a></td>";
                            }
                        */
                       rezultat+="</tr>"+
                           "</toby></table>";                       
                       // rezultat="<a href=''>"+arrRezultat.firstName+" "+arrKorisnici.lastName+"</a><br/>";  
                       //}else if(jsonRezultat.toString()=='false' && kljucnaRec.value.length>1){              
                   }
               }                                                                                                                                                    
                          
               document.getElementById('sadrzaj').innerHTML=rezultat;      
           }
           //izbor pretrage  
           //filter pretrage 1 je naziv ankete
           xmlhttp.open("GET","index.php?p=54&filterPretrage=1&kljucna_rec="+kljucnaRec.value);                            
           //xmlhttp.open("GET","index.php?p=54&ispitanik="+getVrednostCelije(0, 'tblIspitanik')+"&filterPretrage="+document.frmPretraga.filterPretrage.value+"&kljucna_rec="+kljucnaRec.value);                        
                
           xmlhttp.send();

       }//pronadjiAnketu

</script>
<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 750px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";   
    if (isset($ankete)) {
        ?>    
        <div class="naslov">Aktuelne ankete</div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:  0px dotted grey; width: 99%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; margin: 5px 5px 0px 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: right; border: 0px solid #bdf;">Sortiraj po:<br/> 
                    <select id="orderby" name="orderby" onchange="promeniStranu(0)" style="font-size: 12px;">
            <!--<select onchange="sorttable.innerSortFunction.apply(document.getElementById(this.value), [])">-->
                        <!-- trenutno odustao od jQuery sortiranja
                        <select id="trigger-link">-->
                        <option value="nazivAsc" <?php echo ($orderby === 'nazivAsc' ? 'selected' : ''); ?>>Nazivu: A - Z</option>
                        <option value="nazivDesc" <?php echo ($orderby === 'nazivDesc' ? 'selected' : ''); ?>>Nazivu: Z - A</option>
                        <option value="datumPocetkaAsc" <?php echo ($orderby === 'datumPocetkaAsc' ? 'selected' : ''); ?>>Dat. početka: rastuće</option>
                        <option value="datumPocetkaDesc" <?php echo ($orderby === 'datumPocetkaDesc' ? 'selected' : ''); ?>>Dat. početka: opadajuće</option>
                        <option value="datumIstekaAsc" <?php echo ($orderby === 'datumIstekaAsc' ? 'selected' : ''); ?>>Dat. isteka: rastuće</option>
                        <option value="datumIstekaDesc" <?php echo ($orderby === 'datumIstekaDesc' ? 'selected' : ''); ?>>Dat. isteka: opadajuće</option>
                    </select>
                </div>

                <div style="float: left; border: 0px solid #bdf;">
                    <form name="frmPretraga">
                        Pretraga po nazivu ankete:<br/>
                        <input value="" onkeyup="promeniStranu(0)" type="text" name="kljucna_rec" id="pretraga" style="width:137px;">
                    </form>
                </div>
            </div>


            <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%;">  
               <!-- <div style="border: 0px solid red; float: left; width: 30%; height: 27px; line-height: 27px;">
                    <span>Prikaži: </span>
                    <select id="limit0" onchange="promeniStranu(0)">
                        <option value="2">2</option>
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>                        
                    </select>
                    po strani
                </div>
                -->
                <?php
                include_once 'modules/mod_paginacija/paginacija_gore.php';
                ?>
            </div>                                

        </div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%">            
            <table class="hor-minimalist-b0" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope='col'>Naziv</th>
                        <th scope='col'>Dat. početka</th>
                        <th scope='col'>Dat. isteka</th>
                        <th scope='col'>Personalizovana</th>
                        <th scope='col'>Broj pitanja</th> 
                        <!-- ne trebaju mi strelice na linku, tako da je dodata nepostojeća slika> -->
                        <th style="background-image: none;"> </th>
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (count($ankete) != count($ankete, 1)) {
                        foreach ($ankete as $a) {
                            echo "<tr>";
                            echo "<td>{$a['naziv']}</td>";
                            echo "<td>" . substr($a['datumPocetka'], 0, 10) . "</td>";
                            echo "<td>" . substr($a['datumZavrsetka'], 0, 10) . "</td>";
                            //echo "<td>{$a['datumZavrsetka']}</td>";                            
                            echo "<td>" . ($a['isPersonalizovana'] == 1 ? 'Da' : 'Ne') . "</td>";
                            echo "<td>{$a['ukupnoPitanja']}</td>";
                            //provera isPopunjen se radi, zato što, ako nismo prijavljeni, promenljiva ne postoji
                            if (isset($a['isPopunjen']) && $a['isPopunjen'] == 0) {
                                echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&anketa={$a['idAnketa']}'>Nastavi</a></td>";
                            } else if (!isset($a['isPopunjen'])) {
                                echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&amp;anketa={$a['idAnketa']}'>Popuni anketu</a></td>";
                            } else {
                                echo "<td><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}'>Popunjena</a></td>";
                            }
                            //echo "<td><a href='edit_anketa.php?idAnketa={$a['idAnketa']}'>izmeni</a></td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                        echo "<td>{$ankete['naziv']}</td>";
                        echo "<td>" . substr($ankete['datumPocetka'], 0, 10) . "</td>";
                        echo "<td>" . substr($ankete['datumZavrsetka'], 0, 10) . "</td>";
                        //echo "<td>{$a['datumZavrsetka']}</td>";
                        echo "<td>" . ($ankete['isPersonalizovana'] == 1 ? 'Da' : 'Ne') . "</td>";
                        echo "<td>{$ankete['ukupnoPitanja']}</td>";
                        if (isset($ankete['isPopunjen']) && $ankete['isPopunjen'] == 0) {
                            echo "<td><a style='color:red; font-weight:700;' href='index.php?p=4&amp;anketa={$ankete['idAnketa']}'>Nastavi</a></td>";
                        } else if (!isset($ankete['isPopunjen'])) {
                            ?><td>
                            <a style='color:green; font-weight:700;' href="index.php?p=4&amp;anketa=<?php echo $ankete['idAnketa'] ?>">Popuni anketu</a>
                        </td>
                        <?php
                        // echo "<td><a style='color:green; font-weight:700;' href='javascript:void(0)' onclick='index.php?p=4&anketa={$ankete['idAnketa']}'>Popuni anketu</a></td>";
                        //echo "<td><a style='color:green; font-weight:700;' href='index.php?p=4&anketa={$ankete['idAnketa']}'>Popuni anketu</a></td>";
                    } else {
                        echo "<td><a style='font-weight:700;' href='index.php?p=5&amp;anketa={$ankete['idAnketa']}'>Popunjena</a></td>";
                    }
                    //echo "<td><a href='edit_anketa.php?idAnketa={$a['idAnketa']}'>izmeni</a></td>";
                    echo "</tr>";
                }
                ?>            
                </tbody>
            </table>
        </div>       

        <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">

            <?php
            include_once 'modules/mod_paginacija/paginacija_dole.php';
            ?>

            <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
                Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> anketa
            </div>
        </div>
        <div style="border: 0px solid red; float: right; font-size: 10px;">                          
          <!--  <div class="dropdown">
                <p>
                    <span>Prikaži:</span>
                    <select style="font-size: 12px;" id="limit" onchange="promeniStranu(0)">
                        <option value="2">2</option>
                        <option value="5" selected>5 po strani</option>
                        <option value="10">10 po strani</option>
                        <option value="20">20 po strani</option>                        
                    </select>            
                <p/>
            </div> 
          -->
            <div style="border: 0px solid red; float: right; margin-top: 10px;">
                <span>Prikaži: </span>
                <select id="limit" onchange="promeniStranu(0)">
                    <option value="2" <?php if($limit==2) echo 'selected'; ?>>2</option>
                    <option value="5" <?php if($limit==5) echo 'selected'; ?>>5</option>
                    <option value="10" <?php if($limit==10) echo 'selected'; ?>>10</option>
                    <option value="20" <?php if($limit==20) echo 'selected'; ?>>20</option>                        
                </select>
               po strani
            </div>
        </div>

    <?php }
    ?>
</div>