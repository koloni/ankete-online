
<?php if (isset($message) && message != "") { ?>
    <div id='message' style='width: 100%; height: auto; color: green; font-weight: bold;'>
        <?php echo $message; ?>
    </div>
<?php } ?>


<?php if (isset($error) && $error != "") { ?>
    <div id='error' style='width: 100%; height: auto; color: red; font-weight: bold;'>    
        <?php echo $error; ?>
    </div>
<?php } ?>



<?php if (isset($pitanje)) { ?>
    <form action="" method="POST">
        <div class = 'pitanje' id = 'pitanje'>
            <!--<div style = "border: 1px solid black; height:auto;">-->
            <div class = 'kolona'>
                <div>Tip pitanja: </div>
                <div>Nalov pitanja: </div>
                <?php
                if ($pitanje instanceof PitanjeMatricaRadio || $pitanje instanceof PitanjeMatricaCheckBox || $pitanje instanceof PitanjeSlobodanUnos) {
                    echo "<div>Potpitanja: </div>";
                }
                ?>
                <!-- <div>Ogovori: </div>-->
            </div>

            <div class = 'kolona'>       

                <input name='idPitanje' type='hidden' value='<?php echo $pitanje->getIdPitanje(); ?>'>
                <div> <select disabled='true' name = 'tipOgovora' value='<?php echo $pitanje->getIdVrstaOdgovora(); ?>'> <option>izbor više odgovora</option></select></div>            
                <div><input name='tekst' type = 'text' size = '30' value='<?php echo $pitanje->getTekst(); ?>'/></div>

                <?php
                if ($pitanje instanceof PitanjeMatricaRadio || $pitanje instanceof PitanjeMatricaCheckBox || $pitanje instanceof PitanjeSlobodanUnos) {
                    if ($pitanje instanceof PitanjeSlobodanUnos) {
                        $potpitanja = $pitanje->getPotpitanja();
                    } else {
                        $kolekcijaPotpitanjaMatrica = $pitanje->getKolekcijaPotpitanjaMatrica();
                        $potpitanja = $kolekcijaPotpitanjaMatrica->getPotpitanja();
                    }

                    foreach ($potpitanja as $pot) {
                        ?>
                        <div><input type = 'text' name = 'potpitanja[]' value='<?php echo $pot->getTekst(); ?>'><a href = '#' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                        <?php
                    }
                    ?>
                    <div><a href = "#" onclick = "addPotpitanje(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj potpitanje</a></div>

                    <?php
                }
                ?>

                      
                <?php foreach ($pitanje->getOdgovori() as $odg) { ?>
                          <div><input type = 'text' name = 'odgovori[]' value='<?php echo $odg->getTekst(); ?>'><a href = '#' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                <?php } ?>

        <!--<div><input type = 'text' name = 'odgovori[]' value='$odg'><a href = '#' onclick = "removeOdgovor(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>-->
                <div><a href = "#" onclick = "addOdgovor(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj odgovor</a></div>
                <!--<div><button onclick = 'addOdgovor(this.parentNode);'>dodaj odgovor</button></div>-->
                <div>
                    <input style = "float:left;" type = 'submit' onclick = '' value = 'Sačuvaj izmene'/>
                    <input style = "float:left;" type = 'button' onclick = '' value = 'Odustani'/>
                </div>

            </div>
            <!--</div>-->
            <!--
            <div style = "width: 380px; float: left;border: 1px solid red;">
            <div style = "margin: 0 auto;">
            <input style = "float:left;width: 100px;" type = 'button' onclick = '' value = 'Sačuvaj'/>
            <input style = "float:right;width: 100px;" type = 'button' onclick = '' value = 'Obriši'/>
            </div>
            </div>
            -->
        </div>

        <div class = 'pitanje' id = 'pitanje'>
            <!--<div style = "border: 1px solid black; height:auto;">-->
            <div class = 'kolona'>
                <div>Ogovori: </div>
            </div>

            <div class = 'kolona'>  
                <?php
                foreach ($pitanje->getOdgovori() as $odg) {
                    ?>
                    <div><input type = 'text' name = 'odgovori[]' value='<?php echo $odg->getTekst(); ?>'><a href = '#' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                <?php }
                ?>
                <div><a href = "#" onclick = "addOdgovor(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj odgovor</a></div>
                <!--<div><button onclick = 'addOdgovor(this.parentNode);'>dodaj odgovor</button></div>-->
                <div>
                    <input style = "float:left;" type = 'submit' onclick = '' value = 'Sačuvaj izmene'/>
                    <input style = "float:left;" type = 'button' onclick = '' value = 'Odustani'/>
                </div>
            </div>
        </div>

    </form>
<?php }
?>