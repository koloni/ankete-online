<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmEditPitanje").validate({
            rules: {                
                tekst:{
                    required: true,                    
                    minlength: 3,
                    maxlength: 200                    
                },
                'odgovori[]': {
                    required: true,                    
                    minlength: 1,
                    maxlength: 100
                },            
                'potpitanja[]': {
                    required: true,                    
                    minlength: 1,
                    maxlength: 100
                
                }
            },
            messages: {
                tekst:{
                    required: "Tekst pitanja je obavezan!",                    
                    minlength: $.format("Naslov pitanja mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")                    
                },
                'odgovori[]': {
                    required: "Svi <b>odgovori</b> moraju biti popunjeni!",                    
                    minlength: $.format("Odgovor mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                'potpitanja[]': {
                    required: "Sva <b>potpitanja</b> moraju biti popunjena!",                    
                    minlength: $.format("Potpitanje mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                }
            }
        });
                
    });
</script>

<?php
include_once MODULES_DIR . "/mod_message/message.php";
if (isset($pitanje)) {
    ?>
    <div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
        <div class="naslov">Izmena pitanja</div>
        <form name="frmEditPitanje" id="frmEditPitanje" action="" method="POST">



            <div class = 'edit_pitanje' id = 'pitanje0'>
                <!--<div style = "border: 1px solid black; height:auto;">-->
                <div class = 'edit_pitanje_kolona_levo'>   
                    <div>Tip pitanja: </div>                    
                </div>

                <div class = 'edit_pitanje_kolona_desno'>                        
                    <div> 
                        <select disabled='true' name = 'tipOgovora' value='<?php echo $pitanje->getIdVrstaOdgovora(); ?>'>
                            <option><?php echo $pitanje->getOpisVrste(); ?></option>
                        </select>
                    </div>                                                          
                </div>
            </div>

            <div class = 'edit_pitanje' id = 'pitanje'>
                <div style="width: 100%;"><label style="padding-left: 220px;" for="tekst" class="error"></label></div>                   
                <div class = 'edit_pitanje_kolona_levo'>                       
                    <div>Tekst pitanja: </div>                                    
                </div>

                <div class = 'edit_pitanje_kolona_desno'>       
                    <input name='idAnketa' type='hidden' value='<?php echo $idAnketa; ?>'>
                    <input name='idPitanje' type='hidden' value='<?php echo $pitanje->getIdPitanje(); ?>'>                                       
                    <div><input style='width:380px' name='tekst' type = 'text'value='<?php echo $pitanje->getTekst(); ?>'/></div>                                               
                </div>
            </div>

            <?php if ($pitanje instanceof PitanjeMatricaRadio2 || $pitanje instanceof PitanjeMatricaCheckBox2 || $pitanje instanceof PitanjeSlobodanUnos) { ?>
                <div class ='edit_pitanje' id='potpitanja'>
                    <div style="width: 100%;"><label style="padding-left: 220px;" for="potpitanja[]" class="error"></label></div>            
                    <div class="edit_pitanje_kolona_levo">
                        <div>Potpitanja: </div>
                    </div>

                    <div class="edit_pitanje_kolona_desno">
                        <?php
                        if ($pitanje instanceof PitanjeSlobodanUnos) {
                            $potpitanja = $pitanje->getPotpitanja();
                        } else {
                            //$potpitanja = $pitanje->getKolekcijaPotpitanjaMatrica();
                            $potpitanja = $pitanje->getPotpitanja();
                        }

                        foreach ($potpitanja as $pot) {
                            ?>
                            <div><input style='width:350px' type = 'text' name = 'potpitanja[]' value='<?php echo $pot->getTekst(); ?>'><a href="javascript:void(0);" onclick = 'removeOdgovor(this.parentNode)'><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                            <?php
                        }
                        ?>
                        <div><a href='javascript:void(0);' onclick = "addPotpitanje(this.parentNode)"><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj potpitanje</a></div>
                    </div>
                </div>
            <?php } ?>


            <div class = 'edit_pitanje' id = 'odgovori'>
                <div style="width: 100%;"><label style="padding-left: 220px;" for="odgovori[]" class="error"></label></div>   

                <div class = 'edit_pitanje_kolona_levo'>
                    <div>Odgovori: </div>
                </div>

                <div class = 'edit_pitanje_kolona_desno' style="width: 350px;">  
                    <?php
                    foreach ($pitanje->getOdgovori() as $odg) {
                        ?>
                        <div>
                            <input style="width: 85%;" type = 'text' name = 'odgovori[]' value='<?php echo $odg->getTekst(); ?>'>
                            <a href="javascript:void(0);" onclick = 'removeOdgovor(this.parentNode)' title="Izbriši odgovor"><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a>
                        </div>
                    <?php }
                    ?>
                    <div><a href="javascript:void(0);" onclick = "addOdgovor(this.parentNode)"><img src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj novi odgovor</a></div>

                    <!--
                    <div style="padding-top: 10px;">
                        <input style = "float:left;" type = 'submit' onclick = '' value = 'Sačuvaj izmene'/>                            
                        <button onclick='javascript:history.back();'>Odustani</button>                   
                    </div>
                    -->
                </div>
            </div>  


            <?php
            if ($pitanje instanceof PitanjeCheckBox || $pitanje instanceof PitanjeRadio) {
               /* $cheched = "";
                $display = "none";
                
                */
                ?>
                <div class = 'edit_pitanje' id = 'nesto_drugo' style='display: block;'>            
                    <div class="edit_pitanje_kolona_levo"></div>


                    <div class="edit_pitanje_kolona_desno">           
                        <?php
                        if ($pitanje instanceof PitanjeCheckBoxWithNestoDrugo || $pitanje instanceof PitanjeRadioWithNestoDrugo) {
                            $cheched = "checked";
                            $display = "block";
                            ?>
                            <input <?php echo $cheched; ?> type="checkbox" name="nestoDrugo" value="1" id="nestoDrugo" onchange="document.getElementById('nestoDrugoTekst').style.display=='none'?document.getElementById('nestoDrugoTekst').style.display='block':document.getElementById('nestoDrugoTekst').style.display='none';"/><label for="nestoDrugo">Dozvoli jedan slobodan unos</label>                                   
                            <div id="nestoDrugoTekst" style="padding:0 0 20px 20px; display: <?php echo $display; ?>;">Tekst labele slobodnog unosa:
                                                                                                                     
                                       <select onchange='' name = 'idOdgovorNestoDrugo' >                                     
                                    <?php
                                     //echo "<option value='{$odgovorNestoDrugo[0]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[0]['tekst']}</option>";
                                    for ($i=0; $i<count($odgovorNestoDrugo); $i++) {
                                        if($odgovorNestoDrugo[$i]['idOdgovorNestoDrugo']== $pitanje->getNestoDrugo()->getIdOdgovor()){
                                            echo "<option selected value='{$odgovorNestoDrugo[$i]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[$i]['tekst']}</option>";    
                                        }else{
                                            echo "<option value='{$odgovorNestoDrugo[$i]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[$i]['tekst']}</option>";
                                        }
                                    }
                                    ?>               
                                </select><br/>  
               
                               
                            
                            </div>
                        <?php } else { ?>
                            <input type="checkbox" name="nestoDrugo" value="1" id="nestoDrugo" onchange="document.getElementById('nestoDrugoTekst').style.display=='none'?document.getElementById('nestoDrugoTekst').style.display='block':document.getElementById('nestoDrugoTekst').style.display='none';"/><label for="nestoDrugo">Dozvoli jedan slobodan unos</label>                                   
                            <div id="nestoDrugoTekst" style="padding:0 0 20px 20px; display: none">Tekst slobodnig unosa: (podrazumevani tekst "Nešto drugo:")<br/>
                                <input style="width: 250px;"type="text" name="nestoDrugoTekst" value="">   
                                
                                       <select onchange='' name = 'idOdgovorNestoDrugo' >                                     
                                    <?php
                                     echo "<option value='{$odgovorNestoDrugo[0]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[0]['tekst']}</option>";
                                    for ($i=1; $i<count($odgovorNestoDrugo); $i++) {
                                        echo "<option value='{$odgovorNestoDrugo[$i]['idOdgovorNestoDrugo']}'>{$odgovorNestoDrugo[$i]['tekst']}</option>";
                                    }
                                    ?>               
                                </select><br/>  
                                
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <div class = 'edit_pitanje' id = 'obavezno'>
                <div class = 'edit_pitanje_kolona_levo'></div>

                <div class = 'edit_pitanje_kolona_desno'>  
                    <input type="checkbox" name="isObavezno" value="1" id="isObavezno" <?php echo ($pitanje->getIsObavezno() ? 'checked' : ''); ?>/><label for="isObavezno">Obavezan odgovor</label>
                </div>
            </div>

            <div class = 'edit_pitanje' id = 'kontrole'>
                <div class="edit_pitanje_kolona_levo">           
                </div>
                <div class="edit_pitanje_kolona_desno">
                    <input style="width: 135px; height: 27px; margin-right: 10px;" type = 'submit' value = 'Sačuvaj izmene'/>                     
                    <button style="width: 125px; height: 27px;" onclick='javascript:history.back();'>Odustani</button>
                    <!--<input style = "float:left;" type = 'button' onclick = '' value = 'Odustani'/>-->
                </div>
            </div>
        </form>
    </div>
<?php }
?>
