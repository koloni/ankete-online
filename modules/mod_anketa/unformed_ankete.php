<?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
<?php
if (isset($ankete) && is_array($ankete)) {
    ?>
    <div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
        <?php //include MODULES_DIR . '/mod_menu/menu_dubina.php'; ?>
        <div class="naslov">Uređivanje anketa</div>
        <table class="hor-minimalist2-b" summary="Nedovrsene ankete">
            <thead>     
                <tr>
                    <th scope='col'>Naziv</th>
                    <th scope='col'>Dat. početka</th>
                    <th scope='col'>Dat. isteka</th>
                    <th scope='col'>Personalizovana</th>
                    <th scope='col'>Ukupno pitanja</th>
                    <!--<th scope='col'>Redosled pitanja</th>                    -->
                    <th scope='col'>Izmeni</th>                
                </tr>        
            </thead>
            <tbody>            
                <?php
                if (count($ankete) != count($ankete, 1)) {
                    foreach ($ankete as $a) {
                        ?>
                        <tr>
                            <td><?php echo $a['naziv'] ?></td>
                            <td><?php echo substr($a['datumPocetka'], 0, 10); ?></td>
                            <td><?php echo substr($a['datumZavrsetka'], 0, 10); ?></td>                  
                            <td><?php echo $a['isPersonalizovana'] == 0 ? 'Ne' : 'Da'; ?></td>
                            <?php
                            if ($a['ukupnoPitanja'] == 0) {
                                echo "<td style='color: red;'>{$a['ukupnoPitanja']}</td>";
                            } else {
                                echo "<td>{$a['ukupnoPitanja']}</td>";
                            }
                            ?>                                                                       
                            <!--<td><?php //echo $a['redosledPitanjaOpis'];    ?></td>                        -->
                            <td>
                                <a style="font-size: 12px; color: green; font-weight:700;" href='index.php?p=28&idAnketa=<?php echo $a['idAnketa']; ?>'><img src="images/edit.gif" alt="" title="" border="0" /> Izmeni</a>                                                              

                                     <!--<a style="font-size: 12px; color: green; font-weight:700;" href='index.php?p=28&idAnketa=<?php echo $a['idAnketa']; ?>'>Izmeni</a>   -->
                                <!--
                                <button class="btnKontrole" onclick="location.href='index.php?p=28&idAnketa=<?php echo $a['idAnketa']; ?>'">Izmeni</button>
                                <button class="btnKontrole" onclick="location.href='index.php?p=25&idAnketa=<?php echo $a['idAnketa']; ?>'">Predaj</button>
                                <button class="btnKontrole" onclick="location.href='index.php?p=25&idAnketa=<?php echo $a['idAnketa']; ?>'">Obriši</button>
                                -->
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td><?php echo $ankete['naziv']; ?></td>
                        <td><?php echo $ankete['datumPocetka']; ?></td>
                        <td><?php echo $ankete['datumZavrsetka']; ?></td>                  
                        <td><?php echo $ankete['isPersonalizovana'] == 0 ? 'Ne' : 'Da'; ?></td>
                        <?php
                        if ($ankete['ukupnoPitanja'] == 0) {
                            echo "<td style='color: red;'>{$ankete['ukupnoPitanja']}</td>";
                        } else {
                            echo "<td>{$ankete['ukupnoPitanja']}</td>";
                        }
                        ?>                                             
                       <!-- <td><?php //echo $ankete['redosledPitanjaOpis'];    ?></td>                                                -->
                        <td>
                            <a style="font-size: 12px; color: green; font-weight:700;" href='index.php?p=28&idAnketa=<?php echo $ankete['idAnketa']; ?>'>Izmeni</a>
                            <!-- <a href='#'>Predaj</a>                                
                             <a href='#'>Obriši</a> 
                            -->
                             <!--<button class="btnKontrole" onclick="location.href='index.php?p=28&idAnketa=<?php echo $ankete['idAnketa']; ?>'">Izmeni</button>
                             <button class="btnKontrole" onclick="location.href='index.php?p=25&idAnketa=<?php echo $ankete['idAnketa']; ?>'">Predaj</button>
                             <button class="btnKontrole" onclick="location.href='index.php?p=25&idAnketa=<?php echo $ankete['idAnketa']; ?>'">Obriši</button>
                            -->
                        </td>                        
                    </tr>
                <?php }
                ?>         
            </tbody>
        </table>
    </div>
<?php } ?>
