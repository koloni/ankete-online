<div class="naslov">Izmena pitanja</div>
<?php
include_once MODULES_DIR . "/mod_message/message.php";

if (isset($pitanje)) {
    ?>
    <div style="margin: 0 auto; border: 0px solid red; display: table;">
        <form action="" method="POST">
            <div class = 'edit_pitanje' id = 'pitanje'>
                <!--<div style = "border: 1px solid black; height:auto;">-->
                <div class = 'kolona'>
                    <div>Tip pitanja: </div>
                    <div>Nalov pitanja: </div>
                    <div>Obavezno: </div>
                    <?php
                    //ako postoje potpitanja 
                    if ($pitanje instanceof PitanjeMatricaRadio || $pitanje instanceof PitanjeMatricaCheckBox || $pitanje instanceof PitanjeSlobodanUnos) {
                        echo "<div>Potpitanja: </div>";
                    }
                    ?>
                    <!-- <div>Ogovori: </div>-->
                </div>

                <div class = 'kolona'>       
                    <input name='idAnketa' type='hidden' value='<?php echo $idAnketa; ?>'>
                    <input name='idPitanje' type='hidden' value='<?php echo $pitanje->getIdPitanje(); ?>'>
                    <div> <select disabled='true' name = 'tipOgovora' value='<?php echo $pitanje->getIdVrstaOdgovora(); ?>'><option><?php echo $pitanje->getOpisVrste(); ?></option></select></div>            
                    <div><input style='width:420px' name='tekst' type = 'text'value='<?php echo $pitanje->getTekst(); ?>'/></div>
                    <div> 
                        <select name = 'isObavezno'> 
                            <option value='1' <?php echo ($pitanje->getIsObavezno() ? 'selected' : ''); ?> >Da</option>
                            <option value='0' <?php echo ($pitanje->getIsObavezno() ? '' : 'selected'); ?>>Ne</option>
                        </select>
                    </div>                              

                    <?php
                    if ($pitanje instanceof PitanjeMatricaRadio || $pitanje instanceof PitanjeMatricaCheckBox || $pitanje instanceof PitanjeSlobodanUnos) {
                        if ($pitanje instanceof PitanjeSlobodanUnos) {
                            $potpitanja = $pitanje->getPotpitanja();
                        } else {
                            $potpitanja = $pitanje->getKolekcijaPotpitanjaMatrica()->getPotpitanja();
                        }

                        foreach ($potpitanja as $pot) {
                            ?>
                            <div><input style='width:350px' type = 'text' name = 'potpitanja[]' value='<?php echo $pot->getTekst(); ?>'><a href="javascript:void(0);" onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                            <?php
                        }
                        ?>
                        <div><a href='javascript:void(0);' onclick = "addPotpitanje(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj potpitanje</a></div>

                        <?php
                    }
                    ?>
                </div>

                <div class = 'edit_pitanje' id = 'pitanje'>
                    <!--<div style = "border: 1px solid black; height:auto;">-->
                    <div class = 'kolona'>
                        <div>Ogovori: </div>
                    </div>

                    <div class = 'kolona'>  
                        <?php
                        foreach ($pitanje->getOdgovori() as $odg) {
                            ?>
                            <div><input type = 'text' name = 'odgovori[]' value='<?php echo $odg->getTekst(); ?>'><a href="javascript:void(0);" onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                        <?php }
                        ?>
                        <div><a href="javascript:void(0);" onclick = "addOdgovor(this.parentNode)"><img width = "16px" height = "16px" src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj odgovor</a></div>

                        <div>
                            <input style = "float:left;" type = 'submit' onclick = '' value = 'Sačuvaj izmene'/>                            
                            <button onclick='javascript:history.back();'>Odustani</button>
                            <!--<input style = "float:left;" type = 'button' onclick = '' value = 'Odustani'/>-->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php }
?>