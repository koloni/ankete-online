<div style="display: table; width: 400px; margin: 0px auto; height: auto; border: 0px solid red;">

    <span style="font-size: 18px; font-weight: 700"><?php echo $pitanje['tekst']; ?></span>
    <table style="width: 400px;" border="0px">
        <tbody>
            <tr style="font-size: 13px;">
                <td> </td>
                <td style="text-align: right;">Br. odgovora</td>
                <td style="text-align: right;">Procenat</td>     
            </tr>
            <?php foreach ($odgovori as $odg) {
                ?>
                <tr>
                    <td><?php echo $odg['tekst']; ?></td>
                    <td style="text-align: right;"><?php echo $odg['brOdgovora']; ?></td>
                    <td style="text-align: right;"><?php ?>%</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-bottom:10px;"><progress style="width: 100%; height: 13px;" value="50" max="100"></progress></td>
                </tr>
            <?php } ?>
        </tbody>   
    </table>

</div>