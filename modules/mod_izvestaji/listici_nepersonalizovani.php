<style>

    div.pagination {
        font-size: 10px;
        padding:3px;
        margin:3px;
        text-align:center;
    }

    div.pagination a {
        padding: 2px 5px 2px 5px;
        margin-right: 2px;
        border: 1px solid #ddd;

        text-decoration: none; 
        color: #aaa;
        color: #666;
    }
    div.pagination a:hover, div.pagination a:active {
        padding: 2px 5px 2px 5px;
        margin-right: 2px;
        border: 1px solid #a0a0a0;
    }
    div.pagination span.current {
        padding: 2px 5px 2px 5px;
        margin-right: 2px;
        border: 1px solid #e0e0e0;
        font-weight: bold;
        background-color: #f0f0f0;
        background-color: #e0e0e0;
        color: #aaa;
        color: #666;
    }
    div.pagination span.disabled {
        padding: 2px 5px 2px 5px;
        margin-right: 2px;
        border: 1px solid #f3f3f3;
        color: #ccc;
    }
</style>

<script>

    var trenutna=0;
    var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
    var limit=5;
    function promeniStranu(str, brStrana){ 
        if(brStrana>=1) {    
            trenutna=str;          
            var strane="";
            //prethodna strana
            if(trenutna >= 1) {  
                strane+="<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson("+(trenutna-1)+" ,"+brStrana+")'>&#171; Prethodna</a>"                        
            }else{
                strane+="<span class='disabled'>&#171; Prethodna</span>"
            }   
                    
            for (i = 0; i < brStrana; i++) {
                if (i == str) {
                    strane+="<span class='current'>"+ (i + 1) +"</span>";                                                        
                } else {
                    strane+="<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson("+i+","+brStrana+")'>"+ (i + 1) +"</a>";                                                        
                }
            }
            //sledeća strana
            if (trenutna < brStrana - 1) {                                
                strane+="<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson("+(trenutna+1)+" ,"+brStrana+")' class='next'>Sledeća &#187;</a>";                        
            }else{
                strane+="<span class='disabled'>Sledeća &#187;</span>";
            }   
            strane+="</div>";
                    
            document.getElementById("paginacijaGore").innerHTML=strane;
            document.getElementById("paginacijaDole").innerHTML=strane;                     
        
            var pocetniZapis=trenutna*limit;
            var zapisDo=(pocetniZapis+limit*1);
            if(zapisDo>ukupnoZapisa){
                zapisDo=ukupnoZapisa;
            }
            //alert(limit);
            var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa+" anketnih listića";
            document.getElementById("prikazanoZapisa").innerHTML=prikazanoZapisa;
        }else {
            document.getElementById("paginacijaGore").innerHTML="";
            document.getElementById("paginacijaDole").innerHTML="";                    
            document.getElementById("prikazanoZapisa").innerHTML="";
        }       
    }

    function getListiciInfoAjaxJson(strana, brStrana){ 
        var redniBrojListica=strana*limit+1;
        
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {   
                var jsonAnkete=xmlhttp.responseText;                    
                var rezultat="";   
                //ukalnjam novi red iz rezultata jSona, ne znam zašto ga stampa?
                //if(jsonAnkete.replace(/\r\n|\r|\n/g, "")=='false' && kljucnaRec.value.length>1){
                //if(jsonAnkete.replace(/\r\n|\r|\n/, "")=='false'){                
                if(jsonAnkete=='\nfalse'){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                    ukupnoZapisa=0;
                }else  {        
                    //alert(jsonAnkete);
                    var json=JSON.parse(jsonAnkete); 
                    //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    var arrAnkete = json.ankete;                                   
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Listići</th>"+                   
                        "<th scope='col'>Dat. završetka</th>"+                                                   
                        "<th scope='col'></th>"+ 
                        "</tr>"+
                        "</thead>"+
                        "<tbody>";                        
                    if(arrAnkete.length>=1){ 
                        for(i=0; i<arrAnkete.length; i++){                            
                            rezultat+="<tr id='tr"+arrAnkete[i].idAnketa+"'>"+
                                "<td>Listić " +redniBrojListica+ "</td>"+
                                "<td>"+arrAnkete[i].datumZavrsetka+"</td>";                                                               
                            rezultat+="<td style='text-align:right;'><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete[i].idAnketa+"&amp;listic="+arrAnkete[i].idAnketniListic+"'>Prikaži</a></td>";                                                                                
                            redniBrojListica++;
                        }//for                    
                    } else {                                                                                                                                       
                        rezultat+="<tr id='tr"+arrAnkete.idAnketa+"'>"+
                            "<td>Listić " +redniBrojListica+"</td>"+
                            "<td>"+arrAnkete.datumZavrsetka+"</td>";  
                        rezultat+="<td style='text-align:right;'><a style='font-weight:700;' href='index.php?p=5&anketa="+arrAnkete.idAnketa+"&amp;listic="+arrAnkete.idAnketniListic+"'>Prikaži</a></td>";                                                                                
                    }
                    rezultat+="</tr>"+
                        "</toby></table>";
                }                                                                                                                                                    
                    
                if(ukupnoZapisa>=1){
                    document.getElementById('sadrzaj').innerHTML=rezultat;                             
                    var brStrana = Math.ceil(ukupnoZapisa / limit);
                    promeniStranu(strana, brStrana);            
                }else {
                    //document.getElementById('sadrzaj').innerHTML="";
                    document.getElementById('sadrzaj').innerHTML=rezultat;
                    promeniStranu(0, 0);
                }
            }
        }
        
        //uzima izbranu vrstu sortiranja
        var e = document.getElementById("orderby");
        var orderby = e.options[e.selectedIndex].value; 
        //limt prikaza po strani
        e = document.getElementById("limit");
        limit = e.options[e.selectedIndex].value;
           
        //xmlhttp.open("GET","index.php?p=2&ajax&str="+strana+"&brZapisa="+brZapisa+"&orderby="+orderby,true);
        xmlhttp.open("GET","index.php?p=16&anketa="+<?php echo $idAnketa; ?>+"&ajaxJson&str="+strana+"&limit="+limit+"&filterPretrage=&kljucnaRec=&orderby="+orderby, true);    
    
        //xmlhttp.open("GET","index.php?p=2&orderby="+orderBy,true);    
        xmlhttp.send();
    }//getListiciInfoAjaxJson
</script>
<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 600px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($ankete)) {
        ?>    
        <div class="naslov">Anketni listići</div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:  0px dotted grey; width: 100%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; padding: 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: left; border: 0px solid #bdf;">Sortiraj po:<br/> 
                    <select id="orderby" name="orderby" onchange="getListiciInfoAjaxJson(0<?php echo isset($brStrana) ? "," . $brStrana : ''; ?>)"> 
                        <option value="datumZavrsetkaAsc" <?php echo ($orderby === 'datumZavrsetkaAsc' ? 'selected' : ''); ?>>Dat. završetka: rasuće</option>
                        <option value="datumZavrsetkaDesc" <?php echo ($orderby === 'datumZavrsetkaDesc' ? 'selected' : ''); ?>>Dat. završetka: opadajuće</option>
                    </select>
                </div>

                <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%; height: 23px;">  
                    <div style="float: left; width: 40%">
                        <span>Prikaži: </span>
                        <select id="limit" onchange="getListiciInfoAjaxJson(0<?php echo isset($brStrana) ? "," . $brStrana : ''; ?>)">
                            <option value="2">2</option>
                            <option value="5" selected>5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>                        
                        </select>                    
                    </div>

                    <div id="paginacijaGore" class="pagination" style="float: right;">
                        <?php
                        if (isset($brStrana)) {
                            if ($trenutna >= 1) {
                                echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson(" . ($trenutna - 1) . " ,{$brStrana})'>&#171; Prethodna</a>";
                            } else {
                                echo "<span class='disabled'>&#171; Prethodna</span>";
                            }
                            for ($i = 0; $i < $brStrana; $i++) {
                                if ($i == $trenutna) {
                                    echo "<span class='current'>" . ($i + 1) . "</span>";
                                } else {
                                    echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson({$i}, {$brStrana})'>" . ($i + 1) . "</a>";
                                }
                            }
                            if ($trenutna < $brStrana - 1) {
                                echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson(" . ($trenutna + 1) . " ,{$brStrana})' class='next'>Sledeća &#187;</a>";
                            } else {
                                echo "<span class='disabled'>Sledeća &#187;</span>";
                            }
                        }
                        ?>
                    </div><!--  paginacijaGore -->
                </div>
            </div>

            <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

            <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%;">            
                <table class="hor-minimalist-b0" style="width: 100%; margin: 0 auto;">
                    <thead>
                        <tr>
                            <th scope='col'>Listići</th>
                            <th scope='col'>Dat. završetka</th>                                                
                            <th scope='col'></th>
                        </tr>
                    </thead>
                    <tbody>            
                        <?php
                        if (count($ankete) != count($ankete, 1)) {
                            $i=1;
                            foreach ($ankete as $a) {
                                echo "<tr>";
                                echo "<td>Listić {$i}</td>";
                                echo "<td>{$a['datumZavrsetka']}</td>";
                                echo"<td style='text-align:right;'><a style='font-weight:700;' href='index.php?p=5&anketa={$a['idAnketa']}&listic={$a['idAnketniListic']}'>Prikaži</a></td>";
                                echo "</tr>";
                                $i++;
                            }
                        } else {
                            echo "<tr>";
                            echo "<td>Listić 1</td>";
                            echo "<td>{$ankete['datumZavrsetka']}</td>";
                            echo"<td style='text-align:right;'><a style='font-weight:700;' href='index.php?p=5&anketa={$ankete['idAnketa']}&listic={$ankete['idAnketniListic']}'>Prikaži</a></td>";
                            echo "</tr>";
                        }
                        ?>            
                    </tbody>
                </table>
            </div>       

            <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">
                <div id="paginacijaDole" class="pagination" style="float: right;">
                    <?php
                    if (isset($brStrana)) {
                        if ($trenutna >= 1) {
                            echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson(" . ($trenutna - 1) . " ,{$brStrana})'>&#171; Prethodna</a>";
                        } else {
                            echo "<span class='disabled'>&#171; Prethodna</span>";
                        }
                        for ($i = 0; $i < $brStrana; $i++) {
                            if ($i == $trenutna) {
                                echo "<span class='current'>" . ($i + 1) . "</span>";
                            } else {
                                echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson({$i}, {$brStrana})'>" . ($i + 1) . "</a>";
                            }
                        }
                        if ($trenutna < $brStrana - 1) {
                            echo "<a href='javascript:void(0)' onclick='getListiciInfoAjaxJson(" . ($trenutna + 1) . " ,{$brStrana})' class='next'>Sledeća &#187;</a>";
                        } else {
                            echo "<span class='disabled'>Sledeća &#187;</span>";
                        }
                    }
                    ?>
                </div><!--  paginacijaDole -->

                <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
                    Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> anketnih listića
                </div>

            </div>

            <?php
        } else {
            echo "Nema anketa";
        }
        ?>
    </div>