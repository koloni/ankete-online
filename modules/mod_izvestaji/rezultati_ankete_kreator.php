
<script>
    var trenutna=0;
    var limit=<?php echo $limit; ?>;
    var ukupnoZapisa=<?php echo $ukupnoZapisa; ?>;
    var brojStrana=<?php echo $brStrana; ?>;
 
    function idiNaStranu(strana) {      
        strana-=1;
        if(strana<0) {
            document.getElementById('idiNaStranu0').value="";
            document.getElementById('idiNaStranu1').value="";
            return;
        }else if(strana>=brojStrana) {
            getAnketeInfoAjaxJson(brojStrana-1);    
            return;
        }        
        getAnketeInfoAjaxJson(strana);    
    }//idiNaStranu
    
    function promeniStranu(strana){       
        getAnketeInfoAjaxJson(strana);
    }//promeniStranu

    function getAnketeInfoAjaxJson(strana){       
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {   
                var jsonRezultat=xmlhttp.responseText;                              
                var rezultat="";                      
                if(jsonRezultat=='\nfalse'){                
                    rezultat="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 5px; font-weight: 600;'>Nema rezultata pretrage!</div>";                                  
                    ukupnoZapisa=0;
                    brojStrana=0;
                }else  {        
                    //alert(jsonRezultat);
                    var json=JSON.parse(jsonRezultat); 
                    //var ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    ukupnoZapisa=json.ukupnoZapisaPretrage;                  
                    var arrRezultat = json.ankete;                                   
                    rezultat="<table border='0px' id='tblRezultatPretrage' class='hor-minimalist-b0' style='width: 100%;'>"+
                        "<thead>"+
                        "<tr>"+
                        "<th scope='col'>Naziv</th>"+
                        "<th scope='col'>Dat. počekta</th>"+
                        "<th scope='col'>Dat. isteka</th>"+
                        "<th scope='col'>Personalizovana</th>"+
                        "<th scope='col'>Broj Ispitanika</th>"+                                                   
                        "<th scope='col'></th>"+ 
                        "<th scope='col'></th>"+
                        "</tr>"+
                        "</thead>"+
                        "<tbody>";                        
                    if(arrRezultat.length>=1){ 
                        for(i=0; i<arrRezultat.length; i++){                            
                            rezultat+="<tr id='tr"+arrRezultat[i].idAnketa+"'>"+
                                "<td>"+arrRezultat[i].naziv+"</td>"+
                                "<td>"+arrRezultat[i].datumPocetka+"</td>"+
                                "<td>"+arrRezultat[i].datumZavrsetka+"</td>"+
                                "<td>"+(arrRezultat[i].isPersonalizovana==1?'Da':'Ne')+"</td>"+
                                "<td>"+arrRezultat[i].brIspitanika+"</td>";                                
                            if (arrRezultat[i].isPersonalizovana == 1) {
                                rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=15&amp;anketa="+arrRezultat[i].idAnketa+"'>Anketni listići</a></td>";
                            } else if (arrRezultat[i].isPersonalizovana == 0) {
                                rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=16&amp;anketa="+arrRezultat[i].idAnketa+"'>Anketni listići</a></td>";
                            }                                                                                                                                                            
                            rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=11&amp;anketa="+arrRezultat[i].idAnketa+"'>Zbirni izveštaj</a></td>";                                                  
                            rezultat+="</tr>";
                        }//for                    
                    } else {                                                                                                                                       
                        rezultat+="<tr id='tr"+arrRezultat.idAnketa+"'>"+
                            "<td>"+arrRezultat.naziv+"</td>"+
                            "<td>"+arrRezultat.datumPocetka+"</td>"+
                            "<td>"+arrRezultat.datumZavrsetka+"</td>"+
                            "<td>"+(arrRezultat.isPersonalizovana==1?'Da':'Ne')+"</td>"+
                            "<td>"+arrRezultat.brIspitanika+"</td>";
                        if (arrRezultat.isPersonalizovana == 1) {
                            rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=15&amp;anketa="+arrRezultat.idAnketa+"'>Anketni listići</a></td>";
                        } else if (arrRezultat.isPersonalizovana == 0) {
                            rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=16&amp;anketa="+arrRezultat.idAnketa+"'>Anketni listići</a></td>";
                        }  
                        rezultat+="<td><a style='color:green; font-weight:700;' href='index.php?p=11&amp;anketa="+arrRezultat.idAnketa+"'>Zbirni izveštaj</a></td>"; 
                        rezultat+="</tr>";                            
                    }
                    rezultat+="</toby></table>";
                }                                                                                                                                                    
                    
                if(ukupnoZapisa>=1){
                    document.getElementById('sadrzaj').innerHTML=rezultat;                             
                    brojStrana=Math.ceil(ukupnoZapisa / limit);
                    odstampajPaginaciju(strana);
                }else {
                    //document.getElementById('sadrzaj').innerHTML="";
                    document.getElementById('sadrzaj').innerHTML=rezultat;
                    odstampajPaginaciju(0);
                }
            }//if
        }//onreadystatechange
        
        //uzima izbranu vrstu sortiranja
        var e = document.getElementById("orderby");
        var orderby = e.options[e.selectedIndex].value; 
        //limt prikaza po strani
        e = document.getElementById("limit");
        limit = e.options[e.selectedIndex].value;
        
        var kljucnaRec=document.frmPretraga.kljucna_rec.value;
    
        //xmlhttp.open("GET","index.php?p=2&ajax&str="+strana+"&brZapisa="+brZapisa+"&orderby="+orderby,true);
        xmlhttp.open("GET","index.php?p=14&ajaxJson&str="+strana+"&limit="+limit+"&kljucnaRec="+kljucnaRec+"&orderby="+orderby, true);    
    
        //xmlhttp.open("GET","index.php?p=2&orderby="+orderBy,true);    
        xmlhttp.send();
    }//getListiciInfoAjaxJson
    
</script>
<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 750px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($ankete)) {
        ?>    
        <div class="naslov">Izveštaji</div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div style="border:  0px dotted grey; width: 100%; border-bottom: 0px dotted grey; margin: 0 auto; display: table; padding: 5px; font-size: 12px;">      

            <div style="float: left; width: 100%;">
                <div style="float: right; border: 0px solid #bdf;">Sortiraj po:<br/> 
                    <select id="orderby" name="orderby" onchange="promeniStranu(0)">
                        <option value="nazivAsc" <?php echo ($orderby === 'nazivAsc' ? 'selected' : ''); ?>>Nazivu: A - Z</option>
                        <option value="nazivDesc" <?php echo ($orderby === 'nazivDesc' ? 'selected' : ''); ?>>Nazivu: Z - A</option>
                        <option value="datumPocetkaAsc" <?php echo ($orderby === 'datumPocetkaAsc' ? 'selected' : ''); ?>>Dat. početka: rasuće</option>
                        <option value="datumPocetkaDesc" <?php echo ($orderby === 'datumPocetkaDesc' ? 'selected' : ''); ?>>Dat. početka: opadajuće</option>
                        <option value="datumIstekaAsc" <?php echo ($orderby === 'datumIstekaAsc' ? 'selected' : ''); ?>>Dat. isteka: rasuće</option>
                        <option value="datumIstekaDesc" <?php echo ($orderby === 'datumIstekaDesc' ? 'selected' : ''); ?>>Dat. isteka: opadajuće</option>
                    </select>
                </div>

                <div style="float: left; border: 0px solid #bdf;">
                    <form name="frmPretraga">
                        Pretraga po nazivu ankete:<br/>
                        <input value="" onkeyup="promeniStranu(0)" type="text" name="kljucna_rec" id="pretraga">
                    </form>
                </div>
            </div>


            <div style="border: 0px solid blue; margin-top: 5px; float: left; width: 100%;">  
                <div style="border: 0px solid red; float: left; width: 30%; height: 27px; line-height: 27px;">
                    <span>Prikaži: </span>
                    <select id="limit" onchange="promeniStranu(0)">
                        <option value="2">2</option>
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>                        
                    </select>                    
                </div>

                <?php
                include_once 'modules/mod_paginacija/paginacija_gore.php';
                ?>

            </div>

        </div>

        <div style="float:left; width: 100%; height: 1px; border-top:  1px dotted #666; margin-top: 0px;"></div>

        <div id="sadrzaj" style="margin-top: 20px; border: 0px solid #609; float: left; width: 100%">            
            <table class="hor-minimalist-b0" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope='col'>Naziv</th>
                        <th scope='col'>Dat. početka</th>
                        <th scope='col'>Dat. isteka</th>
                        <th scope='col'>Personalizovana</th>
                        <th scope='col'>Broj ispitanika</th>                         
                        <!-- ne trebaju mi strelice na linku, tako da je dodata nepostojeća slika> -->
                        <th scope='col'></th>
                        <th scope='col'></th>
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (count($ankete) != count($ankete, 1)) {
                        foreach ($ankete as $a) {
                            echo "<tr>";
                            echo "<td>{$a['naziv']}</td>";
                            echo "<td>" . substr($a['datumPocetka'], 0, 10) . "</td>";
                            echo "<td>" . substr($a['datumZavrsetka'], 0, 10) . "</td>";
                            //echo "<td>{$a['datumZavrsetka']}</td>";                            
                            echo "<td>" . ($a['isPersonalizovana'] == 1 ? 'Da' : 'Ne') . "</td>";
                            echo "<td>{$a['brIspitanika']}</td>";
                            if ($a['isPersonalizovana'] == 1) {
                                echo "<td><a style='color:green; font-weight:700;' href='index.php?p=15&amp;anketa={$a['idAnketa']}'>Anketni listići</a></td>";
                            } else if ($a['isPersonalizovana'] == 0) {
                                echo "<td><a style='color:green; font-weight:700;' href='index.php?p=16&amp;anketa={$a['idAnketa']}'>Anketni listići</a></td>";
                            }
                            echo "<td><a style='color:green; font-weight:700;' href='index.php?p=11&amp;anketa={$a['idAnketa']}'>Zbirni izveštaj</a></td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                        echo "<td>{$ankete['naziv']}</td>";
                        echo "<td>" . substr($ankete['datumPocetka'], 0, 10) . "</td>";
                        echo "<td>" . substr($ankete['datumZavrsetka'], 0, 10) . "</td>";
                        //echo "<td>{$a['datumZavrsetka']}</td>";
                        echo "<td>" . ($ankete['isPersonalizovana'] == 1 ? 'Da' : 'Ne') . "</td>";
                        echo "<td>{$ankete['brIspitanika']}</td>";
                        if ($ankete['isPersonalizovana'] == 1) {
                            echo "<td><a style='color:green; font-weight:700;' href='index.php?p=15&amp;anketa={$ankete['idAnketa']}'>Anketni listići</a></td>";
                        } else {
                            echo "<td><a style='color:green; font-weight:700;' href='index.php?p=16&amp;anketa={$ankete['idAnketa']}'>Anketni listići</a></td>";
                        }

                        echo "<td><a style='color:green; font-weight:700;' href='index.php?p=11&amp;anketa={$ankete['idAnketa']}'>Zbirni izveštaj</a></td>";
                        echo "</tr>";
                    }
                    ?>            
                </tbody>
            </table>
        </div>       

        <div style="float: left; margin-top: 10px; width: 100%; border: 0px solid #609;">
            <?php
            include_once 'modules/mod_paginacija/paginacija_dole.php';
            ?>
            <div id="prikazanoZapisa" style="float: left; font-size: 12px;">               
                Prikaz <?php echo ($pocetniZapis + 1) . " do " . ($pocetniZapis + $limit > $ukupnoZapisa ? $ukupnoZapisa : $pocetniZapis + $limit) . " od ukupno {$ukupnoZapisa}"; ?> pitanja
            </div>
        </div>

        <?php
    } else {
       //echo "Nema rezultata anketa";
    }
    ?>
</div>