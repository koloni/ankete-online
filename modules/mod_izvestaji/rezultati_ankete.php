<div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (isset($ankete)) {
        ?>    
        <div class="naslov">Rezultati anketa</div>

        <div id="sadrzaj" style="margin-top: 30px;">            
            <table id="active_ankete" class="hor-minimalist-b" summary="Aktivne ankete">
                <thead>
                    <tr>
                        <th scope='col'>Naziv</th>
                        <th scope='col'>Dat. početka</th>
                        <th scope='col'>Dat. isteka</th>                    
                       <!-- <th scope='col'>Broj pitanja</th> -->
                        <th scope='col'>Broj ispitanika</th>                        
                        <th style="background-image: none;"> </th> 
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (count($ankete) != count($ankete, 1)) {
                        foreach ($ankete as $a) {
                            ?>
                            <tr>
                                <td><?php echo $a['naziv']; ?></td>
                                <td><?php echo substr($a['datumPocetka'], 0, 10); ?></td>
                                <td><?php echo substr($a['datumZavrsetka'], 0, 10); ?></td>
                               <!--<td><?//php echo $a['ukupnoPitanja']; ?></td>-->
                                <td><?php echo $a['brIspitanika']; ?></td>
                                <td><a style='color:green; font-weight:700;' href='index.php?p=10&anketa=<?php echo $a['idAnketa'] ?>'>Detalji</a></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td><?php echo $ankete['naziv']; ?></td>
                            <td><?php echo substr($ankete['datumPocetka'], 0, 10); ?></td>
                            <td><?php echo substr($ankete['datumZavrsetka'], 0, 10); ?></td>
                            <!--<td><?php echo $ankete['ukupnoPitanja']; ?></td>-->
                            <td><?php echo $ankete['brIspitanika']; ?></td>
                            <td><a style='color:green; font-weight:700;' href='index.php?p=10&anketa=<?php echo $ankete['idAnketa'] ?>'>Detalji</a></td>
                        </tr>
                    <?php } ?>            
                </tbody>
            </table>
        </div>
    <?php }
    ?>
</div>
