







<!--<div style="display: table; width: 600px; margin: 0px auto; height: auto; border: 0px solid red;">-->
<div class="paper" style="margin: 0 auto;display: table; min-height: 400px;" >

    <div class="naslov"><?php echo $anketaInfo['naziv']; ?></div>
    <?php foreach ($pitanjaStatistika as $p) { ?>
        <script>
            $(function () {
                $('#container').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Browser market shares at a specific website, 2010'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    },
                    series: [
                        {                           
                            type: 'pie',
                            name: 'Browser share',
                            data: [""
                                //<?php for ($i = 0; $i < count($p->odgovori); $i++) { ?>                                                   
                                    ['Firefox',   45.0],
                                    ['IE',       26.8],
                                    {
                                        name: 'Chrome',
                                        y: 12.8,
                                        sliced: true,
                                        selected: true
                                    },
                                    ['Safari',    8.5],
                                    ['Opera',     6.2],
                                    ['Others',   0.7]  
                                    //  <?php } ?>
                            ]
                        }                     
                    ]
                });
            });
            
            
        </script>

        <?php for ($i = 0; $i < count($p->odgovori); $i++) { ?>
            <tr>
                <td><?php //echo $p->odgovori[$i]->tekst;   ?></td>
                <td style="text-align: right;"><?php //echo $p->odgovori[$i]->brOdgovora;   ?></td>
                <td style="text-align: right;"><?php //echo $p->getProcenatByIndexOdgovora($i);   ?>%</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom:10px;"><progress style="width: 100%; height: 12px;" value="<?php echo $p->odgovori[$i]->brOdgovora; ?>" max="<?php echo $p->ukupnoOdgovora; ?>"></progress></td>
            </tr>
        <?php } ?>
    </tbody>   
    </table>
    </div>-->

    <?php
    $p->prikazi();
}
?>

</div>