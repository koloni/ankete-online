<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; width: 650px;" >
    <div style=" margin: 0 auto; display: table;"><?php echo $pocetnoPitanje; ?> / <?php echo $anketaInfo['ukupnoPitanja']; ?></div>
    <div style=" margin: 0 auto; display: table;"><progress style="width: 200px;" value="<?php echo $procenat; ?>" max="100"></progress> <?php echo $procenat != 0 ? number_format($procenat, 2, '.', '') : '0'; ?>%</div>

    <form name="frmOdgovori" action="index.php?p=27&anketniListic=<?php echo $idAnketniListic; ?>" method="POST">
        <?php
        $pitanja->prikazi();
        ?>    
    </form>
</div>
<div style="padding-top: 15px; margin: 0 auto; border: 0px solid greenyellow;  display: table;">       
    <?php if ($trenutna == $poslednja && $trenutna == 0) { ?> 
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna + 1; ?>')">Završi popunjavanje i predaj</button>        
    <?php } else if ($trenutna == 0) { ?>
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna + 1; ?>')">Sledeća >></button>   
    <?php } else if ($trenutna == $poslednja) { ?>
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna - 1; ?>')"><< Prethodna</button> 
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna + 1; ?>')">Završi popunjavanje i predaj</button>        
    <?php } else { ?>
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna - 1; ?>')"><< Prethodna</button>         
        <button onclick="posaljiFrmOdgovori('<?php echo $trenutna + 1; ?>')">Sledeća >></button>       
    <?php } ?>
</div>
