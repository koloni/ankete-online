
<?php

$error = "";
if (isset($_POST["username"]) && isset($_POST["password"])) {
    Session::SetKey("user", "nikola");
    // return;

    $username = $_POST['username'];
    if (!Validator::Alphanumeric($username)) {
        $error.="<br/>Korisničko ime mora da sadrži minimum 3 karaktera!";
    }

    $password = $_POST['password'];
    if (!Validator::Alphanumeric($password, 5)) {
        $error.="<br/>Lozinka mora da sadrži minimum 5 karaktera!";
    }

    if ($error == "") {
        if (User::Login($username, $password) != null) {
            header("Location: index.php");
            //exit();
        }
    }
}

$data = array();
$data['error'] = $error;
?>
