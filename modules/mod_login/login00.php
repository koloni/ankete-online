<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmLogin").validate({
            rules: {
                username: {
                    required: true,
                    minlength: 3,
                    maxlength: 20
                },
                password: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                }
            },
            messages: {
                username: {
                    required: "Polje za 'Korisničko ime' je obavezno!",
                    minlength: $.format("Korisničko ime mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                password: {
                    required: "Polje za 'Lozinku' je obavezno!",
                    minlength: $.format("Lozinka mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                }
            }
        });
    });
    


    onload = function()
    {
        document.frmLogin.username.focus();
    }


</script>


<ul class="tabs primary">
    <li ><a href="index.php?p=8" >Registracija</a></li>
    <li class="active" ><a href="index.php?p=1" class="active">Prijava</a></li>
    <li ><a href="#">Nova lozinka</a></li>
</ul>

<!--
<ul class="menu_tabbed">
    <li><a <?php echo $menu == 2 || $menu == -1 ? "class='selected'" : ""; ?>  href="index.php?p=2">Registracija</a></li> 
    <li><a class="selected">Prijava</a></li> 
    <li><a href="#">Nova lozinka</a></li>                                             
</ul>
-->
<div style="font-family: Verdana,Geneva,sans-serif; font-size: 12px;">
    <div class="naslov" style="padding-top: 15px;">Prijava</div>
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
    <!--<div style="margin: 0 auto; border: 0px solid red;">-->           
    <form action="index.php?p=1" name="frmLogin" id="frmLogin" method="post">                
        Korisničko ime:<br/>
        <input type="text" name="username" id="username" value="" style="width: 250px;" required /><br/><br/>                     
        Lozinka:<br/>
        <input type="password" name="password" id="password" value="" style="width: 250px;" /><br/><br/>
        <input type="submit" name="btnLogin" id="submit" value="Prijavi se" style="width: 130px;"/>                    
    </form>
</div>
