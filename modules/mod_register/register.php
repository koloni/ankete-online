<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmRegister").validate({
            rules: {
                username: {
                    required: true,      
                    alphanumericUtf:true,
                    minlength: 3,
                    maxlength: 20
                },
                password: {
                    required: true,
                    alphanumericUtf:true,
                    minlength: 5,
                    maxlength: 20
                },                
                passwordRepeated: {
                    required: true,
                    minlength: 5,                                
                    equalTo : "#password"
                },
                firstName: {
                    required: true,
                    alphabetUtf:true,                    
                    minlength: 2,
                    maxlength: 50
                },
                lastName: {
                    required: true,
                    alphabetUtf:true,                     
                    minlength: 2,
                    maxlength: 50
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                firstName: {
                    required: "Polje za <b><i>'Ime'</i></b> je obavezno!",                    
                    minlength: $.format("Ime mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                lastName: {
                    required: "Polje za 'Prezime' je obavezno!",
                    minlength: $.format("Prezime mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                username: {
                    required: "Polje za 'Korisničko ime' je obavezno!",
                    minlength: $.format("Korisničko ime mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                password: {
                    required: "Polje za 'Lozinku' je obavezno!",
                    minlength: $.format("Lozinka mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },passwordRepeated: {
                    required: "Polje za 'Ponovljenu lozinku' je obavezno!",
                    minlength: $.format("Ponovljena lozinka mora da sadrži minimu {0} karaktera!"),
                    equalTo: "Ponovljena lozinka i Lozinka, moraju biti iste!"
                },
                email: {
                    required: "Polje za 'E-adresu' je obavezno!",
                    email: "'E-adresa' je unetea u pogrešnom formatu!"
                }
            }
        });
    });
    
    $.validator.addMethod("alphabetWithOneSpace",
    function(value, element) {                
        //ovde dozvoljava jedan ' ' ili jednu '_'
        //return /^([a-zA-Z]+(_[a-zA-Z]+)*)(\s([a-zA-Z]+(_[a-zA-Z]+)*))*$/.test(value);
        
        return /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/.test(value);
    },
    "Dozvoljena su samo slova, sa jednim razmakom!"
);
    
    $.validator.addMethod("alphabet",
    function(value, element) {                 
        return /^[A-Za-z]+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
    
    $.validator.addMethod("alphabetUtf",
    function(value, element) {                 
        return /^([A-Z]|[a-џ])+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
    
    $.validator.addMethod("alphanumeric",
    function(value, element) {                 
        return /^\w+$/.test(value);
    },
    "Dozvoljena su samo slova i brojevi!"
);
    
    
    $.validator.addMethod("alphanumericUtf",
    function(value, element) {                 
        return /^([A-Z]|[a-џ]|\d)+$/.test(value);
    },
    "Dozvoljena su samo slova i brojevi!"
);
        
    onload = function()
    {
        document.frmRegister.firstName.focus();
    }
</script>

<!--<div style="width: 400px; height: auto; margin: 0px auto; border: 0px solid red; display: table;">-->
<div class="paper paper-raise" style="margin: 0 auto;display: table; padding-top: 8px;" >
    <ul class="tabs primary">
        <li class="active"><a href="index.php?p=8" class="active" >Registracija</a></li>
        <li><a href="index.php?p=1">Prijava</a></li>
        <li><a href="index.php?p=3">Nova lozinka</a></li>
    </ul>
    <div class="naslov" style="padding-top: 15px;">Registracija</div>
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
    <div style="color: gray; size: 15px; margin-bottom: 10px; ">* Polja sa zvezdicom su obavezna</div>
    <form name="frmRegister" id="frmRegister" action="index.php?p=8" method="post"> 
        <table class="tableForm">   
            <!--
            <tr>                    
                <td colspan="2" style="color: red; text-align: center; font-size: 15px; font-weight: bold;"><?php //echo $error;                                 ?></td>
            </tr>
            -->
            <tr>                    
                <td>Tip naloga:<span class="form-required" title="Ovo polje јe obаvezno.">*</span></td>
                <td><input type="radio" name="type" id="type1" value="ispitanik" checked="true"/><label for="type1">Ispitanik</label> <input type="radio" name="type" id="type2" value="kreator"/><label for="type2">Kreator</label></td>
            </tr>

            <tr>                    
                <td>Ime:* </td>
                <td><input type="text" style="width: 200px;" name="firstName" value=""/></td>
            </tr>

            <tr>                    
                <td>Prezime:* </td>
                <td><input type="text" style="width: 200px;" name="lastName" value=""/></td>
            </tr>

            <tr>                    
                <td>Pol: </td>
                <td>
                    <input type="radio" name="gender" id="gender0" value="nepoznato" checked="true"/><label for="gender0">nepoznato</label>
                    <input type="radio" name="gender" id="gender1" value="male" /><label for="gender1">muško</label>
                    <input type="radio" name="gender" id="gender2" value="female"/><label for="gender2">žensko</label>
                </td>
            </tr>

            <tr>                    
                <td>Datum rođenja: </td>
               <!--<td><input type="date" size="24" name="birthDate" value=""/></td>-->
                <td>
                    <!--<input type="hidden" name="birthDate" id="birthDate" /><span style="font-style: italic;"id="datumRodjendana">mesec/dan/godina </span><button type="reset" id="btnBirthDate">izaberite datum</button>-->
                    <input type="hidden" name="birthDate" id="birthDate" /><span style="font-style: italic;"id="datumRodjendana">izaberite datum </span><button type="reset" id="btnBirthDate">...</button>
                </td>
            </tr>

            <tr>                    
                <td>Telefon: </td>
                <td><input type="text" name="phone" value="" style="width: 200px;"/></td>
            </tr>

            <tr>                    
                <td>E-adresa:* </td>
                <td><input type="text" size="24" name="email" value="" style="width: 200px;"/></td>
            </tr>

            <tr>
                <td>Korisničko ime:* </td>
                <td><input type="text" size="24" name="username" value="" style="width: 200px;" /></td>
            </tr>

            <tr>                    
                <td>Lozinka:* </td>
                <td><input type="password" size="24" name="password" id="password" value="" style="width: 200px;" /></td>
            </tr>

            <tr>                    
                <td>Lozinka ponovljena:* </td>
                <td><input type="password" size="24" name="passwordRepeated" value="" style="width: 200px;"/></td>
            </tr>
            <tr>               
                <td>
                </td>
                <!--<td><input type="button" name="btnRegister" value="Registruj se" onclick="validateFrmRegister()" /></td>-->
                <td><input type="submit" name="btnRegister" value="Registruj se" style="width: 202px; height: 25px;"/></td>
            </tr>     

        </table>

        <!--
                <div>
                    <div style="font-weight: bold;">Ime:*</div>
                    <div><input type="text" size="24" name="firstName" value=""/></div>                        
                </div>
                <div>
                    <div>Prezime:*</div>
                    <div><input type="text" size="24" name="firstName" value=""/></div>                        
                </div>
        
        -->
    </form>
</div>

<script type="text/javascript">    
    Calendar.setup({
        inputField     :    "birthDate",      // id of the input field        
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        displayArea    :    "datumRodjendana",
        daFormat       :    "%B %e, %Y",//"%B %e, %Y %I:%M %p" format of the displayed date
        showsTime      :    false,            // will display a time selector
        button         :    "btnBirthDate",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
   
</script>
