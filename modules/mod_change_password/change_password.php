<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmChangePassword").validate({
            rules: {
                username: {
                    required: true,  
                    alphanumericUtf:true,
                    minlength: 3,
                    maxlength: 20
                },
                passwordCurrent: {
                    required: true,
                    alphanumericUtf:true,
                    minlength: 5,                    
                    maxlength: 20
                }, 
                passwordNew: {
                    required: true,
                    alphanumericUtf:true,
                    minlength: 5,
                    maxlength: 20,
                    razlicito : "#passwordCurrent"
                },                
                passwordRepeated: {
                    required: true,
                    minlength: 5,                                
                    equalTo : "#passwordNew"
                }
                
            },
            messages: {
                username: {
                    required: "Polje za 'Korisničko ime' je obavezno!",
                    minlength: $.format("Korisničko ime mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },
                passwordCurrent: {
                    required: "Polje za 'Trenutnu Lozinku' je obavezno!",
                    minlength: $.format("Lozinka mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")                    
                },
                passwordNew: {
                    required: "Polje za 'Novu Lozinku' je obavezno!",
                    minlength: $.format("Lozinka mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}"),
                    razlicito: "'Nova Lozinka' mora da bude različita od 'Trenutne Lozinke'!"
                    
                },passwordRepeated: {
                    required: "Polje za 'Ponovljenu lozinku' je obavezno!",
                    minlength: $.format("Ponovljena lozinka mora da sadrži minimu {0} karaktera!"),
                    equalTo: "Ponovljena lozinka i Lozinka, moraju biti iste!"
                }
            }
        });
    });
    
    $.validator.addMethod("alphanumericUtf",
    function(value, element) {                 
        return /^([A-Z]|[a-џ]|\d)+$/.test(value);
    },
    "Dozvoljena su samo slova i brojevi!"
);
    $.validator.addMethod("razlicitoo",
    function(value, element) {                 
        return /^[A-Za-z]+$/.test(value);
    },
    "Dozvoljena su samo slova!"
);
    /*ako su vrednosti iste vraća true*/ 
    jQuery.validator.addMethod("razlicito", function(value, element, param) {
        
        var target = $(param);
        return this.optional(element) || value != target.val();
    }, "'Morate uneti različite vrenosti'!"
);
    
    onload=function(){        
       document.frmChangePassword.username.focus();
    }
    
</script>


<div class="paper paper-lift" style="margin: 0 auto;display: table;" >
    <div class="naslov">Promena lozinke</div>
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>
    <form name="frmChangePassword" id="frmChangePassword" action="index.php?p=6" method="post"> 
        <table class="tableForm">   
            <tr>
                <td>Korisničko ime:</td>
                <td><input type="text" name="username" value="" style="width: 180px;" /></td>
            </tr>
            <tr>                    
                <td>Trenutna lozinka: </td>
                <td><input type="password" size="20" name="passwordCurrent" id="passwordCurrent" value="" style="width: 180px;"/></td>
            </tr>
            <tr>                    
                <td>Nova lozinka: </td>
                <td><input type="password" size="20" name="passwordNew" id="passwordNew" value="" style="width: 180px;"/></td>
            </tr>
            <tr>                    
                <td>Ponovi lozinku: </td>
                <td><input type="password" size="20" name="passwordRepeated" value="" style="width: 180px;"/></td>
            </tr>
            <tr>               
                <td>
                </td>
                <td><input type="submit" name="btnChangePassword" value="Promeni Lozinku" style="width: 180px;" /></td>
            </tr>        
        </table>
    </form>
</div>



