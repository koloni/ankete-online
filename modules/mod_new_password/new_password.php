<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmNewPassword").validate({
            rules: {
                password: {
                    required: true,
                    alphanumericUtf:true,
                    minlength: 5,
                    maxlength: 20
                },                
                passwordRepeated: {
                    required: true,
                    minlength: 5,                                
                    equalTo : "#password"
                }
            },
            messages: {
                password: {
                    required: "Polje za 'Lozinku' je obavezno!",
                    minlength: $.format("Lozinka mora da sadrži minimu {0} karaktera!"),
                    maxlength: $.format("Maksimalni broj karaktera je: {0}")
                },passwordRepeated: {
                    required: "Polje za 'Ponovljenu lozinku' je obavezno!",
                    minlength: $.format("Ponovljena lozinka mora da sadrži minimu {0} karaktera!"),
                    equalTo: "Ponovljena Lozinka i Lozinka, moraju biti iste!"
                }
            }
        });
    });
 
    $.validator.addMethod("alphanumericUtf",
    function(value, element) {                 
        return /^([A-Z]|[a-џ]|\d)+$/.test(value);
    },
    "Dozvoljena su samo slova i brojevi!"
);
 
    onload = function()
    {
        document.frmNewPassword.password.focus();
    }


</script>

<div class="paper paper-lift" style="margin: 0 auto;display: table; padding-top: 8px;" ><!--style="border:1px dotted #c1c3bc;display: table; width: 100%; height: 500px; margin: 0px auto; height: auto;">-->

    <div class="naslov" style="padding-top: 15px;">Resetovanje lozinke</div>   
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>    
    <form action="index.php?p=3" name="frmNewPassword" id="frmNewPassword" method="post"> 
        <input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
        <div class="InputRow">
            <!--<p class="Msg">Unesite e-mail adresu sa kojom ste se registrovali, kako bi Vam poslali link za promenu lozinke</p>-->
            <label class="Label" for="password">Lozinka: </label>
            <input type="password" name="password" id="password" value="" style="width: 282px;"/>                                           
        </div>
        <div class="InputRow">            
            <label class="Label" for="passwordRepeated">Lozinka ponovljena: </label>
            <input type="password" name="passwordRepeated" id="passwordRepeated" value="" style="width: 282px;"/>                                           
        </div>

        <div class="InputRow" style="margin-top: 20px;">
            <input class="pure-button" type="submit" name="btnNewPassword" id="submit" value="Promeni Lozinku" style="width: 182px;"/>            
        </div>
    </form>   
</div>
