<style>

</style>

<script>
    // validate signup form on keyup and submit
    $().ready(function() {
        $("#frmResetPassword").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Polje za 'E-adresu' je obavezno!",
                    email: "'E-adresa' je unetea u pogrešnom formatu!"
                }
            }
        });
    });
 
    onload = function()
    {
        document.frmResetPassword.email.focus();
    }


</script>

<div class="paper paper-lift" style="margin: 0 auto;display: table; padding-top: 8px;" ><!--style="border:1px dotted #c1c3bc;display: table; width: 100%; height: 500px; margin: 0px auto; height: auto;">-->
    <ul class="tabs primary">
        <li><a href="index.php?p=8" >Registracija</a></li>
        <li><a href="index.php?p=1">Prijava</a></li>
        <li class="active" ><a href="#" class="active">Zaboravljena Lozinka?</a></li>
    </ul>

    <div class="naslov" style="padding-top: 15px;">Resetovanje Lozinke</div>   
    <?php include_once MODULES_DIR . "/mod_message/message.php"; ?>    
    <!--<div style="margin: 0 auto; border: 0px solid red;">-->          

    <!--<div class="head" style="padding-top: 15px;">Resetovanje Lozinke</div>  -->
    <form action="index.php?p=3" name="frmResetPassword" id="frmResetPassword" method="post">    
        <div class="InputRow">
            <p class="Msg">Unesite e-mail adresu sa kojom ste se registrovali, kako bi Vam poslali link za promenu lozinke</p>
            <label class="Label" for="email">E-adresa: </label>
            <input type="text" name="email" id="email" value="" style="width: 282px;"/>                                           
        </div>
        <div class="InputRow" style="margin-top: 20px;">
            <input class="pure-button" type="submit" name="btnLogin" id="submit" value="Pošalji" style="width: 182px;"/>            
        </div>

    </form>   


    <!--

   <form action="index.php?p=1" name="frmResetPassword" id="frmResetPassword" method="post"> 
       <table class="tableForm" border="0px">   
           <tr>
               <td>E-adresa: </td>
               <td><input type="text" name="email" id="email" value="" style="width: 180px;" /></td>
           </tr>
           <tr>
               <td></td>
               <td><input class="pure-button" type="submit" name="btnLogin" id="submit" value="Pošalji" style="width: 182px;"/></td>
           </tr>  
       </table>
   </form>      
    -->
</div>

