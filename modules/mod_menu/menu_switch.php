<?php

//$page->AddModuleMenu(new Modul("mod_menu", "menu_default.php"));
 //$page->AddModuleMenuLoginLogout(new Modul("mod_menu", "menu_login_logout.php")); 

if (!empty($data['user'])) {
    switch ($data['user']->type) {
        case 1: {
                $page->AddModuleMenu(new Modul("mod_menu", "menu_default_sluzbenik.php"));
                $page->AddModuleMenuHidden(new Modul("mod_menu", "menu_sluzbenik.php"));
                break;
            }
        case 2: {
                $page->AddModuleMenu(new Modul("mod_menu", "menu_default_kreator.php"));
                $page->AddModuleMenuHidden(new Modul("mod_menu", "menu_kreator.php"));
                break;
            }
        case -1: {
                $page->AddModuleMenu(new Modul("mod_menu", "menu_default_admin.php"));
                $page->AddModuleMenuHidden(new Modul("mod_menu", "menu_admin.php"));
                break;
            }
        default : {
                $page->AddModuleMenu(new Modul("mod_menu", "menu_default.php"));
            }
    }
} else {
    $page->AddModuleMenu(new Modul("mod_menu", "menu_default.php"));
}

