<div class="paper" style="margin: 0 auto;display: table; min-height: 400px; min-width: 600px;" >
    <?php
    include_once MODULES_DIR . "/mod_message/message.php";
    if (!empty($korisnici)) {
        ?>    
        <!--<div style="font-family: arial; font-size: 12px; font-weight: bold; margin: 0 auto; display: table;">Neaktivirani | Aktivirani | Blokirani | Napravi nalog</div>-->
        <div class="naslov">Neaktivirani nalozi</div>

        <div id="sadrzaj" style="margin-top: 30px;">            
            <table id="active_ankete" class="hor-minimalist-b" summary="Aktivne ankete">
                <thead>
                    <tr>
                        <th scope='col'>Ime</th>
                        <th scope='col'>Prezime</th>
                        <th scope='col'>Korisničko ime</th>
                        <th scope='col'>E-mail adresa</th>
                        <th scope='col'>Tip</th>
                        <th scope='col'>Stanje</th> 
                        <!-- ne trebaju mi strelice na linku, tako da je dodata nepostojeća slika> -->
                        <th style="background-image: none;"> </th>
                        <th style="background-image: none;"> </th>
                        <th style="background-image: none;"> </th>
                    </tr>
                </thead>
                <tbody>            
                    <?php
                    if (is_array($korisnici)) {
                        foreach ($korisnici as $k) {
                            ?>
                            <tr id="tr<?php echo $k->idKorisnik; ?>">
                                <td><?php echo $k->firstName; ?></td>
                                <td><?php echo $k->lastName; ?></td>
                                <td><?php echo $k->username; ?></td>
                                <td><?php echo $k->email; ?></td>
                                <td><?php echo $k->typeName; ?></td>
                                <td><?php echo $k->stateName; ?></td>
                                <td><a style='font-weight:700; color: green;' href="javascript:void(0)" onclick="changeStateNalog(<?php echo "$k->idKorisnik, 'akt'"; ?>)">aktiviraj</a></td>
                                <td><a style='font-weight:700;' href="javascript:void(0)" onclick="changeStateNalog(<?php echo "$k->idKorisnik, 'blo'"; ?>)">blokiraj</a></td>
                                <td><a style='font-weight:700; color: red;' href="javascript:void(0)" onclick="changeStateNalog(<?php echo "$k->idKorisnik, 'obr'"; ?>)">obriši</a></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="tr<?php echo $korisnici->idKorisnik; ?>">
                            <td><?php echo $korisnici->firstName; ?></td>
                            <td><?php echo $korisnici->lastName; ?></td>
                            <td><?php echo $korisnici->username; ?></td>
                            <td><?php echo $korisnici->email; ?></td>
                            <td><?php echo $korisnici->typeName; ?></td>
                            <td><?php echo $korisnici->stateName; ?></td>                            
                            <td><a style='font-weight:700; color: green;' href="javascript:void(0)" onclick="changeStateNalog(<?php echo "$korisnici->idKorisnik, 'akt'"; ?>)">aktiviraj</a></td>
                            <td><a style='font-weight:700;' href="javascript:void(0)" onclick="changeStateNalog(<?php echo "$korisnici->idKorisnik, 'blo'"; ?>)">blokiraj</a></td>
                            <td><a style='font-weight:700; color: red;' href='index.php?p=40&pro=obr&id=<?php echo $korisnici->idKorisnik; ?>'>obriši</a></td>
                        </tr>
                    <?php }
                    ?>            
                </tbody>
            </table>
        </div>
    <?php
    } else {
         echo "Nema novopristiglih naloga za aktivaciju.";
    }
    ?>
</div>
