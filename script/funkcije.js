

function brPreostalihKaratera(text, id, max) {    
    var preostalo=max-text.value.length;
    if(preostalo<=0) {
        //obriši višak karatera
        text.value=text.value.substring(0, max);
        //document.getElementById(id).innerHTML="Iskoristili ste maksimalan broj karaktera!";
        document.getElementById(id).innerHTML="<span style='color:red'>0</span>";
    }else {
        document.getElementById(id).innerHTML=preostalo;
    }
}

//Proverava da li su kontrole izabrane, u slučaju dinamičkog postavljanja kontrola
function proveraOdgovora(form) {
    var imaGreske=false;
    //var greska="<ul style='color:red;'><li>Pitanje je obavezno!</li></ul>";                   
    // var greska="<span style='color:red; padding: 0 5px 5px 0 px; font-weight: bold;'>Ovo pitanje je obavezno!</span>";
    var greska="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 0; font-weight: 600;'>Ovo pitanje je obavezno!</div>";
    var izabrano="";
    //prolazi kroz sve elemente forme
    for(i=0; i<form.length; i++) {
        //ako nije obavezno preskoči proveru
        if(form[i].name[0]=='n') {                        
            continue;
        }
        if(form[i].type=='radio' || form[i].type=='checkbox') {                                                                                            
            //da li postoji sledeći element
            if(form[i+1]!=undefined) {
                //ako su dva susedna elementa istog naziva, i već nije štikliran
                if(form[i].name==form[i+1].name && form[i+1].name!=izabrano) { 
                               
                    if(form[i].checked==false && form[i+1].checked==false) {
                                    
                        //ako sledeći ne postoji ili sledeći ima različitio ime, ispiši grešku
                        if(form[i+2]==undefined || form[i+2].name!=form[i+1].name ) {                                                                                                        
                            var x=document.getElementById(form[i].name+"gr");                                                
                            //x.innerHTML="<font color='red'>Pitanje je obavezno!</font>";
                            imaGreske=true;
                            x.innerHTML=greska;                                                      
                        }                                                
                    }else {
                        //upisuje ime kontrole koja je štiklirana
                        izabrano=form[i+1].name;                                 
                    }                                 
                }
            }
        }else if(form[i].type=='text' || form[i].type=='textarea') {                                                  
            
            //Provera da li je Nešto drugo izabrano i popunjeno
            //da li postoji prethodni
            if(i>1) {
                //da li je istog imena 
                if(form[i].name==form[i-1].name){
                    //alert("xxxx");
                    //i tipa radio ili check box
                    if(form[i-1].type=='radio' || form[i-1].type=='checkbox'){
                        if(form[i-1].checked && form[i].value.length<1) {                                   
                            var x=document.getElementById(form[i].name+"gr"); 
                            imaGreske=true;                            
                            //x.innerHTML="<font color='red'>Pitanje je obavezno!</font>";                                                         
                            x.innerHTML="<div style='color:red; font-size:13px;font-style: italic; padding: 10px 0px 5px 0; font-weight: 600;'>Izabrano polje 'Nešto drugo', ne sme biti prazno!</div>";
                            continue;
                        }else {
                            continue;
                        }                    
                    }                
                }            
            }
            
            //da li postoji sledeći element
            if(form[i+1]!=undefined) {
                //ako su dva susedna elementa istog naziva, i već nije štikliran, ali šta ako postoji samo jedna 
                if(form[i].name==form[i+1].name && form[i+1].name!=izabrano) { 
                    
                    form[i].value=form[i].value.trim();
                    form[i+1].value=form[i+1].value.trim();
                    //staro
                    //if(form[i].value.length<1 && form[i+1].value.length<1) {
                    // if(form[i].value.length<1 || form[i+1].value.length<1) {
                    if(form[i].value.length<1 || form[i+1].value.length<1) {
                                    
                        //ako sledeći ne postoji ili sledeći ima različitio ime prijavi grešku
                        if(form[i+2]==undefined || form[i+2].name!=form[i+1].name ) {
                            //greska                                                                      
                            var x=document.getElementById(form[i].name+"gr"); 
                            imaGreske=true;
                            //x.innerHTML="<font color='red'>Pitanje je obavezno!</font>";                                                         
                            x.innerHTML=greska;                                                      
                        }                                                
                    }else {
                        //upisuje ime kontrole koja je štiklirana
                        izabrano=form[i+1].name;                                 
                    }                                 
                }else{
                    //ako postoji samo jedan element
                    form[i].value=form[i].value.trim();
                    if(form[i].value.length<1){
                        var x=document.getElementById(form[i].name+"gr"); 
                        imaGreske=true;                                                                              
                        x.innerHTML=greska;                                                            
                    }
                }
            }else{
                //ako ne postoji sledeci element u formi
                form[i].value=form[i].value.trim();
                if(form[i].value.length<1){
                    var x=document.getElementById(form[i].name+"gr"); 
                    imaGreske=true;
                    x.innerHTML="<font color='red'>Pitanje je obavezno!</font>";                                                         
                    x.innerHTML=greska;  
                                           
                }
            }
        }else if(form[i].type=='select-one' || form[i].type=='select-multiple') {    
            //'0' označava da ništa nije izabrano
            if(form[i].value.length<1 || form[i].value=='0'){
                var x=document.getElementById(form[i].name+"gr"); 
                imaGreske=true;
                x.innerHTML="<font color='red'>Pitanje je obavezno!</font>";                                                         
                x.innerHTML=greska;                                             
            }
        } 
                       
    }//for  
    
    return imaGreske;                                                                           
}//end function proveraOdgovora

//Prosleđuje sledeću stranicu koja treba da se otvori i salje formu koja sadrži odgovore na pitanja
function posaljiFrmOdgovori(nextPage) {
    if(proveraOdgovora(document.frmOdgovori)){
        return;
    }
    
    var frmOdgovori=document.frmOdgovori;
    var nextPageDiv=document.createElement('div');   
    //strana koja sledeća treba da se otvori
    nextPageDiv.innerHTML="<input type='hidden' name='nextPage' value='"+nextPage+"'>";
    frmOdgovori.appendChild(nextPageDiv);
        
    frmOdgovori.submit();                
}//posaljiFrmOdgovori

//није довршена
function posaljiFrmOdgovoriIpredajAListic(nextPage) {
    if(proveraOdgovora(document.frmOdgovori)){
        return;
    }
    
    var frmOdgovori=document.frmOdgovori;
    var nextPageDiv=document.createElement('div');   
    //strana koja sledeća treba da se otvori
    nextPageDiv.innerHTML="<input type='hidden' name='nextPage' value='"+nextPage+"'>";
    frmOdgovori.appendChild(nextPageDiv);
        
    frmOdgovori.submit();               
}

//Briše grešku 'Pitanje obavezno'
function obrisiGresku(errorDiv) {
    //ako koristimo direkno u onclick document. ne mora da se koristi
    if(document.getElementById(errorDiv.name+'gr')!=null){
        document.getElementById(errorDiv.name+'gr').innerHTML='';
    }        
}

//Vraća sortiranje aktivne ankete
function getActiveAnketeInfoAjax0(orderBy){
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
            //alert(xmlhttp.responseText);                       
            //var tabela=document.getElementById('active_ankete');
            var divSadrzaj=document.getElementById('sadrzaj');
            //divSadrzaj.removeChild(tabela);
            divSadrzaj=document.getElementById('sadrzaj').innerHTML=xmlhttp.responseText;
        //document.getElementById('sadrzaj').innerHTML="xxx
        }
    }
                
    //var trenutna=document.getElementById('trenutna').innerHTML;
    xmlhttp.open("GET","index.php?p=2&str=0&orderby="+orderBy,true);
    //xmlhttp.open("GET","index.php?p=2&orderby="+orderBy,true);
    
    xmlhttp.send();
}//getActiveAnketeInfoAjax0

function getActiveAnketeInfo(trenutna){   
    var e = document.getElementById("orderby");
    var orderby = e.options[e.selectedIndex].value;    
    //var trenutna=document.getElementById('trenutna').innerHTML;     
    location.href="index.php?p=2&str="+trenutna+"&orderby="+orderby;        
}//getActiveAnketeInfo


function editPitanje(x)
{
    // alert(x);
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
            alert(xmlhttp.responseText);
            document.getElementById('q'+x).innerHTML=xmlhttp.responseText;
        }
    }
                
    alert("edit_pitanje.php?idPitanje="+x);
    xmlhttp.open("GET","edit_pitanje.php?idPitanje="+x,true);
    
    xmlhttp.send();
}//editPitanje


function removeOdgovor(x){
    var parentDiv =x.parentNode;                
    parentDiv.removeChild(x);        
}
        
function addPotpitanje(x)
{
    //alert(i);
    //nažalost uništava svu prethodnu decu
    //document.getElementById(x.parentNode.id).innerHTML+="<div id='potpitanje"+(idPotpitanje++)+"'><input type='text' name='potpitanje"+(idPotpitanje++)+"'></div>";
             
    var pitanjeDiv =document.getElementById(x.parentNode.id);
    var potpitanjeDiv = document.createElement('div');
    potpitanjeDiv.id="potpitanje"+(++idPotpitanje);
    potpitanjeDiv.innerHTML = "<input style='width=80%;' type='text' name='potpitanje"+idPotpitanje+"'><button type='button' onclick='removePotpitanje(this)'>obriši</button>";
    //mydiv.appendChild(newcontent);
    pitanjeDiv.insertBefore(potpitanjeDiv, x);                                        
}//addPotpitanje

function addOdgovor(x) {
    try{
        //var pitanjeDiv =document.getElementById(x.parentNode.id);
        var parentDiv=x.parentNode;                               
        var odgovorDiv = document.createElement('div');
        // odgovorDiv.id="potpitanje"+(++idPotpitanje);
        //odgovorDiv.innerHTML = "<input type='text' name='odgovori[]'><button type='button' onclick='removeOdgovor(this.parentNode)'>obriši</button>";
        //<div><input type='text' name='odgovori[]'><a href="#" onclick="removeOdgovor(this.parentNode)"><img width="16px" height="16px" src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
        odgovorDiv.innerHTML = "<input style='width=80%;' type='text' name='odgovori[]'><a href='javascript:void(0);' onclick='removeOdgovor(this.parentNode)'><img width='16px' height='16px' src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a>";
        //mydiv.appendChild(newcontent);
        parentDiv.insertBefore(odgovorDiv, x); 
    }catch(gr){
        alert(gr);
    }
}


function showPitanje(tipPitanja){    
    var pitanjeDiv=document.getElementById('pitanje');
    var podnaslovDiv=document.getElementById('podnaslov');
    var potpitanjaDiv=document.getElementById('potpitanja');
    var odgovoriDiv=document.getElementById('odgovori');
    var kontroleDiv=document.getElementById('kontrole');
    var obaveznoDiv=document.getElementById('obavezno');
    var slobodanUnosDiv=document.getElementById('nesto_drugo');
    
    if(tipPitanja==0) {
        pitanjeDiv.style.display="none";
        obaveznoDiv.style.display="none";
        potpitanjaDiv.style.display="none";
        odgovoriDiv.style.display="none";   
        kontroleDiv.style.display="none"; 
        podnaslovDiv.style.display="none";
        slobodanUnosDiv.style.display="none";
        return;
    }
    
    if(tipPitanja==1 || tipPitanja==2 || tipPitanja==3 || tipPitanja==4) {
        pitanjeDiv.style.display="block";
        obaveznoDiv.style.display="block";
        potpitanjaDiv.style.display="none";
        odgovoriDiv.style.display="block";                
        podnaslovDiv.style.display="none";
        kontroleDiv.style.display="block";
        slobodanUnosDiv.style.display="block";        
    }else if(tipPitanja==5 || tipPitanja==6){
        pitanjeDiv.style.display="block";
        obaveznoDiv.style.display="block";
        potpitanjaDiv.style.display="block";
        odgovoriDiv.style.display="block";                
        kontroleDiv.style.display="block";
        podnaslovDiv.style.display="none";
        slobodanUnosDiv.style.display="none";
    }else if(tipPitanja==7 || tipPitanja==8){               
        pitanjeDiv.style.display="block";
        obaveznoDiv.style.display="block";
        podnaslovDiv.style.display="none";
        potpitanjaDiv.style.display="block";
        odgovoriDiv.style.display="none";  
        kontroleDiv.style.display="block";
        slobodanUnosDiv.style.display="none";
    }else if(tipPitanja==9){
        pitanjeDiv.style.display="block";
        obaveznoDiv.style.display="block";
        podnaslovDiv.style.display="block";
        potpitanjaDiv.style.display="none";
        odgovoriDiv.style.display="none";  
        kontroleDiv.style.display="block";
        slobodanUnosDiv.style.display="none";
    }        
}//showPitanje

//izbacuje pitanje iz ankete
function removePitanje(x, anketa, pitanje){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {     
            //i ovo radi           
            //var divPitanje=document.getElementById(x);
            //var divParent=divPitanje.parentNode;
            //divParent.removeChild(divPitanje);
                      
            //ali ovo je jednostavnije
            document.getElementById(x).innerHTML="";                                                            
        }
    }
                     
    xmlhttp.open("GET","index.php?p=23&idAnketa="+anketa+"&idPitanje="+pitanje, true);       
    xmlhttp.send();
}//removePitanje

//Brisanje pitanja iz ankete, samo ako se ono nalazi u aketi koja se trenutno kreira
function deletePitanje(x, anketa, pitanje){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {       
            alert(xmlhttp.responseText);
            document.getElementById(x).innerHTML="";                                                            
        }
    }
                     
    xmlhttp.open("GET","index.php?p=24&anketa="+anketa+"&pitanje="+pitanje, true);       
    xmlhttp.send();
}//deletePitanje


//odustao od ove metode, prešao na izbacivanje reda
//dodaje postoje pitanje anketi i izbacuje divPitanje iz html stranice
function dodajPitanje0(divPitanje, anketa, pitanje){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {       
            var parentDiv =divPitanje.parentNode;                
            parentDiv.removeChild(divPitanje);  
        //document.getElementById('q'+divPitanje).innerHTML=xmlhttp.responseText;
        }
    }
                      
    xmlhttp.open("GET","index.php?p=22&idAnketa="+anketa+"&idPitanje="+pitanje, true);       
    xmlhttp.send();
}//end dodjaPitanje0


//dodaje postoje pitanje anketi i izbacuje trPitanje iz html stranice
function dodajPitanje(pitanje, anketa){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {                  
            //var parentDiv =divPitanje.parentNode;                
            //parentDiv.removeChild(divPitanje);  
                          
            //ne radi (IE11, firefox ubuntu, opera ubuntu)
            //radi (firefox windows, chrome ubuntu)
            //document.getElementById(trPitanje).remove();                     
            
            if(xmlhttp.responseText=='\ntrue'){
                //radi (na svim browsverima)
                document.getElementById(pitanje).innerHTML="";                            
            }else {
                alert("Došlo je do greške prilikom dodavanja pitanja!");    
            }
        /*
              //ovo radi svuda, ali glupo izgleda
              var divTr=document.getElementById(trPitanje);
              (divTr.parentNode).removeChild(divTr);
              */                          
        }
    }
                      
    xmlhttp.open("GET","index.php?p=22&anketa="+anketa+"&pitanje="+pitanje, true);       
    xmlhttp.send();
}//dodajPitanje

//briše anketu
function removeAnketa(anketa){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {       
            //var parentDiv =divPitanje.parentNode;                
            //parentDiv.removeChild(divPitanje);  
            
            document.getElementById(anketa).innerHTML="";
        // document.getElementById('q'+divPitanje).innerHTML=xmlhttp.responseText;
        }
    }
                      
    xmlhttp.open("GET","index.php?p=33&anketa="+anketa, true);       
    xmlhttp.send();
}//removeAnketa


function odstampajPaginaciju(str){
    if(brojStrana>=1) {    
        trenutna=str;          
        var strane="";//"<div class='pagination' style='float:left;'>";
        //prethodna strana
        if(trenutna >= 1) {  
            strane+="<a title='Strana "+(trenutna)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna-1)+")'>&#171; Prethodna</a>"                        
        }else{
        //strane+="<span class='disabled'>&#171; Prethodna</span>"
        }   
                    
        for (i = 0; i < brojStrana; i++) {
            if (i == str) {
                strane+="<span class='current'>"+ (i + 1) +"</span>"; 
            //štampanje dve sa desne strane
            } else if (i+1 == str || i+2==str) {
                strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>"; 
            }else if (i == str-3) {
                //štampanje prve strane
                strane+="<a title='Strana 1' href='javascript:void(0)' onclick='promeniStranu(0)'>1</a>";
                strane+=" ... ";                
            }else if (i == str+3) {
                strane+=" ... ";
                //štampanje poslednje strane
                strane+="<a title='Poslednja strana' href='javascript:void(0)' onclick='promeniStranu("+(brojStrana-1)+")'>"+ (brojStrana) +"</a>";
                break;
            //štampanje dve sa leve strane
            }else if (i-1 == str || i-2==str) {
                strane+="<a title='Strana "+(i+1)+"' href='javascript:void(0)' onclick='promeniStranu("+i+")'>"+ (i + 1) +"</a>";                                                        
            }            
        }//for
        //sledeća strana
        if (trenutna < brojStrana - 1) {                                
            strane+="<a title='Strana "+(trenutna+2)+"' href='javascript:void(0)' onclick='promeniStranu("+(trenutna+1)+")' class='next'>Sledeća &#187;</a>";                        
        }else{
        //strane+="<span class='disabled'>Sledeća &#187;</span>";
        }
        
        //strane+="</div>";
        //odustao sam od stampanja jer imam dva iput type text
        //strane+="<div style='font-size:10px;float:left;'> Idi na <input size='2' type='text'>&nbsp;<input type='button' style='font-size:11px;' value='stranu'></div>"             
             
        document.getElementById("paginacijaGore").style.display='block';
        document.getElementById("paginacijaDole").style.display='block';
        document.getElementById("paginacijaGoreStrane").innerHTML=strane;
        document.getElementById("paginacijaDoleStrane").innerHTML=strane; 
        //poništi tekst
        document.getElementById('idiNaStranu0').value="";
        document.getElementById('idiNaStranu1').value="";
        //$('#idiNaStranu1').value="";
        //$("#idiNaStranu1").html("");
        
        var pocetniZapis=trenutna*limit;
        var zapisDo=(pocetniZapis+limit*1);
        if(zapisDo>ukupnoZapisa){
            zapisDo=ukupnoZapisa;
        }
       
        //var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa+" pitanja";
        var prikazanoZapisa="Prikaz " +(pocetniZapis+1)+ " do " +zapisDo+ " od ukupno " +ukupnoZapisa;
        document.getElementById("prikazanoZapisa").innerHTML=prikazanoZapisa;
    }else {
        document.getElementById("paginacijaGore").style.display='none';
        document.getElementById("paginacijaDole").style.display='none';                  
        document.getElementById("prikazanoZapisa").innerHTML="";
    //document.getElementById("paginacijaGore").innerHTML="";        
    //document.getElementById("paginacijaDole").innerHTML=""; 
    }       
}//odstampajPaginaciju




function changeStateNalog(id, stanje){                   
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
            
            //ne radi u svim browsverima, kada se briše <tr>
            //document.getElementById("tr"+id).remove();  
            
            document.getElementById("tr"+id).innerHTML="";
            
        //document.getElementById("tr"+id).innerHTML=xmlhttp.responseText;
        }
    }
    
    /*
    var p="";
    if(stanje=="obr") {
        p="40";        
    }else if(stanje=="akt") {    
        p="40";        
    }else if(stanje=="blo") {
        p="40";
    } 
    */
   
    //xmlhttp.open("GET","index.php?p="+p+"&pro="+stanje+"&id="+id, true);  
    //alert("index.php?p=40&pro="+stanje+"&id="+id);
    // alert("index.php?p=39&pro="+stanje+"&id="+id);
    xmlhttp.open("GET","index.php?p=39&pro="+stanje+"&id="+id, true);      
    xmlhttp.send();
    
}//changeStateNalog

//menja stane ankete u aktivnu ili blokiranu, menja dugmiće u tabeli u aktivno i blokirano
function changeStateAnketa(id, stanje){  
    //ShowProgressAnimation();
    //setTimeout(function(){HideProgressAnimation();}, 500, null, null);
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {              
            var rezultat=xmlhttp.responseText;
            if(rezultat==="\ntrue"){                        
                var trAnketa = document.getElementById("tr"+id);        
                if(stanje==-1) {        
                    cells=trAnketa.getElementsByTagName('td');
                    cells[cells.length-1].innerHTML="<button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+id+", -1)'>blokiraj</button>";
                    cells[cells.length-2].innerHTML="<button style='font-weight:600; color: green;' onclick='changeStateAnketa("+id+", 1)'>aktiviraj</button>";                      
                    cells[cells.length-3].innerHTML="<span style='color: red;'>Blokirana</span>";
                }else if(stanje==1) {
                    cells=trAnketa.getElementsByTagName('td');
                    cells[cells.length-1].innerHTML="<button style='font-weight:600; color: red;' onclick='changeStateAnketa("+id+", -1)'>blokiraj</button>";
                    cells[cells.length-2].innerHTML="<button disabled style='font-weight:600; color: gray;' onclick='changeStateAnketa("+id+", 1)'>aktiviraj</button>";        
                    cells[cells.length-3].innerHTML="<span style='color: green;'>Aktivna</span>";
                }
            }else {
                alert(rezultat);   
            }
            
        }//if
    }//onreadystatechange
    
    xmlhttp.open("GET","index.php?p=60&stanje="+stanje+"&anketa="+id, true);      
    xmlhttp.send();
    
}//changeStateAnketa



function setPitanjeIsObavezno(anketa, pitanje, isObavezno) {
    //<?php echo $this->idAnketa.", ".$p->getIdPitanje(); ?>, this.value
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
            if(xmlhttp.responseText!='\ntrue'){
                alert("Došlo je do greške!");   
            }                         
        }
    }

    xmlhttp.open("GET","index.php?p=35&anketa="+anketa+"&pitanje="+pitanje+"&isObavezno="+isObavezno, true);      
    xmlhttp.send();        
}//setPitanjeIsObavezno


function setRedniBrojPitanja(anketa, pitanje, redniBroj) {
    //<?php echo $this->idAnketa.", ".$p->getIdPitanje(); ?>, this.value
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
                   
        }
    }

    xmlhttp.open("GET","index.php?p=36&anketa="+anketa+"&pitanje="+pitanje+"&redniBroj="+redniBroj, true);      
    xmlhttp.send();
        
}//setRedniBrPitanja


//od ovoga sam odustao    
//var prethodnaVrednost="";
//čuvanje prethodne vrednosti
function setPrethodnaVrednost(vrednost) {                      
    //ako se ne stavi var moguće je da je onda statičko polje
    prethodnaVrednost=vrednost;                
}
            
//Prethodnu vrednost, upiši u sve select kontrole, a izabranu izbriši iz svih select kontrola
function izmeniSelectKontrole(select) {                              
    var form=document.frmPitanja;
                
    //prolazi kroz sve selekt kontrole
    for(i=0; i<form.length; i++) {
                                   
        if(form[i].type=='select-one') { 
            //ako ime select kontrole počinje sa 's'
            if(form[i].name[0]=='s'){ 
                //if ovi sa vrednostima različitim od nule sam nahnadno dodao da se element sa vrednosti nula ne bi brisao i dodavao
                if(select.value!=0) {
                    //ako je ime različito od tekućeg ukloni trenutno izabrani element iz ostalih select kontrola
                    if(form[i].name!=select.name ){
                        $("#"+form[i].id+" option[value='"+(select.value)+"']").remove();                           
                    }

                    if(prethodnaVrednost!=0) {
                        //dodaj prethodnu select vrednost u sve ostale selekt kontrole, jer se tamo ne nalaze, a u tekućoj se već nalazi
                        if(form[i].name!=select.name ){
                            $("#"+form[i].id).append("<option value='"+prethodnaVrednost+"'>"+prethodnaVrednost+"</option>");
                        }           
                    }
                }else {
                    if(prethodnaVrednost!=0) {
                        //dodaj prethodnu select vrednost u sve ostale selekt kontrole, jer se tamo ne nalaze, a u tekućoj se već nalazi
                        if(form[i].name!=select.name ){
                            $("#"+form[i].id).append("<option value='"+prethodnaVrednost+"'>"+prethodnaVrednost+"</option>");
                        }
                    }
                }
            }
        }                   
    }   

}//izmeniSelectKontrole


//ne radi ne znam zašto
function changeStateNalog2(id, stanje) {
    $("#load_get").click(function(){  
        $("#result")  
        .html(ajax_load)  
        .load("index.php", "p=40&pro="+stanje+"&id="+id);  
    });  
    
}










//ovo ne koristim, i nisam ga isprobao
function clearYourListSelection() {  
    var list = document.yourFormName.yourListName;  
    list.options[list.selectedIndex].selected=false;  
} 


function removePitanje3(x){   
    //alert(x);
    var divPitanje=document.getElementById(x);
    var divParent=divPitanje.parentNode;
    divParent.removeChild(divPitanje);
    
/*var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {     
                var parentDiv =x.parentNode;                
                parentDiv.removeChild(x);  
            //document.getElementById('q'+x).innerHTML=xmlhttp.responseText;
        }
    }
                
    xmlhttp.open("GET","index.php?p=22&idAnketa="+anketa+"&idPitanje="+pitanje, true);       
    xmlhttp.send();*/
}

//odustao
function addPitanje(x){
    var pitanjeDiv=document.getElementById('pitanje');
    if(x==1) {    
        pitanjeDiv.innerHTML="<div class = 'kolona'><div>Nalov pitanja: </div><div>Obavezno: </div><div>Ogovori: </div></div><div class = 'kolona'><input name='idAnketa' type='hidden' value='<?php echo $idAnketa; ?>'>"+
    "<div><input  type = 'text' size = '30' name='tekst'/></div>"+
    "          <div> "+
    "<select name = 'isObavezno'> "+
    "<option value='1' selected >Da</option>"+
    "<option value='0' >Ne</option>"+
    "</select>"+
    "</div> "+
    "<div><input type = 'text' name = 'odgovori[]'><a href='javascript:void(0);' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>"+
    "<div><a href='javascript:void(0);' onclick = 'addOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj odgovor</a></div>"+
    //<!--<div><button onclick = 'addOdgovor(this.parentNode);'>dodaj odgovor</button></div>-->
    "<div>"+
    "    <input style = 'float:left; width: 100px;' type = 'submit' onclick = '' value = 'Sačuvaj'/>"+
    "                <input style = 'float:right; width: 100px;' type = 'button' onclick = '' value = 'Obriši'/>"+
    "</div>"+
    "</div>"+
    "</div>"; 
    }else {
        pitanjeDiv.innerHTML="";
    }
}

//odustao
function addPitanje2(x){
    var pitanjeDiv=document.getElementById('pitanje');
    if(x==1) {    
        pitanjeDiv.innerHTML="<div class = 'kolona'><div>Nalov pitanja: </div><div>Obavezno: </div><div>Ogovori: </div></div><div class = 'kolona'><input name='idAnketa' type='hidden' value='<?php echo $idAnketa; ?>'>"+
    "<div><input  type = 'text' size = '30' name='tekst'/></div>"+
    "          <div> "+
    "<select name = 'isObavezno'> "+
    "<option value='1' selected >Da</option>"+
    "<option value='0' >Ne</option>"+
    "</select>"+
    "</div> "+
    "<div><input type = 'text' name = 'odgovori[]'><a href='javascript:void(0);' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>"+
    "<div><a href='javascript:void(0);' onclick = 'addOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Add.png'> dodaj odgovor</a></div>"+
    //<!--<div><button onclick = 'addOdgovor(this.parentNode);'>dodaj odgovor</button></div>-->
    "<div>"+
    "    <input style = 'float:left; width: 100px;' type = 'submit' onclick = '' value = 'Sačuvaj'/>"+
    "                <input style = 'float:right; width: 100px;' type = 'button' onclick = '' value = 'Obriši'/>"+
    "</div>"+
    "</div>"+
    "</div>"; 
    }else {
        pitanjeDiv.innerHTML="";
    }
}

