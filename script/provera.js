function validateFrmRegister()
{
    var tacno=true;            
    var uzorTri=/^([A-Z]|[a-џ]){3,}$/;
    var uzorPet=/^\w{5,}$/;
    //var uzorAdresa=/^([A-Z]|[a-џ]){4,20}\s{0,1}([A-Z]|[a-џ]){0,15}\s?\d+([A-Z][a-џ]){0,2}(\/\d+)?$/;
    //var uzorEmail=/^(\w|(\.|\_)){2,30}@\w{2,10}(\.([A-Z]|[a-z]){2,10})?\.([A-Z]|[a-z]){2,4}$/;
               
               
    //var uzorEmail=/^(\w{1,30}(\.|\_))?\w{1,30}@\w{2,10}(\.([A-Z]|[a-z]){2,10})?\.([A-Z]|[a-z]){2,4}$/;                
    var uzorEmail=/^\w[a-z0-9_\.\-]*@[a-z0-9\.\-]+\.[a-z]{2,4}$/
    //^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$  skinuo gotov sa neta   
    //^[[:alnum:]][a-z0-9_\.\-]*@[a-z0-9\.\-]+\.[a-z]{2,4}$                  skinuo gotov, ne radi sa alnum
                                                                
    var uzorTel=/^\d{2,}((\/|-)\d{2,})?(-\d{2,})?(-\d{2,})?(-\d{2,})?(-\d{2,})?$/;                  
                
    /*if (document.frm.ime.value=="" && tacno )
                {
                    tacno=false;
                    alert("Unesite svoje ime! (minimum tri karaktera)");
                    document.frm.ime.focus();
                }
                */
    var x=document.getElementById("error");           
    if (!uzorTri.test(document.frmRegister.firstName.value) && tacno)
    {
        tacno=false;               
        x.innerHTML="Unesite svoje ime! (minimum tri karaktera)";
        //alert("Unesite svoje ime! (minimum tri karaktera)");
        document.frmRegister.firstName.focus();
    }
               

    if (!uzorTri.test(document.frmRegister.lastName.value) && tacno)
    {
        tacno=false;        
        x.innerHTML="Unesite svoje prezime! (minimum tri karaktera)";
        //alert("Unesite svoje prezime! (minimum tri karaktera)");
        document.frmRegister.lastName.focus();
    }

    /*
    if ( document.frmRegister.address.value=="" && tacno)
    {
        tacno=false;
         x.innerHTML="Unesite svoju adresu!"
        //alert("Unesite svoju adresu!");
        document.frmRegister.address.focus();
    }
*/
    if (!uzorTel.test(document.frmRegister.phone.value) && tacno)
    {
        tacno=false;
        x.innerHTML="Unesite telefon u ispravnom formatu!"
        //alert("Unesite telefon u ispravnom formatu!");
        document.frmRegister.phone.focus();
    }

    if (!uzorEmail.test(document.frmRegister.email.value) && tacno)
    {
        tacno=false;
        x.innerHTML="Unesite e-mail adresu u ispravnom formatu!"
        //alert("Unesite e-mail adresu u ispravnom formatu!");
        document.frmRegister.email.focus();
    }
    
    if (!uzorTri.test(document.frmRegister.username.value) && tacno)
    {
        tacno=false;
        x.innerHTML="Unesite svoje korisničko ime! (minimum tri karaktera)";
        //alert("Unesite svoje korisnicko ime! (minimum tri karaktera)");
        document.frmRegister.username.focus();
    }

    if (!uzorPet.test(document.frmRegister.password.value) && tacno)
    {
        tacno=false;
        x.innerHTML="Unesite lozinku! (minimum pet karaktera)";
        //alert("Unesite lozinku! (minimum pet karaktera)");
        document.frmRegister.password.focus();
    }

    if(document.frmRegister.password.value!=document.frmRegister.passwordRepeated.value && tacno) {
        tacno=false;
        x.innerHTML="lozinka i lozinka ponovljena moraju biti iste!";
        //alert("lozinka i lozinka ponovljena moraju biti iste!");
        document.frmRegister.passwordRepeated.focus();
    }
             
    // if (tacno==true){
    if (true){
        document.frmRegister.submit();
    }

}//provFrmReg()
