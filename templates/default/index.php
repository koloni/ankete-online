<?php $time = microtime(true); ?>
<!DOCTYPE html>
<html>
    <head>     
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $this->title; ?></title>
        <!--
        <script type="text/javascript" src="script/jQueryTable/jquery-latest.js"></script> 
        -->

        <script type="text/javascript" src="script/jquery-1.10.2.js"></script>                         

        <script type="text/javascript" src="script/sortable.js"></script>

        <script type="text/javascript" src="script/jQueryTable/jquery.tablesorter.js"></script>
        <link rel="stylesheet" href="script/jQueryTable/themes/blue/style.css" type="text/css" media="print, projection, screen" />
        <script type="text/javascript">                        
            $(document).ready(function() 
            { 
                $("#active_ankete").tablesorter(); 
            } 
        );
        </script>

        <link href="templates/default/css/jconfirmaction.css" rel="stylesheet" type="text/css" media="all" />        
        <script type="text/javascript" src="script/jconfirmaction.jquery.js"></script>
        <script type="text/javascript">	
            $(document).ready(function() {
                $('.ask').jConfirmAction();
            });	
        </script>

        <script type="text/javascript" src="script/jquery.validate.js"></script>

        <!-- calendar stylesheet -->
        <link rel="stylesheet" type="text/css" media="all" href="script/k/calendar-win2k-cold-1.css" title="win2k-cold-1" />
        <!-- main calendar program -->
        <script type="text/javascript" src="script/k/calendar.js"></script>
        <!-- language for the calendar -->
        <script type="text/javascript" src="script/k/lang/calendar-en.js"></script>
        <!-- the following script defines the Calendar.setup helper function, which makes
             adding a calendar a matter of 1 or 2 lines of code. -->
        <script type="text/javascript" src="script/k/calendar-setup.js"></script>                                


        <script src="script/funkcije.js"></script>
        <script src="script/provera.js"></script>

        <link href="templates/default/css/tables" rel="stylesheet" type="text/css" media="all" />
        <link href="templates/default/css/anketa" rel="stylesheet" type="text/css" media="all" />
        <link href="templates/default/css/paper" rel="stylesheet" type="text/css" media="all" />
        <script>
            
            //ako koristim funcije iz fajla funkcije.js ne radi sa slikama kako treba
            function addOdgovor(x) {
                try{
                    //var pitanjeDiv =document.getElementById(x.parentNode.id);
                    var parentDiv=x.parentNode;                               
                    var odgovorDiv = document.createElement('div');
                    // odgovorDiv.id="potpitanje"+(++idPotpitanje);
                    //odgovorDiv.innerHTML = "<input type='text' name='odgovori[]'><button type='button' onclick='removeOdgovor(this.parentNode)'>obriši</button>";
                    //<div><input type='text' name='odgovori[]'><a href="#" onclick="removeOdgovor(this.parentNode)"><img width="16px" height="16px" src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                    odgovorDiv.innerHTML = "<input style='width:85%;' type='text' name='odgovori[]'><a href='javascript:void(0);' onclick='removeOdgovor(this.parentNode)' title='Izbriši odgovor'><img src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a>";
                    //mydiv.appendChild(newcontent);
                    parentDiv.insertBefore(odgovorDiv, x); 
                }catch(gr){
                    alert(gr);
                }
            }
            
            
            function addPotpitanje(x) {
                try{
                    //var pitanjeDiv =document.getElementById(x.parentNode.id);
                    var parentDiv=x.parentNode;                               
                    var odgovorDiv = document.createElement('div');
                    // odgovorDiv.id="potpitanje"+(++idPotpitanje);
                    //odgovorDiv.innerHTML = "<input type='text' name='odgovori[]'><button type='button' onclick='removeOdgovor(this.parentNode)'>obriši</button>";
                    //<div><input type='text' name='odgovori[]'><a href="#" onclick="removeOdgovor(this.parentNode)"><img width="16px" height="16px" src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                    odgovorDiv.innerHTML = "<input style='width:350px;' type='text' name='potpitanja[]'><a href='javascript:void(0);' onclick='removeOdgovor(this.parentNode)'><img src='<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a>";
                    //mydiv.appendChild(newcontent);
                    parentDiv.insertBefore(odgovorDiv, x); 
                }catch(gr){
                    alert(gr);
                }
            }
    
        </script>
        <style>

            select {
                /*font-family: Cursive;*/
                /*font-size: 13px;*/
            }

            /* IE10 */
            progress {
                -webkit-appearance: none;
                color: black;                
            }

            /* Firefox */
            progress::-moz-progress-bar { 
                background: black;	
            }

            /* Chrome */
            progress::-webkit-progress-value {                
                /*  -webkit-appearance: none;*/
              /*  background: black;*/
                
              background: black;              
            }

            progress::-webkit-progress-bar {                
               /* background: black; */
            }


            /* Polyfill */
            progress[aria-valuenow]:before  {
                background: black;
            }
            

            body{
                background-color: white;
            }


            body {
                font-family: Verdana,Geneva,sans-serif;
                /*font-size: 12px;       */
            }

            #messageERROR{ border:1px solid darkred;  padding:5px; font-size:14px; font-weight:bold; text-align: center; margin-bottom:15px;/*font-size:16px;margin-bottom:20px; padding:10px;*/
                           background: -webkit-gradient(linear, left top, left 25, from(#2e74b2), color-stop(4%, blue), to(#2e74b2));
                           background: -moz-linear-gradient(top, #2e74b2, blue 1px, #2e74b2 25px);

                           background:red;
                           color:#fff;
            }

            .table_pitanje {
                font-size: 14px;
            }

            .table_pitanje td {
                padding: 3px 6px 3px 6px;
                text-align:center;
                vertical-align: middle;
            }

            #header {
                width: 100%; 
                height:90px;

                /*border: 1px solid green;*/
                /*background-color: ghostwhite;*/
                /*border-bottom: 0px solid gainsboro; */

                /* background-color: #fff;*/
                /* -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                 -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                 box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                */

            }

            #header_in{                
                width: 960px; 
                height: 90px;

                border: 0px solid black;
                margin: 0 auto;
                /*border: 1px solid yellow;*/
            }

            /*stoji ispod navigacije*/
            #header2{                
                width: 100%; 
                height: 18px;                 
                margin: 0 auto;                
                /* border-top: 1px dotted gray;*/
                background-color: #fff;
                /*-webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);*/
            }

            #header3{     
                border: 0px;
                width: 100%; 
                height: 30px;                 
                margin: 0 auto;                
                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #footer02{                
                width: 100%;
                height: 80px;
                margin-top: 10px;
                float: left;

                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #footer_in{
                width: 960px; 
                height: 50px; 
                margin: 0 auto;
            }


            #naslov {
                font-size: 31px;                         
                font-weight: 700;
                font-family: Segoe UI;
                /*font-style: italic;*/
                font-style: oblique;
                padding: 7px 0 0 5px;     
                /* border: 1px #008;*/
            }

            #naslov a{
                color: gray;                
                text-decoration: none;                
            }

            #naslov a:hover{
                color: lightgray;                
            }

            .naslov {
                /*float: left;*/
                font-family: Segoe UI;
                font-size: 22px;
                /*float: left;               */
                font-weight: 700;
                font-style: oblique;
                color: gray; 
                padding:0 0 20px 0;
                width: 100%;
                /*font-style: italic;*/
                /*padding: 15px 0 0 5px;     */
                /* border: 1px solid black;*/
            }

            .nav_white{
                font-family: Segoe UI;
                text-decoration: none; 
                color: #2e74b2;                
                /* text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;*/
                margin-left: 15px;
                margin-right:15px;                
                font-weight: 700;                                                
                font-size: 14px;
                font-family:Arial,Helvetica,sans-serif; 
                font-family:Verdana, Times, serif;

            }

            .nav_black{
                font-family: Segoe UI;
                text-decoration: none; 
                color: black;
                /*text-shadow: -1px 0 moccasin, 0 1px moccasin, 1px 0 moccasin, 0 -1px moccasin;*/
                margin-left: 15px;
                margin-right:15px;                
                font-weight: 700;                                                
                font-size: 14px;
                font-family:Arial,Helvetica,sans-serif; 
                font-family:Verdana, Times, serif;
            }

            .nav_white2{
                font-family: Segoe UI;
                text-decoration: none;
                color: blue;
                /*text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;*/
                margin-left: 15px;
                margin-right:15px;                
                font-weight: 700;                                                
                font-size: 15px;
                font-size: 13px;  
                font-weight: bold;
                font-family:Arial,Helvetica,sans-serif; 
                font-family:Verdana, Times, serif;
            }

            .nav_black2{
                font-family: Segoe UI;
                text-decoration: none; 
                color: black;
                /*text-shadow: -1px 0 moccasin, 0 1px moccasin, 1px 0 moccasin, 0 -1px moccasin;*/
                margin-left: 15px;
                margin-right:15px;                
                font-weight: 700;                                                
                font-size: 15px;
                font-size: 13px;  
                font-weight: bold;
                font-family:Arial,Helvetica,sans-serif; 
                font-family:Verdana, Times, serif;
            }

            nav{
                width: 100%;
                border: 1px solid lightcyan; 
                background-color: steelblue;
                background-color: lightsteelblue;
                background-color: lightgrey;


                text-align: center;                               
                height: 25px;
                line-height: 25px;                
                /* background-color: beige;                  */
                /*margin: 0 auto; border: 1px solid red; display: table;*/
            }

            nav a{                                 
                text-decoration: none; 
                /*color: black;*/
                color: white;
                color: yellow;               
                color: black;              
                margin-left: 15px;
                margin-right:15px;
                font-size: 12px;  
                font-weight: bold;
                font-family:Arial,Helvetica,sans-serif; 
                font-family:Verdana, Times, serif;              
                /* padding: 0 20px 0 20px;*/
            }

            nav a:hover {
                color: grey;                                
            }

            /*Ovde se nalaze sve kolone*/
            #content{
                /* position: relative;*/
                margin: 0px auto;    
                margin-top: 10px;            
                margin-top: 0px;            
                width: 960px;                
                height: auto;
                display: block;

                /*border-top: 1px dotted #c1c3bc;*/







                /*float: left;*/
                /*  height: 1100px;                 */
                /*position: */

                /*  background-color: whitesmoke; */
                /* box-shadow: 1px 1px silver; */
                /*
                              background-color: #fff;
                              -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                              -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                              box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                */


                min-height: 40%;/* ne radi mozila*/
                min-height: 350px;
                height: auto;
            }

            #kontejner{
                width: 960px; 
                height: 2000px; 
                /* border: 1px solid silver; */
                margin: 0 auto; 
                /*  background-color: whitesmoke; */
                box-shadow: 1px 1px silver; 
                border-radius: 0px;    
            }



            #center_con{                
                width: 650px; 
                height:auto;
                float: left; 
                margin-left: 5px; 
                border: 0px solid black; 
                background-color: wheat;
                margin-top: 5px;
            }

            #center_con_cen{
                width: 960px; 
                height: auto;
                background-color: white;
                float: left;   
                border: 1px solid silver;                        
                /*margin-top: 5px;*/
                margin: 5px;                
            }

            #center_con_small
            {
                /* margin-top: 10px;*/
                /* width: 960px;*/
                width: 660px;
                float: left;
                padding: 50px;                
                /*margin: 0px auto;*/                
                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #center_con_one
            {
                /* margin-top: 10px;*/
                /*border: 1px solid #2e74b2;*/
                width: 850px;                    
                height: auto; 
                padding: 20px 50px 30px 50px;                
                margin: 0px auto;
                /* padding: 20px 10px 20px 10px;                
                 margin: 0 auto; border: 1px solid red; display: table;*/
            }


            #left_con{
                width: 160px; 
                height: auto; float: left;
                background-color: #fff;
                margin : 0 10px 0 10px;

                padding: 10px;

                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }

            #left_con p{
                padding: 10px;
            }

            #right_con{
                width: 150px; height:auto; float: right;

                background-color: #fff;
                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
            }


            /*Ubacujemo u njega kontejner se pitanajima, ma da  bi mogao ovo da izmenim*/
            .text {
                /*border: 1px solid red;                */
                /*centrira na serd strane*/
                margin: 0 auto;
                /*width: 650px;*/
                width: 700px;
                height: auto;
                font-size: 14px;                
                /* padding: 20px;  */
            }

            #loginCon {
                width: 960px; 

                /*height: auto; */
                border: 0px solid green;
                height: 16px;
                line-height: 16px;   
                margin: 0 auto;
            }

            /*nalazi se tekst prijava*/
            #login2{
                color:#666666; /* boja separator izmedju login logout*/


                float: right;                  
                padding: 2px 3px 0px 3px; 
                font-size: 12px; 
                font-weight: 600;   
                border: 0px solid #a94297;
                /*border:1px solid red;*/
            }

            #login2 a{
                text-decoration: none;
                color: black;
                color:#666666; 
            }

            #login2 a:hover {
                color: gray;                
            }


            #login{
                float: right; 
                padding: 10px 15px 0px 0px; 
                font-size: 14px; 
                font-weight: 600;                
            }

            #login a{
                text-decoration: none;
                color: black;
            }

            #login a:hover {
                color: gray;                
            }

            .text_black_15 a{
                color: black;
                font-size: 15px;                 
            }

            /*Anketa css*/
            .anketa{                 
                vertical-align: middle;
                /*width: 700px;*/
                height: 240px;
                float: none;
                padding: 15px;
                margin: 5px;
                /* background-color: aliceblue;  */
                /*border: 1px solid #2e74b2;       */            
            }

            .edit_pitanje{     
                vertical-align: middle;
                width: 100%;
                float: left;
                /*float: none;*/
                height: auto;

                padding: 5px 0 0 0;                   
                /*margin: 5px;*/
                /* background-color: aliceblue;  */
                /*border: 1px solid #2e74b2;*/
            }

            .edit_pitanje_kolona_levo{
                /*u operi baguje*/
                width: 200px;
                /*width: auto;*/
                float: left;
                padding: 5px;          
                text-align: right;
            }

            .edit_pitanje_kolona_desno{
                float: left;
                padding: 5px;
                border: 0px solid #2e74b2;  

                /*u operi baguje*/
                /*width: 400px;*/
                /* width: auto;        */
            }

            .edit_pitanje_kolona_desno a{
                text-decoration: none;
            }

            .edit_pitanje_kolona_desno a img{
                width: 16px;
                height: 16px;
                border-style: none;                
                padding-left: 2px;
            }

            .edit_pitanje_kolona_levo div{
                height: 25px;            
                line-height: 25px; 
                padding: 5px;
            }

            .edit_pitanje_kolona_desno div{
                height: 25px;            
                line-height: 25px; 
                padding: 5px;
            }


            /*napravi anketu*/
            .kolona{
                float: left;
                padding: 5px;           
                /*u operi baguje*/
                /*width: 400px;*/
                width: auto;                
            }


            .kolona_levo{
                /*u operi baguje*/
                width: 225px;
                /*width: auto;*/
                float: left;
                padding: 5px 0 5px 0;
                /* border: 1px solid #2e74b2;*/
                text-align: right;
            }

            .kolona_levo div{           
                height: 25px;            
                line-height: 25px; 
                padding: 5px;
            }

            .kolona div{               
                height: 25px;            
                line-height: 25px; 
                padding: 5px;
            }

            .pitanje{     
                float:left; 
                width:100%;                
                border: 0px solid black;     
                padding-top: 25px;
            }

            .link{
                font-size: 14px; 
                font-weight: 600;
                font-style: italic;
                padding-right: 5px;
            }

            .link a{
                text-decoration: none;
                color: blue;                
            }

            .link a:hover{
                color: greenyellow;                   
            }

            .linkPaginacija{
                font-size: 13px; 
                font-weight: 600;                
                padding-right: 5px;

                text-decoration: none;
                color: black;   
            }

            .linkPaginacija a{
                text-decoration: none;
                color: black;                
            }

            .linkPaginacija a:hover{
                color: greenyellow;                   
            }

            .linkMail{
                padding-top: 5px; 
                font-size: 12px; 
                margin: 0 auto; border: 0px solid greenyellow;  
                display: table;                
            }

            .linkMail a{
                text-decoration: none;
                color: black;
            }

            .linkMail a:hover{
                text-decoration: underline;
            }

            .btnKontrole{

                /*font-style: oblique;*/
                font-styel:none;
                font-weight: 600;   
                padding: 3px 5px 3px 5px;
            }

            /*
                        #login{
                float: right; 
                padding: 10px 15px 0px 0px; 
                font-size: 14px; 
                font-weight: 600;                
            }

            #login a{
                text-decoration: none;
                color: black;
            }

            #login a:hover {
                color: gray;                
            }*/


            #topmenu{
                /*margin-top:33px;*/
                width: 960px;
                margin: 0 auto;
                /*loat:left;*/
                height: 50px;
                voice-family:inherit;

                /*border: 1px solid #2d673f;*/
                /*
                                background-color: #fff;
                                -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                                -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);
                                box-shadow: 0 0 5px rgba(0, 0, 0, 0.2), inset 0 0 50px rgba(0, 0, 0, 0.1);*/
            }
            #topmenu ul{
                list-style:none;
                line-height:55px;
            }
            #topmenu li{
                display:inline;
            }
            #topmenu a,#topmenu a:visited{
                padding:5px 12px 5px 12px;
                padding:3px 12px 3px 12px;
                text-decoration:none;
            }
            #topmenu .current a, #topmenu .current a:hover, #topmenu .current a:visited{
                padding:5px 12px 5px 12px;
                padding:3px 12px 3px 12px;

                font-weight:bold;
            }


            #topmenu a,#topmenu a:visited{
                color:#FFF;               
                color: white;       
                background:#7e9dcc;                
                background: lightsteelblue;/*pozadina tabova*/
            }
            #topmenu a:hover{
                color: #FFF;               
            }
            #topmenu .current a, #topmenu .current a:hover, #topmenu .current a:visited{
                color:#375b91; /*slova selektovanog taba*/                
                background: #FFF url(../img/bg_menu_blue.jpg) repeat-x top; /*pozadina selektovanog taba*/


                border-left: #FFF 1px solid;
                border-right: #FFF 1px solid;
            }

        </style>


        <style type="text/css">
            #tabs{

                width: 960px;
                /*float: left;*/


                padding:10px 0 0 0;
                margin:0 auto;
                font-family:Arial, Helvetica, sans-serif;
                font-size:12px;
                color:#FFF;
                font-weight:bold;
            }
            #tabs ul{ 

                /*float: left;*/
                list-style:none;
                margin:0;
                padding:0;
            }
            #tabs ul li{

                display:inline;
                margin:0;
                text-transform:capitalize;
            }
            #tabs ul li a{
                padding:5px 16px;
                color:#FFF;
                background:#E7A272;
                background:#c1c3bc; /*pozadina tabova*/
                float:left;
                text-decoration:none;
                border:1px solid #D17B40;
                border:1px solid #7F9298;
                border-left:0;
                margin:0;
                text-transform:capitalize;
            }
            #tabs ul li a:hover{
                background:#EAEAEA;
                color:#7F9298;
                text-decoration:none;
                border-bottom:1px solid #EAEAEA;
            }
            #tabs ul li a.active{
                background:#EAEAEA;
                background:white;
                color:#7F9298;
                border-bottom:1px solid #EAEAEA;
            }
            #contentt{   
                background:#EAEAEA;
                clear:both;
                font-size:11px;
                color:#000;
                padding:10px;
                font-family:Arial, Helvetica, sans-serif;
            }
        </style>



    </head>

    <body>          
        <!--<div style="width: 960px; height: 1000px; border: 1px solid silver; margin: 0 auto; background-color: whitesmoke; box-shadow: 1px 1px silver; border-radius: 0px;">-->
        <!--<div id="kontejner">-->
        <header id="header" >        
            <div id="header_in">
                <div id="login2"><?php include_once 'modules/mod_menu/menu_login_logout.php'; ?></div> 
                <div id="naslov">
                    <a href="index.php">
                        Ankete online
                    </a>                    
                </div>

                <?php $this->RenderModuleMenu(); ?>
                <!--
                <div id="tabs"> 
                    <ul> 
                        <li><a class="active" href="index.php">Ankete</a></li> 
                        <li><a href="#">Rezultati</a></li> 
                        <li><a href="#">Kontakt</a></li>                                                 
                    </ul> 
                </div> 
                -->

            </div>

            <!--
            <div id="topmenu">
                <ul>
                    <li class="current"><a  href="index.html">Ankete</a></li>
                    <li><a href="#">Rezultati</a></li>
                    <li><a href="#">Kontakt</a></li> 
                </ul>
            </div>    
            
            -->
            <!--<div style="width: 100%; height: 15px; border: 1px red; background: yellow;">dfdsfs</div>-->
        </header>

        <!-- 
        <nav>
             <a href="#">Ankete</a>            
             <a href="#">Rezultati</a>
             <a href="#">Kontakt</a>   
         </nav>
        -->
        <div style="width: 850px; margin: 0 auto;">
            <?php
            $this->RenderModuleMenuHidden()
            // $this->RenderModuleMenu();
            // $this->RenderModuleMenuHidden();
            ?>
        </div>



        <!--
        <div style=" border: 1px solid red; height: 5px;width:100%;background-color: #a94297;"></div>
        <div style=" border: 1px solid red; height: 5px;width:100%;background: red;"></div>
        -->

        <div id="content">            
            <?php if ($this->modulesLeft != null) { ?>
                <div id ="left_con">
                    <!-- <div class="text">-->
                    <?php $this->RenderModulesLeftCont(); ?>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    <!--</div>-->
                </div>
            <?php } ?>

            <?php if ($this->modulesLeft != null) { ?>
                <div id="center_con_small">
                <?php } else { ?>
                    <div id="center_con_one">
                    <?php } ?>

                    <!--<div class="text">-->
                    <?php
                    include_once 'modules/mod_menu/menu_dubina.php';
                    $this->RenderModules(0);
                    ?>
                    <!--</div>-->
                </div>
            </div><!-- end content-->  
            <div id="footer2">
                <p id="copyright">&copy; Nikola Petrović NRT-161/16</p>
                <ul id="footer_menu">
                    <li><a href="index.php?p=2">Ankete</a></li>
                    <li><a href="index.php?p=12">Rezultati</a></li>
                    <li><a href="index.php?p=2">Kontakt</a></li>                   
                </ul>             
                <span style="width: 100%;" class="linkMail"><a href="mailto:nikolanrt5509@yahoo.com">nikolanrt5509@yahoo.com</a></span>
                <?php
                $end = microtime(true);
                //echo "vreme učitanja stranice: " . ($end - $time) . " seconds \n";
                echo "<div style='padding-left:15px; font-size:12px; color:#666;'>" . ($end - $time) . "</div>";
                ?>
            </div>

            <!--
            <div id="footer2">
                <div id="footer_in">
                    <div style="padding-top: 5px; font-size: 12px; margin: 0 auto; border: 0px solid greenyellow;  display: table;">&copy; Nikola Petrović NRT-55/09</div>
                    <span class="linkMail"><a href="mailto:nikolanrt5509@yahoo.com">nikolanrt5509@yahoo.com</a></span>
            <?php
            $end = microtime(true);
            //echo "vreme učitanja stranice: " . ($end - $time) . " seconds \n";
            echo "<div style='font-size:12px'>" . ($end - $time) . "</div>";
            ?>
                </div>
            </div>
            -->

        </div>

    </body>
</html>

