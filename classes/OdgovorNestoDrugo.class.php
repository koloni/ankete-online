<?php

class OdgovorNestoDrugo extends Odgovor {

    //protected $tekst;
    protected $tekstNestoDrugo;

    public function __construct($idOdgovor, $tekst, $tekstNestoDrugo, $isIzabran = false) {
        parent::__construct($idOdgovor, $tekst, $isIzabran);

        if (is_int($idOdgovor + 0) == false) {
            throw new InvalidArgumentException("Argument 'idOdgovor', konstruktora 'Odgovor',  mora da bude tipa 'integer'!");
        }

        //Ne radi
        if (is_bool((bool) $isIzabran) == false) {
            throw new InvalidArgumentException("Argument '\$IsIzabran', konstruktora 'Odgovor',  mora da bude tipa 'bool'!");
        }

        $this->tekstNestodrugo = $tekstNestoDrugo;
    }

    /*
      public function getIdOdgovor() {
      return $this->idOdgovor;
      }

      public function getTekst() {
      return $this->tekst;
      }
     */

    public function getNestoDrugoTekst() {
        return $this->tekstNestoDrugo;
    }

    public function setNestoDrugoTekst($tekstNetoDrugo) {
        $this->tekstNestoDrugo = $tekstNetoDrugo;
        $this->isIzabran = true;
    }

    /*
      public function setTekst($tekst) {
      $this->tekst = $tekst;
      $this->isIzabran = true;
      }

     */

    /*
      public function unesiOdgovor($tekst) {
      $this->tekst = $tekst;
      $this->isIzabran = true;
      }
     */

    public function getIsIzabran() {
        return $this->isIzabran;
    }

    public function obrisiOdgovor() {
        $this->tekstNestoDrugo = "";
        $this->isIzabran = false;
    }

    public function prikazi() {
        $tacno = "false";
        if ($this->isIzabran) {
            $tacno = "true";
        } else {
            $tacno = "false";
        }
        echo "id:{$this->idOdgovor}. '{$this->tekst}', ($tacno)";
    }

}

