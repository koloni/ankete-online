<?php

abstract class Pitanje {

    private static $redniBroj = 0;
    protected $idKreator;
    protected $idPitanje;
    protected $tekst;
    protected $odgovori;
    protected $idVrstaOdgovora;
    protected $opisVrste;
    protected $stanje;
    protected $isObavezno;
    /*disabled se odnosi na input kontrole i ima dve vrednosti disabled ili '' */
    protected $disabled = '';

    /**
     * Set the From and FromName properties
     * @param int $idPitanje
     * @param int $idKreator
     * @param int $tekst
     * @param bool $isObavezno
     * @throws InvalidArgumentException
     * @return void
     */
    public function __construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje = '0', $disabled = '') {
        //pri konverziji ako je vrednost nije uneta ispravno, konvertovaće je u 0, a to mi ne odgovara
        if (is_integer((int) $idPitanje) == false || $idPitanje == 0) {
            throw new InvalidArgumentException("Argument konstruktora 'idPitanje' mora da bude tipa 'integer'!");
        }

        // echo "Id kreator: ".$idKreator;
        if (is_integer($idKreator + 0) == false || $idKreator == 0) {
            throw new InvalidArgumentException("Argument konstruktora 'idKreator' mora da bude tipa 'integer'!");
        }

        if (isset($tekst) == false) {
            throw new InvalidArgumentException("Argument konstruktora 'tekst' mora da se unese!");
        }

        if (is_int($idVrstaOdgovora + 0) == false || $idVrstaOdgovora == 0) {
            throw new InvalidArgumentException("Argument konstruktora 'idVrstaOdgovora' mora da bude tipa 'integer'!");
        }

        if (is_array($odgovori) == false) {
            throw new InvalidArgumentException("Argument funkcije 'odgovori' mora da bude tipa 'Niz'!");
        }

        if (is_bool((bool) $isObavezno) == false) {
            throw new InvalidArgumentException("Argument funkcije IsObavezno mora da bude tipa 'bool'!");
        }

        $this->idPitanje = $idPitanje;
        $this->idKreator = $idKreator;
        $this->tekst = $tekst;
        $this->odgovori = $odgovori;
        $this->idVrstaOdgovora = $idVrstaOdgovora;
        $this->isObavezno = $isObavezno;
        $this->stanje = $stanje;
        $this->disabled = $disabled;
    }

    function getIdPitanje() {
        return $this->idPitanje;
    }

    function getTekst() {
        return $this->tekst;
    }

    function getOdgovori() {
        return $this->odgovori;
    }

    function izaberiOdgovor($idOdgovor) {
        
    }

    function getIsObavezno() {
        return $this->isObavezno;
    }

    function getIdVrstaOdgovora() {
        return $this->idVrstaOdgovora;
    }

    function getOpisVrste() {
        return $this->opisVrste;
    }

    function getBrojOdgovora() {
        return count($this->odgovori);
    }

    function getStanje() {
        return $this->stanje;
    }

    //static polje se u funkciji poziva sa self::
    public function getRedniBroj() {
        return++self::$redniBroj;
    }

    public static function setRedniBroj($redniBroj) {
        static::$redniBroj = $redniBroj;
    }

    public function getIdKreator() {
        return $this->idKreator;
    }

    public function setDisabled() {
        $this->disabled = 'disabled';
    }

    public function setEnabled() {
        $this->disabled = '';
    }

    abstract function prikazi();



    /*
      function prikaziRadio() {

      echo "<p>" . $this->tekst;
      if ($this->isObavezno == false) {
      echo ' (nije obavezno)';
      }
      echo "</p>";
      // echo '<form>';
      $num = "a";
      foreach ($this->odgovori as $odg) {
      echo "<input type='radio' name='{$this->idPitanje}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>";
      echo "<label for='{$odg['idOdgovor']}'> {$num}. {$odg['tekst']}</label><br/>";
      $num++;
      }
      }
     */



    /*

      function prikaziCheckBox() {
      //echo '<br/>';
      echo "<p>" . $this->tekst;
      if ($this->isObavezno == false) {
      echo ' (nije obavezno)';
      }
      echo "</p>";
      // echo '<form>';
      $num = "a";
      foreach ($this->odgovori as $odg) {
      echo "<input type='checkbox' name='{$this->idPitanje}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>" .
      "<label for='{$odg['idOdgovor']}'> {$num}. {$odg['tekst']}</label><br/>";
      $num++;
      }
      }


     */

    /*

      public function prikaziSelect() {
      echo "<p>" . $this->tekst;
      if ($this->isObavezno == false) {
      echo ' (nije obavezno)';
      }
      echo "</p>";

      $num = "a";
      echo "<select name='{$this->idPitanje}' id='{$this->idPitanje}'>";
      foreach ($this->odgovori as $odg) {
      echo "<option value='{$odg['idOdgovor']}'>{$num}. {$odg['tekst']}</option>";
      $num++;
      }
      echo "</select>";
      echo '<br/>';
      }
     */


    /*
      function prikaziUl() {
      echo '<br/>';
      echo $this->tekst;
      echo '<ul>';
      foreach ($this->odgovori as $odg) {
      echo '<li>' . $odg['tekst'] . '</li>';
      }
      echo '</ul>';
      }
     */
}

//end class Pitanje
?>
