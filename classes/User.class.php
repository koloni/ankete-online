<?php

include_once 'classes/DbBroker.class.php';

class User {

    public $idKorisnik;
    public $username;
    public $email;
    public $state;
    public $type;

    public function __construct($idKorisnik, $username, $email, $type, $state) {
        $this->idKorisnik = $idKorisnik;
        $this->username = $username;
        $this->email = $email;
        $this->state = $state;
        $this->type = $type;
    }

    public static function ConfirmRegistration($token) {
        //DbBroker::Connect();
        //DbBroker::Execute("UPDATE Korisnik set state = 1 where token ='{$token}' and state = -1");
        //if (DbBroker::getNumberOfAffectedRecords() == 1) {
        //   return true;
        //}
        //return false;

        $mysql = new MySQL(DB_NAME, DB_USER, DB_PASS, DB_HOST);
        $mysql->ExecuteSQL("UPDATE Korisnik SET state = 1 WHERE token ='{$token}' AND state = -1");
        if ($mysql->affected == 1) {
            return true;
        }
        return false;
    }

    public static function BeginRegistration($username, $password, $firstName, $lastName, $gender, $birthDate, $address, $city, $phone, $email, $type, $state = -1) {
        $token = md5(time());
        DbBroker::Connect();
        $password = md5($password);
        DbBroker::Execute("INSERT INTO Korisnik (username, password, firstName, lastName, gender, birthDate, address, city, phone, email, type, state, token, registerDate) 
            VALUES ('{$username}', '{$password}', '{$firstName}', '{$lastName}', '{$gender}', '{$birthDate}', '{$address}', '{$city}', '{$phone}', '{$email}', '{$type}', '{$state}', '{$token}', NOW())");
        //DbBroker::Execute("insert into korisnik values (null,'{$username}','{$email}','".md5($password)."',-1,'{$token}')");
        //ako je tip naloga ispitanik, on može da potvrdi registraciju preko, emaila
        if ($type == 0 && $state == -1) {
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->CharSet = 'UTF-8';
            $mail->SMTPSecure = "tls";
            $mail->Mailer = "smtp";
            //ispisuje detalje u vezi slanja imejla
            //$mail->SMTPDebug = 2;
            $mail->SMTPAuth = true;
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->Username = EMAIL;
            $mail->Password = EMAIL_PASS;
            //$mail->SetFrom('noreply@mysite.com', 'Site Admin');
            //$mail->Subject = "Confirm registration";

            $mail->SetFrom('noreply@mysite.com', 'Ankete online');
            //$mail->Subject = "Potvrdi registraciju";            
            $mail->Subject = "Dobrodošli na Ankete online";
            $mail->IsHTML(true);
            //$mail->Body = "<a href='" . (SITE_ROOT . "/register.php?confirm_registration=" . $token) . "'>Molimo potvrdite registraciju sa ovim linkom</a>";
            //$mail->Body = "<a href='" . (SITE_ROOT . "/index.php?p=8&confirm_registration=" . $token) . "'>Molimo potvrdite registraciju sa ovim linkom</a>";
            //$mail->Body = "<h2>Dobrodo&#353;li!</h2><p>Uspe&#353;no ste se registrovali na sajt <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a><p/><p>Kliknite na link <a href='" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "'>" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "</a> za aktivaciju.</p>Dobrodo&#353;li na <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a>";
            $mail->Body = "<h2>Dobrodošli!</h2><p>Uspešno ste se registrovali na sajt <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a><p/><p>Kliknite na link <a href='" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "'>" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "</a> za aktivaciju.</p>Dobrodošli na <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a>";
            $mail->AddAddress($email, $username);
            $mail->Send();
        }
        //BeginRegistration
    }

    public static function BeginResetPassword($email) {
        $token = md5(time());
        DbBroker::Connect();
        DbBroker::Execute("UPDATE Korisnik SET token='{$token}' WHERE email='{$email}'");

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->SMTPSecure = "tls";
        $mail->Mailer = "smtp";
        //ispisuje detalje u vezi slanja imejla
        //$mail->SMTPDebug = 2;
        $mail->SMTPAuth = true;
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 587;
        $mail->Username = EMAIL;
        $mail->Password = EMAIL_PASS;
        //$mail->SetFrom('noreply@mysite.com', 'Site Admin');
        //$mail->Subject = "Confirm registration";

        $mail->SetFrom('noreply@mysite.com', 'Ankete online');
        //$mail->Subject = "Potvrdi registraciju";            
        $mail->Subject = "Resetovanje lozinke";
        $mail->IsHTML(true);

        //$mail->Body = "<h2>Dobrodošli!</h2><p>Uspešno ste se registrovali na sajt <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a><p/><p>Kliknite na link <a href='" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "'>" . (SITE_ROOT2 . "/index.php?p=8&confirm_registration=" . $token) . "</a> za aktivaciju.</p>Dobrodošli na <a href='" . SITE_ROOT2 . "/index.php'>ankete online</a>";
        //Did you request a password reset for your Microsoft account (koloni@hotmail.com)?                
        //If you requested this password reset, go here:
        //https://account.live.com/password/resetconfirm?otc=*CsuNPybIpt0iM7kEJe1NGW3JxTM!mpbzH03CzV9sOrZJIJf1hPfTN6X9iHW*2CzdT05Sz!LuMQct8T9uYnB4PC4$&mn=koloni%40hotmail.com&cxt=Default
        //If you didn't make this request, use this link to cancel it:
        //https://account.live.com/password/resetcancel?otc=*CsuNPybIpt0iM7kEJe1NGW3JxTM!mpbzH03CzV9sOrZJIJf1hPfTN6X9iHW*2CzdT05Sz!LuMQct8T9uYnB4PC4$&mn=koloni%40hotmail.com&cxt=Default
        //Thanks,
        //The Microsoft account team

        $mail->Body = "
            <p>Da li ste zahtevali resetovanje lozinke za Vaš Ankete online nalog?</p>
            <p>Ako ste zahtevali resetovanje lozinke, idite ovde:<br/>
            <a href='" . SITE_ROOT2 . "/index.php?p=3&reset_confirm={$token}&email={$email}'>" . SITE_ROOT2 . "/index.php?p=3&reset_confirm={$token}&email={$email}</a>
            </p>
            
            <p>Ako niste napravili ovaj zahtev, koristi ovaj link da ga poništite:<br/>
            <a href='" . SITE_ROOT2 . "/index.php?p=3&reset_cancel={$token}&email={$email}'>" . SITE_ROOT2 . "/index.php?p=3&reset_cancel={$token}&email={$email}</a>
            </p>                        
            <p>Hvala,<br/>Ankete online</p>";

        $mail->AddAddress($email, $username = "");
        $mail->Send();
        //BeginResetPassword
    }

    public static function Login($username, $password) {
        //dodati deo za ciscenje podataka od karaktera: ' < i >

        $sql = new MySQL(DB_NAME, DB_USER, DB_PASS, DB_HOST);
        //DbBroker::Connect();
        //$user_raw = DbBroker::GetRow("select * from Korisnik where username = '{$username}' and password='{$password}'  and state > -1");

        $user_raw = $sql->ExecuteSQL("SELECT * FROM Korisnik WHERE username = '{$username}' and state > -1");
        //return $user_raw;
        if ($user_raw != null) {
            if ($user_raw['password'] != md5($password) || $user_raw['state'] < 1) {
                return null;
            } else {
                $u = new User($user_raw['idKorisnik'], $user_raw['username'], $user_raw['email'], $user_raw['type'], $user_raw['state']);
                return $u;
            }
        } else {
            return null;
        }
    }

    public function SetSessions0() {
        Session::SetKey("idKorisnik", $this->idKorisnik);
        Session::SetKey("type", $this->type);
        Session::SetKey("username", $this->username);
    }

    public function SetSessions() {
        Session::SetKeyWithSerialize("user", $this);
    }

    /**
     * Provera prava pristupa
     * @param int $typePrivilege

      10 - ne registrovani korisnici (bez redirekcije na 'login' stranu, vraća null)

      -1 - admin,
      1 - službenik
      2 - kreator

      0 - ispitanik, službenik, kreator, admin
      3 - službenik, kreator, admin
      4 - kreator, admin
     * @throws InvalidArgumentException
     * @return User 
      vraća User objekat, ako su prava pristupa Ispravna,
      ili preusmerava na login stranu, ako korisnik nije prijavljen,
      ili preusmerava na index stranu, ako su pogrešna prava pristupa
     */
    public static function checkUserPrivilege($typePrivilege = 0) {
        /*
          -- $user->type --
          -1 -admin,
          0 - ispitanik,
          1 - službenik
          2 - kreator

          --- $typePrivilege --
          -1 -admin,
          0 - ispitanik, službenik, kreator, admin
          1 - službenik
          2 - kreator
          3 - službenik, kreator, admin
          4 - kreator, admin

         */
        $user = Session::GetKeyWitUnserialize('user');
        if ($user == null && $typePrivilege != 10) {
            //header("Location: index.php?p=1");
            new C_login("Morate se prijaviti na sitem!");
            exit();
        } else if ($user == null && $typePrivilege == 10) {
            return null;
        } else if ($user != null && $typePrivilege == 10) {
            return $user;
        } else if ($typePrivilege == 0) {
            //odnosi se na sve korisnike
            if ($user->type == 0 || $user->type == 1 || $user->type == 2 || $user->type == -1) {
                return $user;
            }
            header("Location: index.php");
            exit();
        } else if ($user->type == 1 && $typePrivilege == 1) {
            return $user;
        } else if ($user->type == 2 && $typePrivilege == 2) {
            return $user;
        } else if ($user->type == -1 && $typePrivilege == -1) {
            return $user;
        } else if ($typePrivilege == 3) {
            //3 - službenik, kreator, admin
            if ($user->type == 1 || $user->type == 2 || $user->type == -1) {
                return $user;
            }
            header("Location: index.php");
            exit();
        } else if ($typePrivilege == 4) {
            //4 - kreator, admin
            if ($user->type == 2 || $user->type == -1) {
                return $user;
            }
            header("Location: index.php");
            exit();
        } else {
            header("Location: index.php");
            exit();
        }
    }

//checkUserPrivilege
}