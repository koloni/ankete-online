<?php

class Session {

    public static function SetKey($key, $value) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION[$key] = $value;
    }

    public static function GetKey($key, $default = null) {
        if (!isset($_SESSION)) {
            session_start();
            return $default; ////
        }
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return $default;
        }
    }

    public static function SetKeyWithSerialize($key, $value) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION[$key] = serialize($value);
    }

    public static function GetKeyWitUnserialize($key, $default = null) {
        if (!isset($_SESSION)) {
            session_start();
            return $default;
        }
        if (isset($_SESSION[$key])) {
            return unserialize($_SESSION[$key]);
        } else {
            return $default;
        }
    }

    public static function checkSessionTimeOut() {
        if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1200)) {
            // last request was more than 30 minutes ago
            session_unset();     // unset $_SESSION variable for the run-time 
            session_destroy();   // destroy session data in storage
            include 'controller/C_Login.class.php';
            new C_login('Vaša sesija je istekla! Molimo, prijavite se na sistem ponovo.');
            exit();
            //header('Location: login.php');
        }
        $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
    }

}

