<?php
/**
 * Description of PitanjaAnketa
 *
 * @author korisnik
 */
class PitanjaAnketa {

    private $pitanja;

    function __construct($idAnketa, $od = 0, $brPitanja = 0) {
        $sql = new DbAnkete();
        $this->pitanja = $sql->getPitanjaAnketa($idAnketa, $od, $brPitanja);
    }

    function prikazi() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->prikazi();
            }
        }
    }

    function prikaziDisabled() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->setDisabled();
                $p->prikazi();
            }
        }
    }

}

?>
