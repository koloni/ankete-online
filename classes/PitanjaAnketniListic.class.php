<?php
/**
 * Description of PitanjaAnketniListic
 *
 * @author korisnik
 */
class PitanjaAnketniListic extends Pitanja {
    function __construct($idAnketniListic, $od = 0, $brPitanja = 0) {
        $sql = new DbAnkete();
        $this->pitanja=$sql->getPitanjaAnketniListic($idAnketniListic, $od, $brPitanja);  
    }
}