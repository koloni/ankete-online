<?php

class PitanjeMatricaRadio2 extends Pitanje {

    private $potpitanja;

    public function PitanjeMatricaRadio2($idPitanje, $potpitanja, $idKreator, $tekst, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanja[0]->getOdgovori(), $idVrstaOdgovora, $isObavezno, $stanje);

        $this->potpitanja = $potpitanja;
        $this->opisVrste = "Matrica odgovora, jedan izbor poredu";
    }

    function getPotpitanja() {
        return $this->potpitanja;
    }

    function izaberiOdgovorMatrica($idPotpitanje, $idOdgovor) {
        //$this->kolekcijaPotpitanjaMatrica->izaberiOdgovor($idPotpitanje, $idOdgovor);
        foreach ($this->potpitanja as $pot) {
            if ($pot->getIdPitanje() == $idPotpitanje) {
                $pot->izaberiOdgovor($idOdgovor);
            }
        }
    }

    function izaberiOdgovor($idOdgovor) {
        
    }

    function prikazi() {
        echo "<div class='pitanje'>";
        // echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        // echo "</p>";
        // echo "<div style='padding: 0 0 0 15px;'>";
        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";
        //echo "<div style='padding:5px 0 0 20px;'><span style='font-size:10px;'>Izaberite jedan odgovor za svaku vrstu</span></div>";        
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:12px;font-style: italic; font-family: Trebuchet MS;'>Izaberite po jedan odgovor za sve delove pitanja</span></div>";         
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";

        // echo "<table border='0px' class='table_pitanje'>";
        echo "<table border='0px' cellspacing='0' width='600px' class='table_pitanje'>";
        echo "<tr><td></td>";

        $odgovori = $this->potpitanja[0]->getOdgovori();
        //prikazuje podunjene odgovore (prvi red tabele)   
        foreach ($odgovori as $odg) {
            //echo "<td width='100px'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
            echo "<td style='alignment-adjust: central;'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
        }
        echo "</tr>";
        $i = 0; //zbog parnih i neparnih kolona
        //prikazuje kolonu potpitanja 
        foreach ($this->potpitanja as $pot) {

            echo "<tr style='color: black;" . ($i++ % 2 == 0 ? 'background-color: lightgrey;' : '') . "'>";
            //echo '<tr>';
            //echo "<td align='right'>";
            //echo "<td>";
            echo "<td style='text-align:left;'>";
            echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPitanje()}gr'></div>";
            echo $num . ") " . $pot->getTekst();
            echo "</td>";

            //prikazuje checkbox kontrole
            foreach ($pot->getOdgovori() as $odg) {
                //proverava da li je izabran odgovor
                if ($odg->getIsIzabran()) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                //možda bi id trebalo da bude {$pot->getIdPotpitanje()}.$odg->getIdOdgovor()
                //echo "<td width='100px'>";
                echo "<td>";
                echo "<input {$this->disabled} type='radio' name='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPitanje()}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

}