<?php

class PitanjeSelect extends Pitanje {

    private $brojIzabranihOdgovora = 0;

    public function PitanjeSelect($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->opisVrste = "Izbor jednog odgovora od više ponuđenih (select kontrola)";
    }

    public function getBrojIzabranihOdgovora() {
        return $this->brojIzabranihOdgovora;
    }

    public function izaberiOdgovor($idOdgovor) {
        if (is_int((int) $idOdgovor) == false || $idOdgovor == 0) {
            throw new InvalidArgumentException("Argument konstruktora '\$idOdgovor' mora da bude tipa 'integer'!");
        }

        if ($this->brojIzabranihOdgovora == 1) {
            throw new Exception("Ne možete izabrati više od jednog odgovora za pitanje tipa 'PitanjeSelect'!");
        }

        //U odgovorima provera da li izabrani odgovor postoji
        foreach ($this->odgovori as $odgovor) {
            if ($odgovor->getIdOdgovor() == $idOdgovor) {
                $odgovor->izaberiOdgovor();
                $this->brojIzabranihOdgovora++;
                return;
            }
        }
        throw new Exception("Ne postoji odgovor za izabrani '\$idOdgovor'!");
    }

    public function prikazi() {
        echo "<div class='pitanje'>";
        //echo "<p>" . $this->tekst;
        //$isObavezno = "";
        //if ($this->isObavezno) {
        //    $isObavezno = "<font size='4'>* </font>";
        //}
        //echo "<p><b>" . $this->getRedniBroj() . ". " . $isObavezno . $this->tekst . "</b></p>";
        //echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}gr'></div>";
        //echo "<div style='padding: 0 0 0 15px;'>";

        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";        
        echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}gr'></div>";
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:10px;'>Izaberite jedan odgovor</span></div>";
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";
        echo "<select {$this->disabled} name='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}' id='{$this->idPitanje}' onclick='obrisiGresku(this);'>";
        //echo "<option disabled='true'> - - - Izaberite odgovor - - - </option>";        
        echo "<option value='0'> - - - Izaberite odgovor - - - </option>";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            echo "<option value='{$odg->getIdOdgovor()}' {$selected}>{$num}. {$odg->getTekst()}</option>";
            $num++;
        }
        echo "</select>";
        echo '</div>';
        echo '</div>';
    }

    function prikaziStaro() {
        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input type='checkbox' name='{$this->idPitanje}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked}>" .
            "<label for='{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }
    }

}

/*
  class PitanjeSelect extends Pitanje {

  public function PitanjeSelect($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno) {
  parent::Pitanje($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno);
  }

  public function prikazi() {
  echo "<p>" . $this->tekst;
  if ($this->isObavezno == false) {
  echo ' (nije obavezno)';
  }
  echo "</p>";

  $num = "a";
  echo "<select name='{$this->idPitanje}' id='{$this->idPitanje}'>";
  foreach ($this->odgovori as $odg) {
  echo "<option value='{$odg['idOdgovor']}'>{$num}. {$odg['tekst']}</option>";
  $num++;
  }
  echo "</select>";
  }

  } */
?>