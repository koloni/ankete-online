<?php

class PitanjeSlobodanUnosTekst extends PitanjeSlobodanUnos {

    public function __construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->opisVrste = "Slobodan unos kratkog teksta (jedno ili više polja)";
    }

    function unesiOdgovor($idPotpitanje, $tekst) {
        //neka provera unetog teksta, npr neki trim
        $tekst = trim($tekst);
        if (strlen($tekst) < 1) {
            throw new LengthException("Argumnet '\$tekst', funkcije 'unesiOdgovor', mora da sadrži miminum 1 karakter");
        }
        parent::unesiOdgovor($idPotpitanje, $tekst);
    }

    function addPotpitanjeUnos($potpitanjeUnos) {
        parent::addPotpitanjeUnos($potpitanjeUnos);
    }

    function getPotpitanja() {
        return parent::getPotpitanja();
    }

    function getBrojPotpitanja() {
        return parent::getBrojPotpitanja();
    }

    function prikazi() {
        parent::prikazi();
    }

}

?>