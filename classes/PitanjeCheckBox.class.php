<?php

class PitanjeCheckBox extends Pitanje {

    private $brojIzabranihOdgovora = 0;    

    public function PitanjeCheckBox($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);        
        $this->opisVrste = "Izbor jednog odgovora od više ponuđenih (čekboksovi)";
    }

    public function getBrojIzabranihOdgovora() {
        return $this->brojIzabranihOdgovora;
    }

    public function izaberiOdgovor($idOdgovor) {
        if (is_numeric($idOdgovor) == false) {
            throw new InvalidArgumentException("Argument konstruktora '\$idOdgovor' mora da bude tipa 'integer'!");
        }
        //To test if a variable is a number or a numeric string (such as form input, which is always a string), you must use is_numeric().
        if (is_int((int) $idOdgovor) == false || $idOdgovor == 0) {
            //is_nan($val)
            throw new InvalidArgumentException("Argument konstruktora '\$idOdgovor' mora da bude tipa 'integer'!");
        }

        //terbam neku proveru koliko je ogovora izbrano
        //if ($this->brojIzabranihOdgovora == 1) {
        //    throw new Exception("Ne možete izabrati više od jednog odgovora za pitanje tipa 'PitanjeRadio' !");
        //}
        //U odgovorima provera da li izabrani odgovor postoji
        foreach ($this->odgovori as $odgovor) {
            if ($odgovor->getIdOdgovor() == $idOdgovor) {
                $odgovor->izaberiOdgovor();
                $this->brojIzabranihOdgovora++;
                return;
            }
        }
        throw new Exception("Ne postoji odgovor za izabrani '\$idOdgovor'!");
    }

    function prikazi() {
        ?> <div style="font-weight:bold;"></div> <?php
        echo "<div class='pitanje'>";
        //echo "<p>" . $this->tekst;
        //echo "<b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";
        echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]gr'></div>";
        //echo "<div style='padding:5px 0 0 20px;'><span style='font-size:10px;'>Izaberite jedan ili više odgovora</span></div>";
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:12px; font-style: italic; font-family: Trebuchet MS;'>Izaberite jedan ili više odgovora</span></div>";
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input {$this->disabled} type='checkbox' name='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]' value='{$odg->getIdOdgovor()}' id='odg{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>" .
            "<label for='odg{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }

        echo '</div>';
        echo "</div>";
    }

    function prikazi0() {
        echo "<div class='pitanje'>";
        //echo "<p>" . $this->tekst;
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input type='checkbox' name='{$this->idPitanje}[]' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked}>" .
            "<label for='{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }
        echo '</div>';
        echo "</div>";
    }

//end class
}
