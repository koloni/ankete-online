<?php

/*
 * Copyright (C) 2012
 * Ed Rackham (http://github.com/a1phanumeric/PHP-MySQL-Class)
 * Changes to Version 0.8.1 copyright (C) 2013
 * Christopher Harms (http://github.com/neurotroph)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'config.php';

// MySQL Class v0.8.1
class MySQL {

// Base variables
    var $lastError; // Holds the last error
    var $lastQuery; // Holds the last query

    /**
     * Holds the MySQL query result         
     */
    var $result; // Holds the MySQL query result

    /**
     * Holds the total number of records returned
     */
    protected $records; // Holds the total number of records returned
    var $affected; // Holds the total number of records affected

    /**
     * Sadrži poslednji rezultat
     * @var array
     */
    var $rawResults; // Holds raw 'arrayed' results
    var $arrayedResult; // Holds an array of the result
    var $hostname; // MySQL Hostname
    var $username; // MySQL Username
    var $password; // MySQL Password
    var $database; // MySQL Database
    var $databaseLink; // Database Connection Link


	
	
	
    
    /** Sadrži broj rezultata */
    function getRecords() {
        return $this->records;
    }

    /********************
     * Class Constructor*
     ********************/

    //DB_NAME, DB_USER, DB_PASS, DB_HOST);
    function __construct($database = DB_NAME, $username = DB_USER, $password = DB_PASS, $hostname = DB_HOST) {
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
		
		
        //$this->hostname = $hostname;
		
		//$this->hostname = "mysql:host=".192.168.1.62;dbname=dbAnkete;charset=utf8";
		$this->hostname = "mysql:host=".$hostname.";dbname=".$database.";charset=utf8";

        $this->Connect(true);
    }

    /********************
     * Private Functions*
     ********************/

// Connects class to database
// $persistant (boolean) - Use persistant connection?
    private function Connect($persistant = false) {
        $this->databaseLink=new PDO($this->hostname, $this->username, $this->password);
		
		/*
		if ($this->databaseLink) {
            mysql_close($this->databaseLink);
        }

        if ($persistant) {
            //$this->databaseLink = mysql_pconnect($this->hostname, $this->username, $this->password);
			
			//$pdo = new PDO($dsn, $user, $pass, $opt);
			//$db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4', 'username', 'password');
			$this->databaseLink=new PDO($this->hostname, $this->username, $this->password);
			
        } else {
            //$this->databaseLink = mysql_connect($this->hostname, $this->username, $this->password);
			$this->databaseLink=new PDO($this->hostname, $this->username, $this->password);
        }
		*/
	
		/*
        if (!$this->databaseLink) {
            $this->lastError = 'Could not connect to server: ' . mysql_error($this->databaseLink);
            return false;
        }
		
		*/
/*
        if (!$this->UseDB()) {
            $this->lastError = 'Could not connect to database: ' . mysql_error($this->databaseLink);
            return false;
        }
		*/

        //bez ovoga ne može iz baze da isčita UTF-8 karaktere
        //mysql_query("SET NAMES 'utf8'", $this->databaseLink);
        //može i ovo
        //mysql_query("SET NAMES 'utf8'");
        return true;
    }

    //Select database to use
    private function UseDB() {
        if (!mysql_select_db($this->database, $this->databaseLink)) {
            $this->lastError = 'Cannot select database: ' . mysql_error($this->databaseLink);
            return false;
        } else {
            return true;
        }
    }

// Performs a 'mysql_real_escape_string' on the entire array/string
    /*private function SecureData($data) {
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                if (!is_array($data[$key])) {
                    $data[$key] = mysql_real_escape_string($data[$key], $this->databaseLink);
                }
            }
        } else {
            $data = mysql_real_escape_string($data, $this->databaseLink);
        }
        return $data;
    }*/

    /********************
     * Public Functions *
     *******************/

// Executes MySQL query
    function ExecuteSQL($query) {
	//$stmt = $db->query('SELECT * FROM table');
 			
        $this->lastQuery = $query;
					
        //if ($this->result = mysql_query($query, $this->databaseLink) or die("Greška baze: " . mysql_error())) {
		
		/*
		if ($this->result =$this->databaseLink->query($query) or die("Greška baze: " . $db->errorInfo())) {
            //$this->records = $this->databaseLink->rowCount();
            //$this->affected = $this->databaseLink->rowCount();
			$row=array();
			while($row = $this->result->fetch(PDO::FETCH_ASSOC)) {}
        }
		*/
        /*
     
               //var_dump($query);
               //exit();
     */          
               
			$this->result =$this->databaseLink->query($query);
                        $this->records = $this->result->rowCount();
            //$this->affected = $this->databaseLink->rowCount();
			$row=array();			
			//while($row = $this->result->fetch(PDO::FETCH_ASSOC)) {}
			
			if($this->records==1){
				//$row= $this->result->fetch(PDO::FETCH_ASSOC);	
				
				
				//$row= $this->result->fetchAll();
                                
				return $this->affected=$this->arrayedResult=$this->result->fetch(PDO::FETCH_ASSOC);	
				//return $this->result->fetch(PDO::FETCH_ASSOC);	
										
			}else if($this->records>1){
			
				//Ovaj kod je radio ali mi je na kraju dodavao jedan prazan red na kraju niza
				//while($row[] = $this->result->fetch(PDO::FETCH_ASSOC)) {}	
				
				//while($row[] = $this->result->fetchAll()) {}					
				//return $this->result->fetch(PDO::FETCH_ASSOC);
				//return $this->result->fetchAll();
				
				//$this->arrayedResult=$this->result->fetchAll();
				//vratice sve rezultate kao dvodimenzionalni niz							
				//return $this->result->fetchAll();
				
				return $this->arrayedResult=$this->result->fetchAll();
				//return $this->arrayedResult;
								
			}else{
				return true;
			}
			
		
		//return $row;
    }
	
	
	// Executes MySQL query
    function ExecuteSQLEXEC($query) {
        $this->lastQuery = $query;
        //if ($this->result = mysql_query($query, $this->databaseLink) or die("Greška baze: " . mysql_error())) {
		if ($this->result =$this->databaseLink->exec(query) or die("Greška baze: " . $db->errorInfo())) {
            $this->records = $this->result;
            $this->affected = $this->result;			
        }
    }
	

// Adds a record to the database based on the array key names
    function Insert($vars, $table, $exclude = '') {

// Catch Exclusions
        if ($exclude == '') {
            $exclude = array();
        }

        array_push($exclude, 'MAX_FILE_SIZE'); // Automatically exclude this one
    // Prepare Variables
        //$vars = $this->SecureData($vars);

        $query = "INSERT INTO `{$table}` SET ";
        foreach ($vars as $key => $value) {
            if (in_array($key, $exclude)) {
                continue;
            }
//$query .= '`' . $key . '` = "' . $value . '", ';
            $query .= "`{$key}` = '{$value}', ";
        }

        $query = substr($query, 0, -2);
        
        return $this->ExecuteSQL_CHANGE($query);
    }
    
    function ExecuteSQL_CHANGE($query){        
        return $this->affected = $this->databaseLink->exec($query);        
    }



// Deletes a record from the database
    function Delete($table, $where = '', $limit = '', $like = false) {
        $query = "DELETE FROM `{$table}` WHERE ";
        if (is_array($where) && $where != '') {
// Prepare Variables
            //$where = $this->SecureData($where);

            foreach ($where as $key => $value) {
                if ($like) {
//$query .= '`' . $key . '` LIKE "%' . $value . '%" AND ';
                    $query .= "`{$key}` LIKE '%{$value}%' AND ";
                } else {
//$query .= '`' . $key . '` = "' . $value . '" AND ';
                    $query .= "`{$key}` = '{$value}' AND ";
                }
            }

            $query = substr($query, 0, -5);
        }

        if ($limit != '') {
            $query .= ' LIMIT ' . $limit;
        }

        return $this->ExecuteSQL_CHANGE($query);
    }

    // Gets a single row from $from where $where is true
    function Select($from, $where = '', $orderBy = '', $limit = '', $like = false, $operand = 'AND') {
    // Catch Exceptions
        if (trim($from) == '') {
            return false;
        }

        $query = "SELECT * FROM `{$from}` WHERE ";

        if (is_array($where) && $where != '') {
     // Prepare Variables
            
            //odustao
            // $where = $this->SecureData($where);

            foreach ($where as $key => $value) {
                if ($like) {
//$query .= '`' . $key . '` LIKE "%' . $value . '%" ' . $operand . ' ';
                    $query .= "`{$key}` LIKE '%{$value}%' {$operand} ";
                } else {
//$query .= '`' . $key . '` = "' . $value . '" ' . $operand . ' ';
                    $query .= "`{$key}` = '{$value}' {$operand} ";
                }
            }

            $query = substr($query, 0, -(strlen($operand) + 2));
        } else {
            $query = substr($query, 0, -7);
        }

        if ($orderBy != '') {
            $query .= ' ORDER BY ' . $orderBy;
        }

        if ($limit != '') {
            $query .= ' LIMIT ' . $limit;
        }

             //      var_dump($query);
           //    exit();
        return $this->ExecuteSQL($query);
    }

// Updates a record in the database based on WHERE
    function Update($table, $set, $where, $exclude = '') {
// Catch Exceptions
        if (trim($table) == '' || !is_array($set) || !is_array($where)) {
            return false;
        }
        if ($exclude == '') {
            $exclude = array();
        }

        array_push($exclude, 'MAX_FILE_SIZE'); // Automatically exclude this one

        /*
        $set = $this->SecureData($set);
        $where = $this->SecureData($where);
         
         */

// SET

        $query = "UPDATE `{$table}` SET ";

        foreach ($set as $key => $value) {
            if (in_array($key, $exclude)) {
                continue;
            }
            $query .= "`{$key}` = '{$value}', ";
        }

        $query = substr($query, 0, -2);

// WHERE

        $query .= ' WHERE ';

        foreach ($where as $key => $value) {
            $query .= "`{$key}` = '{$value}' AND ";
        }

        $query = substr($query, 0, -5);

        return $this->ExecuteSQL_CHANGE($query);
    }

// 'Arrays' a single result
    function ArrayResult() {
        $this->arrayedResult = mysql_fetch_assoc($this->result) or die(mysql_error($this->databaseLink));
        return $this->arrayedResult;
    }

// 'Arrays' multiple result
    function ArrayResults0() {

        if ($this->records == 1) {
            return $this->ArrayResult();
        }

        $this->arrayedResult = array();
        while ($data = mysql_fetch_assoc($this->result)) {
            $this->arrayedResult[] = $data;
        }
        return $this->arrayedResult;
    }

// 'Arrays' multiple results with a key
    function ArrayResultsWithKey($key = 'id') {
        if (isset($this->arrayedResult)) {
            unset($this->arrayedResult);
        }
        $this->arrayedResult = array();
        while ($row = mysql_fetch_assoc($this->result)) {
            foreach ($row as $theKey => $theValue) {
                $this->arrayedResult[$row[$key]][$theKey] = $theValue;
            }
        }
        return $this->arrayedResult;
    }

}

?>
