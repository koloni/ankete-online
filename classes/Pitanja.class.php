<?php

/**
 * Description of Pitanja
 *
 * @author korisnik
 */
abstract class Pitanja {

    protected $pitanja;

    function __construct() {     
    }

    function prikazi() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->prikazi();
            }
        }
    }

    function prikaziDisabled() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->setDisabled();
                $p->prikazi();
            }
        }
    }

    function getPitanja() {
        return $this->pitanja;
    }

}

