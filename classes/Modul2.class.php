<?php

class Modul2 {

    public $name, $view, $data;

    public function __construct($name, $view, $data) {
        $this->name = $name;
        $this->data = $data;
        $this->view = $view;
    }

    public function Render() {
        //treba provera da li je null
        if (is_array($this->data)) {
            extract($this->data);
        }
        include MODULES_DIR . "/" . $this->name . "/" . $this->view;
    }

}
