<?php

class PitanjeCheckBox3 extends Pitanje {

    private $odgovori = array();

    public function PitanjeCheckBox($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::Pitanje($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
    }

    /*
      public function izaberiOdgovor($idOdgovor) {
      if(is_int((int)$idOdgovor)==false || $idOdgovor==0) {
      throw new InvalidArgumentException("Argument konstruktora 'idOdgovor' mora da bude tipa 'integer'!");
      }

      //Prolazi kroz sve odgovore i provera da li izabrani odgovor postoji
      foreach ($this->odgovori as $odgovor) {
      if($odgovor["idOdgovora"]==$idOdgovor) {

      }


      }

      }
     */

    public function izaberiOdgovor($idOdgovor) {
        if (is_int((int) $idOdgovor) == false || $idOdgovor == 0) {
            throw new InvalidArgumentException("Argument konstruktora 'idOdgovor' mora da bude tipa 'integer'!");
        }

        //Prolazi kroz sve odgovore i provera da li izabrani odgovor postoji
        foreach ($this->odgovori as $odgovor) {
            if ($odgovor->getIdOdgovor == $idOdgovor) {
                $odgovor->setIsIzabran(true);
            }
        }
    }

    function prikazi() {
        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            echo "<input type='checkbox' name='{$this->idPitanje}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}' checked>" .
            "<label for='{$odg['idOdgovor']}'> {$num}. {$odg['tekst']}</label><br/>";
            $num++;
        }
    }

}

//end class
?>
