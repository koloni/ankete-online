<?php

/**
 * Description of Controller
 *
 * @author korisnik
 */
abstract class Controller {

    public $data = array();

    protected function loadPage($page, $data = null) {
        include_once $page . '.php';
    }

}
