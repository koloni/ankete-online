<?php

class Page {

    public $title;
    public $template;
    public $modules = array();
    public $modules_raw;
    public $modulesMenu = array();
    public $moduleMenuHidden;
    public $moduleMenuLoginLogout;
    public $modulesCenter = array();
    public $modulesLeft = array();

    public function __construct($title, $modules, $template) {
        $this->title = $title;
        $this->template = $template;
        $this->modules_raw = $modules;
    }

    public function Render() {
        include TEMPLATES_DIR . "/" . $this->template . "/index.php";
    }

    //za svaku kolonu kolonu učitaj module
    public function RenderModules($column) {
        foreach ($this->modules_raw as $modul) {
            $modul->Render();
        }
    }

    public function AddModuleMenuLoginLogout($moduleMenuLoginLogout) {
        $this->moduleMenuLoginLogout = $moduleMenuLoginLogout;
    }

    public function AddModuleMenu($moduleMenu) {
        $this->modulesMenu[] = $moduleMenu;
    }

    public function AddModuleMenuHidden($moduleMenuHidden) {
        $this->moduleMenuHidden = $moduleMenuHidden;
    }

    public function AddModuleCenterCont($moduleCenter) {
        $this->modulesCenter[] = $moduleCenter;
    }

    public function AddModuleLeftCont($moduleLeft) {
        $this->modulesLeft[] = $moduleLeft;
    }

    public function RenderModuleMenu() {

        foreach ($this->modulesMenu as $menu) {
            $menu->render();
        }
        // $this->modulesMenu->Render();
    }

    public function RenderModuleMenuHidden() {
        if ($this->moduleMenuHidden != null) {
            $this->moduleMenuHidden->render();
        } else {
            return null;
        }
    }

    public function RenderModulesCenterCont() {
        $this->modulesCenter->Render();
    }

    public function RenderModulesMenuLoginLogut() {
        if ($this->moduleMenuLoginLogout != null) {
            $this->moduleMenuLoginLogout->Render();
        }
    }

    public function RenderModulesLeftCont() {
        if ($this->modulesLeft != null) {
            $this->modulesLeft->Render();
        }
    }

}

