<?php

class Odgovor {

    protected $idOdgovor;
    protected $tekst;
    protected $isIzabran;

    public function __construct($idOdgovor, $tekst, $isIzabran = false) {

        if (is_int($idOdgovor + 0) == false || $idOdgovor === '0') {
            throw new InvalidArgumentException("Argument 'idOdgovor', konstruktora 'Odgovor',  mora da bude tipa 'integer'!");
        }

        if (strlen($tekst) < 1) {
            throw new LengthException("Argument '\$tekst', konstruktora 'Odgovor',  mora da sadrži minimu 1 karakter");
        }

        //Ne radi
        if (is_bool((bool) $isIzabran) == false) {
            throw new InvalidArgumentException("Argument '\$IsObavezno', konstruktora 'Odgovor',  mora da bude tipa 'bool'!");
        }

        $this->tekst = $tekst;
        $this->isIzabran = $isIzabran;
        $this->idOdgovor = $idOdgovor;
    }

    public function getIdOdgovor() {
        return $this->idOdgovor;
    }

    public function getTekst() {
        return $this->tekst;
    }

    public function getIsIzabran() {
        return $this->isIzabran;
    }

    public function izaberiOdgovor() {
        // if (is_bool((bool) $isIzabran) == false) {
        //    throw new InvalidArgumentException("Argument funkcije 'isIzabran' mora da bude tipa 'bool'!");
        //}
        $this->isIzabran = true;
    }

    public function ponistiOdgovor() {
        $this->isIzabran = false;
    }

    public function prikazi() {
        $tacno = "false";
        if ($this->isIzabran) {
            $tacno = "true";
        } else {
            $tacno = "false";
        }
        echo "id:{$this->idOdgovor}. '{$this->tekst}', ($tacno)";
    }

    //class Odgovor
}
