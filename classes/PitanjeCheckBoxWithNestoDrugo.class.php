<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PitanjeCheckBoxWithNestoDrugo
 *
 * @author korisnik
 */
class PitanjeCheckBoxWithNestoDrugo extends PitanjeCheckBox {

    protected $nestoDrugo = null;

    public function PitanjeCheckBoxWithNestoDrugo($idPitanje, $idKreator, $tekst, $odgovori, $nestoDrugo, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::PitanjeCheckBox($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->nestoDrugo = $nestoDrugo;
    }

    public function getIsSetNestoDrugo() {
        return $this->nestoDrugo != null ? $this->nestoDrugo->getIsIzabran() : false;
    }

    public function getNestoDrugoTekst() {
        return $this->nestoDrugo != null ? $this->nestoDrugo->getNestoDrugoTekst() : false;
    }

    public function setNestoDrugoTekst($tekst) {
        $this->nestoDrugo != null ? $this->nestoDrugo->setNestoDrugoTekst($tekst) : "";
    }
    
    public function getNestoDrugo(){
        return $this->nestoDrugo;
    }

    public function getBrojIzabranihOdgovora() {
        //echo parent::getBrojIzabranihOdgovora()+$this->getIsSetNestoDrugo()?1:0;
        return parent::getBrojIzabranihOdgovora() + $this->getIsSetNestoDrugo() ? 1 : 0;
    }

    function prikazi() {
        ?> <div style="font-weight:bold;"></div> <?php
        echo "<div class='pitanje'>";
        //echo "<p>" . $this->tekst;
        //echo "<b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";
        echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]gr'></div>";
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:10px;'>Izaberite jedan ili više odgovora</span></div>";
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input {$this->disabled} type='checkbox' name='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]' value='{$odg->getIdOdgovor()}' id='odg{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>" .
            "<label for='odg{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }
        if ($this->nestoDrugo !== null) {
            if ($this->nestoDrugo->getIsIzabran()) {
                $checkedNestoDrugo = "checked";
            } else {
                $checkedNestoDrugo = "";
            }
            echo "<label for='{$this->idPitanje}nestoDrugo'><input {$this->disabled} type='checkbox' name='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]' value='1_{$this->nestoDrugo->getIdOdgovor()}' id='{$this->idPitanje}nestoDrugo' {$checkedNestoDrugo} onclick='obrisiGresku(this);'> {$this->nestoDrugo->getTekst()}</label> ";
            ?><br/><input style="margin-left: 25px; width: 200px;" <?php echo $this->disabled; ?> type='text' name='<?php echo ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]"; ?>' value='<?php echo $this->nestoDrugo->getNestoDrugoTekst(); ?>' <?php echo $checked; ?> onclick="<?php echo "javascript:document.getElementById('{$this->idPitanje}nestoDrugo').checked=true; obrisiGresku(this);"; ?>">
            <br/>
            <?php
        }
        echo '</div>';
        echo "</div>";
    }

    //class PitanjeCheckBoxWithNestoDrugo
}

