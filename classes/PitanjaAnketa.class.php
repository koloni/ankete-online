<?php

/**
 * Description of PitanjaAnketa
 *
 * @author korisnik
 */
class PitanjaAnketa extends Pitanja {

    function __construct($idAnketa, $od = 0, $brPitanja = 0) {
        $sql = new DbAnkete();
        $this->pitanja = $sql->getPitanjaAnketa($idAnketa, $od, $brPitanja);
    }

}
