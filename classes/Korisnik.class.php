<?php

/**
 * Description of Korisnik
 *
 * @author korisnik
 */
class Korisnik {

    public $idKorisnik;
    public $username;
    public $firstName;
    public $lastName;
    public $gender;
    public $birthDate;
    public $address;
    public $city;
    public $phone;
    public $email;
    public $type;
    public $typeName;
    public $state;
    public $stateName;
    public $registerDate;

    public function __construct($idKorisnik, $username, $firstName, $lastName, $gender, $birthDate, $address, $city, $phone, $email, $type, $typeName, $state, $stateName, $registerDate) {
        $this->idKorisnik = $idKorisnik;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->birthDate = $birthDate;
        $this->address = $address;
        $this->city = $city;
        $this->phone = $phone;
        $this->email = $email;
        $this->type = $type;
        $this->typeName = $typeName;
        $this->state = $state;
        $this->stateName = $stateName;
        $this->registerDate = $registerDate;
    }

//end class Korisnik
}

