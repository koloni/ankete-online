<?php

class PitanjeCheckBox2 extends Pitanje {

    public function PitanjeCheckBox2($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::Pitanje($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
    }

    function prikazi() {
        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            echo "<input type='checkbox' name='{$this->idPitanje}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>" .
            "<label for='{$odg['idOdgovor']}'> {$num}. {$odg['tekst']}</label><br/>";
            $num++;
        }
    }

}

//end class
?>
