<?php

class OdgovorNestoDrugo {

    protected $idOdgovor;
    protected $tekst;
    protected $isIzabran;

    public function OdgovorNestoDrugo($idOdgovor = 0, $tekst = "", $isIzabran = false) {
        if (is_int($idOdgovor + 0) == false) {
            throw new InvalidArgumentException("Argument 'idOdgovor', konstruktora 'Odgovor',  mora da bude tipa 'integer'!");
        }

        //Ne radi
        if (is_bool((bool) $isIzabran) == false) {
            throw new InvalidArgumentException("Argument '\$IsObavezno', konstruktora 'Odgovor',  mora da bude tipa 'bool'!");
        }

        $this->tekst = $tekst;
        $this->isIzabran = $isIzabran;
        $this->idOdgovor = $idOdgovor;
    }

    public function getIdOdgovor() {
        return $this->idOdgovor;
    }

    public function getTekst() {
        return $this->tekst;
    }

    public function setTekst($tekst) {
        $this->tekst = $tekst;
        $this->isIzabran = true;
    }

    public function getIsIzabran() {
        return $this->isIzabran;
    }

    public function unesiOdgovor($tekst) {
        $this->tekst = $tekst;
        $this->isIzabran = true;
    }

    public function obrisiOdgovor() {
        $this->tekst = "";
        $this->isIzabran = false;
    }

    public function prikazi() {
        $tacno = "false";
        if ($this->isIzabran) {
            $tacno = "true";
        } else {
            $tacno = "false";
        }
        echo "id:{$this->idOdgovor}. '{$this->tekst}', ($tacno)";
    }

}

