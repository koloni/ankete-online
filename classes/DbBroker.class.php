<?php
class DbBroker {
    public static $connection;
    private static $records = 0;
    private static $results;
    private static $affected = 0;
    public static function getNumberOfRecords() {
        return self::$records;
    }
    public static function getNumberOfAffectedRecords() {
        return self::$affected;
    }
    public static function Connect() {
        self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        self::$connection->set_charset("utf8");
    }
    public static function Execute($query) {
        self::$results = self::$connection->query($query) or die("Greška baze:" . mysql_error());
        //self::records = @mysql_num_rows(self::$connection);
        self::$records = @mysql_num_rows(self::$results);
        // self::$affected = @mysql_affected_rows(self::$connection);
        self::$affected = @mysql_affected_rows();        
    }
    public static function GetString($query) {
        self::$r = self::$connection->query($query) or die("Greška baze:" . mysql_error());
        if (self::$r == true) {          
            if (@mysql_num_rows(self::$r) > 0) {               
                $res_arr = self::$r->fetch_row();
                return $res_arr[0];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    public static function GetRow($query) {
        $r = self::$connection->query($query) or die("Greška baze:" . mysql_error());
        return $r->fetch_row();
        //return $r->mysql_fetch_assoc();                
        //return self::$results->mysql_fetch_assoc();
    }
    public static function GetTable($query) {
        $r = self::$connection->query($query) or die("Greška baze:" . mysql_error());

        $res = array();
        while ($rw = $r->fetch_row()) {
            $res[] = $rw;
        }
        return $res;
    }
    public static function Disconnect() {
        self::$connection->close();
    }
}
