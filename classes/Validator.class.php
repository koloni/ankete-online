<?php

class Validator {

    public static function Numeric($input) {
        $exp = "/[0-9]+/";
        return preg_match($exp, $input);
    }

    public static function Email($input) {
        $exp = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
        return preg_match($exp, $input);
    }

    public static function Phone($input) {
        // $exp = "/[0-9]{3}\/[0-9]{3}-[0-9]{4}/";
        $exp = "/^\d{2,}((\/|-)\d{2,})?(-\d{2,})?(-\d{2,})?(-\d{2,})?(-\d{2,})?$/";
        return preg_match($exp, $input);
    }

    public static function Text($input, $min = 3, $max = 50) {
        //        $exp = "/^([A-Z]|[a-џ]|\s?){{$min},{$max}}$/";
        //$exp = "/^([A-Z]|[a-z]){3,}$/"; 
        $exp = "/^(([A-Z]|[a-џ]|[0-9])|(\s|\.|\!|\?|\@)){{$min},{$max}}$/";
        //   $exp = "/^([A-Za-z0-9.?!,@$#-_])$/";
        // $exp = "/[A-Za-z0-9 .'?!,@$#-_]/";
        return preg_match($exp, $input);
    }

    public static function Alphanumeric($input, $min = 3, $max = 30) {
        $exp = "/^\w{{$min},{$max}}$/";
        return preg_match($exp, $input);
    }

    public static function AlphanumericUTF($input, $min = 3, $max = 30) {
        $exp = "/^([A-Z]|[a-џ]|\d){{$min},{$max}}$/";
        return preg_match($exp, $input);
    }

    public static function AlphabetUTF($input, $min = 3, $max = 30) {
        $exp = "/^([A-Z]|[a-џ]){{$min},{$max}}$/";
        //$exp = "/^([A-Z]|[a-џ]){3,}$/"; 
        return preg_match($exp, $input);
    }

    public static function Alphabet($input, $min = 3, $max = 30) {
        $exp = "/^([A-Z]|[a-z]){{$min},{$max}}$/";
        return preg_match($exp, $input);
    }

//proverno u java scriptu a u php-u nisam jos, ali samo u sredini
    public static function AlphabetWithOneSpace($input, $min = 3, $max = 30) {
        $exp = "/^([a-zA-Z]+)(\s?([a-zA-Z])){{$min},{$max}}$/";
// /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/
        return preg_match($exp, $input);
    }
    
    //нисам проверавао уопште
    public static function AlphabetUTFWithOneSpace($input, $min = 3, $max = 30) {
        $exp = "/^([a-џA-Z]+)(\s?([a-џA-Z])){{$min},{$max}}$/";
// /^([a-zA-Z]*)(\s?([a-zA-Z]))*$/
        return preg_match($exp, $input);
    }
    
    

    //razmak i ostali karakteri mogu da se ponavljanju najveše dvaput u seredini i na kraju rečenice
    public static function AlphabeticWithSpecialCharacterInMiddleAndEnd($input, $min = 3, $max = 30) {
        $exp = "/^([a-žA-Ž]+)(((\s|\.|\!|\,|\?){0,2})([a-žA-Ž]))+((\s|\.|\!|\,|\?){0,2}|([a-žA-Ž])*){{$min},{$max}}$/";
        return preg_match($exp, $input);
    }
    
    
    //razmak i ostali karakteri mogu da se ponavljanju najveše dvaput u seredini i na kraju rečenice
    public static function AlphabetUTFWithSpecialCharacterInMiddleAndEnd($input, $min = 3, $max = 30) {
        $exp = "/^([a-џA-Z]+)(((\s|\.|\!|\,|\?){0,2})([a-џA-Z]))+((\s|\.|\!|\,|\?){0,2}|([a-џA-Z])*){{$min},{$max}}$/";
        return preg_match($exp, $input);
    }

// /^([a-zA-Z]+)(((\s|\.|\!|\, |\?){0, 2})([a-zA-Z]))+((\s|\.|\!|\, |\?){0, 2}|([a-zA-Z])*)$/
//mora da se navede i puno vreme hh:mm:ss
//http://www.webdeveloper.com/forum/showthread.php?t=178277
    public static function DateTime($input) {
        $exp = "/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/";
        return preg_match($exp, $input);
    }

    public static function DateOrDateTime($input) {
        $exp = "/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))?$/";
        return preg_match($exp, $input);
    }

}