<?php

//checkBox
class PitanjeMatricaRadioStaro extends Pitanje {

    private $potpitanja;

    public function PitanjeMatricaRadioStaro($idPitanje, $nizPotpitanja, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->potpitanja = $nizPotpitanja;
         $this->opisVrste="Izbor jednog odgovora od više ponuđenih (radio dugme)";
    }

    function prikazi() {

        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        // echo '<form>';
        $num = "a";

        echo "<table border='0px'>";
        echo "<tr><td></td>";
        //prikazuje podunjene odgovore
        foreach ($this->odgovori as $odg) {
            echo "<td width='100px'><label for='{$odg['idOdgovor']}'>{$odg['tekst']}</label> </td>";
        }
        echo "</tr>";
        //prikazuje potpitanja
        foreach ($this->potpitanja as $pot) {
            echo '<tr>';
            //echo "<td align='right'>";
            echo "<td>";
            echo $num . ". " . $pot['tekst'];
            echo "</td>";

            //prikazuje radio dugmiće
            foreach ($this->odgovori as $odg) {
                echo "<td width='100px'>";
                echo "<input type='radio' name='{$pot['idPotpitanje']}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }
        echo "</table>";
        echo '<br/>';
    }

    function prikaziStaro() {

        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        // echo '<form>';
        $num = "a";

        echo "<table border='0px'>";
        echo "<tr><td></td>";
        //prikazuje podunjene odgovore
        foreach ($this->odgovori as $odg) {
            echo "<td width='100px'><label for='{$odg['idOdgovor']}'>{$odg['tekst']}</label> </td>";
        }
        echo "</tr>";
        //prikazuje potpitanja
        foreach ($this->potpitanja as $pot) {
            echo '<tr>';
            //echo "<td align='right'>";
            echo "<td>";
            echo $num . ". " . $pot['tekst'];
            echo "</td>";

            //prikazuje radio dugmiće
            foreach ($this->odgovori as $odg) {
                echo "<td width='100px'>";
                echo "<input type='radio' name='{$pot['idPotpitanje']}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }
        echo "</table>";
        echo '<br/>';
    }

    function prikaziee() {
        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input type='checkbox' name='{$this->idPitanje}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked}>" .
            "<label for='{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }
    }

//end class
}

?>
