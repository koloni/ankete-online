<?php

/**
 * Description of PitanjaAnketniListic
 *
 * @author korisnik
 * 
 * 
 * Trenutno ne korisnim ovu klasu, pravio sam je zbog dodavanja naslova, 
 */
class AnketniListic extends Pitanja {

    public $naziv="";


    function __construct($idAnketniListic, $od = 0, $brPitanja = 0) {
        $sql = new DbAnkete();
        $this->naziv = $sql->getNazivAnketeByIdAnketniListic($idAnketniListic);
        if (is_array($this->naziv == false)) {
            throw new Exception("Ne postoji anketa za izbrani'\$idAnketa'!");
        }
        $this->pitanja = $sql->getPitanjaAnketniListic($idAnketniListic, $od, $brPitanja);
    }

    function prikaziDisabled() {
        echo $this->naziv;
        $this->prikaziDisabled();
    }

}