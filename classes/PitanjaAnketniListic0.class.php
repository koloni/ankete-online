<?php

/**
 * Description of PitanjaAnketniListic
 *
 * @author korisnik
 */
class PitanjaAnketniListic {

    private $pitanja;

    function __construct($idAnketniListic, $od = 0, $brPitanja = 0) {
        $sql = new DbAnkete();
        $this->pitanja = $sql->getPitanjaAnketniListic($idAnketniListic, $od, $brPitanja);
    }

    function prikazi() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->prikazi();
            }
        }
    }

    function prikaziDisabled() {
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->setDisabled();
                $p->prikazi();
            }
        }
    }

    function getPitanja() {
        return $this->pitanja;
    }

}

?>
