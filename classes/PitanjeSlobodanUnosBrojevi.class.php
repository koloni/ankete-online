<?php

include_once 'classes/PitanjeSlobodanUnos.class.php';

class PitanjeSlobodanUnosBrojevi extends PitanjeSlobodanUnos {

    public function __construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje);
         $this->opisVrste="Slobodan unos numeričke vrednosti (jedno ili više polja)";
    }

    function unesiOdgovor($idPotpitanje, $numVrednost) {
        //$numVrednost = str_replace(",", ".", $numVrednost);
        //if (is_numeric($numVrednost) == false) {
        //    throw new Exception("Argumnet '\$numVrednost' funkcije 'izaberi odgovor', mora da bude numerička vrednsot!");
        //}
        // parent::getBrojPotpitanja()
        parent::unesiOdgovor($idPotpitanje, $numVrednost);
    }

    function getPotpitanja() {
        return parent::getPotpitanja();
    }

    function getBrojPotpitanja() {
        return parent::getBrojPotpitanja();
    }

    function prikazi() {
        parent::prikazi();
    }

    /*
      function prikazi() {

      echo "<p>" . $this->tekst;
      if ($this->isObavezno == false) {
      echo ' (nije obavezno)';
      }
      echo "</p>";

      $num = "a";

      echo "<table border='0px'>";
      echo "<tr><td></td>";

      echo "</tr>";
      //prikazuje kolonu potpitanja
      foreach ($this->nizPotpitanjaUnos as $pot) {
      echo '<tr>';
      //echo "<td align='right'>";
      echo "<td>";
      echo $num . ". " . $pot->getTekst();
      echo "</td>";

      //prikazuje textbox kontrole
      echo '<td>';
      echo "<input type='text' name='{$pot->getIdPotpitanje()}' value='{$pot->getOdgovor()}' id='{$pot->getIdPotpitanje()}'><br/>";
      echo '</td>';
      echo '</tr>';
      $num++;
      }//foreach
      echo "</table>";
      echo '<br/>';
      }
     */
}

?>
