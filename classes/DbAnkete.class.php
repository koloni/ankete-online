<?php

include_once 'classes/Pitanje.class.php';
include_once 'classes/PitanjeMatricaRadio.class.php';
include_once 'classes/PitanjeMatricaRadio2.class.php';
include_once 'classes/PitanjeMatricaCheckBox.class.php';
include_once 'classes/PitanjeMatricaCheckBox2.class.php';
include_once 'classes/PotpitanjeMatrica.class.php';
include_once 'classes/PitanjeRadio.class.php';
include_once 'classes/PitanjeCheckBox.class.php';
include_once 'classes/PitanjeSelect.class.php';
include_once 'classes/PitanjeSelectMultiple.class.php';
include_once 'classes/PitanjeSlobodanUnosBrojevi.class.php';
include_once 'classes/PitanjeSlobodanUnosDugacakTekst.class.php';
include_once 'classes/PitanjeSlobodanUnosTekst.class.php';
include_once 'classes/PitanjeSlobodanUnos.class.php';
include_once 'classes/Odgovor.class.php';

class DbAnkete extends MySQL {

    function __construct() {
        parent::__construct(DB_NAME, DB_USER, DB_PASS, DB_HOST);
    }

    ///////////////////////////////
    /*
      deletePitanjeFromAnketaPitanja($idPitanje, $idKreator = null)
      deletePitanjeFromAlOdgovori($idPitanje, $idKreator = null)
      deletePitanjeFromAlMatricaOdgovori($idPitanje, $idKreator = null)
      deletePitanjeFromAlTekstOdgovori($idPitanje, $idKreator = null)
      deletePitanjeFromPotpitanje($idPitanje, $idKreator = null)
      deletePitanjeFromOdgovori($idPitanje, $idKreator = null)
     */

    function deletePitanjeFromOdgovoriNestoDrugo($idPitanje, $idKreator = null) {

        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM OdgovorNestoDrugo
            WHERE idPitanje = '{$idPitanje}'{$where}";

        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromOdgovorNestoDrugo
    }

    function deletePitanjeFromPotpitanje($idPitanje, $idKreator = null) {

        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM Potpitanje
            WHERE idPitanje = '{$idPitanje}'{$where}";

        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromPotpitanje
    }

    function deletePitanjeFromOdgovori($idPitanje, $idKreator = null) {

        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM Odgovor
            WHERE idPitanje = '{$idPitanje}'{$where}";

        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromOdgovori
    }

    function deletePitanjeFromAnketaPitanja($idPitanje, $idKreator = null) {

        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM AnketaPitanje
            WHERE idPitanje = '{$idPitanje}'{$where}";

        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromAnketaPitanja
    }

    function deletePitanjeFromAlOdgovori($idPitanje, $idKreator = null) {
        if ($idKreator == null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM AlOdgovor
                 WHERE idPitanje='{$idPitanje}'{$where}";
        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromAlOdgovori
    }

    function deletePitanjeFromAlMatricaOdgovori($idPitanje, $idKreator = null) {
        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM AlMatricaOdgovor
            WHERE idPotpitanje IN (SELECT idPotpitanje
                                      FROM Potpitanje
                                      WHERE idPitanje='{$idPitanje}'
                                    ){$where}";
        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanjeFromAlMatricaOdgovori(
    }

    function deletePitanjeFromAlTekstOdgovori($idPitanje, $idKreator = null) {
        if ($idKreator === null) {
            
        } else {
            $where = " AND '{$idKreator}'=(SELECT idKreator 
                                            FROM Pitanje
                                            WHERE idPitanje='{$idPitanje}')";
        }
        $query = "DELETE FROM AlTekstOdgovor
            WHERE idPotpitanje IN (SELECT idPotpitanje
                                      FROM Potpitanje
                                      WHERE idPitanje='{$idPitanje}'
                                    ){$where}";
        $this->ExecuteSQL_CHANGE($query);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
//deletePitanjeFromAlTekstOdgovori
    }

    ///////////////////////////////////

    function createPitanje($tekst, $idKreator, $idVrstaOdgovora, $stanje, $idSlika = null) {
        if ($idSlika === null) {
            $idSlika = "NULL";
        }
        //$sql->Insert(array('idPitanje' => '', 'tekst' => $tekst, 'idKreator' => $user->idKorisnik, 'idVrstaOdgovora' => $idVrstaOdgovora, 'datumKreiranja' => 'now()'), 'Pitanje');
        $query = "INSERT INTO Pitanje (idPitanje, tekst, idKreator, idVrstaOdgovora, idSlika, datumKreiranja, stanje) values('', '{$tekst}' , '$idKreator', '{$idVrstaOdgovora}', {$idSlika}, NOW(), '{$stanje}')";
        $this->ExecuteSQL_CHANGE($query);
    }

    function deleteNalog($idKorisnik) {
        $query = "DELETE FROM Korisnik WHERE idKorisnik='{$idKorisnik}'";
        $this->ExecuteSQL_CHANGE($query);
    }

    function acivateNalog($idKorisnik) {
        $query = "UPDATE Korisnik SET state=1 WHERE idKorisnik='{$idKorisnik}'";
        $this->ExecuteSQL_CHANGE($query);
    }

    function blockNalog($idKorisnik) {
        $query = "UPDATE Korisnik SET state=0 WHERE idKorisnik='{$idKorisnik}'";
        $this->ExecuteSQL_CHANGE($query);
    }

    private function getNalozi($state = null) {
        $query = "";
        //ako stavim dva znaka == uradiće konverziju nule u null
        if ($state === null) {
            $query = "SELECT k.*, ut.naziv as typeName, sk.naziv as stateName 
                    FROM Korisnik as k INNER JOIN UserType as ut ON k.type=ut.idUserType INNER JOIN StanjeKorisnik as sk ON k.state=sk.idStanjeKorisnik";
        } else if ($state == -1) {
            $query = "SELECT k.*, ut.naziv as typeName, sk.naziv as stateName 
                    FROM Korisnik as k INNER JOIN UserType as ut ON k.type=ut.idUserType INNER JOIN StanjeKorisnik as sk ON k.state=sk.idStanjeKorisnik 
                    WHERE state=-1";
        } else if ($state == 0) {
            $query = "SELECT k.*, ut.naziv as typeName, sk.naziv as stateName 
                    FROM Korisnik as k INNER JOIN UserType as ut ON k.type=ut.idUserType INNER JOIN StanjeKorisnik as sk ON k.state=sk.idStanjeKorisnik 
                    WHERE state=0";
        } else if ($state == 1) {
            $query = "SELECT k.*, ut.naziv as typeName, sk.naziv as stateName 
                    FROM Korisnik as k INNER JOIN UserType as ut ON k.type=ut.idUserType INNER JOIN StanjeKorisnik as sk ON k.state=sk.idStanjeKorisnik 
                    WHERE state=1 AND type!=-1";
        } else {
            throw new InvalidArgumentException("Argument funkcije getNalozi '\$state', sadrži ne odgovarajuću vrednost!");
        }

        
		/*
		$result = mysql_query($query) or die("Greška baze: " . mysql_error());
		$niz = array();		
		
        while ($row = mysql_fetch_object($result)) {
            $niz[] = new Korisnik($row->idKorisnik, $row->username, $row->firstName, $row->lastName, $row->gender, $row->birthDate, $row->address, $row->city, $row->phone, $row->email, $row->type, $row->typeName, $row->state, $row->stateName, $row->registerDate);
        }
		
		return $niz;
		*/
		
		$result =  $this->ExecuteSQL($query);
        
		//print_r($result);
		//die();
		
		$korisnici = array();		
		
		if($this->records==1){
			$korisnici[]= new Korisnik($result['idKorisnik'], $result['username'], $result['firstName'], $result['lastName'], $result['gender'], $result['birthDate'], $result['address'], $result['city'], $result['phone'], $result['email'], $result['type'], $result['typeName'], $result['state'], $result['stateName'],$result['registerDate']);
		}
		else if($this->records>1){
			for($i=0; $i<count($result); $i++){
				$korisnici[]= new Korisnik($result[$i]['idKorisnik'], $result[$i]['username'], $result[$i]['firstName'], $result[$i]['lastName'], $result[$i]['gender'], $result[$i]['birthDate'], $result[$i]['address'], $result[$i]['city'], $result[$i]['phone'], $result[$i]['email'], $result[$i]['type'], $result[$i]['typeName'], $result[$i]['state'], $result[$i]['stateName'],$result[$i]['registerDate']);
			}
		}
		
        return $korisnici;
        //getNalozi
    }

    function getNaloziSvi() {
        return $this->getNalozi();
    }

    function getNaloziBlokirani() {
        return $this->getNalozi(0);
    }

    function getNaloziNeaktivirani() {
        return $this->getNalozi(-1);
    }

    function getNaloziAktivirani() {
        return $this->getNalozi(1);
    }

    //treba doraditi
    function getNumberOfPitanja() {
        $query = "SELECT count(*) as brPitanja
                  FROM Pitanje
                  WHERE stanje='0'";
        $brAnketa = $this->ExecuteSQL($query);
        return $brAnketa['brPitanja'];
    }

    function getNumberOfPitanjaSearch($kljucneReci) {
        $upitDodatak = "";
        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $upitDodatak .= "AND tekst LIKE '%" . $kljucnaRec . "%' ";
            }
        }
        $query = "SELECT count(*) as brPitanja
                  FROM Pitanje
                  WHERE stanje='0'{$upitDodatak}";
        $brAnketa = $this->ExecuteSQL($query);
        return $brAnketa['brPitanja'];
        //getNumberOfPitanjaSearch
    }

    function getActivePitanjaInfo2($idKreator, $stanjePitanja, $pocetniZapis, $limit, $kljucneReci, $orderByColumn = "tekst", $ascOrDesc = "asc") {
        // null - sva pitanja
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivne
        // 2 - zavrsene
        // 3 - aktivne i završene
        $where = "WHERE 1=1";
        if ($stanjePitanja === null) {
            
        } else if ($stanjePitanja == -1) {
            $where.= " AND p.stanje='-1'";
        } else if ($stanjePitanja == 0) {
            $where.= " AND p.stanje='0'";
        } else if ($stanjePitanja == 1) {
            $where .= " AND p.stanje='1'";
        } else if ($stanjePitanja == 2) {
            //$where = "WHERE  a.stanje='2'"; odustao je je ne moguće da menjam stanje u završena kada prođe vreme
            $where .= " AND p.stanje='2'";
        } else if ($stanjePitanja == 3) {
            $where .= " AND p.stanje='3'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjePitanja', funkcije 'getPitanjaInfo()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKreator === null) {
            
        } else {
            $where.=" AND idKreator='{$idKreator}'";
        }


        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $where .= " AND p.tekst LIKE '%" . $kljucnaRec . "%'";
            }
        }

        $query = "SELECT p.*, DATE_FORMAT(p.datumKreiranja,'%d.%m.%Y') as 'datumKreiranja'
                  FROM Pitanje as p                                                         
                  {$where}
                  ORDER BY p.{$orderByColumn} {$ascOrDesc}";

        if ($limit >= 1) {
            $query.=" LIMIT {$pocetniZapis}, {$limit}";
        }

        return $this->ExecuteSQL($query);

        //getActivePitanjaInfo2
    }

    function getPitanjaInfo($idAnketa) {
        $query = "SELECT * 
                FROM Pitanje
                WHERE idPitanje IN (SELECT idPitanje
                                    FROM AnketaPitanje
                                    WHERE idAnketa='{$idAnketa}')";

        //stanje=1 izbacio iz uslova, ali trebalo bi da ga ubacim
        /*
		$result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;
		*/
		
        return $this->ExecuteSQL($query);
        

        //getPitanjaInfo               
    }

    function getPitanjaInfo2($idAnketa) {
        $query = "SELECT p.*
                FROM Pitanje as p INNER JOIN AnketaPitanje as ap ON p.idPitanje=ap.idPitanje AND idAnketa='{$idAnketa}'";

        //stanje=1 izbacio iz uslova
        /*$result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
        
        return $niz;*/
		
		return $this->ExecuteSQL($query);
        //getPitanjaInfo2
    }

    /* Vraća niz id-jeva za određenu anketu */
    function getIdPitanja($idAnketa) {
        $query = "SELECT idPitanje 
                FROM Pitanje
                WHERE idPitanje IN (SELECT idPitanje
                                    FROM AnketaPitanje
                                    WHERE idAnketa='{$idAnketa}')";

        //stanje=1 izbacio iz uslova
		/*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }

        return $niz;*/
		return $this->ExecuteSQL($query);
        //getIdPitanja
    }

    function getRedniBrojeviPitanja($idAnketa) {
        $query = "SELECT p.idPitanje, ap.redniBroj
                FROM Pitanje as p INNER JOIN AnketaPitanje as ap ON p.idPitanje=ap.idPitanje AND ap.idAnketa='{$idAnketa}'
                ORDER BY ap.redniBroj asc";

        //stanje=1 izbacio iz uslova
		/*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());
        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;*/
        return $this->ExecuteSQL($query);
        

        //getRedniBrojeviPitanja
    }

    function getPotpitanjaInfo($idPitanje) {
        $query = "SELECT * 
                FROM Potpitanje
                WHERE idPitanje='{$idPitanje}'";

        //stanje=1 izbacio iz uslova
		
		/*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;*/
        return $this->ExecuteSQL($query);
        
        //getPotpitanjaInfo
    }

    //Ovo se odnosi na pitanje u svim anketama, odnosno na sve anketene listiće koje sadrže zadato pitanje (i one ne dovršene)
    function getAllAlOdgovoriWithBrojOdgovora($idPitanje) {
        $query = "SELECT o.idOdgovor, o.tekst, COUNT(al.idOdgovor) as brOdgovora
                    FROM AlOdgovor as al RIGHT JOIN Odgovor as o ON al.idOdgovor=o.idOdgovor
                    WHERE o.idPitanje = '{$idPitanje}'
                    GROUP BY o.idOdgovor
                    ORDER BY brOdgovora desc";
        return $this->ExecuteSQL($query);
        //getAllAlOdgovoriWithBrojOdgovora
    }

    /* Vraća tekst odgovora nešto drugo, i ukupan broj odgovora (jedan zapis) */

    function getAlNestoDrugoInfoWithUkupnoOdgovora($idAnketa, $idPitanje) {
        /*
        $query = "SELECT ond.tekst, COUNT(alndo.idAlNestoDrugoOdgovor) as 'brOdgovora'
                  FROM  AnketniListic as al 
                 INNER JOIN AlNestoDrugoOdgovor as alndo ON al.idAnketniListic=alndo.idAnketniListic AND al.idAnketa='{$idAnketa}' 
                 RIGHT JOIN OdgovorNestoDrugo as ond ON ond.idOdgovorNestoDrugo=alndo.idOdgovorNestoDrugo
                 WHERE ond.idPitanje = '{$idPitanje}'";*/
                 
  /*               
                 $query = "SELECT ond.tekst, COUNT(alndo.idAlNestoDrugoOdgovor) as 'brOdgovora'
                  FROM  AnketniListic as al 
                 INNER JOIN AlNestoDrugoOdgovor as alndo ON al.idAnketniListic=alndo.idAnketniListic AND al.idAnketa='{$idAnketa}'                  
                 RIGHT JOIN OdgovorNestoDrugo as ond ON ond.idOdgovorNestoDrugo=(SELECT idNestoDrugo FROM Pitanje WHERE idPitanje = '{$idPitanje}')";
*/
                 
                $query = " SELECT 
    ond.tekst, COUNT(alndo.idAnketniListic) AS 'brOdgovora'
FROM
    AnketniListic AS al
        INNER JOIN
    AlNestoDrugoOdgovor AS alndo ON al.idAnketniListic = alndo.idAnketniListic
        AND al.idAnketa = '{$idAnketa}' AND alndo.idPitanje='{$idPitanje}'
        INNER JOIN
    OdgovorNestoDrugo AS ond ON ond.idOdgovorNestoDrugo = (SELECT 
            idNestoDrugo
        FROM
            Pitanje
        WHERE
            idPitanje = '{$idPitanje}')";
                 
                 
                 
                 
                 
				 /*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;*/
        return $this->ExecuteSQL($query);
        
        //getAlNestoDrugoInfoWithUkupnoOdgovora
    }

    function getAlNestoDrugoWithBrojOdgovora($idAnketa, $idPitanje) {
        $query = "SELECT alndo.tekst, COUNT(*) as 'brOdgovora'
        FROM AlNestoDrugoOdgovor as alndo
        INNER JOIN AnketniListic as al ON alndo.idAnketniListic = al.idAnketniListic AND al.idAnketa = '{$idAnketa}' AND alndo.idPitanje = '{$idPitanje}'

        GROUP BY tekst
        ORDER BY brOdgovora desc";

		/*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());

        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;*/
        return $this->ExecuteSQL($query);
        
        //getAlNestoDrugoiWithBrojOdgovora
    }

    function getAlTekstOdgovoriWithBrojOdgovora($idAnketa, $idPitanje) {
        $query = "SELECT tekst, COUNT(*) as 'brOdgovora'
                  FROM AlTekstOdgovor
                  WHERE idPotpitanje IN (SELECT idPotpitanje FROM Potpitanje WHERE idPitanje='{$idPitanje}') AND idAnketniListic IN (SELECT idAnketniListic
                                                                          FROM AnketniListic
                                                                          WHERE idAnketa='{$idAnketa}')
                  GROUP BY tekst
                   ORDER BY brOdgovora desc";

		/*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());
        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }        
        return $niz;*/
		return $this->ExecuteSQL($query);
        //getAlTekstOdgovoriWithBrojOdgovora
    }

    function getFinishedAlOdgovoriWithBrojOdgovora($idAnketa, $idPitanje = 0) {
        $query = "";
        //Ovi upiti ne valjaju, jer ne vraćaju odgovore za koje niko nije odgovorio!
        //Problem nastaje u WHERE uslovu, jer se tu poništava efekat RIGHT JOIN
//        if ($idPitanje == 0) {
//            $query = "SELECT o.idOdgovor, o.tekst, COUNT(alo.idOdgovor) as brOdgovora
//                    FROM AlOdgovor as alo RIGHT JOIN Odgovor as o ON alo.idOdgovor=o.idOdgovor
//                    WHERE alo.idAnketniListic IN (SELECT idAnketniListic
//                                                  FROM AnketniListic
//                                                  WHERE isPopunjen='1' AND idAnketa='{$idAnketa}')
//                    GROUP BY o.idOdgovor
//                    ORDER BY brOdgovora desc";
//        } else {
//            $query = "SELECT o.idOdgovor, o.tekst, COUNT(alo.idOdgovor) as brOdgovora
//                    FROM AlOdgovor as alo RIGHT JOIN Odgovor as o ON alo.idOdgovor=o.idOdgovor
//                    WHERE o.idPitanje = '{$idPitanje}' AND alo.idAnketniListic IN (SELECT idAnketniListic
//                                                                                   FROM AnketniListic
//                                                                                   WHERE isPopunjen='1' AND idAnketa='{$idAnketa}')
//                    GROUP BY o.idOdgovor
//                    ORDER BY brOdgovora desc";
//        }
//        
//        //ako se stavi pitanje=0, onda vraća sve odgovore, za zadatu anektu
        //Rešenje je uraditi WHERE pre RIGHT JOIN
        if ($idPitanje == 0) {
            $query = "SELECT o.idOdgovor, o.tekst, COUNT(alo.idOdgovor) as brOdgovora
                    FROM  AnketniListic as al INNER JOIN AlOdgovor as alo ON al.idAnketniListic=alo.idAnketniListic AND al.idAnketa='{$idAnketa}' RIGHT JOIN Odgovor as o ON alo.idOdgovor=o.idOdgovor                    
                    GROUP BY o.idOdgovor
                    ORDER BY brOdgovora desc";
        } else {
            $query = "SELECT o.idOdgovor, o.tekst, COUNT(alo.idOdgovor) as brOdgovora
                    FROM  AnketniListic as al INNER JOIN AlOdgovor as alo ON al.idAnketniListic=alo.idAnketniListic AND al.idAnketa='{$idAnketa}' RIGHT JOIN Odgovor as o ON alo.idOdgovor=o.idOdgovor
                    WHERE o.idPitanje = '{$idPitanje}'
                    GROUP BY o.idOdgovor            
                    ORDER BY o.idOdgovor asc";
            //prikaziju se u svim izvestajima
            //WHERE o.idPitanje = '{$idPitanje}' OR o.idOdgovor=0
            //ORDER BY brOdgovora desc";
        }
        //OR o.idOdgovor=0, koristi se zbog upotrebe Nešo drugo odgovora u različitim pitanjima
        //
        
        /*
        $result = mysql_query($query) or die("Greška baze: " . mysql_error());
        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }
		return $niz;
		*/
        return $this->ExecuteSQL($query);
        
        //getFinishedAlOdgovoriWithBrojOdgovora
    }

    function getFinishedAlMatricaOdgovoriWithBrojOdgovora($idPotpitanje, $idAnketa, $idPitanje = 0) {
        $query = "SELECT o.idOdgovor, o.tekst, COUNT(almo.idOdgovor) as brOdgovora
                    FROM AlMatricaOdgovor as almo RIGHT JOIN Potpitanje as pot ON almo.idPotpitanje=pot.idPotpitanje
                    WHERE o.idPitanje = '{$idPotpitanje}' AND alo.idAnketniListic IN (SELECT idAnketniListic
                                                                                   FROM AnketniListic
                                                                                   WHERE isPopunjen='1' AND idAnketa='{$idAnketa}')
                    GROUP BY o.idOdgovor";

        $query2 = "SELECT almo.idOdgovor, COUNT(almo.idOdgovor) as brOdgovora
                    FROM AlMatricaOdgovor as almo RIGHT JOIN Potpitanje as pot ON almo.idPotpitanje=pot.idPotpitanje
                    WHERE o.idPitanje = '{$idPotpitanje}' AND almo.idAnketniListic IN (SELECT idAnketniLictic
                                                                                   FROM AnketniListic
                                                                                   WHERE isPopunjen='1' AND idAnketa='{$idAnketa}')";

        $query = "SELECT almo.idOdgovor, COUNT(almo.idOdgovor) as brOdgovora 
                FROM AlMatricaOdgovor as almo INNER JOIN Potpitanje as pot ON almo.idPotpitanje = pot.idPotpitanje
                WHERE almo.idPotpitanje = '149' AND almo.idAnketniListic IN (SELECT idAnketniListic
                                                                             FROM AnketniListic
                                                                             WHERE isPopunjen=1 AND idAnketa=27)
                                                                             GROUP BY almo.idOdgovor";
        //ovo iznad su neki pokušaji, mrzi me da se setim

        if ($idPitanje == 0) {
            $query = " SELECT o.idOdgovor, o.tekst, COUNT(almo.idOdgovor) as brOdgovora
                FROM  AnketniListic as al INNER JOIN AlMatricaOdgovor as almo ON al.idAnketniListic=almo.idAnketniListic AND al.idAnketa='{$idAnketa}' AND almo.idPotpitanje='{$idPotpitanje}' RIGHT JOIN Odgovor as o ON almo.idOdgovor=o.idOdgovor
                WHERE o.idPitanje IN (SELECT idPitanje
                                                                              FROM Potpitanje
                                                                              WHERE idPotpitanje'={$idPotpitanje}')
                GROUP BY o.idOdgovor
                ORDER BY o.idOdgovor asc";
        } else {
            $query = " SELECT o.idOdgovor, o.tekst, COUNT(almo.idOdgovor) as brOdgovora
                FROM  AnketniListic as al INNER JOIN AlMatricaOdgovor as almo ON al.idAnketniListic=almo.idAnketniListic AND al.idAnketa='{$idAnketa}' AND almo.idPotpitanje='{$idPotpitanje}' RIGHT JOIN Odgovor as o ON almo.idOdgovor=o.idOdgovor
                WHERE o.idPitanje = '{$idPitanje}'
                GROUP BY o.idOdgovor
                ORDER BY o.idOdgovor asc";
            //ovo ne valja kod matrica pitanja jer mi onda pobrka prvi red liste odgovora, sa rezultatima,
            //je će uvek u drugoj koloni da budu najveće vrednosti, koje ne pripadaju listi odgovora
            //ORDER BY brOdgovora desc";
        }


        //return $this->ExecuteSQL($query);
        /*
		$result = mysql_query($query) or die("Greška baze: " . mysql_error());
        $niz = array();
        while ($row = mysql_fetch_object($result)) {
            $niz[] = $row;
        }        
        return $niz;
		*/
		return $this->ExecuteSQL($query);
        //getFinishedAlMatricaOdgovoriWithBrojOdgovora
    }

    //Briše pitanje, samo ako se ne nalazi ni u jednoj anketi
    function deletePitanje($idPitanje, $idKreator = null, $stanje = null) {

        $where = "WHERE 1=1";
        if ($idKreator === null) {
            
        } else {
            $where.= " AND idKreator='{$idKreator}'";
        }

        if ($stanje === null) {
            
        } else {
            $where.= " AND stanje = '{$stanje}'";
        }

        $query = "DELETE
                FROM Pitanje
                {$where} AND idPitanje = '{$idPitanje}' AND '0'=(SELECT COUNT(idPitanje)
                                                                    FROM AnketaPitanje
                                                                    WHERE idPitanje ='{$idPitanje}')";
        $this->ExecuteSQL_CHANGE($query);
        // return "Broj obrisanih: " . $this->affected . ", Greška: " . $this->lastError;
        //deletePitanje
    }

    //Brisanje pitanja iz ankete samo ako je ono jedino u toj anketi, odnosno ne koristi se u drugim anektama
    function deleteAnketaPitanjeIfOnlyOneRecordsIdPitanje($idAnketa, $idPitanje) {
        $upit = "SELECT COUNT(*) as 'brRedova' FROM AnketaPitanje WHERE idPitanje = '{$idPitanje}'";
        $rez = $this->ExecuteSQL($upit);
        if ($rez['brRedova'] == 1) {
            /*
              //Upit ne radi jer ne može ugnježdeni upit da sadrži istu tabelu
              //Greška baze: You can't specify target table 'AnketaPitanje' for update in FROM clause
              $upit = "DELETE
              FROM AnketaPitanje
              WHERE idPitanje = '{$idPitanje}' AND idAnketa = '{$idAnketa}' AND 1=(SELECT COUNT(*)
              FROM AnketaPitanje ap
              WHERE ap.idPitanje = '{$idPitanje}')";

             */


            /* //Greška baze: Invalid use of group function
              $upit = "DELETE
              FROM AnketaPitanje
              WHERE idPitanje = '{$idPitanje}' AND idAnketa = '{$idAnketa}' AND COUNT(idPitanje)=1"; */

            $upit = "DELETE
            FROM AnketaPitanje
            WHERE idPitanje = '{$idPitanje}' AND idAnketa = '{$idAnketa}'";
            $this->ExecuteSQL_CHANGE($upit);
            if ($this->affected == 1) {
                return true;
            } else {
                return false;
            }
            //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        } else {
            return false;
        }

        /*
          //ne radi delete ako se ugnježdeni upit stavi ista tabela
          //You can't specify target table 'AnketaPitanje' for update in FROM clause
          $upit = "DELETE
          FROM AnketaPitanje
          WHERE idPitanje = '{$idPitanje}' AND idAnketa = '{$idAnketa}' AND 1 = (SELECT COUNT(*)
          FROM AnketaPitanje
          WHERE idPitanje = '{$idPitanje}')";
         */
        //deleteAnketaPitanjeIfOnlyOneRecordsIdPitanje
    }

    function blockAnketa($idAnketa) {
        $query = "UPDATE Anketa SET stanje=-1
                  WHERE idAnketa='{$idAnketa}'";
        $this->ExecuteSQL_CHANGE($query);
        if ($this->affected == 1) {
            return true;
        } else {
            return false;
        }
        //blockAnketa
    }

    function activateAnketa($idAnketa) {
        $query = "UPDATE Anketa SET stanje=1
                  WHERE idAnketa='{$idAnketa}'";
        $this->ExecuteSQL_CHANGE($query);
        if ($this->affected == 1) {
            return true;
        } else {
            return false;
        }
        //activateAnketa
    }

    /*Aktivira pitanja koja su napravljenja za tekuću anketu, a poziva se u trenutku kada se Aktivira*/
    function activatePitanja($idAnketa, $idKreator=null) {
        
        $dodatak="";
        if($idKreator===null){
            
        }else{
            $dodatak=" AND idKreator='$idKreator'";
        }
        
        $query = "UPDATE Pitanje SET stanje=1
                  WHERE idPitanje IN (SELECT idPitanje 
                                      FROM AnketaPitanje 
                                      WHERE idAnketa='{$idAnketa}')
                                      AND stanje=0 {$dodatak}";
        $this->ExecuteSQL_CHANGE($query);
        if ($this->affected == 1) {
            return true;
        } else {
            return false;
        }
        //activatePitanja
    }

    function deleteAnketa($idAnketa, $idKreator = null, $stanje = null) {
        if ($idKreator == -1 && $stanje === null) {
            $this->Delete("Anketa", array('idAnketa' => $idAnketa));
        } else if ($idKreator === null) {
            $this->Delete("Anketa", array('idAnketa' => $idAnketa, 'stanje' => $stanje));
        } else if ($idKreator != null && $stanje != null) {
            $this->Delete("Anketa", array('idAnketa' => $idAnketa, 'idKreator' => $idKreator, 'stanje' => $stanje));
        } else if ($idKreator != null && $stanje === null) {
            $this->Delete("Anketa", array('idAnketa' => $idAnketa, 'idKreator' => $idKreator));
        }
        //return $this->affected . ", Greška: " . $this->lastError;
        //deleteAnketa
    }

    function deleteAnketaPitanja($idAnketa, $idKreator = 0) {
        $upit = "";

        if ($idKreator == 0) {
            $upit = "DELETE FROM AnketaPitanje
            WHERE idAnketa = '{$idAnketa}'";
        } else {
            $upit = "DELETE FROM AnketaPitanje
            WHERE idAnketa = '{$idAnketa}' AND '{$idKreator}'=(SELECT idKreator 
                                                               FROM Anketa
                                                               WHERE idAnketa='{$idAnketa}')";
        }
        $this->ExecuteSQL_CHANGE($upit);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deleteAnketaPitanja
    }

    function deleteAlOdgovori($idAnketa, $idKreator = 0) {
        $upit = "";
        if ($idKreator == 0) {
            $upit = "DELETE FROM AlOdgovor
             WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    )";
        } else {
            $upit = "DELETE FROM AlOdgovor
                       WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    ) AND '{$idKreator}'=(SELECT idKreator 
                                                               FROM Anketa
                                                               WHERE idAnketa='{$idAnketa}')";
        }
        $this->ExecuteSQL_CHANGE($upit);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deleteAlOdgovori
    }

    function deleteAlMatricaOdgovori($idAnketa, $idKreator = 0) {
        $upit = "";
        if ($idKreator == 0) {
            $upit = "DELETE FROM AlMatricaOdgovor
            WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    )";
        } else {
            $upit = "DELETE FROM AlMatricaOdgovor
            WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    ) AND '{$idKreator}'=(SELECT idKreator 
                                                               FROM Anketa
                                                               WHERE idAnketa='{$idAnketa}')";
        }
        $this->ExecuteSQL_CHANGE($upit);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deleteAlMatricaOdgovori(
    }

    function deleteAlTekstOdgovori($idAnketa, $idKreator = 0) {
        $upit = "";
        if ($idKreator == 0) {
            $upit = "DELETE FROM AlTekstOdgovor
            WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    )";
        } else {
            $upit = "DELETE FROM AlTekstOdgovor
                      WHERE idAnketniListic IN (SELECT idAnketniListic 
                                      FROM AnketniListic
                                      WHERE idAnketa='{$idAnketa}'
                                    ) AND '{$idKreator}'=(SELECT idKreator 
                                                               FROM Anketa
                                                               WHERE idAnketa='{$idAnketa}')";
        }
        $this->ExecuteSQL_CHANGE($upit);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
//deleteAlTekstOdgovori
    }

    function deleteAnketniListici($idAnketa, $idKreator = 0) {
        $upit = "";
        if ($idKreator == 0) {
            $upit = "DELETE FROM AnketniListic
            WHERE idAnketa = '{$idAnketa}'";
        } else {
            $upit = "DELETE FROM AnketniListic
            WHERE idAnketa = '{$idAnketa}' AND '{$idKreator}'=(SELECT idKreator 
                                                               FROM Anketa
                                                               WHERE idAnketa='{$idAnketa}')";
        }
        $this->ExecuteSQL_CHANGE($upit);
        //return "Broj obrisanih anketa pitanje: " . $this->affected . ", Greška: " . $this->lastError;
        //deleteAnketniListici
    }

    function getNumberOfQuestion($idAnketa) {
        $query = "SELECT count(*) as brPitanja
                FROM AnketaPitanja
                WHERE idAnketa='{$idAnketa}'";
        return $this->ExecuteSQL($query);

        //getNumberOfQuestion
    }

    function getNumberOfActiveAnketa() {
        $query = "SELECT count(*) as brAnketa
                  FROM Anketa
                  WHERE stanje='1' AND datumPocetka<=NOW() AND datumZavrsetka>NOW()";
        $brAnketa = $this->ExecuteSQL($query);
		
		/*
		echo '<pre>';
		print_r($brAnketa);
		die();
		*/
		
        return $brAnketa['brAnketa'];
    }

    function getNumberOfAnkete($stanjeAnkete = null, $idKreator = null, $kljucneReci = null) {
        // null - sve aknkete
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivne
        // 2 - zavrsene
        // 3 - aktivne i završene
        $where = "WHERE 1=1";

        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $where .= " AND naziv LIKE '%" . $kljucnaRec . "%' ";
            }
        }

        if ($stanjeAnkete === null) {
            
        } else if ($stanjeAnkete == -1) {
            $where.= " AND a.stanje='-1'";
        } else if ($stanjeAnkete == 0) {
            $where.= " AND a.stanje='0'";
        } else if ($stanjeAnkete == 1) {
            $where.= " AND a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW() ";
        } else if ($stanjeAnkete == 2) {
            //$where = "WHERE  a.stanje='2'"; odustao je je ne moguće da menjam stanje u završena kada prođe vreme
            $where.= " AND a.stanje='1' AND a.datumZavrsetka<NOW() ";
        } else if ($stanjeAnkete == 3) {
            $where.= " AND a.stanje='1'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjeAnkete', funkcije 'getNumberOfAnkete()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKreator === null) {
            
        } else {
            $where.=" AND idKreator='{$idKreator}'";
        }

        $query = "SELECT COUNT(a.idAnketa) as 'brAnketa'
                  FROM Anketa as a 
                  {$where}";

        //exit($query);
        $brAnketa = $this->ExecuteSQL($query);
        return $brAnketa['brAnketa'];

        //getNumberOfAnkete
    }

    function getNumberOfActiveAnketaSearch($kljucneReci) {
        $upitDodatak = "";
        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $upitDodatak .= "AND naziv LIKE '%" . $kljucnaRec . "%' ";
            }
        }
        $query = "SELECT count(*) as brAnketa
                  FROM Anketa
                  WHERE stanje='1' AND datumPocetka<=NOW() AND datumZavrsetka>NOW(){$upitDodatak}";
        $brAnketa = $this->ExecuteSQL($query);
        return $brAnketa['brAnketa'];
        //getNumberOfActiveAnketaSearch
    }

    function getNumberOfAnetniListici($idAnketa, $isPopunjen) {
        $where = "WHERE idAnketa='{$idAnketa}'";

        if ($isPopunjen === null) {
            
        } else if ($isPopunjen == 0 || $isPopunjen == 1) {
            $where.=" AND al.isPopunjen='{$isPopunjen}'";
        } else {
            throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
        }

        $query = "SELECT count(*) as brListica
                  FROM AnketniListic as al
                  {$where}";
        $brListica = $this->ExecuteSQL($query);
        return $brListica['brListica'];

        //getNumberOfAnetniListici
    }

    //doraditi
    function getNumberOfAnetniListiciSearch($idAnketa, $isPopunjen, $filterPretrage, $kljucneReci) {
        $where = "WHERE 1=1";

        if ($idAnketa === null) {
            
        } else {
            $where.=" AND al.idAnketa='{$idAnketa}'";
        }

        if ($isPopunjen === null) {
            
        } else if ($isPopunjen == 0 || $isPopunjen == 1) {
            $where.=" AND al.isPopunjen='{$isPopunjen}'";
        } else {
            throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
        }

        //ako je filter pretrage frstName ili lastName
        if ($filterPretrage == 1) {
            if (count($kljucneReci) == 1 && $kljucneReci[0] != '') {
                $where = $where . " AND firstName LIKE '" . $kljucneReci[0] . "%' ";
            } else if (count($kljucneReci) == 2) {
                $where = $where . " AND firstName LIKE '" . $kljucneReci[0] . "%' AND lastName LIKE '" . $kljucneReci[1] . "%' ";
            } else {
                exit();
            }
        } else if ($filterPretrage == 2) {
            $where = $where . " AND username LIKE '" . $kljucneReci[0] . "%' ";
        } else if ($filterPretrage == 3) {
            $where = $where . " AND email LIKE '" . $kljucneReci[0] . "%' ";
        } else {
            exit();
        }

        $query = "SELECT count(*) as brListica
                  FROM AnketniListic as al
                        INNER JOIN
                        Korisnik as k ON k.idKorisnik=al.idIspitanik
                  $where";
        //exit($query);
        $brAnketa = $this->ExecuteSQL($query);

        return $brAnketa['brListica'];

        //getNumberOfAnetniListiciSearch
    }

    //Za izabranu anketu vrati vrati anketne listiće spisak
    function getAnketniListicInfo($idAnketa) {
        
    }

    function newAnketniListic($idAnketa, $idKorisnik) {
        $this->Insert(array('idAnketa' => $idAnketa, 'idIspitanik' => $idKorisnik), 'AnketniListic');
        return $this->getAnketniListicId($idAnketa, $idKorisnik);
    }

    function getAnketniListicId($idAnketa, $idKorisnik) {
        $idAnketniListic = $this->ExecuteSQL("SELECT idAnketniListic 
                            FROM AnketniListic
                            WHERE idAnketa='{$idAnketa}' AND idIspitanik='{$idKorisnik}'");
							//print_r($idAnketniListic);die();
        return $idAnketniListic['idAnketniListic'];
    }

    function predajAnketu($idAnketa, $idKreator) {
        $this->Update("Anketa", array('stanje' => 1), array('idAnketa' => $idAnketa, 'idKreator' => $idKreator));
    }

    function predajAnketniListic($idAnketniListic, $idIspitanik) {
        $this->Update("AnketniListic", array('isPopunjen' => 1), array('idAnketniListic' => $idAnketniListic, 'idIspitanik' => $idIspitanik));
    }

    function dodajPitanje($idAnketa, $idPitanje) {
        $this->Insert(array('idAnketa' => $idAnketa, 'idPitanje' => $idPitanje), 'AnketaPitanje');
        return $this->affected;
        /*
          if ($this->affected == 1) {
          return true;
          }
          return false;
         */
    }

    function izbaciPitanjeAnketa($idAnketa, $idPitanje) {
        //$this->Delete('AnketaPitanje', array('idAnketa' => $idAnketa, 'idPitanje' => $idPitanje));
        $query = "DELETE
                FROM AnketaPitanje
                WHERE idAnketa='{$idAnketa}' AND idPitanje='{$idPitanje}'";
        $this->ExecuteSQL_CHANGE($query);
        //echo $this->lastError;
    }

    function getListaPitanjaNotInAnketa($idAnketa, $stanjePitanja = null, $idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "tekst", $ascOrDesc = "asc") {
        // null - sve aknkete
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivne
        //$where = "WHERE 1=1";
        $where = "";

        if ($stanjePitanja === null) {
            
        } else if ($stanjePitanja == -1) {
            $where.= " AND p.stanje='-1'";
        } else if ($stanjePitanja == 0) {
            $where.= " AND p.stanje='0'";
        } else if ($stanjePitanja == 1) {
            $where.= " AND p.stanje='1'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjePitanja', funkcije 'getListaPitanjaNotInAnketa()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKreator === null) {
            
        } else {
            $where.=" AND idKreator='{$idKreator}'";
        }

        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $where .= " AND p.tekst LIKE '%" . $kljucnaRec . "%'";
            }
        }

        $query = "SELECT p.idPitanje, p.tekst, DATE_FORMAT(p.datumKreiranja, '%d.%m.%Y %H:%i') as 'datumKreiranja'
                FROM Pitanje as p
                WHERE idPitanje NOT IN (SELECT idPitanje 
                                        FROM AnketaPitanje
                                        WHERE idAnketa='{$idAnketa}'){$where}                 
                ORDER BY p.{$orderByColumn} {$ascOrDesc}";
        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }
        return $this->ExecuteSQL($query);

        //getListaPitanjaNotInAnketa
    }

    function getNumberOfPitanjaNotInAnket($idAnketa, $stanjePitanja = null, $idKreator = null, $kljucneReci = null) {
        // null - sva pitanja
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivne
        //$where = "WHERE 1=1";

        $where = "";
        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $where .= " AND tekst LIKE '%" . $kljucnaRec . "%' ";
            }
        }

        if ($stanjePitanja === null) {
            
        } else if ($stanjePitanja == -1) {
            $where.= " AND p.stanje='-1'";
        } else if ($stanjePitanja == 0) {
            $where.= " AND p.stanje='0'";
        } else if ($stanjePitanja == 1) {
            $where.= " AND p.stanje='1'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjePitanja', funkcije 'getNumberOfPitanjaNotInAnket()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKreator === null) {
            
        } else {
            $where.=" AND idKreator='{$idKreator}'";
        }

        $query = "SELECT COUNT(p.idPitanje) as 'brPitanja'
                FROM Pitanje as p
                WHERE idPitanje NOT IN (SELECT idPitanje 
                                        FROM AnketaPitanje
                                        WHERE idAnketa='{$idAnketa}')                                        
                {$where}";

        $brPitanja = $this->ExecuteSQL($query);
        return $brPitanja['brPitanja'];

        //getNumberOfPitanjaNotInAnketa
    }

    function getLastIdAnketa($idKreator) {
        $idAnketa = $this->ExecuteSQL("SELECT idAnketa 
                          FROM Anketa 
                          WHERE idKreator='{$idKreator}'
                          ORDER BY idAnketa desc
                          LIMIT 1
                          ");
        return $idAnketa['idAnketa'];
//getLastIdAnketa 
    }

    /*
      function getAnketniListiciInfoPersonalizovani($idAnketa = null, $idKreator = null, $isPersonalizovan = 1, $isPopunjen = 1, $od = 0, $brZapisa = 0, $filterPretrage = 1, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
      $where = "WHERE al.isPopunjen='{$isPopunjen}'";

      if ($idKreator === null) {

      } else {
      $where.=" AND idKreator='{$idKreator}'";
      }

      if ($idAnketa === null) {

      } else {
      $where.=" AND idAnketa='{$idAnketa}'";
      }

      $upitDodatak = "";
      //ako je filter pretrage frstName ili lastName
      if ($filterPretrage == 1) {
      if (count($kljucneReci) == 1 && $kljucneReci[0] != '') {
      $upitDodatak = $upitDodatak . " AND firstName LIKE '" . $kljucneReci[0] . "%' ";
      } else if (count($kljucneReci) == 2) {
      $upitDodatak = $upitDodatak . " AND firstName LIKE '" . $kljucneReci[0] . "%' AND lastName LIKE '" . $kljucneReci[1] . "%' ";
      } else {
      exit();
      }
      } else if ($_GET['filterPretrage'] == 2) {
      $upitDodatak = $upitDodatak . " AND username LIKE '" . $kljucneReci[0] . "%' ";
      } else if ($_GET['filterPretrage'] == 3) {
      $upitDodatak = $upitDodatak . "AND email LIKE '" . $kljucneReci[0] . "%' ";
      } else {
      exit();
      }

      $query = "SELECT al*, k.*'
      FROM AnketniListic as al
      INNER JOIN
      Korisnik as k ON k.idKorisnik=al.idKorisnik

      {$where} {$upitDodatak}
      GROUP BY al.idAnketa
      ORDER BY k.{$orderByColumn} {$ascOrDesc}";

      if ($brZapisa >= 1) {
      $query.=" LIMIT {$od}, {$brZapisa}";
      }

      return $this->ExecuteSQL($query);

      //getAnketniListiciInfoPersonalizovani
      }
     */

    function getAnketniListiciInfoNepersonalizovani($idAnketa = null, $idKreator = null, $isPopunjen = null, $od = 0, $brZapisa = 0, $orderByColumn = "firstName", $ascOrDesc = "asc") {
        $where = "WHERE 1=1";

        if ($idKreator === null) {
            
        } else {
            $where.=" AND a.idKreator='{$idKreator}'";
        }

        if ($idAnketa === null) {
            
        } else {
            $where.=" AND al.idAnketa='{$idAnketa}'";
        }

        if ($isPopunjen === null) {
            
        } else if ($isPopunjen == 0 || $isPopunjen == 1) {
            $where.=" AND al.isPopunjen='{$isPopunjen}'";
        } else {
            throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfoNepersonalizovani()', sadrži ne dozvoljenu vrednost!");
        }

        $query = "SELECT al.idAnketniListic, al.idAnketa, DATE_FORMAT(al.datumZavrsetka,'%d.%m.%Y %H:%i') as 'datumZavrsetka'
                    FROM AnketniListic as al
                        INNER JOIN
                            Korisnik as k ON k.idKorisnik=al.idIspitanik
                        INNER JOIN
                            Anketa as a ON a.idAnketa=al.idAnketa
                  {$where}              
                  ORDER BY {$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }

        return $this->ExecuteSQL($query);
        exit($query);
        //getAnketniListiciInfoNepersonalizovani
    }

    function getAnketniListiciInfoPersonalizovani($idAnketa = null, $idKreator = null, $isPopunjen = null, $od = 0, $brZapisa = 0, $filterPretrage = 1, $kljucneReci = array(), $orderByColumn = "firstName", $ascOrDesc = "asc") {
        return $this->getAnketniListiciInfo($idAnketa, $idKreator, $idKreator, 1, $isPopunjen, $od, $brZapisa, $filterPretrage, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getAnketniListiciInfo($idAnketa = null, $idKreator = null, $isPersonalizovana = null, $isPopunjen = null, $od = 0, $brZapisa = 0, $filterPretrage = 1, $kljucneReci = array(), $orderByColumn = "firstName", $ascOrDesc = "asc") {
        $where = "WHERE 1=1";

        if ($idKreator === null) {
            
        } else {
            $where.=" AND a.idKreator='{$idKreator}'";
        }

        if ($idAnketa === null) {
            
        } else {
            $where.=" AND al.idAnketa='{$idAnketa}'";
        }

        if ($isPopunjen === null) {
            
        } else if ($isPopunjen == 0 || $isPopunjen == 1) {
            $where.=" AND al.isPopunjen='{$isPopunjen}'";
        } else {
            throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
        }

        //vrati i personalizovane i ne personalizovan
        if ($isPersonalizovana === null) {
            
        } else if ($isPersonalizovana == 0 || $isPersonalizovana == 1) {
            $where.=" AND a.isPersonalizovana='{$isPersonalizovana}'";
        } else {
            throw new InvalidArgumentException("Argument '\$isPersonalizovana', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
        }

        //filter pretrage
        //null -bez pretrage
        //1 -firstName
        //2 -username
        //3 -email
        if (is_array($kljucneReci)) {
            if ($filterPretrage === null) {
                
            }
            //ako je filter pretrage frstName ili lastName
            else if ($filterPretrage == 1) {
                if (count($kljucneReci) == 1 && $kljucneReci[0] != '') {
                    $where.= " AND firstName LIKE '" . $kljucneReci[0] . "%' ";
                } else if (count($kljucneReci) == 2) {
                    $where.= " AND firstName LIKE '" . $kljucneReci[0] . "%' AND lastName LIKE '" . $kljucneReci[1] . "%' ";
                } else {
                    //uvek je array, a ovde ulazi ako nije ništa unešeno
                }
            } else if ($filterPretrage == 2) {
                $where.= " AND username LIKE '" . $kljucneReci[0] . "%' ";
            } else if ($filterPretrage == 3) {
                $where.= "AND email LIKE '" . $kljucneReci[0] . "%' ";
            } else {
                throw new InvalidArgumentException("Argument '\$fileterPretrage', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
            }
        }
        $query = "SELECT al.*, k.idKorisnik, k.firstName, k.lastName, k.username, k.email
                    FROM AnketniListic as al
                        INNER JOIN
                            Korisnik as k ON k.idKorisnik=al.idIspitanik
                        INNER JOIN
                            Anketa as a ON a.idAnketa=al.idAnketa
                  {$where}              
                  ORDER BY {$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }

        return $this->ExecuteSQL($query);
        // exit($query);
        //getAnketniListiciInfo
    }

    //prešao sam na limit, i pretragu

    private function getAnketeInfo0($stanjeAnkete = null, $idKorisnik = null, $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // null - sve aknkete
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivna
        // 2 - zavrsena
        $where = "";
        if ($stanjeAnkete === null) {
            $where = "";
        } else if ($stanjeAnkete == -1) {
            $where = "WHERE a.stanje='-1'";
        } else if ($stanjeAnkete == 0) {
            $where = "WHERE  a.stanje='0'";
        } else if ($stanjeAnkete == 1) {
            $where = "WHERE  a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW() ";
        } else if ($stanjeAnkete == 2) {
            $where = "WHERE  a.stanje='2'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjeAnkete', funkcije 'getAnketeInfo()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKorisnik === null) {
            
        } else {
            $where.=" AND idKreator='{$idKorisnik}' ";
        }

        //i ovo iznad radi, ovde sam odsekao višak vreme
        //$query = "SELECT a.*, DATE_FORMAT(a.datumPocetka,'%Y-%m-%d') as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
        $query = "SELECT a.*, DATE(a.datumPocetka) as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
                  FROM Anketa as a 
                            LEFT JOIN 
                        AnketaPitanje as ap ON a.idAnketa=ap.idAnketa 
                            INNER JOIN StanjeAnkete as sa ON a.stanje=sa.idStanjeAnkete
                  " . $where . "
                  GROUP BY a.idAnketa
                  ORDER BY a.{$orderByColumn} {$ascOrDesc}";

        return $this->ExecuteSQL($query);
        
        //getAnketeInfo0
    }


    private function getAnketeInfo($stanjeAnkete = null, $idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // null - sve aknkete
        // -1 - blokirane
        // 0 - u procesu kreiranja
        // 1 - aktivne
        // 2 - zavrsene
        // 3 - aktivne i završene
        $where = "WHERE 1=1";
        if ($stanjeAnkete === null) {
            
        } else if ($stanjeAnkete == -1) {
            $where.= " AND a.stanje='-1'";
        } else if ($stanjeAnkete == 0) {
            $where.= " AND a.stanje='0'";
        } else if ($stanjeAnkete == 1) {
            $where.= " AND a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW() ";
        } else if ($stanjeAnkete == 2) {
            //$where = "WHERE  a.stanje='2'"; odustao je je ne moguće da menjam stanje u završena kada prođe vreme
            $where.= " AND a.stanje='1' AND a.datumZavrsetka<NOW() ";
        } else if ($stanjeAnkete == 3) {
            $where.= " AND a.stanje='1'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjeAnkete', funkcije 'getAnketeInfo()', sadrži ne dozvoljenu vrednost!");
        }

        if ($idKreator === null) {
            
        } else {
            $where.=" AND idKreator='{$idKreator}'";
        }

        //WHERE 1=1 suži jer ne može da upit kreće sa WHERE AND naziv LIKE
        //$upit = "SELECT * FROM Anketa WHERE 1=1 ";
        //ako je filter pretrage je naziv ankete

        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $where .= " AND a.naziv LIKE '%" . $kljucnaRec . "%'";
            }
        }

        //i ovo iznad radi, ovde sam odsekao višak vreme
        //$query = "SELECT a.*, DATE_FORMAT(a.datumPocetka,'%Y-%m-%d') as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
        //$query = "SELECT a.*, DATE(a.datumPocetka) as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
        $query = "SELECT a.*, DATE_FORMAT(a.datumPocetka,'%d.%m.%Y') as 'datumPocetka', DATE_FORMAT(a.datumZavrsetka,'%d.%m.%Y') as 'datumZavrsetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
                  FROM Anketa as a 
                            LEFT JOIN 
                        AnketaPitanje as ap ON a.idAnketa=ap.idAnketa 
                            INNER JOIN StanjeAnkete as sa ON a.stanje=sa.idStanjeAnkete
                  {$where}
                  GROUP BY a.idAnketa
                  ORDER BY a.{$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }
        return $this->ExecuteSQL($query);

        //getAnketeInfo
    }

    function getAnketeAllInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(null, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getAnketeBlockedInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(-1, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getAnketeUnformedInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(0, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getAnketeFinishedInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(2, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getActiveAnketeInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(1, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    function getActiveAndFinishedAnketeInfo($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfo(3, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
    }

    /*
      function getActiveAnketeInfo($idKorisnik, $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
      $query = "SELECT a.*, COUNT(ap.idAnketa) as 'ukupnoPitanja'
      FROM Anketa as a LEFT JOIN AnketaPitanje as ap ON a.idAnketa=ap.idAnketa
      WHERE a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW()
      GROUP BY a.idAnketa
      ORDER BY a.{$orderByColumn} {$ascOrDesc}";
      return $this->ExecuteSQL($query);
      }
     */

    //funkcija radi ali sam je ponovo napisao
    /*
      function getAnketeInfoUnformed($idKreator = 0) {
      //odustao trebao mi je coutn pitanja
      // return $anketeUnformed = $this->Select('Anketa', array('idKreator' => $idKreator, 'stanje' => 0), "idAnketa DESC");
      //LEFT JOIN sam koristio u sličaju da anketea nema ni jedno pitanje, pa da bi se izlistala, odnosno ispisala nula za ukupnoPitanja
      $query = "SELECT a.*, COUNT(ap.idAnketa) as 'ukupnoPitanja'
      FROM Anketa as a LEFT JOIN AnketaPitanje as ap ON a.idAnketa=ap.idAnketa
      WHERE a.stanje='0' AND a.idKreator='{$idKreator}'
      GROUP BY a.idAnketa
      ORDER BY a.idAnketa DESC";

      return $this->ExecuteSQL($query);

      //array('stanje' => $stanje)
      // if (is_array($anketeUnformed == false)) {
      //     throw new Exception("Ne postoji anketa za izbrani'\$idAnketa'!");
      //}

      // SELECT a.*, COUNT(ap.idAnketa) as 'Ukupno pitanja'
      // FROM Anketa as a, AnketaPitanje as ap
      // WHERE a.idAnketa=ap.idAnketa AND ap.idAnketa=1
      // ORDER BY a.idAnketa DESC

      //SELECT a.*, COUNT(ap.idAnketa) as 'Ukupno pitanja'
      //FROM Anketa as a INNER JOIN AnketaPitanje as ap ON a.idAnketa=ap.idAnketa
      //GROUP BY a.idAnketa
      //ORDER BY a.idAnketa DESC
      }
     */
    function getNazivAnketeByIdAnketa($idAnketa) {
        $query = "SELECT a.naziv
                FROM Anketa  as a
                WHERE a.idAnketa='{$idAnketa}'";
        return $this->ExecuteSQL($query);
    }

    function getNazivAnketeByIdAnketniListic($idAnketniListic) {
        $query = "SELECT a.naziv
                FROM Anketa  as a
                WHERE a.idAnketa=(
                    SELECT idAnketa 
                    FROM AnketniListic as al
                    WHERE idAnketniListic='{$idAnketniListic}')";
        return $this->ExecuteSQL($query);
    }

    //ovo nevraća ukupan broj pitanja
    function getAnketeActive() {
        //return $this->Select('Anketa', array('stanje' => 1));

        $query = "SELECT* 
                FROM Ankete  
                WHERE a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW()";
        return $this->ExecuteSQL($query);
    }

    //ne može da se rade dva COUNT, radiće uvek onaj gde se uradilo prvo spajanje
    function getRezultatiAnketeInfo0($orderByColumn = "idAnketa", $ascOrDesc = "desc") {
        $query = "SELECT a.*, COUNT(al.idAnketa) as 'brIspitanika', COUNT(ap.idAnketa) as 'ukupnoPitanja'
                  FROM Anketa as a LEFT JOIN AnketaPitanje as ap ON a.idAnketa=ap.idAnketa INNER JOIN AnketniListic as al ON al.idAnketa=a.idAnketa
                  WHERE (a.stanje='1' OR a.stanje='2')
                  GROUP BY a.idAnketa, al.idAnketa
                  ORDER BY a.{$orderByColumn} {$ascOrDesc}";
        return $this->ExecuteSQL($query);
    }

    //ovde sam mogao da dam naziv metode getAnketeInfoWithBrojPopunjenihListica
    function getRezultatiAnketeInfo($orderByColumn = "idAnketa", $ascOrDesc = "desc") {
        $query = "SELECT a.*, COUNT(al.idAnketa) as 'brIspitanika'
                  FROM Anketa as a INNER JOIN AnketniListic as al ON al.idAnketa=a.idAnketa
                  WHERE (a.stanje='1' OR a.stanje='2')
                  GROUP BY a.idAnketa, al.idAnketa
                  ORDER BY a.{$orderByColumn} {$ascOrDesc}";
        return $this->ExecuteSQL($query);
        //getRezultatiAnketeInfo
    }

    function getAnketeInfoWithBrojPopunjenihListica($stanjeAnkete = null, $idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // null - sve aknkete
        // -1 - blokirana
        // 0 - u procesu kreiranja
        // 1 - aktivne
        // 2 - zavrsene
        // 3 - aktivne i završene
        $where = "";
        if ($stanjeAnkete === null) {
            $where = "";
        } else if ($stanjeAnkete == -1) {
            $where = "WHERE a.stanje='-1'";
        } else if ($stanjeAnkete == 0) {
            $where = "WHERE  a.stanje='0'";
        } else if ($stanjeAnkete == 1) {
            $where = "WHERE  a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW() ";
        } else if ($stanjeAnkete == 2) {
            //$where = "WHERE  a.stanje='2'"; odustao je je ne moguće da menjam stanje u završena kada prođe vreme
            $where = "WHERE  a.stanje='1' AND a.datumZavrsetka<NOW() ";
        } else if ($stanjeAnkete == 3) {
            $where = "WHERE  a.stanje='1'";
        } else {
            throw new InvalidArgumentException("Argument '\$stanjeAnkete', funkcije 'getAnketeInfo()', sadrži ne dozvoljenu vrednost!");
        }

        $isPopunjen = 1;
        /*
          if ($isPopunjen === null) {

          } else if ($isPopunjen == 0 || $isPopunjen == 1) {
          $where.=" AND al.isPopunjen='{$isPopunjen}'";
          } else {
          throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
          } */

        /*

          if ($isPopunjen === null) {

          } else if ($isPopunjen == 0) {
          $where.=" AND al.isPopunjen='{$isPopunjen}'";
          } else if ($isPopunjen == 1) {
          $where.=" AND al.isPopunjen=1 OR al.isPopunjen IS NULL";
          } else {
          throw new InvalidArgumentException("Argument '\$isPopunjen', funkcije 'getAnketniListiciInfo()', sadrži ne dozvoljenu vrednost!");
          }
         */


        if ($idKreator === null) {
            exit($idKreator);
        } else {

            $where.=" AND a.idKreator='{$idKreator}' ";
            //exit($where);
        }

        $upitDodatak = "";
        //WHERE 1=1 suži jer ne može da upit kreće sa WHERE AND naziv LIKE
        //$upit = "SELECT * FROM Anketa WHERE 1=1 ";
        //ako je filter pretrage je naziv ankete

        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $upitDodatak .= "AND a.naziv LIKE '%" . $kljucnaRec . "%' ";
            }
        }

        //i ovo iznad radi, ovde sam odsekao višak vreme
        //$query = "SELECT a.*, DATE_FORMAT(a.datumPocetka,'%Y-%m-%d') as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', sa.naziv as stanjeAnkete
        //a.*, DATE_FORMAT(a.datumPocetka,'%d.%m.%Y') as 'datumPocetka', DATE_FORMAT(a.datumZavrsetka,'%d.%m.%Y') as 'datumZavrsetka'
        $query = "SELECT a.*, DATE_FORMAT(a.datumPocetka,'%d.%m.%Y') as 'datumPocetka', DATE_FORMAT(a.datumZavrsetka,'%d.%m.%Y') as 'datumZavrsetka', COUNT(al.isPopunjen=1) as 'brIspitanika'
                    FROM Anketa as a 
                            LEFT JOIN 
                        AnketniListic as al ON al.idAnketa=a.idAnketa
                  {$where} {$upitDodatak}
                  GROUP BY a.idAnketa                  
                  ORDER BY a.{$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }

        return $this->ExecuteSQL($query);

        //getAnketeInfoWithBrojPopunjenihListica
    }

    function getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($idKreator = null, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        return $this->getAnketeInfoWithBrojPopunjenihListica(3, $idKreator, $od, $brZapisa, $kljucneReci, $orderByColumn, $ascOrDesc);
        //getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica
    }

    function getActiveAnketeInfoWithIsPopunjavana($idIspitanik, $od = 0, $brZapisa = 0, $kljucneReci = array(), $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // LEFT JOIN AnetaPitanje da bi moga da izbrojim pitanja, a pošto se to odnosi na sve ankete, a ne na određenu onda moram da radim GROUP BY
        //zbog ponavljanja zapisa(kada nema COUNT), a kad stavim COUNT(ap.idAnketa) prikazuje jedan red

        $upitDodatak = "";
        //WHERE 1=1 suži jer ne može da upit kreće sa WHERE AND naziv LIKE
        //$upit = "SELECT * FROM Anketa WHERE 1=1 ";
        //ako je filter pretrage je naziv ankete

        if (is_array($kljucneReci)) {
            foreach ($kljucneReci as $kljucnaRec) {
                $upitDodatak .= "AND naziv LIKE '%" . $kljucnaRec . "%' ";
            }
        }

        $query = "SELECT 
                        a.*, DATE_FORMAT(a.datumPocetka,'%d.%m.%Y') as 'datumPocetka', DATE_FORMAT(a.datumZavrsetka,'%d.%m.%Y') as 'datumZavrsetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', al.isPopunjen
                  FROM 
                        Anketa as a 
                            INNER JOIN 
                        AnketaPitanje as ap ON a.idAnketa=ap.idAnketa 
                            LEFT JOIN 
                        AnketniListic al ON a.idAnketa=al.idAnketa AND al.idIspitanik='{$idIspitanik}'
                  WHERE 
                        a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW() {$upitDodatak}
                  GROUP BY 
                        a.idAnketa
                  ORDER BY 
                        a.{$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }
        return $this->ExecuteSQL($query);

        //getAnketeInfoWithIsPopunjavana
    }

    function getActiveAnketeInfoWithIsPopunjavana0($idIspitanik, $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // LEFT JOIN AnetaPitanje da bi moga da izbrojim pitanja, a pošto se to odnosi na sve ankete, a ne na određenu onda moram da radim GROUP BY
        //zbog ponavljanja zapisa(kada nema COUNT), a kad stavim COUNT(ap.idAnketa) prikazuje jedan red
        $query = "SELECT 
                        a.*, DATE(a.datumPocetka) as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', al.isPopunjen
                  FROM 
                        Anketa as a 
                            INNER JOIN 
                        AnketaPitanje as ap ON a.idAnketa=ap.idAnketa 
                            LEFT JOIN 
                        AnketniListic al ON a.idAnketa=al.idAnketa AND al.idIspitanik='{$idIspitanik}'
                  WHERE 
                        a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW()
                  GROUP BY 
                        a.idAnketa
                  ORDER BY 
                        a.{$orderByColumn} {$ascOrDesc}";
        return $this->ExecuteSQL($query);
    }

//getAnketeInfoWithIsPopunjavana0

    function getActiveAnketeInfoWithIsPopunjavana00($idIspitanik, $od = 0, $brZapisa = 0, $orderByColumn = "idAnketa", $ascOrDesc = "asc") {
        // LEFT JOIN AnetaPitanje da bi moga da izbrojim pitanja, a pošto se to odnosi na sve ankete, a ne na određenu onda moram da radim GROUP BY
        //zbog ponavljanja zapisa(kada nema COUNT), a kad stavim COUNT(ap.idAnketa) prikazuje jedan red
        $query = "SELECT 
                        a.*, DATE(a.datumPocetka) as 'datumPocetka', COUNT(ap.idAnketa) as 'ukupnoPitanja', al.isPopunjen
                  FROM 
                        Anketa as a 
                            INNER JOIN 
                        AnketaPitanje as ap ON a.idAnketa=ap.idAnketa 
                            LEFT JOIN 
                        AnketniListic al ON a.idAnketa=al.idAnketa AND al.idIspitanik='{$idIspitanik}'
                  WHERE 
                        a.stanje='1' AND a.datumPocetka<=NOW() AND a.datumZavrsetka>NOW()
                  GROUP BY 
                        a.idAnketa
                  ORDER BY 
                        a.{$orderByColumn} {$ascOrDesc}";

        if ($brZapisa >= 1) {
            $query.=" LIMIT {$od}, {$brZapisa}";
        }
        return $this->ExecuteSQL($query);
    }

//getAnketeInfoWithIsPopunjavana00


    /*
      //Nazivi anketa vreme početka, završetka, broj, broj listića po strani trenutno popunjenih listića
      function getAnketeInfo($idKreatora) {

      }
     */

    function getAnketaInfo($idAnketa) {
        //Greška: Unknown column 'a.idRedosledPitanja' in 'on clause'
        //FROM Anketa as a, AnketaPitanje as ap INNER JOIN RedosledPitanja as rp ON rp.idRedosledPitanja=a.idRedosledPitanja
        $query = "SELECT a . *, COUNT(ap.idAnketa) as 'ukupnoPitanja', rp.opis as redosledPitanjaOpis
                  FROM AnketaPitanje as ap, Anketa as a INNER JOIN RedosledPitanja as rp ON rp.idRedosledPitanja=a.idRedosledPitanja
                  WHERE a.idAnketa = ap.idAnketa AND a.idAnketa = '{$idAnketa}'";
        return $this->ExecuteSQL($query);
        //getAnketaInfo
    }

    //niz ponuđenih odgovora za zadato pitanje
    private function getNizOdgovora($idPitanje) {
        //Za svako pitanje izvlači ponuđene odgovore
        $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                                   FROM Odgovor                            
                                   WHERE idPitanje='{$idPitanje}'");
        $nizOdg = array();
		//print_r($odg);die();
		
        //Pitanja slobodanUnost nemaju ponjuđene odgovore, i tom slučaju nije niz
        //if (is_array($odg)) {
        //proverava da li je dvodimenzionalni niz
        if (count($odg) != count($odg, 1)) {
            foreach ($odg as $o) {
                $nizOdg[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
            }
        } else {

            //$odg može da bude 'true', ako se upit dobro izvršio, ali nema rezultata
            if ($this->records == 1) {
                $nizOdg[] = new Odgovor($odg["idOdgovor"], $odg["tekst"]);
            }
        }
        return $nizOdg;
        //getNizOdgovora
    }

    function getOdgovorNestoDrugo($idPitanje) {
        $odg = parent::ExecuteSQL("SELECT idOdgovorNestoDrugo, tekst 
                                   FROM OdgovorNestoDrugo                            
                                   WHERE idOdgovorNestoDrugo =( SELECT idNestoDrugo FROM Pitanje WHERE idPitanje='{$idPitanje}')");
        if ($this->records == 1) {
            $odgovorNestoDrugo = new OdgovorNestoDrugo($odg["idOdgovorNestoDrugo"], $odg["tekst"], "");
            return $odgovorNestoDrugo;
        }

        return false;
        //getOdgovorNestoDrugo
    }

    //niz ponuđenih odgovora za zadato pitanje, ali sam odustao od ovoga
    private function getNizOdgovoraMatrica($idPitanje) {
        //Za svako pitanje izvlači ponuđene odgovore
        $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                                   FROM Odgovor 
                                   WHERE idPitanje IN (SELECT idPitanjeParent
                                                       FROM Pitanje 
                                                       WHERE idPitanje='{$idPitanje}')");
        $nizOdg = array();
        //Pitanja slobodanUnost nemaju ponjuđene odgovore, i tom slučaju nije niz
        //if (is_array($odg)) {
        //proverava da li je dvodimenzionalni niz
        if (count($odg) != count($odg, 1)) {
            foreach ($odg as $o) {
                $nizOdg[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
            }
        } else {

            //$odg može da bude 'true', ako se upit dobro izvršio, ali nema rezultata
            if ($this->records == 1) {
                $nizOdg[] = new Odgovor($odg["idOdgovor"], $odg["tekst"]);
            }
        }
        return $nizOdg;
    }

    //niz ponuđenih odgovora za zadato pitanje
    public function getNizPotpitanja($idPitanje) {
        //Za svako pitanje izvlači ponuđene odgovore
        $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                            FROM Potpitanje                            
                                            WHERE idPitanje='{$idPitanje}'");

        $nizPotUnos = array();
        //Pitanja slobodanUnost nemaju ponjuđena potpitanja, i tom slučaju nije niz
        //a ako ima jedno potpitanje, onda je to jednodimenzionalni niz, i ne treba da ulazimo u petlju
        //provera da li dvodimenzionalni niz
        if (count($nizPotpitanja) != count($nizPotpitanja, 1)) {
            foreach ($nizPotpitanja as $potpitanje) {
                $nizPotUnos[] = new PotpitanjeUnos($potpitanje["idPotpitanje"], $potpitanje["tekst"]);
            }
        } else {

            //$odg može da bude 'true', ako se upit dobro izvršio, ali nema rezultata
            if ($this->records == 1) {
                $nizPotUnos[] = new PotpitanjeUnos($nizPotpitanja["idPotpitanje"], $nizPotpitanja["tekst"]);
            }
        }
        return $nizPotUnos;
    }

    //Vraća anketni listić sa popunjenim odgovorima
    function getPitanjaAnketniListic($idAnketniListic, $od = 0, $brPitanja = 0) {
        $idAnketa = $this->ExecuteSQL("SELECT idAnketa
                                       FROM AnketniListic
                                       WHERE idAnketniListic='{$idAnketniListic}'");

        //vraca ne popunjena pitanja za izabranu anketu
        //$pitanjaAnketa = $this->getPitanjaAnketa($idAnketa['idAnketa'], $od, $brPitanja);
        $pitanjaAnketa = $this->getPitanjaAnketa($idAnketa['idAnketa'], $od, $brPitanja);

        //izvlači izabrane odgovore za određeni anketni listić
        $query = "SELECT idPitanje, idOdgovor
                  FROM AlOdgovor
                  WHERE idAnketniListic ='{$idAnketniListic}'";

        //izvlači izabrane odgovore za određeni anketni listić
        $query2 = "SELECT idPotpitanje, idOdgovor
                  FROM AlMatricaOdgovor
                  WHERE idAnketniListic ='{$idAnketniListic}'";

        //izvlači tekstualne odgovore za određeni anketni listić
        $query3 = "SELECT idPotpitanje, tekst
                   FROM AlTekstOdgovor
                   WHERE idAnketniListic ='{$idAnketniListic}'";

        //Ovde treba proveriti da li pitanje nesto drugo setovano
        $query4 = "SELECT idPitanje, tekst
                  FROM AlNestoDrugoOdgovor
                  WHERE idAnketniListic ='{$idAnketniListic}'";


        //niz izabranih odgovora      
        $odgovoriAnketniLitic = parent::ExecuteSQL($query);
		
		/*
		print_r($odgovoriAnketniLitic);
		die();
		*/

        //var_dump($odgovoriAnketniLitic);
        //exit();
		
        //niz izabranih odgovara matrice
        $odgovoriAnketniLiticMatrica = parent::ExecuteSQL($query2);

        //niz unetih odgovara za slobodan unos
        $odgovoriAnketniLiticSlobodanUnos = parent::ExecuteSQL($query3);

        //niz unetih odgovara za nešto drugo
        $odgovoriAnketniLiticNestoDrugo = parent::ExecuteSQL($query4);

        //Popuni pitanja sa izabranim odgovorima
        //is_array($)
        if (is_array($pitanjaAnketa)) { //ovo sam zadnje dodao
            foreach ($pitanjaAnketa as $pitanje) {
                //Ako je pitanje Matrica
                // if ($pitanje->getIdVrstaOdgovora() == 5 || $pitanje->getIdVrstaOdgovora() == 6) {
                if ($pitanje instanceof PitanjeMatricaCheckBox || $pitanje instanceof PitanjeMatricaRadio) {
                    //Prolazi kroz sve odgovore matrica
                    if (is_array($odgovoriAnketniLiticMatrica)) {
                        if (count($odgovoriAnketniLiticMatrica) != count($odgovoriAnketniLiticMatrica, 1)) {
                            foreach ($odgovoriAnketniLiticMatrica as $om) {
                                //Ako pitanje sadrži potpitanje, upiši izabrani odgovor
                                if ($pitanje->getKolekcijaPotpitanjaMatrica()->isSadrziPotpitanje($om['idPotpitanje'])) {
                                    //upisuje izabrani odgovor
                                    $pitanje->izaberiOdgovorMatrica($om['idPotpitanje'], $om['idOdgovor']);
                                }
                            }
                        } else {
                            //Ako pitanje sadrži potpitanje, upiši izabrani odgovor
                            if ($pitanje->getKolekcijaPotpitanjaMatrica()->isSadrziPotpitanje($odgovoriAnketniLiticMatrica['idPotpitanje'])) {
                                //upisuje izabrani odgovor
                                $pitanje->izaberiOdgovorMatrica($odgovoriAnketniLiticMatrica['idPotpitanje'], $odgovoriAnketniLiticMatrica['idOdgovor']);
                            }
                        }
                    }
                    //$pitanje->prikaziPotpitanja();
                    continue;
                }

                if ($pitanje instanceof PitanjeMatricaCheckBox2) {
                    if (is_array($odgovoriAnketniLiticMatrica)) {
                        // $potpitanjaMatrica = $pitanje->getPotpitanjaMatrica();
                        foreach ($odgovoriAnketniLiticMatrica as $o) {
                            $pitanje->izaberiOdgovorMatrica($o['idPotpitanje'], $o['idOdgovor']);
                        }
                    }
                    continue;
                }

                if ($pitanje instanceof PitanjeMatricaRadio2) {
                    if (is_array($odgovoriAnketniLiticMatrica)) {
                        // $potpitanjaMatrica = $pitanje->getPotpitanjaMatrica();
                        foreach ($odgovoriAnketniLiticMatrica as $o) {
                            $pitanje->izaberiOdgovorMatrica($o['idPotpitanje'], $o['idOdgovor']);
                        }
                    }
                    continue;
                }

                //if ($pitanje->getIdVrstaOdgovora() == 7 || $pitanje->getIdVrstaOdgovora() == 8 || $pitanje->getIdVrstaOdgovora() == 9) {
                if ($pitanje instanceof PitanjeSlobodanUnos) {
                    //Prolazi kroz sve odgovore slobodan unos
                    if (is_array($odgovoriAnketniLiticSlobodanUnos)) {
                        if (count($odgovoriAnketniLiticSlobodanUnos) != count($odgovoriAnketniLiticSlobodanUnos, 1)) {
                            foreach ($odgovoriAnketniLiticSlobodanUnos as $om) {
                                //Ako pitanje sadrži potpitanje, upiši izabrani odgovor
                                if ($pitanje->isSadrziPotpitanje($om['idPotpitanje'])) {
                                    //upisuje izabrani odgovor
                                    $pitanje->unesiOdgovor($om['idPotpitanje'], $om['tekst']);
                                }
                            }
                        } else {
                            if ($pitanje->isSadrziPotpitanje($odgovoriAnketniLiticSlobodanUnos['idPotpitanje'])) {
                                //upisuje izabrani odgovor
                                $pitanje->unesiOdgovor($odgovoriAnketniLiticSlobodanUnos['idPotpitanje'], $odgovoriAnketniLiticSlobodanUnos['tekst']);
                            }
                        }
                    }
                    continue;
                }

				//print_r($odgovoriAnketniLitic); die();
				
                //Prolazi kroz sve odgovore aketnih listića
                if (is_array($odgovoriAnketniLitic)) {
                    if (count($odgovoriAnketniLitic) != count($odgovoriAnketniLitic, 1)) {
                        foreach ($odgovoriAnketniLitic as $o) {
                            if ($o["idPitanje"] == $pitanje->getIdPitanje()) {
                                //Ponuđeni odgovori
                                // if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                //if ($o['drugo'] != null) {
                                //if ($o['idOdgovor'] == 0) {
                                //   if ($o['drugo'] != "") {
                                //     $pitanje->setNestoDrugoTekst($o['drugo']);
                                //} else {
                                //$pitanje->izaberiOdgovor($o['idOdgovor']);
                                //}
                                //} else {
                                //$pitanje->izaberiOdgovor($o['idOdgovor']);
                                //}
                                $pitanje->izaberiOdgovor($o['idOdgovor']);
                            }
                        }
                    } else {
							//print_r($odgovoriAnketniLitic); die();
					
                        if ($odgovoriAnketniLitic["idPitanje"] == $pitanje->getIdPitanje()) {
                            //Ponuđeni odgovori
                            if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                //if ($odgovoriAnketniListic['drugo'] != null) {
                                //if ($odgovoriAnketniLitic['idOdgovor'] == 0) {
                                //  if ($odgovoriAnketniLitic['drugo'] != "") {
                                //$pitanje->setNestoDrugoTekst($odgovoriAnketniLitic['drugo']);
                                // } else {
                                //    $pitanje->izaberiOdgovor($odgovoriAnketniLitic['idOdgovor']);
                                // }
                                //} else {
                                //  $pitanje->izaberiOdgovor($odgovoriAnketniLitic['idOdgovor']);
                                //}
                            }
                            $pitanje->izaberiOdgovor($odgovoriAnketniLitic['idOdgovor']);
                        }
                    }
                }





                //Prolazi kroz sve odgovore Nešto drugo
                if (is_array($odgovoriAnketniLiticNestoDrugo)) {
                    if (count($odgovoriAnketniLiticNestoDrugo) != count($odgovoriAnketniLiticNestoDrugo, 1)) {
                        foreach ($odgovoriAnketniLiticNestoDrugo as $o) {
                            if ($o["idPitanje"] == $pitanje->getIdPitanje()) {
                                //Ponuđeni odgovori
                                if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                    //if ($o['drugo'] != null) {
                                    //if ($o['idOdgovor'] == 0) {
                                    if ($o['tekst'] != "") {
                                        $pitanje->setNestoDrugoTekst($o['tekst']);
                                    }
                                }
                            }
                        }
                    } else {
                        if ($odgovoriAnketniLiticNestoDrugo["idPitanje"] == $pitanje->getIdPitanje()) {
                            //Ponuđeni odgovori
                            if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                if ($odgovoriAnketniLiticNestoDrugo['tekst'] != "") {
                                    $pitanje->setNestoDrugoTekst($odgovoriAnketniLiticNestoDrugo['tekst']);
                                }
                            }
                        }
                    }
                }
            }//foreach ($pitanjaAnketa as $pitanje)
        }
        return $pitanjaAnketa;
    }

    //Vraća ne popunja pitanja za izabranu anketu
    function getPitanjaAnketa($idAnketa, $od = 0, $brPitanja = 0) {
        // $upit = "SELECT * FROM Pitanje WHERE idPitanje IN( SELECT idPitanje FROM AnketaPitanje WHERE idAnketa='{$idAnketa}')";
        //ne valja
        // $upit = "SELECT p.*, ap.isObavezno FROM Pitanje as p, AnketaPitanje as ap WHERE p.idPitanje IN( SELECT idPitanje FROM AnketaPitanje WHERE idAnketa='{$idAnketa}')";
        //neki glupi bag kad radimo sa bitovima, pa moramo da sabiramo sa 0, inače vraća prazan string
        //nahnadno dodao isNestoDrugo
        /* order by nahnadno dodao zbog rednog broja */
        //$upit = "SELECT p.*, (isObavezno+0) as isObavezno
        $upit = "SELECT p.*, ap.isObavezno
                 FROM Pitanje as p, AnketaPitanje as ap 
                 WHERE p.idPitanje=ap.idPitanje AND ap.idAnketa='{$idAnketa}'                
                 ORDER BY ap.redniBroj asc";


        if ($brPitanja >= 1) {
            $upit.=" LIMIT {$od}, {$brPitanja}";
        }
        $pitanja = $this->getPitanja($upit);
        return $pitanja;
        //getPitanjaAnketa
    }

    function getPitanjaSva() {
        $upit = "SELECT * FROM Pitanje";
        return $this->getPitanja($upit);

        //getPitanjaSva
    }

    function getPitanje($idPitanje) {
        //ovo se mora prepraviti jer nam i nije bitno da li je obavezno kad menjamo pitanje
        //$upit = "SELECT p.*, (isObavezno+0) as isObavezno
        $upit = "SELECT p.*, ap.isObavezno    
                 FROM Pitanje as p, AnketaPitanje as ap 
                 WHERE p.idPitanje=ap.idPitanje AND p.idPitanje='{$idPitanje}'";

        //$n = $this->getPitanja("SELECT * FROM Pitanje WHERE idPitanje='{$idPitanje}'");
        $pitanje = $this->getPitanja($upit);

        return $pitanje;
        //getPitanje()
    }

    private function getPitanja($upit) {
        //Dohovati sva pitanja
        //$nizPitanje = parent::Select("Pitanje");
        //Vraca dvodimenzional niz pitanja ili jednodimenzionalni usluču da ima samo jedno pitanje
        $nizPitanje = parent::ExecuteSQL($upit);
							
		/*
		$nizPitanje = parent::ExecuteSQL("Select * FROM Korisnik Where state=0");
		echo '<pre>';
		print_r($nizPitanje);
		die();
		*/

        //Ovde smeštamo napravljenje objekte pitanja
        $pitanja = array();
        // $pitanjaMatrica = new PotpitanjaMatrica();
        $p = null;
        if ($this->records > 0) {
            if ($this->records == 1) {
                /* $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                  FROM Odgovor
                  WHERE idPitanje='{$nizPitanje['idPitanje']}'");
                  $p = new Pitanje($nizPitanje["idPitanje"], $nizPitanje["idKreator"], $nizPitanje["tekst"],
                  $odg, $nizPitanje['idVrstaOdgovora'], $nizPitanje['isObavezno']
                  );
                  $pitanja[] = $p;
                 */

                $pit = $nizPitanje;

                //Za svako pitanje izvlači ponuđene odgovore
                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje='{$pit['idPitanje']}'");
                 */

                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje=(SELECT idPitanjeParent
                  FROM Pitanje
                  WHERE idPitanje='{$pit['idPitanje']}')");
                 */


                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje='{$idPitanje}'");
                 */

                $nizOdg = $this->getNizOdgovora($pit['idPitanje']);
                //$nizOdg2 = $this->getNizOdgovoraMatrica($pit['idPitanje']);
                //Napravi pitanja u zavisnosti od vrste pitanja
                switch ($pit['idVrstaOdgovora']) {
                    case 1: {
                            if ($pit['idNestoDrugo'] >0) {
                                $odgovorNestoDrugo = $this->getOdgovorNestoDrugo($pit['idPitanje']);
                                $pitanja[] = new PitanjeRadioWithNestoDrugo($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $odgovorNestoDrugo, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                            } else {
                                $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                            }
                            break;
                        }
                    case 2: {
                            $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 3: {
                            if ($pit['idNestoDrugo'] > 0) {
                                $odgovorNestoDrugo = $this->getOdgovorNestoDrugo($pit['idPitanje']);
                                $pitanja[] = new PitanjeCheckBoxWithNestoDrugo($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $odgovorNestoDrugo, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                            } else {
                                $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                            }
                            // $p = new PitanjeRadio((int) $pit["idPitanje"], (int) $pit["idKreator"], $pit["tekst"], $odg, (int) $pit['idVrstaOdgovora'], (bool) $pit['isObavezno']);
                            break;
                            //
                            //$p = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                            //                $odg, $pit['idVrstaOdgovora'], $pit['isObavezno']
                            //);
                            // break;
                        }
                    case 4: {
                            $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 5: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                            //popunjavanje kolekcije potpitanja matrice
                            // $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            $potpitanjaMatrica = array();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($nizOdg as $o) {
                                    $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                }
                                $potpitanjaMatrica[] = new PitanjeRadio($potpitanje['idPotpitanje'], $pit["idKreator"], $potpitanje['tekst'], $nizOdgovori, $pit['idVrstaOdgovora'], $pit['isObavezno']);
                                //$potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }

                            //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                            $pitanja[] = new PitanjeMatricaRadio2($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 6: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                            //popunjavanje kolekcije potpitanja matrice
                            // $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            $potpitanjaMatrica = array();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($nizOdg as $o) {
                                    $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                }
                                $potpitanjaMatrica[] = new PitanjeCheckBox($potpitanje['idPotpitanje'], $pit["idKreator"], $potpitanje['tekst'], $nizOdgovori, $pit['idVrstaOdgovora'], $pit['isObavezno']);
                                //$potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }

                            //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                            $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 7: {     //7 i 8 i  9 je isti kod                           
                            //tekst numeriče vrednosti
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 8: {
                            //tekst string jedan ili više
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 9: {
                            //tekst jedan dugačak

                            /*
                              //uzima potpitanja
                              $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst
                              FROM Potpitanje
                              WHERE idPitanje='{$pit['idPitanje']}'");

                              //pravi niz objekata potpitanjaUnos
                              $potpitanjaUnos = array();
                              var_dump($nizPotpitanja);
                              foreach ($nizPotpitanja as $potpitanje) {
                              $potpitanjaUnos[] = new PotpitanjeUnos($potpitanje["idPotpitanje"], $potpitanje["tekst"]);
                              }
                             */

                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 10: {
                            //vreme                        
                        }

                    case 11: {
                            $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 12: {
                            $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            // $p = new PitanjeRadio((int) $pit["idPitanje"], (int) $pit["idKreator"], $pit["tekst"], $odg, (int) $pit['idVrstaOdgovora'], (bool) $pit['isObavezno']);
                            break;
                        }
                }//switch                   
                /////////////////////////////////////////////////////
            } else {

                foreach ($nizPitanje as $pit) {
                    //Za svako pitanje izvlači ponuđene odgovore
                    /*
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje='{$pit['idPitanje']}'");

                     */
                    /*
                      //dodao
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje IN (SELECT idPitanjeParent
                      FROM Pitanje
                      WHERE idPitanje='{$pit['idPitanje']}')");
                     */
                    /*
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje='{$idPitanje}'");
                     */
                    $nizOdg = $this->getNizOdgovora($pit['idPitanje']);

                    //$nizOdg2 = $this->getNizOdgovoraMatrica($pit['idPitanje']);
                    //Napravi pitanja u zavisnosti od vrste pitanja
                    switch ($pit['idVrstaOdgovora']) {
                        case 1: {
                                if ($pit['idNestoDrugo'] > 0) {
                                    $odgovorNestoDrugo = $this->getOdgovorNestoDrugo($pit['idPitanje']);

                                    //var_dump($pit);

                                    $pitanja[] = new PitanjeRadioWithNestoDrugo($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                    //$nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                                    //dodao trecu argument      
                                                    $nizOdg, $odgovorNestoDrugo, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                    );
                                } else {
                                    $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                    //$nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                                    $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']);
                                }
                                break;
                            }
                        case 2: {
                                $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 3: {
                                if ($pit['idNestoDrugo'] > 0) {

                                    $odgovorNestoDrugo = $this->getOdgovorNestoDrugo($pit['idPitanje']);
                                    $pitanja[] = new PitanjeCheckBoxWithNestoDrugo($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                    $nizOdg, $odgovorNestoDrugo, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                    );
                                } else {
                                    $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                    $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                    );
                                }
                                // $p = new PitanjeRadio((int) $pit["idPitanje"], (int) $pit["idKreator"], $pit["tekst"], $odg, (int) $pit['idVrstaOdgovora'], (bool) $pit['isObavezno']);
                                break;
                                //
                                //$p = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                //                $odg, $pit['idVrstaOdgovora'], $pit['isObavezno']
                                //);
                                // break;
                            }
                        case 4: {
                                $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 5: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                // $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                $potpitanjaMatrica = array();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($nizOdg as $o) {
                                        $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                    }
                                    $potpitanjaMatrica[] = new PitanjeRadio($potpitanje['idPotpitanje'], $pit["idKreator"], $potpitanje['tekst'], $nizOdgovori, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']);
                                    //$potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }

                                //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                                $pitanja[] = new PitanjeMatricaRadio2($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 6: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                // $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                $potpitanjaMatrica = array();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($nizOdg as $o) {
                                        $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                    }
                                    $potpitanjaMatrica[] = new PitanjeCheckBox($potpitanje['idPotpitanje'], $pit["idKreator"], $potpitanje['tekst'], $nizOdgovori, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']);
                                    //$potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }

                                //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                                $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 7: {     //7 i 8 i  9 je isti kod                           
                                //tekst numeriče vrednosti
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 8: {
                                //tekst string jedan ili više
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 9: {
                                //tekst jedan dugačak

                                /*
                                  //uzima potpitanja
                                  $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst
                                  FROM Potpitanje
                                  WHERE idPitanje='{$pit['idPitanje']}'");

                                  //pravi niz objekata potpitanjaUnos
                                  $potpitanjaUnos = array();
                                  var_dump($nizPotpitanja);
                                  foreach ($nizPotpitanja as $potpitanje) {
                                  $potpitanjaUnos[] = new PotpitanjeUnos($potpitanje["idPotpitanje"], $potpitanje["tekst"]);
                                  }
                                 */

                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 10: {
                                $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $pitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }

                        case 11: {
                                $pitanjaMatrica->addPotpitanje(new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg2, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                ));

                                break;
                            }
                        //}
                    }//switch                   
                    //$pitanja[] = $p;
                }//foreach
            }

            //Štampanje pitanja
            //foreach ($pitanja as $p) {
            //    $p->prikazi();
            //}
            // exit();
            return $pitanja;
        }
        return false;
        //getPitanja
    }

    function getPitanja2() {
        //Dohovati sva pitanja
        //$nizPitanje = parent::Select("Pitanje");
        //Vraca dvodimenzional niz pitanja ili jednodimenzionalni usluču da ima samo jedno pitanje
        $nizPitanje = parent::ExecuteSQL($upit);

        //Ovde smeštamo napravljenje objekte pitanja
        $pitanja = array();
        $pitanjaMatrica = new PotpitanjaMatrica();
        $p = null;
        if ($this->records > 0) {
            if ($this->records == 1) {
                /* $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                  FROM Odgovor
                  WHERE idPitanje='{$nizPitanje['idPitanje']}'");
                  $p = new Pitanje($nizPitanje["idPitanje"], $nizPitanje["idKreator"], $nizPitanje["tekst"],
                  $odg, $nizPitanje['idVrstaOdgovora'], $nizPitanje['isObavezno']
                  );
                  $pitanja[] = $p;
                 */
                $pit = $nizPitanje;
                ///////////////////////////////////////////////////
                //Za svako pitanje izvlači ponuđene odgovore
                $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                                               FROM Odgovor 
                                               WHERE idPitanje='{$pit['idPitanje']}'");

                $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                                               FROM Odgovor 
                                               WHERE idPitanje=(SELECT idPitanje
                                                                FROM Pitanje 
                                                                WHERE idPitanje='{$pit['idPitanje']}'");

                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje='{$idPitanje}'");
                 */

                $nizOdg = $this->getNizOdgovora($pit['idPitanje']);


                //Napravi pitanja u zavisnosti od vrste pitanja
                switch ($pit['idVrstaOdgovora']) {
                    case 1: {
                            $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 2: {
                            $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 3: {
                            $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 4: {
                            $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 5: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");
                            //popunjavanje kolekcije potpitanja matrice
                            $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($odg as $o) {
                                    $nizOdgovori[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
                                }

                                $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }

                            $pitanja[] = new PitanjeMatricaRadio($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 6: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                            //popunjavanje kolekcije potpitanja matrice
                            $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($odg as $o) {
                                    $nizOdgovori[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
                                }

                                $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }
                            //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                            $pitanja[] = new PitanjeMatricaCheckBox($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 7: {     //7 i 8 i  9 je isti kod                           
                            //tekst numeriče vrednosti
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 8: {
                            //tekst string jedan ili više
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 9: {
                            //tekst jedan dugačak

                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 10: {
                            //vreme                        
                            $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $pitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }

                    case 11: {
                            $pitanjaMatrica->addPotpitanje(new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            ));

                            break;
                        }
                }//switch                   
            } else {

                foreach ($nizPitanje as $pit) {
                    //Za svako pitanje izvlači ponuđene odgovore
                    $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                                               FROM Odgovor 
                                               WHERE idPitanje='{$pit['idPitanje']}'");

                    /*
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje='{$idPitanje}'");
                     */
                    $nizOdg = $this->getNizOdgovora($pit['idPitanje']);
                    //Napravi pitanja u zavisnosti od vrste pitanja

                    switch ($pit['idVrstaOdgovora']) {
                        case 1: {
                                $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 2: {
                                $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 3: {
                                $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );

                                break;
                            }
                        case 4: {
                                $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 5: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($odg as $o) {
                                        $nizOdgovori[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
                                    }

                                    $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }

                                $pitanja[] = new PitanjeMatricaRadio($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 6: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($odg as $o) {
                                        $nizOdgovori[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
                                    }

                                    $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }


                                //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                                $pitanja[] = new PitanjeMatricaCheckBox($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 7: {     //7 i 8 i  9 je isti kod                           
                                //tekst numeriče vrednosti
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 8: {
                                //tekst string jedan ili više
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 9: {
                                //tekst jedan dugačak


                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 10: {
                                //vreme                        
                                $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $pitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }

                        case 11: {
                                $pitanjaMatrica->addPotpitanje(new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                ));

                                break;
                            }
                        //}
                    }//switch                   
                    //$pitanja[] = $p;
                }//foreach
            }
            return $pitanja;
        }
        return false;
    }

    private function getPitanja0($upit) {
        //Dohovati sva pitanja
        //$nizPitanje = parent::Select("Pitanje");
        //Vraca dvodimenzional niz pitanja ili jednodimenzionalni usluču da ima samo jedno pitanje
        $nizPitanje = parent::ExecuteSQL($upit);

        //Ovde smeštamo napravljenje objekte pitanja
        $pitanja = array();
        $pitanjaMatrica = new PotpitanjaMatrica();
        $p = null;
        if ($this->records > 0) {
            if ($this->records == 1) {
                /* $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst 
                  FROM Odgovor
                  WHERE idPitanje='{$nizPitanje['idPitanje']}'");
                  $p = new Pitanje($nizPitanje["idPitanje"], $nizPitanje["idKreator"], $nizPitanje["tekst"],
                  $odg, $nizPitanje['idVrstaOdgovora'], $nizPitanje['isObavezno']
                  );
                  $pitanja[] = $p;
                 */

                $pit = $nizPitanje;

                //Za svako pitanje izvlači ponuđene odgovore
                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje='{$pit['idPitanje']}'");
                 */

                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje=(SELECT idPitanjeParent
                  FROM Pitanje
                  WHERE idPitanje='{$pit['idPitanje']}')");
                 */


                /*
                  $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                  FROM Odgovor
                  WHERE idPitanje='{$idPitanje}'");
                 */

                $nizOdg = $this->getNizOdgovora($pit['idPitanje']);
                $nizOdg2 = $this->getNizOdgovoraMatrica($pit['idPitanje']);

                //Napravi pitanja u zavisnosti od vrste pitanja
                switch ($pit['idVrstaOdgovora']) {
                    case 1: {
                            $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 2: {
                            $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 3: {
                            $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            // $p = new PitanjeRadio((int) $pit["idPitanje"], (int) $pit["idKreator"], $pit["tekst"], $odg, (int) $pit['idVrstaOdgovora'], (bool) $pit['isObavezno']);
                            break;
                            //
                            //$p = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                            //                $odg, $pit['idVrstaOdgovora'], $pit['isObavezno']
                            //);
                            // break;
                        }
                    case 4: {
                            $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 5: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                 FROM Potpitanje 
                                                                 WHERE idPitanje='{$pit['idPitanje']}'");

                            //popunjavanje kolekcije potpitanja matrice
                            $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($nizOdg as $o) {
                                    $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                }

//                                foreach ($odg as $o) {
//                                    $nizOdgovori[] = new Odgovor($o["idOdgovor"], $o["tekst"]);
//                                }

                                $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }

                            $pitanja[] = new PitanjeMatricaRadio($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 6: {
                            //uzima potpitanja                            
                            $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                            //popunjavanje kolekcije potpitanja matrice
                            $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                            foreach ($nizPotpitanja as $potpitanje) {
                                //za svako potpitanje napravi niz objekata Odgovori
                                $nizOdgovori = array();
                                foreach ($nizOdg as $o) {
                                    $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                }

                                $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                            }

                            //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                            $pitanja[] = new PitanjeMatricaCheckBox($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 7: {     //7 i 8 i  9 je isti kod                           
                            //tekst numeriče vrednosti
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 8: {
                            //tekst string jedan ili više
                            //uzima potpitanja
                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 9: {
                            //tekst jedan dugačak

                            /*
                              //uzima potpitanja
                              $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst
                              FROM Potpitanje
                              WHERE idPitanje='{$pit['idPitanje']}'");

                              //pravi niz objekata potpitanjaUnos
                              $potpitanjaUnos = array();
                              var_dump($nizPotpitanja);
                              foreach ($nizPotpitanja as $potpitanje) {
                              $potpitanjaUnos[] = new PotpitanjeUnos($potpitanje["idPotpitanje"], $potpitanje["tekst"]);
                              }
                             */

                            $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                            $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }
                    case 10: {
                            //vreme                        
                            $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $pitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                            $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            );
                            break;
                        }

                    case 11: {
                            $pitanjaMatrica->addPotpitanje(new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                            $nizOdg2, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                            ));

                            break;
                        }
                }//switch                   
                /////////////////////////////////////////////////////
            } else {

                foreach ($nizPitanje as $pit) {
                    //Za svako pitanje izvlači ponuđene odgovore

                    $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje='{$pit['idPitanje']}'");


                    /*
                      //dodao
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje IN (SELECT idPitanjeParent
                      FROM Pitanje
                      WHERE idPitanje='{$pit['idPitanje']}')");
                     */
                    /*
                      $odg = parent::ExecuteSQL("SELECT idOdgovor, tekst
                      FROM Odgovor
                      WHERE idPitanje='{$idPitanje}'");
                     */
                    $nizOdg = $this->getNizOdgovora($pit['idPitanje']);
                    $nizOdg2 = $this->getNizOdgovoraMatrica($pit['idPitanje']);

                    //Napravi pitanja u zavisnosti od vrste pitanja
                    switch ($pit['idVrstaOdgovora']) {
                        case 1: {
                                $pitanja[] = new PitanjeRadio($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 2: {
                                $pitanja[] = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 3: {
                                $pitanja[] = new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                // $p = new PitanjeRadio((int) $pit["idPitanje"], (int) $pit["idKreator"], $pit["tekst"], $odg, (int) $pit['idVrstaOdgovora'], (bool) $pit['isObavezno']);
                                break;
                                //
                                //$p = new PitanjeSelect($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                //                $odg, $pit['idVrstaOdgovora'], $pit['isObavezno']
                                //);
                                // break;
                            }
                        case 4: {
                                $pitanja[] = new PitanjeSelectMultiple($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 5: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($nizOdg as $o) {
                                        $nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                    }

                                    $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }

                                $pitanja[] = new PitanjeMatricaRadio($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 6: {
                                //uzima potpitanja                            
                                $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst 
                                                                     FROM Potpitanje 
                                                                     WHERE idPitanje='{$pit['idPitanje']}'");

                                //popunjavanje kolekcije potpitanja matrice
                                $potpitanjaMatrica = new KolekcijaPotpitanjaMatrica();
                                foreach ($nizPotpitanja as $potpitanje) {
                                    //za svako potpitanje napravi niz objekata Odgovori
                                    $nizOdgovori = array();
                                    foreach ($odg as $o) {
                                        //$nizOdgovori[] = new Odgovor($o->getIdOdgovor(), $o->getTekst());
                                        $nizOdgovori[] = new Odgovor($o['idOdgovor'], $o['tekst']);
                                    }

                                    $potpitanjaMatrica->addPotpitanjeMatrica(new PotpitanjeMatrica($potpitanje['idPotpitanje'], $potpitanje['tekst'], $nizOdgovori));
                                }

                                //ne treba foreach ($nizPotpitanje as $potpitanje) {}                               
                                $pitanja[] = new PitanjeMatricaCheckBox($pit["idPitanje"], $potpitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 7: {     //7 i 8 i  9 je isti kod                           
                                //tekst numeriče vrednosti
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosBrojevi($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 8: {
                                //tekst string jedan ili više
                                //uzima potpitanja
                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 9: {
                                //tekst jedan dugačak

                                /*
                                  //uzima potpitanja
                                  $nizPotpitanja = parent::ExecuteSQL("SELECT idPotpitanje, tekst
                                  FROM Potpitanje
                                  WHERE idPitanje='{$pit['idPitanje']}'");

                                  //pravi niz objekata potpitanjaUnos
                                  $potpitanjaUnos = array();
                                  var_dump($nizPotpitanja);
                                  foreach ($nizPotpitanja as $potpitanje) {
                                  $potpitanjaUnos[] = new PotpitanjeUnos($potpitanje["idPotpitanje"], $potpitanje["tekst"]);
                                  }
                                 */

                                $potpitanjaUnos = $this->getNizPotpitanja($pit['idPitanje']);
                                $pitanja[] = new PitanjeSlobodanUnosDugacakTekst($pit["idPitanje"], $pit["idKreator"], $pit["tekst"], $potpitanjaUnos,
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }
                        case 10: {
                                $pitanja[] = new PitanjeMatricaCheckBox2($pit["idPitanje"], $pitanjaMatrica, $pit["idKreator"], $pit["tekst"],
                                                $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                );
                                break;
                            }

                        case 11: {
                                $pitanjaMatrica->addPotpitanje(new PitanjeCheckBox($pit["idPitanje"], $pit["idKreator"], $pit["tekst"],
                                                $nizOdg2, $pit['idVrstaOdgovora'], $pit['isObavezno'], $pit['stanje']
                                ));

                                break;
                            }
                        //}
                    }//switch                   
                    //$pitanja[] = $p;
                }//foreach
            }

            //Štampanje pitanja
            //foreach ($pitanja as $p) {
            //    $p->prikazi();
            //}
            // exit();
            return $pitanja;
        }
        return false;
        //getPitanja0
    }

}
