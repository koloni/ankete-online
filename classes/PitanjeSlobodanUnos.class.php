<?php

class PotpitanjeUnos {

    protected $tekst;
    //Ovo mi ne treba jer, svako potpitanje može da ima sam jedan odgovor    
    //protected $nizOdgovora = array();
    protected $odgovor;
    protected $idPotpitanje;

    public function __construct($idPotpitanje, $tekst, $odgovor = "") {
        $this->idPotpitanje = $idPotpitanje;
        $this->tekst = $tekst;
        $this->odgovor = $odgovor;
        $this->opisVrste = "Izbor jednog odgovora od više ponuđenih (radio dugme)";
    }

    public function getOdgovor() {
        return $this->odgovor;
    }

    public function getTekst() {
        return $this->tekst;
    }

    function unesiOdgovor($tekst) {
        $this->odgovor = $tekst;
    }

    public function getIdPotpitanje() {
        return $this->idPotpitanje;
    }

    // public function getIdOdgovor() {
    //     return $this->idPotpitanje;
    //}
//end class
}

abstract class PitanjeSlobodanUnos extends Pitanje {

    //ako nema potpitanja ovde stoje odgovori
    //protected $odgovori;
    protected $nizPotpitanjaUnos = array();

    public function __construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, array(), $idVrstaOdgovora, $isObavezno, $stanje);

        $this->nizPotpitanjaUnos = $potpitanjaUnos;
        $this->opisVrste = "Izbor jednog odgovora od više ponuđenih (radio dugme)";
    }

    //dodavanje jednog PotpitanjaUnos objekta
    public function addPotpitanjeUnos($potpitanjeUnos) {
        if (!$potpitanjeUnos instanceof PotpitanjeUnos) {
            throw new Exception("Argument '\$potpitanjeMatrica', funkcije 'addPotpitanjeUnos', treba da bude tipa 'PotpitanjeUnos'");
        }
        $this->nizPotpitanjaUnos[] = $potpitanjeUnos;
    }

    function unesiOdgovor($idPotpitanje, $tekst) {
        if (sizeof($this->nizPotpitanjaUnos) == 0) {
            throw new Exception("Ne postoji ni jedno potpitanje!");
        }
        //prolazi kroz sva potpitanja
        foreach ($this->nizPotpitanjaUnos as $pu) {
            //ako pronađe potpitanje, izabira odgovor
            if ($pu->getIdPotpitanje() == $idPotpitanje) {
                $pu->unesiOdgovor($tekst);
                return;
            }
        }
        throw new Exception("Ne postoji potpitanje za izabrano '\$idPotpitanje'!");
    }

    public function getPotpitanja() {
        return $this->nizPotpitanjaUnos;
    }

    // public function getOdgovori() {                
    //    return $this->nizPotpitanjaUnos;
    //}

    public function getBrojPotpitanja() {
        return count($this->nizPotpitanjaUnos);
    }

    //Da li potpitanje postoji
    public function isSadrziPotpitanje($idPotpitanje) {
        if (is_int($idPotpitanje + 0) == false) {
            throw new InvalidArgumentException("Argument '\$idPotpitanje', funkcije 'isSadrziPotpitanje', mora da bude tipa 'integer'!");
        }
        foreach ($this->nizPotpitanjaUnos as $potpitanje) {
            if ($potpitanje->getIdPotpitanje() == $idPotpitanje) {
                return true;
            }
        }
        return false;
    }

    function prikazi() {
        echo "<div class='pitanje'>";
        // echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}[]gr'></div>";
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";
        echo "<table border='0px' style='font-size:14px;'>";

        //prikazuje kolonu potpitanja 
        foreach ($this->nizPotpitanjaUnos as $pot) {
            echo '<tr>';

            //echo "<td align='right'>";
            echo "<td style='text-align:left;'>";
            echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}gr'></div>";
            //echo $num . ". <span style='padding:5px;'>" . $pot->getTekst() . "</span>";
            echo $num . ". " . $pot->getTekst();
            echo "</td>";

            //prikazuje textbox kontrole
            echo "<td>";
            echo "<input {$this->disabled} type='text' size='50' name='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}' value='{$pot->getOdgovor()}' id='{$pot->getIdPotpitanje()}' onclick='obrisiGresku(this);'><br/>";
            echo '</td>';
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

    function prikazi0() {
        echo "<div class='pitanje'>";
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";
        echo "<table border='0px'>";

        //prikazuje kolonu potpitanja 
        foreach ($this->nizPotpitanjaUnos as $pot) {
            echo '<tr>';

            //echo "<td align='right'>";
            echo "<td style='text-align:left;'>";
            //echo $num . ". <span style='padding:5px;'>" . $pot->getTekst() . "</span>";
            echo $num . ". " . $pot->getTekst();
            echo "</td>";

            //prikazuje textbox kontrole
            echo "<td>";
            echo "<input type='text' size='50' name='{$pot->getIdPotpitanje()}' value='{$pot->getOdgovor()}' id='{$pot->getIdPotpitanje()}'><br/>";
            echo '</td>';
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

}

?>
