<?php

class PitanjeMatricaRadio extends Pitanje {

    private $potpitanja;
    private $kolekcijaPotpitanjaMatrica;

    public function PitanjeMatricaRadio($idPitanje, $potpitanjaMatrica, $idKreator, $tekst, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori(), $idVrstaOdgovora, $isObavezno, $stanje);

        $this->kolekcijaPotpitanjaMatrica = $potpitanjaMatrica;
        $this->potpitanja = $potpitanjaMatrica;
        $this->opisVrste = "Matrica odgovora, jedan izbor po redu";        
    }

    function getKolekcijaPotpitanjaMatrica() {
        return $this->kolekcijaPotpitanjaMatrica;
    }

    function izaberiOdgovorMatrica($idPotpitanje, $idOdgovor) {
        $this->kolekcijaPotpitanjaMatrica->izaberiOdgovor($idPotpitanje, $idOdgovor);
    }

    //ne koristim
    function izaberiOdgovor($idPotpitanje) {
        
    }

    function prikaziPotpitanja() {
        $this->kolekcijaPotpitanjaMatrica->prikazi();
    }

    /*

     */

    function prikazi() {
        echo "<div class='pitanje'>";
        foreach ($this->kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
            //echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}gr'></div>";
        }
        //echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        //echo "</p>";
        //echo "<div style='padding: 0 0 0 15px;'>";
        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:10px;'>Izaberite jedan ili više odgovora</span></div>";        
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";

        echo "<table border='0px' cellspacing='0' width='600px' class='table_pitanje'>";
        echo "<tr><td></td>";

        $odgovori = $this->kolekcijaPotpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori();
        //prikazuje podunjene odgovore (prvi red tabele)   

        foreach ($odgovori as $odg) {
            echo "<td style='alignment-adjust: central;'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
            //echo "<td width='100px'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
        }
        echo "</tr>";

        $i = 0; //zbog parnih i neparnih kolona
        //prikazuje kolonu potpitanja 
        foreach ($this->kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
            if ($i++ % 2 == 0) {
                echo "<tr style='background-color: lightgrey; color: black;'>";
            } else {
                echo "<tr style='color: black;'>";
            }

            //echo "<td align='right'>";                        
            echo "<td style='text-align:left;'>";
            echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}gr'></div>";
            echo $num . ". " . $pot->getTekst();
            echo "</td>";

            //prikazuje radio kontrole
            foreach ($pot->getOdgovori() as $odg) {
                //proverava da li je izabran odgovor
                if ($odg->getIsIzabran()) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                //možda bi id trebalo da bude {$pot->getIdPotpitanje()}.$odg->getIdOdgovor()
                //echo "<td width='100px'>";
                echo "<td style=''>";
                echo "<input {$this->disabled} type='radio' name='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

    function prikazi0() {
        echo "<div class='pitanje'>";
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";

        echo "<table border='0px' cellspacing='0' width='500px' class='table_pitanje'>";
        echo "<tr><td></td>";

        $odgovori = $this->kolekcijaPotpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori();
        //prikazuje podunjene odgovore (prvi red tabele)   
        foreach ($odgovori as $odg) {
            echo "<td style='alignment-adjust: central;'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
            //echo "<td width='100px'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
        }
        echo "</tr>";

        $i = 0; //zbog parnih i neparnih kolona
        //prikazuje kolonu potpitanja 
        foreach ($this->kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
            if ($i++ % 2 == 0) {
                echo "<tr style='background-color: lightgrey; color: black;'>";
            } else {
                echo "<tr style='color: black;'>";
            }
            //echo "<td align='right'>";                        
            echo "<td style='text-align:left;'>";
            echo $num . ". " . $pot->getTekst();
            echo "</td>";

            //prikazuje radio kontrole
            foreach ($pot->getOdgovori() as $odg) {
                //proverava da li je izabran odgovor
                if ($odg->getIsIzabran()) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                //možda bi id trebalo da bude {$pot->getIdPotpitanje()}.$odg->getIdOdgovor()
                //echo "<td width='100px'>";
                echo "<td style=''>";
                echo "<input type='radio' name='{$pot->getIdPotpitanje()}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked}>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

}

//end class
