<?php

//checkBox
class PitanjeMatricaCheckBox extends Pitanje {

    private $potpitanja;
    private $kolekcijaPotpitanjaMatrica;

    /*
      public function PitanjeMatricaCheckBox($idPitanje, $nizPotpitanja, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno) {
      parent::__construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno);
      $this->potpitanja = $nizPotpitanja;
      }
     */

    public function PitanjeMatricaCheckBox($idPitanje, $potpitanjaMatrica, $idKreator, $tekst, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori(), $idVrstaOdgovora, $isObavezno, $stanje);

        $this->kolekcijaPotpitanjaMatrica = $potpitanjaMatrica;
        $this->potpitanja = $potpitanjaMatrica;
        $this->opisVrste = "Matrica odgovora, više izbora po redu";
    }

    
    function getKolekcijaPotpitanjaMatrica() {
        return $this->kolekcijaPotpitanjaMatrica;
    }

    function izaberiOdgovorMatrica($idPotpitanje, $idOdgovor) {
        $this->kolekcijaPotpitanjaMatrica->izaberiOdgovor($idPotpitanje, $idOdgovor);
    }

    function izaberiOdgovor($idOdgovor) {
        
    }

    function prikaziPotpitanja() {
        $this->kolekcijaPotpitanjaMatrica->prikazi();
    }

    function prikazi() {
        echo "<div class='pitanje'>";
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";

        // echo "<table border='0px' class='table_pitanje'>";
        echo "<table border='0px' cellspacing='0' width='600px' class='table_pitanje'>";
        echo "<tr><td></td>";

        $odgovori = $this->kolekcijaPotpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori();
        //prikazuje podunjene odgovore (prvi red tabele)   
        foreach ($odgovori as $odg) {
            //echo "<td width='100px'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
            echo "<td style='alignment-adjust: central;'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
        }
        echo "</tr>";
        $i = 0; //zbog parnih i neparnih kolona
        //prikazuje kolonu potpitanja 
        foreach ($this->kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
            /* if ($i++ % 2 == 0) {
              echo "<tr style='background-color: lightgrey; color: black;'>";
              } else {
              echo "<tr style='color: black;'>";
              } */

            echo "<tr style='color: black;" . ($i++ % 2 == 0 ? 'background-color: lightgrey;' : '') . "'>";
            //echo '<tr>';
            //echo "<td align='right'>";
            //echo "<td>";
            echo "<td style='text-align:left;'>";
            echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}[]gr'></div>";
            echo $num . ") " . $pot->getTekst();
            echo "</td>";

            //prikazuje checkbox kontrole
            foreach ($pot->getOdgovori() as $odg) {
                //proverava da li je izabran odgovor
                if ($odg->getIsIzabran()) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                //možda bi id trebalo da bude {$pot->getIdPotpitanje()}.$odg->getIdOdgovor()
                //echo "<td width='100px'>";
                echo "<td>";
                echo "<input {$this->disabled} type='checkbox' name='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}[]' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

    function prikazi0() {
        echo "<div class='pitanje'>";

        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";
        $num = "a";

        // echo "<table border='0px' class='table_pitanje'>";
        echo "<table border='0px' cellspacing='0' width='500px' class='table_pitanje'>";
        echo "<tr><td></td>";

        $odgovori = $this->kolekcijaPotpitanjaMatrica->getPotpitanjeByIndeks(0)->getOdgovori();
        //prikazuje podunjene odgovore (prvi red tabele)   
        foreach ($odgovori as $odg) {
            //echo "<td width='100px'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
            echo "<td style='alignment-adjust: central;'><label for='{$odg->getIdOdgovor()}'>{$odg->getTekst()}</label> </td>";
        }
        echo "</tr>";
        $i = 0; //zbog parnih i neparnih kolona
        //prikazuje kolonu potpitanja 
        foreach ($this->kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
            /* if ($i++ % 2 == 0) {
              echo "<tr style='background-color: lightgrey; color: black;'>";
              } else {
              echo "<tr style='color: black;'>";
              } */

            echo "<tr style='color: black;" . ($i++ % 2 == 0 ? 'background-color: lightgrey;' : '') . "'>";

            //echo '<tr>';
            //echo "<td align='right'>";
            //echo "<td>";
            echo "<td style='text-align:left;'>";
            echo $num . ". " . $pot->getTekst();
            echo "</td>";

            //prikazuje checkbox kontrole
            foreach ($pot->getOdgovori() as $odg) {
                //proverava da li je izabran odgovor
                if ($odg->getIsIzabran()) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                //možda bi id trebalo da bude {$pot->getIdPotpitanje()}.$odg->getIdOdgovor()
                //echo "<td width='100px'>";
                echo "<td>";
                echo "<input type='checkbox' name='{$pot->getIdPotpitanje()}[]' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked}>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }//foreach
        echo "</table>";
        echo '</div>';
        echo "</div>";
    }

    function prikaziStaro() {

        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";

        $num = "a";

        echo "<table border='0px'>";
        echo "<tr><td></td>";
        //prikazuje podunjene odgovore
        foreach ($this->odgovori as $odg) {
            echo "<td width='100px'><label for='{$odg['idOdgovor']}'>{$odg['tekst']}</label> </td>";
        }
        echo "</tr>";
        //prikazuje potpitanja
        foreach ($this->potpitanja as $pot) {
            echo '<tr>';
            //echo "<td align='right'>";
            echo "<td>";
            echo $num . ". " . $pot['tekst'];
            echo "</td>";

            //prikazuje radio dugmiće
            foreach ($this->odgovori as $odg) {
                echo "<td width='100px'>";
                echo "<input type='checkbox' name='{$pot['idPotpitanje']}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>";
                echo "</td>";
            }
            echo '</tr>';
            $num++;
        }
        echo "</table>";
        echo '<br/>';
    }

}
?>

