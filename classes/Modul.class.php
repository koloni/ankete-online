<?php

class Modul {

    public $name, $controller, $view;

    public function __construct($name, $view, $controller = "") {
        $this->name = $name;
        $this->controller = $controller;
        $this->view = $view;
    }

    public function Render() {
        if ($this->controller != "")
            include MODULES_DIR . "/" . $this->name . "/" . $this->controller;
        include MODULES_DIR . "/" . $this->name . "/" . $this->view;
    }

}