<?php

class PitanjeSlobodanUnosDugacakTekst extends PitanjeSlobodanUnos {

    private $maxKaraktera = 10;

    public function __construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $potpitanjaUnos, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->opisVrste = "Slobodan unos dugačkog teksta";
    }

    function unesiOdgovor($idPotpitanje, $tekst) {
        //neka provera unetog teksta, npr neki trim
        $tekst = trim($tekst);
        if (strlen($tekst) < 1) {
            throw new LengthException("Argumnet '\$tekst', funkcije 'unesiOdgovor', mora da sadrži miminum 1 karakter");
        }
        parent::unesiOdgovor($idPotpitanje, $tekst);
    }

    function addPotpitanjeUnos($potpitanjeUnos) {
        if (parent::getBrojPotpitanja() == 1) {
            throw new Exception("objekat 'PitanjeSlobodanUnosDugacakTekst', može imati samo jedno potpitanje!");
        }
        parent::addPotpitanjeUnos($potpitanjeUnos);
    }

    function getPotpitanja() {
        return parent::getPotpitanja();
    }

    function getBrojPotpitanja() {
        return parent::getBrojPotpitanja();
    }

    function prikazi() {
        echo "<div class='pitanje'>";
        foreach ($this->nizPotpitanjaUnos as $pot) {
            //echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}gr'></div>";
        }
        echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
        echo "</p>";
        echo "<div style='padding: 0 0 0 15px;'>";

        //prikazuje kolonu potpitanja 
        foreach ($this->nizPotpitanjaUnos as $pot) {
            echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}gr'></div>";
            echo "<span style='padding:0px; font-size: 12px;'>" . $pot->getTekst() . "</span><br/>";
            ?><textarea onkeyup="brPreostalihKaratera(this, '<?php echo $this->idPitanje . "brKaratera"; ?>', '<?php echo $this->maxKaraktera ?>')" <?php echo "{$this->disabled} rows='2' cols='60' name='" . ($this->isObavezno ? "d" : "n") . "_pot{$pot->getIdPotpitanje()}' value='{$pot->getOdgovor()}' id='{$pot->getIdPotpitanje()}' onclick='obrisiGresku(this);'>{$pot->getOdgovor()}</textarea><br/>"; ?>
                      <?php
                      echo "<div style='font-size:12px;'>broj preosalih karaktera: <span style='font-size:13px;' id='{$this->idPitanje}brKaratera'>" . ($this->maxKaraktera - strlen($pot->getOdgovor())) . "</span></div>";
                  }//foreach
                  //<textarea rows="4" cols="50"></textarea>

                  echo "</div>";
                  echo "</div>";
              }

              function prikazi0() {
                  echo "<div class='pitanje'>";
                  echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b>";
                  echo "</p>";
                  echo "<div style='padding: 0 0 0 15px;'>";

                  //prikazuje kolonu potpitanja 
                  foreach ($this->nizPotpitanjaUnos as $pot) {
                      echo "<span style='padding:0px;'>" . $pot->getTekst() . "</span><br/>";
                      echo "<textarea rows='2' cols='60' name='{$pot->getIdPotpitanje()}' value='{$pot->getOdgovor()}' id='{$pot->getIdPotpitanje()}'>{$pot->getOdgovor()}</textarea><br/>";
                  }//foreach
                  //<textarea rows="4" cols="50"></textarea>
                  echo "</div>";
                  echo "</div>";
              }

          }
          ?>
