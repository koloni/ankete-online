<?php

//Jedno potpitanje sa više ponuđenih odgovora
class PotpitanjeMatrica {

    protected $tekst;
    //niz ponuđenih odgovora
    protected $nizOdgovora = array();
    protected $idPotpitanje;
    protected $brojIzabranihOdgovora;

    public function __construct($idPotpitanje, $tekst, $nizOdgovora) {
        $this->idPotpitanje = $idPotpitanje;
        $this->tekst = $tekst;
        $this->nizOdgovora = $nizOdgovora;
    }

    public function getTekst() {
        return $this->tekst;
    }

    public function getOdgovori() {
        return $this->nizOdgovora;
    }

    public function getBrojOdgovora() {
        return count($this->nizOdgovora);
    }

    public function getBrojIzabranihOdgovora() {
        return $this->brojIzabranihOdgovora;
    }

    public function getIdPotpitanje() {
        return $this->idPotpitanje;
    }

    public function izaberiOdgovor($idOdgovor) {
        //Možda i ovo izbacim
        if (count($this->nizOdgovora) == 0) {
            throw new Exception("Ne postoji ni jedan odgovor!");
        }

        if (is_numeric($idOdgovor) == false) {
            throw new InvalidArgumentException("Argument '\$idOdgovor', funkcije 'izaberiOdgovor', mora biti tipa 'integer'!");
        }

        //ako nije ceo borj pretvori ga u ceo broj
        if (!is_int($idOdgovor)) {
            $idOdgovor = (int) $idOdgovor;
        }

        foreach ($this->nizOdgovora as $o) {
            if ($o->getIdOdgovor() == $idOdgovor) {
                $o->izaberiOdgovor();
                $this->brojIzabranihOdgovora++;
                return;
            }
        }
        throw new Exception("Ne postoji odgovor za izabrani '\$idOdgovor'!");
    }

    public function prikazi() {
        echo "Potpitanje id: {$this->idPotpitanje}. {$this->tekst} [";
        foreach ($this->nizOdgovora as $o) {
            echo "{$o->prikazi()}, ";
        }
        echo ']';
    }

//end class
}

//Sadrži niz PotpitanjeMatrica objekata, koji u sebi imaju niz objekata odgovora
class KolekcijaPotpitanjaMatrica {

    public $nizPotpitanjaMatrica;

    public function __construct($nizPotpitanjaMatrica = array()) {
        if (is_array($nizPotpitanjaMatrica) == false) {
            throw new InvalidArgumentException("Argument '\$nizPotpitanjaMatrica', konstruktora 'PotpitanjaMatrica', mora da bude tipa 'Array()'!");
        }
        $this->nizPotpitanjaMatrica = $nizPotpitanjaMatrica;
    }

    //dodavanje jednog PotpitanjeMatrica objekta
    public function addPotpitanjeMatrica($potpitanjeMatrica) {
        //treba proveriti tip objekta
        if (!$potpitanjeMatrica instanceof PotpitanjeMatrica) {
            throw new Exception("Argument '\$potpitanjeMatrica', funkcije 'addPotpitanjeMatrica', treba da bude tipa 'PotpitanjeMatrica'");
        }
        $this->nizPotpitanjaMatrica[] = $potpitanjeMatrica;
    }

    //za izabrano potpitanje, izabira odgovor
    public function izaberiOdgovor($idPotpitanje, $idOdgovor) {
        if (sizeof($this->nizPotpitanjaMatrica) == 0) {
            throw new Exception("Ne postoji ni jedno potpitanje!");
        }

        //prolazi kroz sva potpitanja
        foreach ($this->nizPotpitanjaMatrica as $pm) {
            //ako pronađe potpitanje, izabira odgovor
            if ($pm->getIdPotpitanje() == $idPotpitanje) {
                $pm->izaberiOdgovor($idOdgovor);
                return;
            }
        }
        throw new Exception("Ne postoji potpitanje za izabrano '\$idPotpitanje'!");
    }

    public function getBrojPotpitanja() {
        return count($this->nizPotpitanjaMatrica);
    }

    public function getPotpitanja() {
        return $this->nizPotpitanjaMatrica;
    }

    public function getPotpitanjeByIndeks($indeks) {
        if (is_int($indeks + 0) == false) {
            throw new InvalidArgumentException("Argument '\$indeks' funkcije 'getPotpitanjeIndeks'mora da bude tipa 'integer'!");
        }
        return $this->nizPotpitanjaMatrica[(int) $indeks];
    }

    //Da li potpitanje postoji
    public function isSadrziPotpitanje($idPotpitanje) {
        if (is_int($idPotpitanje + 0) == false) {
            throw new InvalidArgumentException("Argument '\$idPotpitanje', funkcije 'isSadrziPotpitanje', mora da bude tipa 'integer'!");
        }
        foreach ($this->nizPotpitanjaMatrica as $potpitanjeMatrica) {
            if ($potpitanjeMatrica->getIdPotpitanje() == $idPotpitanje) {
                return true;
            }
        }
        return false;
    }

    //vraća izabrano potpitanje
    public function getPotpitanjeByIdPotpitanje($idPotpitanje) {
        if (is_int($idPotpitanje + 0) == false) {
            throw new InvalidArgumentException("Argument '\$idPotpitanje' funkcije 'getPotpitanjeIndeks'mora da bude tipa 'integer'!");
        }
        foreach ($this->nizPotpitanjaMatrica as $potpitanjeMatrica) {
            if ($potpitanjeMatrica->getIdPotpitanje() == $idPotpitanje) {
                return $potpitanjeMatrica;
            }
        }
        return false;
    }

    public function prikazi() {
        foreach ($this->nizPotpitanjaMatrica as $pot) {
            echo "{$pot->prikazi()}<br/>";
        }
    }

//end class
}

?>
