<?php

/**
 * Description of Anketa
 *
 * @author korisnik
 */
class Anketa {

    private $idAnketa;
    private $idKreator;
    private $naziv;
    private $datumPocetka;
    private $datumIsteka;
    private $brPitanjaPoStrani;
    private $isPersonalizovana;
    private $redosledPitanja;
    private $redosledPitanjaOpis;
    private $stanje;
    private $pitanja;
    private $brPitanja;
    private $redniBrojevi = array();

//      stanje:
//         -2 - blokirana u procesu kreiranja
//         -1 - blokirana
//          0 - u procesu kreiranja
//          1 - aktivna
//          2 - zavrsena

    public function __construct($idAnketa, $stanje = "1") {
        $dbAnkete = new DbAnkete();
        $this->pitanja = $dbAnkete->getPitanjaAnketa($idAnketa);
        $anketa = $dbAnkete->getAnketaInfo($idAnketa);
        if (is_array($anketa == false)) {
            throw new Exception("Ne postoji anketa za izbrani'\$idAnketa'!");
        }

        $this->idAnketa = $idAnketa;
        $this->idKreator = $anketa['idKreator'];
        $this->naziv = $anketa['naziv'];
        $this->datumPocetka = $anketa['datumPocetka'];
        $this->datumIsteka = $anketa['datumZavrsetka'];
        $this->isPersonalizovana = $anketa['isPersonalizovana'];
        $this->brPitanjaPoStrani = $anketa['brPitanjaPoStrani'];
        $this->redosledPitanja = $anketa['idRedosledPitanja'];
        $this->redosledPitanjaOpis = $anketa['redosledPitanjaOpis'];
        $this->brPitanja = $anketa['ukupnoPitanja'];
        $this->stanje = $stanje;


        $this->redniBrojevi = $dbAnkete->getRedniBrojeviPitanja($idAnketa);
        //var_dump($this->redniBrojevi);
    }

    function prikazi() {
        //echo "<div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>";
        echo "<div class='paper' style='margin: 0 auto;display: table; min-height: 400px;'>";
        echo "Naziv: {$this->naziv}<br/>";
        echo "Datum početka: {$this->datumPocetka}<br/>";
        echo "Datum isteka: {$this->datumIsteka}<br/>";
        echo "Pesonalizovana: {$this->isPersonalizovana}<br/>";
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->prikazi();
            }
        }
        echo "</div>";
    }

    function prikaziDisabled() {
        //echo "<div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>";
        echo "<div class='paper' style='margin: 0 auto;display: table; min-height: 400px;'>";
        echo "Naziv: {$this->naziv}<br/>";
        echo "Datum početka: {$this->datumPocetka}<br/>";
        echo "Datum isteka: {$this->datumIsteka}<br/>";
        echo "Pesonalizovana: {$this->isPersonalizovana}<br/>";
        if (is_array($this->pitanja)) {
            foreach ($this->pitanja as $p) {
                $p->setDisabled();
                $p->prikazi();
            }
        }
        echo "</div>";
    }

    function prikaziWithEditLink() {
        //<div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>
        ?>

        <div class='paper' style='margin: 0 auto;display: table; width: 700px;min-height: 400px;'>
            <div class="naslov">Uređivanje ankete</div>
            <!--<div style="width: 600px; height: auto; margin: 0px auto; border: 1px solid red;">-->
            <div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>                
                <button style="margin: 0 5px 0 5px;" class="btnKontrole" onclick="location.href='index.php?p=20&idAnketa=<?php echo $this->idAnketa ?>'">Napravi pitanje i dodaj</button>
                <button style="margin: 0 5px 0 5px;" class="btnKontrole" onclick="location.href='index.php?p=22&anketa=<?php echo $this->idAnketa ?>'">Dodaj postojeća pitanja </button>                 
                <a href='index.php?p=25&anketa=<?php echo $this->idAnketa ?>&redosledPitanja=<?php echo $this->redosledPitanja ?>' class="ask"><button style="margin: 0 5px 0 20px; color: green;" title="Aktiviranje se radi kada završi editovanje ankete. Nahnadne izmene ankete nisu moguće." <?php echo $this->brPitanja >= 1 ? "" : "disabled"; ?> class="btnKontrole">Aktiviraj anketu</button></a>
                <a href='index.php?p=26&anketa=<?php echo $this->idAnketa ?>' class="ask"><button class="btnKontrole" style="margin: 0 5px 0 10px; color:red;">Izbriši anketu</button></a>
            </div>
            <hr/>
            <br/>
            <div style="font-size: 15px;">
                <table class="tableAnketaInfo">
                    <tbody>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Naziv ankete:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo $this->naziv; ?></td>                                                      
                        </tr>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Datum početka:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo substr($this->datumPocetka, 0, 10); ?></td>  
                        </tr>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Datum isteka:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo substr($this->datumIsteka, 0, 10); ?></td>  
                        </tr>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Pesonalizovana:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo $this->isPersonalizovana == 0 ? 'Ne' : 'Da'; ?></td>  
                        </tr>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Broj pitanja po strani:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo $this->brPitanjaPoStrani ?></td>  
                        </tr>
                        <tr>
                            <td class="tableAnketaInfoLeftCol">Redosled prikazivanja pitanja:</td>
                            <td class="tableAnketaInfoRightCol"><?php echo $this->redosledPitanjaOpis ?></td>  
                        </tr>
                        <tr>
                            <td style="text-align: center;" colspan="2" class="link"><b><a style="color: green; font-size: 14px;" href='index.php?p=30&idAnketa=<?php echo $this->idAnketa ?>'>Izmeni</a></b><td>
                        </tr>
                    </tbody>
                </table>
                <!--<br/><br/>
               Naziv: <b><?php echo $this->naziv; ?></b><br/>
               Datum početka: <b><?php echo substr($this->datumPocetka, 0, 10); ?></b><br/>
               Datum isteka: <b><?php echo substr($this->datumIsteka, 0, 10); ?></b><br/>
               Pesonalizovana: <span style="font-size: 14px; font-weight: bold;"><?php echo $this->isPersonalizovana == 0 ? 'Ne' : 'Da'; ?></span><br />
               Broj odgovora po strani: <b><?php echo $this->brPitanjaPoStrani ?></b><br/>
               Redosled prikazivanja pitanja: <b><?php echo $this->redosledPitanjaOpis; ?></b><br/>
               
                <div class="link"><b><a style="color: green; font-size: 14px;" href='index.php?p=30&idAnketa=<?php echo $this->idAnketa ?>'>Izmeni</a></b></div>                
                -->
            </div>
            <br/>

            <div style="float:left; width: 95%; height: 1px; border-top:  1px dashed #666; margin-top: 20px;"></div>  


            <form name="frmPitanja">
                <?php
                if (is_array($this->pitanja)) {
                    $i = 0;
                    foreach ($this->pitanja as $p) {
                        ?>                        
                        <div id='p<?php echo $p->getIdPitanje(); ?>' >
                            <div style="width: 100%;float: left; padding-top: 15px; border: 0px solid blue;"><span style="font-size: 12px;">Obavezno</span>
                                <select onchange="setPitanjeIsObavezno(<?php echo $this->idAnketa . ", " . $p->getIdPitanje(); ?>, this.value)">                            
                                    <?php
                                    echo "<option value='0' " . ($p->getIsObavezno() ? "" : "selected") . "> Ne </option>";
                                    echo "<option value='1' " . ($p->getIsObavezno() ? "selected" : "") . "> Da </option>";
                                    ?>                                                                        
                                </select>
                            </div>
                            <?php if ($this->redosledPitanja == 1) { ?>
                                <div style="width: 100%; float:left;padding-top: 10px;"><span style="font-size: 12px;">Redni broj</span>
                                    <select id="selectRedniBroj<?php echo $i; ?>" name="selectRedniBroj<?php echo $i; ?>" onchange="setRedniBrojPitanja(<?php echo $this->idAnketa . ", " . $p->getIdPitanje(); ?>, this.value); izmeniSelectKontrole(this);" onfocus="setPrethodnaVrednost(this.value)"> 
                                        <?php
                                        if ($this->redniBrojevi[$i]->redniBroj == 0) {
                                            echo "<option value='0'> - - - bez rednog broja - - - <option>";
                                        } else {
                                            echo "<option value='0'> - - - bez rednog broja - - - <option>";
                                            echo "<option value='{$this->redniBrojevi[$i]->redniBroj}' selected>{$this->redniBrojevi[$i]->redniBroj}</option>";
                                        }

                                        //if ($this->redniBrojevi[$i]->redniBroj == 0) {
                                        //echo "<option value='0'> --- bez rednog broja ---  <option>";
                                        //} else {
                                        //echo "<option value='{$this->redniBrojevi[$i]->redniBroj}' selected>{$this->redniBrojevi[$i]->redniBroj}</option>";
                                        //}
                                        $i++;

                                        for ($j = 1; $j <= $this->brPitanja; $j++) {
                                            $postoji = false;
                                            foreach ($this->redniBrojevi as $rb) {
                                                if ($rb->redniBroj == $j) {
                                                    $postoji = true;
                                                    break;
                                                }
                                            }

                                            if ($postoji == false) {
                                                echo "<option value='{$j}'>{$j}</option>";
                                            }
                                        }
                                        //echo "<option value='" . ($this->brPitanja + 1) . "'>" . ($this->brPitanja + 1) . "</option>";
                                        //echo "<option value='-1'> -- poslednje -- </option>";
                                        ?>                                                                        
                                    </select>
                                </div>
                            <?php } //if redni broj==1 ?>




                            <?php $p->prikazi();
                            ?>     

                            <div style='float:left; width:100%; border:0px solid red; padding:5px 0 0 15px;'>                        

                                <div class="link" style="padding-top: 5px;">
                                    <?php
                                    if ($p->getIdKreator() == $this->idKreator && $p->getStanje() == 0) {
                                        echo $p->getStanje();
                                        ?>
                                        <a style="color: green;"href="index.php?p=29&anketa=<?php echo $this->idAnketa; ?>&pitanje=<?php echo $p->getIdPitanje(); ?>">Izmeni</a> 
                                        <a class="ask" style="color: red;" href="javascript:deletePitanje('p<?php echo $p->getIdPitanje(); ?>','<?php echo $this->idAnketa; ?>', '<?php echo $p->getIdPitanje(); ?>')"><span style="padding: 0 5px 0 5px;">Izbriši</span></a> 
                                    <?php } else if ($p->getStanje() == 1) { ?>   
                                        <a href='javascript:void(0);' onclick="removePitanje('p<?php echo $p->getIdPitanje(); ?>','<?php echo $this->idAnketa; ?>', '<?php echo $p->getIdPitanje(); ?>')">Izbaci</a>
                                        <!--<a href='index.php?p=20&idAnketa=<?php echo $this->idAnketa; ?>&idPitanje=<?php echo $p->getIdPitanje(); ?>'>Izbaci</a>-->
                                    <?php } ?>
                                </div>                       

                                <!--     
                                 <div><a onclick='editPitanje(<?php $p->getIdPitanje(); ?>);'>Izmeni</a> <a href='edit_pitanje.php?idPitanje=<?php echo $p->getIdPitanje(); ?>'>Izbaci</a></div>
                                 <div id='q<?php echo $p->getIdPitanje(); ?>'></div>";
                                -->
                            </div>
                            <div style="float:left; width: 95%; height: 1px; border-top:  1px dashed #666; margin-top: 20px;"></div>                           
                            <!-- 
                             <div style="float:left; width: 95%; height: 1px; border-top:  1px solid #666; margin-top: 20px;"></div>
                            <div style="float:left; width: 95%; height: 1px; border-top:  1px dotted #666; margin-top: 20px;"></div>
                             <div style="float:left; width: 95%; height: 1px; border-top:  1px dashed rgb(204, 204, 204); margin-top: 20px;"></div>
                             <div style="float:left; width: 95%; height: 1px; border-top:  1px dotted rgb(204, 204, 204); margin-top: 20px;"></div>-->
                        </div>
                        <?php
                    }
                }
                ?>
            </form>

        </div>
        <?php
    }
//prikaziWithEditLink

    function prikaziWithEditLink0() {
        ?>
        <div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>
            <div class="naslov">Uređivanje ankete</div>
            <!--<div style="width: 600px; height: auto; margin: 0px auto; border: 1px solid red;">-->
            <div style='margin: 0 auto; border: 0px solid greenyellow; display: table;'>
                <button style="font-style: oblique;font-weight: 650;" onclick="location.href='index.php?p=20&idAnketa=<?php echo $this->idAnketa ?>'">Napravi pitanje i dodaj</button> 
                <button onclick="location.href='index.php?p=22&idAnketa=<?php echo $this->idAnketa ?>'">Dodaj postojeća pitanja </button>                 
                <button>Predaj anketu</button>
            </div>
            <hr/>
            <br/>
            <div style="font-size: 15px;">
                Naziv: <b><?php echo $this->naziv; ?></b><br/>
                Datum početka: <b><?php echo substr($this->datumPocetka, 0, 10); ?></b><br/>
                Datum isteka: <b><?php echo substr($this->datumIsteka, 0, 10); ?></b><br/>
                Pesonalizovana: <span style="font-size: 14px; font-weight: bold;"><?php echo $this->isPersonalizovana == 0 ? 'Ne' : 'Da'; ?></span><br />
                Broj odgovora po strani: <b><?php echo $this->brPitanjaPoStrani ?></b><br/>
                Redosled prikazivanja pitanja: <b>
                    <?php
                    if ($this->redosledPitanja == 0) {
                        echo "Po redosledu unosa";
                    } else if ($this->redosledPitanja == 1) {
                        echo 'Ručno izabrano';
                    } else if ($this->redosledPitanja == 2) {
                        echo 'Slučajno generisan';
                    } else {
                        //neka greška
                    }
                    ?></b><br/>

                <div class="link"><b><a href='edit_anketa_main.php?idAnketa=<?php echo $this->idAnketa ?>'>Izmeni</a></b></div>                
            </div>
            <br/>

            <?php
            if (is_array($this->pitanja)) {
                foreach ($this->pitanja as $p) {
                    $p->prikazi();
                    ?>
                    <div style='float:left; width:100%; border:0px solid red; padding:0 0 0 15px;'>              
                        <div>
                            <button style="color: green" onclick="location.href='edit_pitanje.php?idAnketa=<?php echo $this->idAnketa; ?>&idPitanje=<?php echo $p->getIdPitanje(); ?>'">Izmeni</button> 
                            <button style="color: red;" onclick="location.href='index.php?p=23&idAnketa=<?php echo $this->idAnketa; ?>&idPitanje=<?php echo $p->getIdPitanje(); ?>'">Izbaci</button> 
                        </div>
                        <div class="link">
                            <a href="edit_pitanje.php?idAnketa=<?php echo $this->idAnketa; ?>&idPitanje=<?php echo $p->getIdPitanje(); ?>">Izmeni</a> 
                            <a href='index.php?p=23&idAnketa=<?php echo $this->idAnketa; ?>&idPitanje=<?php echo $p->getIdPitanje(); ?>'>Izbaci</a>
                        </div>                       

                        <!--     
                         <div><a onclick='editPitanje(<?php $p->getIdPitanje(); ?>);'>Izmeni</a> <a href='edit_pitanje.php?idPitanje=<?php echo $p->getIdPitanje(); ?>'>Izbaci</a></div>
                         <div id='q<?php echo $p->getIdPitanje(); ?>'></div>";
                        -->
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }

}

