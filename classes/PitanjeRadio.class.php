<?php

include_once 'classes/Odgovor.class.php';

class PitanjeRadio extends Pitanje {

    private $brojIzabranihOdgovora = 0;

    //class Odgovor   
    //public function PitanjeRadio($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno) {
    //    parent::Pitanje($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno);
    //}

    public function PitanjeRadio($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje) {
        parent::__construct($idPitanje, $idKreator, $tekst, $odgovori, $idVrstaOdgovora, $isObavezno, $stanje);
        $this->opisVrste = "Izbor jednog odgovora od više ponuđenih (radio dugme)";
    }

    public function getBrojIzabranihOdgovora() {
        return $this->brojIzabranihOdgovora;
    }

    public function izaberiOdgovor($idOdgovor) {
        if (is_int((int) $idOdgovor) == false || $idOdgovor == 0) {
            throw new InvalidArgumentException("Argument '\$idOdgovor'funkcije 'izaberiOdgovor' mora da bude tipa 'integer'!");
        }
        if ($this->brojIzabranihOdgovora == 1) {
            throw new Exception("Ne možete izabrati više od jednog odgovora za pitanje tipa 'PitanjeRadio' !");
        }
        //U odgovorima provera da li izabrani odgovor postoji
        foreach ($this->odgovori as $odgovor) {
            if ($odgovor->getIdOdgovor() == $idOdgovor) {
                //$odgovor->setIsIzabran(true);
                $odgovor->izaberiOdgovor();
                $this->brojIzabranihOdgovora++;
                return;
            }
        }
        throw new Exception("Ne postoji odgovor za izabrani'\$idOdgovor'!");
    }

    function prikazi() {
        echo "<div class='pitanje'>";
        //echo "<div></div>";
        //echo "<p>" . $this->tekst;
        //echo "<p><b>" . $this->getRedniBroj() . ". <font size='4'>* </font>" . $this->tekst . "</b>";
        //echo "<p><b>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</b></p>";
        echo "<span style='font-size:16px; font-weight:bold;'>" . $this->getRedniBroj() . ". " . ($this->isObavezno ? "<font size='4'>* </font>" : "") . $this->tekst . "</span>";
        echo "<div id='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}gr'></div>";
        echo "<div style='padding:5px 0 0 20px;'><span style='font-size:12px; font-style: italic; font-family: Trebuchet MS;'>Izaberite jedan odgovor</span></div>";
        echo "<div style='padding: 10px 0 0 15px;'>";
        $num = "a";
        foreach ($this->odgovori as $odg) {
            if ($odg->getIsIzabran()) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            echo "<input {$this->disabled} type='radio' name='" . ($this->isObavezno ? "d" : "n") . "_pit{$this->idPitanje}' value='{$odg->getIdOdgovor()}' id='{$odg->getIdOdgovor()}' {$checked} onclick='obrisiGresku(this);'>" .
            "<label for='{$odg->getIdOdgovor()}'> {$num}. {$odg->getTekst()}</label><br/>";
            $num++;
        }

        echo "</div>";
        echo "</div>";
    }

//prikazi

    function prikaziStaro() {
        echo "<p>" . $this->tekst;
        if ($this->isObavezno == false) {
            echo ' (nije obavezno)';
        }
        echo "</p>";
        // echo '<form>';
        $num = "a";
        foreach ($this->odgovori as $odg) {
            echo "<input type='radio' name='{$this->idPitanje}' value='{$odg['idOdgovor']}' id='{$odg['idOdgovor']}'>";
            echo "<label for='{$odg['idOdgovor']}'> {$num}. {$odg['tekst']}</label><br/>";
            $num++;
        }
    }

}

//end class
?>
