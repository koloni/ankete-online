<?php

class C_ZabranePitanja extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(-1);

        $error = "";
        if (isset($_GET["pitanje"])) {
            $idPitanje = $_GET['pitanje'];
            if ($idPitanje != "") {
                if (!Validator::Numeric($idPitanje)) {
                    $error.="<br/>Pogrešan format idPitanje!";
                }
            } else {
                $error.="<br/>idPitanje je obavezno!";
            }

            if ($error == "") {
                
            }
        } else {

            $strana = 0;
            if (isset($_GET['str'])) {
                //neka provra da li je ceo broj
                $strana = $_GET['str'];
                if ($strana < 0) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['trenutna'] = $strana;

            $limit = 5;
            if (isset($_GET['limit'])) {
                //neka provra da li je ceo broj
                $limit = $_GET['limit'];
                if ($limit < 1 || $limit > 50) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['limit'] = $limit;

            $orderby = "tekstAsc";
            if (isset($_GET['orderby'])) {
                $orderby = $_GET['orderby'];
            }

            $kljucneReci = "";
            if (isset($_GET['kljucnaRec'])) {
                $kljucneReci = explode(' ', $_GET['kljucnaRec']);
            }

            $ukupnoZapisa = 0;
            $sql = new DbAnkete();

            if (!empty($_GET['kljucnaRec'])) {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfPitanjaSearch($kljucneReci);
            } else {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfPitanja();
            }

            $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);

            $pocetniZapis = $strana * $limit;

            $this->data['pocetniZapis'] = $pocetniZapis;
            $this->data['poslednja'] = $brStrana - 1;

            $activePitanjaInfo = new stdClass();
            $activePitanjaInfo->ukupnoZapisaPretrage = $ukupnoZapisa;

            $this->data['orderby'] = $orderby;
            switch ($orderby) {
                case 1: case "tekstAsc":
                    $activePitanjaInfo->ankete = $this->data['pitanja'] = $sql->getActivePitanjaInfo2(null, 0, $pocetniZapis, $limit, $kljucneReci, 'tekst', 'asc');
                    break;
                case 2: case "tekstDesc":
                    $activePitanjaInfo->ankete = $this->data['pitanja'] = $sql->getActivePitanjaInfo2(null, 0, $pocetniZapis, $limit, $kljucneReci, 'tekst', 'desc');
                    break;
                case 3: case "datumKreiranjaAsc":
                    $activePitanjaInfo->ankete = $this->data['pitanja'] = $sql->getActivePitanjaInfo2(null, 0, $pocetniZapis, $limit, $kljucneReci, 'datumKreiranja', 'asc');
                    break;
                case 4: case "datumKreiranjaDesc":
                    $activePitanjaInfo->ankete = $this->data['pitanja'] = $sql->getActivePitanjaInfo2(null, 0, $pocetniZapis, $limit, $kljucneReci, 'datumKreiranja', 'desc');
                    break;
            }

            if (isset($_GET['ajaxJson'])) {
                if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                    echo json_encode($activePitanjaInfo);
                } else {
                    echo "false";
                }
            } else {
                //kada prvi put otvaramo stranu
                if ($this->data['ukupnoZapisa']==0) {
                    $this->data['message'] = "Trenutno nema ni jedno pitanje.";
                    $this->data['pitanja'] = null;
                }

                $this->data['error'] = $error;
                $this->loadPage("pages/zabrane_pitanja", $this->data);
            }
        }
    }

//__construct
//end class
}

