<?php

class C_EditAnketa extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);
        
        $error = "";
        if (isset($_GET["idAnketa"])) {
            $idAnketa = $_GET['idAnketa'];
            if ($idAnketa != "") {
                if (!Validator::Numeric($idAnketa)) {
                    $error.="<br/>Pogrešan format idAnketa!";
                }
            } else {
                $error.="<br/>idAnketa je obavezno!";
            }

            if ($error == "") {
                $this->data['anketa'] = new Anketa($idAnketa, 0);
                // $anketa->prikaziWithEditLink();
            }
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/edit_anketa", $this->data);
    }

}

