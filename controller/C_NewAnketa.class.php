<?php

class C_NewAnketa extends Controller {

    function __construct() {

        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";
        //if(isset($_POST['btnNew']))
        if (isset($_POST["naziv"]) && isset($_POST["datumPocetka"]) && isset($_POST["datumIsteka"])
                && isset($_POST["personalizovana"]) && isset($_POST["brPitanjaPoStrani"]) && isset($_POST["redosledPitanja"])) {
            $idKreator = $user->idKorisnik;
            $naziv = $_POST['naziv'];
            if ($naziv != "") {
                //if (!Validator::Text($naziv)) {
                //$error.="<br/>Naziv ankete mora da sadrži minimum 3 karaktera!";
                if (!Validator::AlphabetUTFWithSpecialCharacterInMiddleAndEnd($naziv,3, 100)) {
                    $error.="<br/>Neispravan unos!";
                }
            } else {
                $error.="<br/>Naziv ankete je obavezan!";
            }

            $datumPocetka = $_POST['datumPocetka'];
            if ($datumPocetka != "") {
                if (!Validator::DateTime($datumPocetka . " 00:00:00")) {
                    $error.="<br/>Datum početka je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Datum početka je obavezan!";
            }

            $datumIseka = $_POST['datumIsteka'];
            if ($datumPocetka != "") {
                if (!Validator::DateTime($datumIseka)) {
                    $error.="<br/>Datum isteka je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Datum isteka je obavezan!";
            }

            $personalizovana = $_POST['personalizovana'];
            if ($personalizovana == 'true') {
                $personalizovana = true;
            } elseif ($personalizovana == 'false') {
                $personalizovana = false;
            } else {
                $error.="<br/>Personalizovana je uneto u pogrešnom formatu!";
            }

            $redosledPitanja = $_POST['redosledPitanja'];

            if ($redosledPitanja != 0 && $redosledPitanja != 1 && $redosledPitanja != 2) {
                $error.="<br/>Redosled pitanja je uneto u pogrešnom formatu!";
            }

            $brPitanjaPoStrani = $_POST['brPitanjaPoStrani'];
            if ($brPitanjaPoStrani != "") {
                if (!Validator::Numeric($brPitanjaPoStrani)) {
                    $error.="<br/>Broj pitanja po strani je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Broj pitanja po strani je obavezan!";
            }


            if ($error == "") {
                try{
                
                
                $sql = new DbAnkete();
                               
                //ne može da se prosledi null kroj promenljivu, jedino da se ručno ištampa null
                //radi
                //$sql->ExecuteSQL_CHANGE("CALL procKreirajAnketu('{$naziv}', 'kljlkjl', '{$idKreator}','{$datumPocetka}','{$datumIseka}','{$personalizovana}','{$brPitanjaPoStrani}', '{$redosledPitanja}' )");
                                  
                 //$sql->ExecuteSQL("CALL procKreirajAnketu('{$naziv}', 'fdsfsdf', '{$idKreator}','{$datumPocetka}','{$datumIseka}','{$personalizovana}','{$brPitanjaPoStrani}', '{$redosledPitanja}' )");
                  
                
                //Radni
                
                //$idKreator=1111111111111111;
                $hostname = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
                  $opis=null;                  
                  $pdo=new PDO($hostname, DB_USER, DB_PASS);                                    
                 //$stmt = $pdo->prepare("CALL procKreirajAnketu(?,?,?,?,?,?,?,?)");                  
                 /* $stmt->bindParam(1, $naziv);
                  $stmt->bindParam(2, $opis);
                  $stmt->bindParam(3, $idKreator);
                  $stmt->bindParam(4, $datumPocetka);
                  $stmt->bindParam(5, $datumIseka);
                  $stmt->bindParam(6, $personalizovana);
                  $stmt->bindParam(7, $brPitanjaPoStrani);
                  $stmt->bindParam(8, $redosledPitanja);
                  $stmt->execute();                        
                  */         
                  
                  $poruka="";
                  //radni, ovi nazivi uopšte nemaju veze sa nazivima kolona u bazi, jer on ovde gleda redosled, mada je i logično jer sama funkcija ima takav redosled argumenata
                 $stmt = $pdo->prepare("CALL sp_KreirajAnketu(:naziv,:opis,:idKreator,:datumPocetka,:datumZavrsetka,:isPersonalizovana,:brPitanjaPoStrani,:idRedosledPitanja)"); 
                 $stmt->bindParam(':naziv', $naziv);
                  $stmt->bindParam(':opis', $opis);
                  $stmt->bindParam(':idKreator', $idKreator);
                  $stmt->bindParam(':datumPocetka', $datumPocetka);
                  $stmt->bindParam(':datumZavrsetka', $datumIseka);
                  $stmt->bindParam(':isPersonalizovana', $personalizovana);
                  $stmt->bindParam(':brPitanjaPoStrani', $brPitanjaPoStrani);
                  $stmt->bindParam(':idRedosledPitanja', $redosledPitanja);              
                
                  //exit();
                  /*
                  if (!$stmt)
                  {
                    echo $stmt->errorInfo();
                    exit();
                  }                                    
                   var_dump($stmt->errorInfo());
                    exit();                                    */                             
                if (true /*$sql->Insert(array('naziv' => $naziv, 'idKreator' => $idKreator, 'datumPocetka' => $datumPocetka, 'datumZavrsetka' => $datumIseka,
                            'isPersonalizovana' => $personalizovana, 'brPitanjaPoStrani' => $brPitanjaPoStrani,
                            'idRedosledPitanja' => $redosledPitanja), 'Anketa')*/) {
                    
                  
                    $idAnketa = $sql->getLastIdAnketa($idKreator);
                    //edit_anketa
                    header("Location: index.php?p=28&idAnketa={$idAnketa}");
                    exit();
                } else {
                    // $error = '<br/>' . $sql->lastQuery;
                    $this->data['error'] = $error;
                    $error = '<br/>' . $sql->lastError;
                }
                
                } catch (PDOException $e){
                    die("Greška: ".$e->getMessage());
                }
                
                
            } else {
                $this->data['error'] = $error;
            }
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/new_anketa", $this->data);
    }

}