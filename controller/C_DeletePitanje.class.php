<?php

class C_DeletePitanje {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        if (isset($_GET["pitanje"]) && isset($_GET["anketa"])) {

            $sql = new DbAnkete();

            //treba proveriti da obrišemo neka pitanja koja koja su u odgovorima anketnih listića
            //ako se pitanje samo nalazi u jednoj anketi, odnosno u tekućoj
            if ($sql->deleteAnketaPitanjeIfOnlyOneRecordsIdPitanje($_GET['anketa'], $_GET['pitanje'])) {

                $sql->deletePitanjeFromAlOdgovori($_GET['pitanje'], $user->idKorisnik);
                $sql->deletePitanjeFromAlMatricaOdgovori($_GET['pitanje'], $user->idKorisnik);
                $sql->deletePitanjeFromAlTekstOdgovori($_GET['pitanje'], $user->idKorisnik);
                
                $sql->deletePitanjeFromAnketaPitanja($_GET['pitanje'], $user->idKorisnik);

                $sql->deletePitanjeFromPotpitanje($_GET['pitanje'], $user->idKorisnik);
                $sql->deletePitanjeFromOdgovori($_GET['pitanje'], $user->idKorisnik);

                $sql->deletePitanjeFromOdgovoriNestoDrugo($_GET['pitanje'], $user->idKorisnik);

                $sql->deletePitanje($_GET['pitanje'], $user->idKorisnik);
            }
        } else {
            header("Location: index.php");
            exit();
        }
    }

}

?>
