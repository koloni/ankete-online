<?php

class C_DeleteUnformedAnketa extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        if (isset($_GET['anketa'])) {
            //id kreatora uzimamo iz sesije
            $sql = new DbAnkete();
            $sql->deleteAnketaPitanja($_GET['anketa'], $user->idKorisnik);
            $sql->deleteAnketa($_GET['anketa'], $user->idKorisnik);
            header("Location: index.php?p=17&idKreator={$user->idKorisnik}");
            exit();
        }
    }

}

?>
