<?php

/**
 * Description of C_Listic
 *
 * @author korisnik
 */
include_once 'classes/PitanjaAnketniListic.class.php';

class C_ListicPopunjen extends Controller {

    function __construct() {

        $user = $this->data['user'] = $user = User::checkUserPrivilege(0);

        $error = "";

        if (isset($_GET["anketa"]) && isset($_GET["listic"])) {
            //provera da li listic int
            $pitanja = new PitanjaAnketniListic($_GET["listic"]);
            $this->data['error'] = $error;
            //bespotrebno dva puta pravimo objekat DbAnkete, je ga pravimo 
            $sql = new DbAnkete();
            $this->data['naziv'] = $sql->getNazivAnketeByIdAnketniListic($_GET["listic"]);
            $this->data['pitanja'] = $pitanja;
            $this->loadPage("pages/listic_popunjen", $this->data);
        } else if (isset($_GET["anketa"])) {

            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            }

            if ($error == "") {
                $sql = new DbAnkete();

                $idAnketniListic = $sql->getAnketniListicId($idAnketa, $user->idKorisnik);
                $pitanja = new PitanjaAnketniListic($idAnketniListic);
                //if($sql->rawResults;)
                $this->data['error'] = $error;

                $this->data['naziv'] = $sql->getNazivAnketeByIdAnketniListic($idAnketniListic);
                $this->data['pitanja'] = $pitanja;
                $this->loadPage("pages/listic_popunjen", $this->data);
            }
        }
    }

}
