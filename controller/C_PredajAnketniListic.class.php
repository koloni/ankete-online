<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_PredajAnketniListic
 *
 * @author korisnik
 */
class C_PredajAnketniListic extends Controller {

    function __construct() {

        $this->data['user'] = $user = User::checkUserPrivilege(0);
        //neka provera
        $idIspitanik=  Session::GetKey('idIspitanik');

        $error = "";
        if (isset($_GET["anketniListic"])) {
            $idAnketniListic = $_GET['anketniListic'];
            if ($idAnketniListic != "") {
                if (!Validator::Numeric($idAnketniListic)) {
                    $error.="<br/>Pogrešan format idAnketa!";
                }
            } else {
                $error.="<br/>idAnketa je obavezno!";
            }

            if ($error == "") {
                $sql = new DbAnkete();

                $sql->predajAnketniListic($idAnketniListic, $idIspitanik);

                header("Location: index.php?p=2");
                exit();
            }
        }
    }

}

?>
