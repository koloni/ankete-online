<?php

class C_ListiciNepersonalizovani extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);
        $error = "";

        $idAnketa = null;
        if (isset($_GET['anketa'])) {
            $this->data['idAnketa'] = $idAnketa = $_GET['anketa'];
        }


        if (isset($_GET['str'])) {
            //neka provra da li je ceo broj
            $strana = $_GET['str'];
            if ($strana < 0) {
                header("Location: index.php?p=2");
            }
            $this->data['trenutna'] = $strana;
        } else {
            $this->data['trenutna'] = $strana = 0;
        }

        $this->data['limit'] = $limit = 5;
        if (isset($_GET['limit'])) {
            //neka provra da li je ceo broj
            $limit = $_GET['limit'];
            if ($limit < 1 || $limit > 50) {
                header("Location: index.php?p=2");
            }
        }
        $this->data['limit'] = $limit;

        $orderby = "datumZavrsetkaDesc";
        if (isset($_GET['orderby'])) {
            $orderby = $_GET['orderby'];
        }

        $sql = new DbAnkete();
        $ukupnoZapisa = 0;
        $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnetniListici($idAnketa, 1);
		
		/*echo '<pre>';
		print_r($ukupnoZapisa);die();
		*/
		
        $filterPretrage = 1;
        if (isset($_GET['filterPretrage']) && ($_GET['filterPretrage'] == 1 || $_GET['filterPretrage'] == 2 || $_GET['filterPretrage'] == 3)) {
            $filterPretrage = $_GET['filterPretrage'];
            // exit($filterPretrage);
        }

        $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);

        $pocetniZapis = $strana * $limit;

        $this->data['pocetniZapis'] = $pocetniZapis;
        $this->data['poslednja'] = $brStrana - 1;

        $anketeStd = new stdClass();
        $anketeStd->ukupnoZapisaPretrage = $ukupnoZapisa;

        $this->data['orderby'] = $orderby;
        switch ($orderby) {
            case 5: case "datumZavrsetkaAsc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoNepersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, 'datumZavrsetka', 'asc');
                break;
            case 6: case "datumZavrsetkaDesc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoNepersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, 'datumZavrsetka', 'desc');
                break;
        }


        if (isset($_GET['ajaxJson'])) {
            //echo json_encode($this->data['ankete']);
            //echo json_encode($activeAnketeInfo);
            if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                echo json_encode($anketeStd);
            } else {
                echo "false";
            }
        } else {
            //kada prvi put otvaramo stranu
			
				/*echo '<pre>';
			print_r($sql->arrayedResult);die();
			*/
			
			//$sql->arrayedResult='ddfewfwewfwef';
			//var_dump($sql->arrayedResult);exit();
		
			
            if ($sql->arrayedResult == 0) {
                $this->data['message'] = "Trenutno nema ni jedna anketa.";
                $this->data['ankete'] = null;
            }


            // var_dump($this->data['ankete']);exit();
            //$this->data['ankete']=$ankete;
            $this->data['error'] = $error;
            $this->loadPage("pages/listici_nepersonalizovani", $this->data);
        }
    }

}

