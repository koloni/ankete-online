<?php

class C_DeleteAnketa extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);
        $sql = new DbAnkete();
        if (isset($_GET['anketa'])) {
            $idAnketa = $_GET['anketa'];
            $sql->deleteAlMatricaOdgovori($idAnketa);
            $sql->deleteAlOdgovori($idAnketa);            
            $sql->deleteAlTekstOdgovori($idAnketa);

            $sql->deleteAnketniListici($idAnketa);

            $sql->deleteAnketaPitanja($idAnketa);
            $sql->deleteAnketa($idAnketa, $user->idKorisnik);
            
            //ove ima exit() zato što se prilikom brisanja ankete poziva ajax i nema potrebe da se ponovo učitava strana sa anketama
            exit();
        }

        //$this->data['ankete'] = $sql->getActiveAnketeInfo();
        $this->data['ankete'] = $sql->getAnketeAllInfo(null, 'idAnketa', 'desc');
        $this->loadPage("pages/delete_ankete", $this->data);
    }

}

?>
