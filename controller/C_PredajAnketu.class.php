<?php

/**
 * Description of C_PredajAnketu
 *
 * @author korisnik
 */
class C_PredajAnketu {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";

        if (isset($_GET["anketa"]) && isset($_GET["redosledPitanja"])) {

            $idAnketa = $_GET['anketa'];
            if ($idAnketa != "") {
                if (!Validator::Numeric($idAnketa)) {
                    $error.="<br/>Pogrešan format ankete!";
                }
            } else {
                $error.="<br/>polje 'anketa' je obavezno!";
            }

            $redosledPitanja = $_GET['redosledPitanja'];
            if ($redosledPitanja != 0 && $redosledPitanja != 1 && $redosledPitanja != 2) {
                $error.="<br/>Redosled pitanja sadrži ne dozvoljene vrednosti";
            }

            if ($error == "") {
                $sql = new DbAnkete();

                if ($redosledPitanja == 2) {
                    $idPitanja = $sql->getIdPitanja($idAnketa);

                    //pravljenje niza 
                    $redniBrojevi = range(1, count($idPitanja));

                    //mešanje niza
                    shuffle($redniBrojevi);

                    for ($i = 0; $i < count($redniBrojevi); $i++) {
                        $sql->Update("AnketaPitanje", array('redniBroj' => $redniBrojevi[$i]), array('idPitanje' => $idPitanja[$i]->idPitanje, 'idAnketa' => $idAnketa));
                    }
                }


                $sql->predajAnketu($idAnketa, $user->idKorisnik);
                $sql->activatePitanja($idAnketa, $user->idKorisnik);
                
                header("Location: index.php?p=17&idKreator={$user->idKorisnik}");
                exit();
            }
        } else {
            header("Location: index.php?");
            exit();
        }
    }

}

?>
