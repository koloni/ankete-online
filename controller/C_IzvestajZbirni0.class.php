<?php

/**
 * Description of C_IzvestajiZbirni
 *
 * @author korisnik
 */
class PitanjeStatistikaa {

    public $tekst;
    public $idPitanje;
    public $odgovori;
    public $ukupnoOdgovora;

    function __construct($idPitanje, $tekst, $odgovori) {
        $this->tekst = $tekst;
        $this->id = $idPitanje;
        $this->odgovori = $odgovori;
        foreach ($odgovori as $o) {
            $this->ukupnoOdgovora+=$o->brOdgovora;
        }
    }

    function getProcenatByIndexOdgovora($index) {
        if ($this->ukupnoOdgovora >= 1) {
            return $this->odgovori[$index]->brOdgovora * (100 / $this->ukupnoOdgovora);
        }
        return 0;
    }

    function getIdOdgovoraByIndexOdgovora($index) {
        return $this->odgovori[$index]->idOdgovora;
    }

    function getOdgovori() {
        return $this->odgovori;
    }

    function prikazi() {
        ?>
        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border: 0px dotted black; width: 650px; padding: 0 0 20px 0; margin: 0 auto;">           
            <div style="padding: 0 0 0 30px;"><span style="border: 0px solid red;font-size: 17px; font-weight: 700;"><?php echo $this->tekst; ?></span></div>
            <table style="width: 500px;margin: 0px auto;" border="0px">
                <tbody>
                    <tr style="font-size: 12px;">
                        <td> </td>
                        <td style="text-align: right; font-style: italic;">Br. odgovora</td>
                        <td style="text-align: right; font-style: italic;">Procenat</td>     
                    </tr>
                    <?php
                    for ($i = 0; $i < count($this->odgovori); $i++) {
                        ?>
                        <tr>
                            <td style="font-size: 15px;"><?php echo $this->odgovori[$i]->tekst; ?></td>
                            <td style="text-align: right;"><?php echo $this->odgovori[$i]->brOdgovora; ?></td>
                            <td style="text-align: right;"><?php echo number_format($this->getProcenatByIndexOdgovora($i), 2, '.', ''); ?>%</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-bottom:10px;"><progress style="width: 100%; height: 12px;" value="<?php echo $this->odgovori[$i]->brOdgovora; ?>" max="<?php echo $this->ukupnoOdgovora; ?>"></progress></td>                            
                        </tr>
                    <?php } ?>
                </tbody>   
            </table>
        </div>
        <?php
    }

//end class PitanjeStatistika
}

class PitanjeStatistikaMatrica {

    private $potpitanja;

    function __construct($idPitanje, $tekst, $potpitanja) {
        $this->tekst = $tekst;
        $this->id = $idPitanje;
        $this->potpitanja = $potpitanja;
    }

    public function prikaziProgersBar() {
        
    }

    public function prikazi0() {
        foreach ($this->potpitanja as $pot) {
            $pot->prikazi();
        }
    }

    public function prikazi() {
        $num = "a";
        ?>
        
        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border-top: 0px dotted black; width: 100%; padding: 0 0 30px 0;">
            <div style='padding: 0 0 0 30px;'>
                <span style='border: 0px solid red;font-size: 17px; font-weight: 700;'><?php echo $this->tekst; ?></span>
                <div style='padding: 0 0 0 15px;'>

                    <table border='0px' cellspacing='0' width='700px' class='table_pitanje'>
                        <tr><td></td>
                            <?php
                            $odgovori = $this->potpitanja[0]->getOdgovori();
                            //prikazuje podunjene odgovore (prvi red tabele)                                   
                            foreach ($odgovori as $odg) {
                                ?>                                    
                                <td>
                                    <span style='font-weight:bold; width:100%;'><?php echo $odg->tekst; ?></span><br/>
                                    <span style='font-style:italic; font-size:12px;'>Br. odg. / Proc.</span>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                        $j = 0; //zbog parnih i neparnih kolona
                        //prikazuje kolonu potpitanja 
                        for ($i = 0; $i < count($this->potpitanja); $i++) {
                            ?>
                            <tr style='color: black; <?php echo ($j++ % 2 == 0 ? 'background-color: lightgrey;' : '') ?>'>                         
                                <td style='text-align:left; font-size:15px;'>
                                    <?php echo $num . ") " . $this->potpitanja[$i]->tekst; ?>
                                </td>
                                <?php
                                $odgovori = $this->potpitanja[$i]->getOdgovori();
                                for ($ii = 0; $ii < count($odgovori); $ii++) {
                                    ?>      
                                    <td>
                                        <?php echo $odgovori[$ii]->brOdgovora . " / " . number_format($this->potpitanja[$i]->getProcenatByIndexOdgovora($ii), 2, '.', '') . "%"; ?>
                                    </td>
                                    <!-- može i ovako
                                    <td style="text-align:center;padding: 0px;">
                                        <table border="0px;" style="font-size: 14px; border-spacing:0; border-collapse:collapse; margin: 0 auto;">
                                            <tr>
                                                <td style="text-align: right;">
                                    <?php echo $odgovori[$ii]->brOdgovora; ?>
                                                </td>
                                                <td>/</td>
                                                <td style="text-align: right;">
                                    <?php echo number_format($this->potpitanja[$i]->getProcenatByIndexOdgovora($ii), 2, '.', '') . "%"; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    -->
                                <?php } ?>
                            </tr>
                            <?php
                            $num++;
                        }//foreach 
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?
    }

//end class PitanjeStatistikaMatrica
}

class C_IzvestajZbirni extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(0);
        $error = "";
        if (isset($_GET["anketa"])) {
            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            } else {
                
            }

            $sql = new DbAnkete();
            $this->data['anketaInfo'] = $anketaInfo = $sql->getAnketaInfo($idAnketa);

            $pitanja = $sql->getPitanjaInfo($idAnketa);
            $pitanjaStatistika = array();
            foreach ($pitanja as $p) {

                //Ako je pitanje matrica
                if ($p->idVrstaOdgovora == 5 || $p->idVrstaOdgovora == 6) {
                    $potpitanja = $sql->getPotpitanjaInfo($p->idPitanje);
                    $potpitanjaStatistika = array();

                    foreach ($potpitanja as $pot) {
                        $odgovori = $sql->getFinishedAlMatricaOdgovoriWithBrojOdgovora($pot->idPotpitanje, $idAnketa, $p->idPitanje);
                        $potpitanjaStatistika[] = new PitanjeStatistika($pot->idPotpitanje, $pot->tekst, $odgovori);
                    }
                    $pitanjaStatistika[] = new PitanjeStatistikaMatrica($p->idPitanje, $p->tekst, $potpitanjaStatistika);
                } else {

                    $odgovori = $sql->getFinishedAlOdgovoriWithBrojOdgovora($idAnketa, $p->idPitanje);
                    if (count($odgovori) >= 1) {
                        $pitanjaStatistika[] = new PitanjeStatistika($p->idPitanje, $p->tekst, $odgovori);
                    }
                }
            }
            $this->data['error'] = $error;
            $this->data['pitanjaStatistika'] = $pitanjaStatistika;

            $this->loadPage('pages/izvestaj_zbirni', $this->data);
            //$this->loadPage("pages/edit_pitanje", $this->data);
        }
    }

}
