<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_SetRedniBrPitanja
 *
 * @author korisnik
 */
class C_SetRedniBrojPitanja {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";
        if (isset($_GET["anketa"]) && isset($_GET['pitanje']) && isset($_GET['redniBroj'])) {
            $idPitanje = $_GET['pitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format pitanja!";
            }

            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            }

            $redniBroj = $_GET['redniBroj'];
            if ($redniBroj < 1 && $redniBroj > 100) {
                $error = "Greška!";
            }

            if ($error == "") {
                $sql = new MySQL();
                $sql->Update("AnketaPitanje", array('redniBroj' => $redniBroj), array('idPitanje' => $idPitanje, 'idAnketa' => $idAnketa));
            }
        } else {
            header("Location: index.php");
        }
    }

}

?>
