<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_IzbaciPitanje
 *
 * @author korisnik
 */
class C_IzbaciPitanje {

    function __construct() {
        $user = User::checkUserPrivilege(4);
        $this->data['user'] = $user;

        $error = "";
        if (isset($_GET["idAnketa"]) && isset($_GET['idPitanje'])) {

            $idAnketa = $_GET['idAnketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            $idPitanje = $_GET['idPitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format idPitanje!";
            }

            if ($error == "") {
                $sql = new DbAnkete();
                $sql->izbaciPitanjeAnketa($idAnketa, $idPitanje);
            }
        }
    }

}

?>
