<?php

include_once 'classes/PitanjaAnketniListic.class.php';

class C_SacuvajOdgovore extends Controller {

    function __construct() {
        $error = "";
        if (isset($_GET['idAnketniListic']) && isset($_POST['nextPage'])) {
            $idAnketniListic = $_GET['idAnketniListic'];
            $strana = $_POST['nextPage'];
            $pitanja = unserialize($_SESSION['pitanja']);

            foreach ($pitanja->getPitanja() as $p) {
                //$tipPitanja = $p->getIdVrstaOdgovora();
                $sql = new DbAnkete();
                if ($p instanceof PitanjeRadio || $p instanceof PitanjeSelect) {
                    //if ($tipPitanja == 1 || $tipPitanja == 2) {
                    if (!isset($_POST[$p->getIdPitanje()]) && $p->getIsObavezno()) {
                        //$strana-=1;
                        $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                        continue;
                        //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                        //exit();
                    } else if (!isset($_POST[$p->getIdPitanje()])) {
                        continue;
                    }

                    $idOdgovor = $_POST[$p->getIdPitanje()];
                    $brIzabranihOdgovora = $p->getBrojIzabranihOdgovora();

                    $vecIzabranIdOdgovor = false;
                    //ako postoji odgovor ažuriraj ga 
                    if ($brIzabranihOdgovora == 1) {
                        $odgovori = $p->getOdgovori();

                        //proveri da li je izabrani odgovor isti kao i prethodno izabran
                        foreach ($odgovori as $o) {
                            if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovor) {
                                echo "<br/>Odgovor je isti: " . $idOdgovor;
                                $vecIzabranIdOdgovor = true;
                                break;
                            }
                        }

                        if ($vecIzabranIdOdgovor == false) {
                            if ($sql->Update("AlOdgovor", array('idOdgovor' => $idOdgovor), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()))) {
                                
                            }
                        }
                    } else {
                        $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");
                    }
                } else if ($p instanceof PitanjeCheckBox || $p instanceof PitanjeSelectMultiple) {
                    //} else if ($tipPitanja == 3 || $tipPitanja == 4) {
                    if (!isset($_POST[$p->getIdPitanje()]) && $p->getIsObavezno()) {
                        //$strana-=1;
                        $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                        continue;
                        //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                    } else if (!isset($_POST[$p->getIdPitanje()])) {
                        continue;
                    }

                    $odgovoriNoviId = $_POST[$p->getIdPitanje()];

                    //da li niz
                    if (is_array($odgovoriNoviId)) {
                        $odgovoriStari = $p->getOdgovori();

                        //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                        foreach ($odgovoriStari as $oStari) {
                            $stariPostoji = false;
                            for ($i = 0; $i < count($odgovoriNoviId); $i++) {
                                if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                    $stariPostoji = true;
                                    $odgovoriNoviId[$i] = 0;
                                    break;
                                }
                            }
                            //ako stari odgovor nije izabran, brišemo ga
                            if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                $sql->Delete("AlOdgovor", array('idAnketniListic' => $idAnketniListic, 'idOdgovor' => $oStari->getIdOdgovor()));
                            }
                        }

                        //unosimo nove odgovore
                        foreach ($odgovoriNoviId as $oNoviId) {
                            if ($oNoviId > 0) {
                                $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $oNoviId), "AlOdgovor");
                                //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");                                
                            }
                        }
                    } else {
                        //neka greska checkbox pitanje mora da bude niz                        
                    }
                } else if ($p instanceof PitanjeMatricaRadio) {
                    //} else if ($tipPitanja == 5) {
                    $potpitanja = $p->getKolekcijaPotpitanjaMatrica()->getPotpitanja();

                    foreach ($potpitanja as $pot) {
                        if (!isset($_POST[$pot->getIdPotpitanje()]) && $p->getIsObavezno()) {
                            //ako je pitanje obavezno, ostaje se na istoj strani
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                        } else if (!isset($_POST[$pot->getIdPotpitanje()])) {
                            break;
                        }

                        $idOdgovor = $_POST[$pot->getIdPotpitanje()];
                        $brIzabranihOdgovora = $pot->getBrojIzabranihOdgovora();

                        $vecIzabranIdOdgovor = false;
                        //ako postoji odgovor ažuriraj ga 
                        if ($brIzabranihOdgovora == 1) {
                            $odgovori = $pot->getOdgovori();

                            //proveri da li je izabrani odgovor isti kao i prethodno izabran
                            foreach ($odgovori as $o) {
                                if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovor) {
                                    //echo "<br/>Odgovor je isti: " . $idOdgovor;
                                    $vecIzabranIdOdgovor = true;
                                    break;
                                }
                            }

                            if ($vecIzabranIdOdgovor == false) {
                                if ($sql->Update("AlMatricaOdgovor", array('idOdgovor' => $idOdgovor), array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje()))) {
                                    
                                }
                            }
                        } else {
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $idOdgovor), "AlMatricaOdgovor");
                        }
                    }
                    //exit();
                } else if ($p instanceof PitanjeMatricaCheckBox) {
                    //} else if ($tipPitanja == 6) {
                    $potpitanja = $p->getKolekcijaPotpitanjaMatrica()->getPotpitanja();
                    foreach ($potpitanja as $pot) {
                        if (!isset($_POST[$pot->getIdPotpitanje()]) && $p->getIsObavezno()) {
                            //ako je pitanje obavezno, ostaje se na istoj strani
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$pot->getIdPotpitanje()])) {
                            // continue;
                            break;
                        }

                        $odgovoriNoviId = $_POST[$pot->getIdPotpitanje()];
                        var_dump($odgovoriNoviId);
                        echo '<br>-------------------<br>';
                        if (is_array($odgovoriNoviId)) {
                            $odgovoriStari = $pot->getOdgovori();

                            //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                            foreach ($odgovoriStari as $oStari) {
                                $stariPostoji = false;
                                for ($i = 0; $i < count($odgovoriNoviId); $i++) {
                                    if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                        $stariPostoji = true;
                                        $odgovoriNoviId[$i] = 0;
                                        break;
                                    }
                                }
                                //ako stari odgovor nije izabran, brišemo ga
                                if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                    $sql->Delete("AlMatricaOdgovor", array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $oStari->getIdOdgovor()));
                                    echo '<br>delete';
                                    echo $sql->affected . "<br>";
                                }
                            }

                            //unosimo nove odgovore
                            foreach ($odgovoriNoviId as $oNoviId) {
                                if ($oNoviId > 0) {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $oNoviId), "AlMatricaOdgovor");
                                    //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");                                

                                    echo '<br>insert';
                                    echo $sql->lastError;
                                }
                            }
                        } else {
                            //neka greska checkbox pitanje mora da bude niz                        
                        }
                    }//foreach
                    //  exit();
                } else if ($p instanceof PitanjeSlobodanUnos) {
                    //} else if ($tipPitanja == 7 || $tipPitanja == 8 || $tipPitanja == 9) {
                    $potpitanja = $p->getPotpitanja();
                    echo "Obavezno: " . $p->getIsObavezno();

                    $prazno = false;
                    foreach ($potpitanja as $pot) {
                        //ako je obavezno i prazno učitaj ponovo stranicu
                        if (empty($_POST[$pot->getIdPotpitanje()]) && $p->getIsObavezno()) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            $prazno = true;
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (empty($_POST[$pot->getIdPotpitanje()])) {
                            //ako nije obavezno, a prazno je
                            $prazno = true;
                            break;
                        }
                    }
                    if ($prazno) {
                        continue;
                    }
                    //ako nije prazno
                    // if (!$prazno) {
                    foreach ($potpitanja as $pot) {

                        if ($pot->getOdgovor() == "") {
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'tekst' => $_POST[$pot->getIdPotpitanje()]), "AlTekstOdgovor");
                            // echo $sql->lastError;
                        } else if ($pot->getOdgovor() != $_POST[$pot->getIdPotpitanje()]) {
                            $sql->Update("AlTekstOdgovor", array('tekst' => $_POST[$pot->getIdPotpitanje()]), array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje()));
                            //echo $sql->lastError;
                        }
                    }
                    //}
                }
            }//endforeach
            // header("Location: index.php?idAnketniListic={$_GET['idAnketniListic']}&strana={$_POST['nextPage']}");
            //exit();
            header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
        }
    }

}

?>
