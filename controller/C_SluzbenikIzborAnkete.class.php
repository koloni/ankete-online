<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_SluzbenikIzborAnkete
 *
 * @author korisnik
 */
class C_SluzbenikIzborAnkete extends Controller {

    public function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(3);
        
        
        $sql=new DbAnkete();
        
        $this->data['korisnici']=$sql->getNaloziAktivirani();
        
        $this->loadPage('pages/sluzbenik_izbor_ankete', $this->data);
    }

}

?>
