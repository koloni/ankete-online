<?php

include_once 'classes/PitanjeRadio.class.php';
include_once 'classes/PitanjeMatricaRadio.class.php';
include_once 'classes/PitanjeMatricaCheckBox.class.php';
include_once 'classes/PotpitanjeMatrica.class.php';

class C_EditPitanje extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";
        if (isset($_POST["idAnketa"]) && isset($_POST['idPitanje']) && isset($_POST['tekst'])) {
            $idPitanje = $_POST['idPitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format idPitanje!";
            }

            $idAnketa = $_POST['idAnketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            $isObavezno = 0;
            if (isset($_POST['isObavezno'])) {
                $isObavezno = $_POST['isObavezno'];
                if ($isObavezno != '1') {
                    $error.= "<br/>Greška, pogrešna vrednost obavezno!";
                } else {
                    $isObavezno = 1;
                }
            }

            if (isset($_POST['odgovori'])) {
                $odgovoriNew = $_POST['odgovori'];
                if (!is_array($odgovoriNew)) {
                    $error = "Greška nema odgovora!";
                } else {
                    //moada da se stavi &new, u suprotnom kopira podatak
                    foreach ($odgovoriNew as &$onew) {
                        //$onew = str_replace(array('<', '>', '"','\''), array('&lt;', '&gt;', '&quot;', '&quot;'), $onew);                   
                        $onew = str_replace(array('<', '>'), array('&lt;', '&gt;'), $onew);

                        if ($onew == '') {
                            $error = "Prazno poolje odgovor!";
                        }
                    }
                }
            } else {
                $odgovoriNew = array();
            }

            /*
              $odgovoriNew = $_POST['odgovori'];
              if (!is_array($odgovoriNew)) {
              $error = "Greška!";
              }
             */

            //provera da li tekst prazan?
            $tekst = $_POST['tekst'];

            if ($error == "") {
                //U ovom delu, korisnik je već prijavljen na sistem i ima prava izmene,
                //ali pokušava nelegalno da menja pitanje, ili se ponovo ulogovao a ostala mu otvorena stranica edit_pitanje otvorena od ranije i on pokušava da izmeni pitanje
                if (!isset($_SESSION['pitanje'])) {
                    //ovo treba izmeniti
                    header('Location: unformed_ankete.php?idKreator=1');
                    exit();
                }
                //ne mogu ovako da koristim, jer kasnije kad budem probao da pristum objetu, buniće se da nije unseializovan
                //$pitanje = $_SESSION['pitanje'];
                $pitanje = unserialize($_SESSION['pitanje']);

                //provera da li idPitanja podudara sa idPitanja iz sesije, ako ne, neko je pokušao nelegalno preko linka da izmeni idPitanja
                if ($idPitanje == $pitanje->getIdPitanje()) {

                    $odgovoriOld = $pitanje->getOdgovori();
                    $sql = new MySQL();

/////////
                    if ($pitanje instanceof PitanjeRadio || $pitanje instanceof PitanjeCheckBox) {
                        if (isset($_POST['nestoDrugo'])) {
                            if ($_POST['nestoDrugo'] == "1") {
                                if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                    
                                      /*
                                    //ako je isto nema potrebe da ga menjamo
                                    //ovde treba da ubacim novi tekst
                                    if (isset($_POST['nestoDrugoTekst'])) {
                                        if ($_POST['nestoDrugoTekst'] == "") {
                                            //$sql->Insert(array('idPitanje' => $idPitanje['idPitanje'], 'tekst' => "Nešto drugo:"), "OdgovorNestoDrugo");
                                            $sql->Update("OdgovorNestoDrugo", array('tekst' => "Nešto drugo:"), array('idPitanje' => $idPitanje));
                                        } else {
                                            //$sql->Insert(array('idPitanje' => $idPitanje['idPitanje'], 'tekst' => $_POST['nestoDrugoTekst']), "OdgovorNestoDrugo");
                                            $sql->Update("OdgovorNestoDrugo", array('tekst' => $_POST['nestoDrugoTekst']), array('idPitanje' => $idPitanje));
                                        }
                                    }
                                    */
                                                                            
                                       if (isset($_POST['idOdgovorNestoDrugo'])) {
                                           
                                           //ubacivanje pitanja u anketu, sa nestoDrugo               
                                           $sql->Update("Pitanje", array('idNestoDrugo' => $_POST['idOdgovorNestoDrugo']), array('idPitanje' => $idPitanje, 'idKreator' => $user->idKorisnik));                     
                                           
                                       }

                                    
                                    
                                    
                                } else {
                                    /*
                                    //update isNestoDrugo
                                    $sql->Update("Pitanje", array('isNestoDrugo' => 1), array('idPitanje' => $idPitanje, 'idKreator' => $user->idKorisnik));
                                    //$sql->Update("AnketaPitanje", array('isNestoDrugo' => 1), array('idPitanje' => $pitanje->getIdPitanje(), 'idAnketa' => $idAnketa));

                                    if ($_POST['nestoDrugoTekst'] == "") {
                                        $sql->Insert(array('idPitanje' => $idPitanje, 'tekst' => "Nešto drugo:"), "OdgovorNestoDrugo");
                                    } else {
                                        $sql->Insert(array('idPitanje' => $idPitanje, 'tekst' => $_POST['nestoDrugoTekst']), "OdgovorNestoDrugo");
                                    }*/
                                    
                                            
                                       if (isset($_POST['idOdgovorNestoDrugo'])) {
                                           //ubacivanje pitanja u anketu, sa nestoDrugo               
                                           $sql->Update("Pitanje", array('idNestoDrugo' => $_POST['idOdgovorNestoDrugo']), array('idPitanje' => $idPitanje, 'idKreator' => $user->idKorisnik));                     
                                       }
                                    
                                }
                            } else {
                                $error.= "<br/>Greška, nešto drugo se setovano u pogrešnom formatu!";
                            }
                        } else {
                            //Ako nije setovano nešto drugo poništi ga
                            if ($pitanje instanceof PitanjeRadioWithNestoDrugo || $pitanje instanceof PitanjeCheckBoxWithNestoDrugo) {
                                //poništavanje nešto drugo                                
                                //$sql->Update("AnketaPitanje", array('isNestoDrugo' => 0), array('idPitanje' => $pitanje->getIdPitanje(), 'idAnketa' => $idAnketa));
                                $sql->Update("Pitanje", array('idNestoDrugo' => 0), array('idPitanje' => $idPitanje, 'idKreator' => $user->idKorisnik));
                                
                            } else {
                                //nije ni bili ranije setovano nešto drugo                                
                            }
                        }
                    }
/////////////
                    //ako isObavezno nije izmenjeno ne tregba ga menjati u bazi
                    if ($isObavezno == $pitanje->getIsObavezno()) {
                        // $error .= "Pitanje nije promenjeno!";
                    } else {
                        $sql = new MySQL();
                        $sql->Update("AnketaPitanje", array('isObavezno' => $isObavezno), array('idPitanje' => $pitanje->getIdPitanje(), 'idAnketa' => $idAnketa));
                    }

                    //ako tekst pitanja nije izmenjen nije ga potrebno menjati u bazi
                    if ($tekst == $pitanje->getTekst()) {
                        // $error .= "Pitanje nije promenjeno!";
                    } else {
                        $sql = new MySQL();
                        $sql->Update("Pitanje", array('tekst' => $tekst), array('idPitanje' => $pitanje->getIdPitanje()));
                    }

                    if (isset($_POST["potpitanja"])) {
                        $potpitanjaNew = $_POST['potpitanja'];
                        if (!is_array($potpitanjaNew)) {
                            $error = "Greška!";
                        }
                        /*
                          $kolekcijaPotpitanjaMatrica = $pitanje->getKolekcijaPotpitanjaMatrica();
                          foreach ($kolekcijaPotpitanjaMatrica->getPotpitanja() as $pot) {
                          ?>
                          <div><input type = 'text' name = 'potpitanja[]' value='<?php echo $pot->getTekst(); ?>'><a href = '#' onclick = 'removeOdgovor(this.parentNode)'><img width = '16px' height = '16px' src = '<?php echo TEMPLATES_DIR; ?>/default/images/Close.png'></a></div>
                          <?php
                          }
                         */

                        $sql = new MySQL();
                        $potpitanjaOld = $pitanje->getPotpitanja();
                        if ($pitanje instanceof PitanjeSlobodanUnos) {
                            //ako novih potpitanja ima više, prvo ažuriramo postojeće, pa tek onda ubacujemo nove
                            if (count($potpitanjaNew) >= count($potpitanjaOld)) {
                                $i = 0;
                                //updajet postojećih potpitanja
                                for (; $i < count($potpitanjaOld); $i++) {
                                    $sql->Update("Potpitanje", array('tekst' => $potpitanjaNew[$i]), array('idPotpitanje' => $potpitanjaOld[$i]->getIdPotpitanje()));
                                }
                                //unos novih potpitanja
                                for (; $i < count($potpitanjaNew); $i++) {
                                    $sql->Insert(array('idPitanje' => $pitanje->getIdPitanje(), 'tekst' => $potpitanjaNew[$i]), 'Potpitanje');
                                }
                            } else {
                                $i = 0;
                                //updejt postojećih                            
                                for (; $i < count($potpitanjaNew); $i++) {
                                    $sql->Update("Potpitanje", array('tekst' => $potpitanjaNew[$i]), array('idPotpitanje' => $potpitanjaOld[$i]->getIdPotpitanje()));
                                }

                                //brisanje viška
                                for (; $i < count($potpitanjaOld); $i++) {
                                    $sql->Delete('Potpitanje', array('idPotpitanje' => $potpitanjaOld[$i]->getIdPotpitanje()));
                                }
                            }
                        } else {
                            //isto ko if segment samo sto se ovde poziva $potpitanjaOld[$i]->getIdPotpitanje()
                            //ako novih potpitanja ima više, prvo ažuriramo postojeće, pa tek onda ubacujemo nove
                            if (count($potpitanjaNew) >= count($potpitanjaOld)) {
                                $i = 0;
                                //updajet postojećih potpitanja
                                for (; $i < count($potpitanjaOld); $i++) {
                                    $sql->Update("Potpitanje", array('tekst' => $potpitanjaNew[$i]), array('idPotpitanje' => $potpitanjaOld[$i]->getIdPitanje()));
                                }
                                //unos novih potpitanja
                                for (; $i < count($potpitanjaNew); $i++) {
                                    $sql->Insert(array('idPitanje' => $pitanje->getIdPitanje(), 'tekst' => $potpitanjaNew[$i]), 'Potpitanje');
                                }
                            } else {
                                $i = 0;
                                //updejt postojećih                            
                                for (; $i < count($potpitanjaNew); $i++) {
                                    $sql->Update("Potpitanje", array('tekst' => $potpitanjaNew[$i]), array('idPotpitanje' => $potpitanjaOld[$i]->getIdPitanje()));
                                }

                                //brisanje viška
                                for (; $i < count($potpitanjaOld); $i++) {
                                    $sql->Delete('Potpitanje', array('idPotpitanje' => $potpitanjaOld[$i]->getIdPitanje()));
                                }
                            }
                        }
                    }//end post potpitanja


                    if (!($pitanje instanceof PitanjeSlobodanUnos)) {
                        //ako novih odgovora ima više, prvo updejtujem postojeće, pa tek onda insertujem nove
                        if (count($odgovoriNew) >= $pitanje->getBrojOdgovora()) {
                            $i = 0;
                            //updajet postojećih odgovora
                            for (; $i < $pitanje->getBrojOdgovora(); $i++) {
                                $sql->Update("Odgovor", array('tekst' => $odgovoriNew[$i]), array('idOdgovor' => $odgovoriOld[$i]->getIdOdgovor()));
                            }
                            //unos novih odgovora
                            for (; $i < count($odgovoriNew); $i++) {
                                $sql->Insert(array('idPitanje' => $pitanje->getIdPitanje(), 'tekst' => $odgovoriNew[$i]), 'Odgovor');
                            }
                        } else {
                            $i = 0;
                            //updejt postojećih
                            for (; $i < count($odgovoriNew); $i++) {
                                $sql->Update("Odgovor", array('tekst' => $odgovoriNew[$i]), array('idOdgovor' => $odgovoriOld[$i]->getIdOdgovor()));
                            }

                            //brisanje viška
                            for (; $i < $pitanje->getBrojOdgovora(); $i++) {
                                $sql->Delete('Odgovor', array('idOdgovor' => $odgovoriOld[$i]->getIdOdgovor()));
                            }
                        }

                        /*
                          kada je pitanje promenjeno vrati se jednu stranu unazan
                          <td><a href='edit_anketa.php?idAnketa=<?php echo $a['idAnketa']; ?>'>izmeni</a><a href='#'>predaj</a></td>
                         * 
                         */
                    }

                    //header("Location: edit_anketa.php?idAnketa={$_GET['idAnketa']}");
                    header("Location: index.php?p=28&idAnketa={$_POST['idAnketa']}");
                    exit();
                } else {
                    //neko je pokušao da promeni pitanje preko linka. a ne legalnim putem
                    //$error = "Pogrešno pitanje!";
                    header("Location: index.php");
                    exit();
                }
            }
        } else if (isset($_GET["pitanje"]) && isset($_GET["anketa"])) {            
            $idPitanje = $_GET['pitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format idPitanje!";
            }

            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            if ($error == "") {                
                $sql = new DbAnkete();
                $pitanje = $sql->getPitanje($idPitanje);                                
                $this->data['pitanje'] = $pitanje[0];
                $this->data['odgovorNestoDrugo']=$sql->Select('OdgovorNestoDrugo');
                $this->data['idAnketa'] = $idAnketa;
                //ubacujemo pitanje u sesiju da bi mogli kasnije da uporedimo
                $_SESSION['pitanje'] = serialize($this->data['pitanje']);
            } else {
                header("Location: index.php");
            }
        } else {
            //ovo ne prikazivati u produkciji
            $error = "Pitanje nije setovano!";
            header("Location: index.php");
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/edit_pitanje", $this->data);
    }

}

