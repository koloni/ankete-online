<?php

class C_RezultatiAnketaKreator extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);
     
        $error = "";

        if (isset($_GET['str'])) {
            //neka provra da li je ceo broj
            $strana = $_GET['str'];
            if ($strana < 0) {
                header("Location: index.php?p=2");
            }
            $this->data['trenutna'] = $strana;
        } else {
            $this->data['trenutna'] = $strana = 0;
        }

        $this->data['limit'] = $limit = 5;
        if (isset($_GET['limit'])) {
            //neka provra da li je ceo broj
            $limit = $_GET['limit'];
            if ($limit < 1 || $limit > 50) {
                header("Location: index.php?p=2");
            }
        }
        $this->data['limit'] = $limit;


        $orderby = "datumPocetkaDesc";
        if (isset($_GET['orderby'])) {
            $orderby = $_GET['orderby'];
        }

        $kljucneReci = "";
        if (isset($_GET['kljucnaRec'])) {
            $kljucneReci = explode(' ', $_GET['kljucnaRec']);
        }

        $ukupnoZapisa = 0;
        $sql = new DbAnkete();

        if (!empty($_GET['kljucnaRec'])) {
            $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnkete(3, $user->idKorisnik, $kljucneReci);
        } else {
            $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnkete(3, $user->idKorisnik);
        }

        $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);

        $pocetniZapis = $strana * $limit;

        $this->data['pocetniZapis'] = $pocetniZapis;
        $this->data['poslednja'] = $brStrana - 1;

        $ankete = new stdClass();
        $ankete->ukupnoZapisaPretrage = $ukupnoZapisa;

        $this->data['orderby'] = $orderby;
        switch ($orderby) {
            case 1: case "nazivAsc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'asc');
                break;
            case 2: case "nazivDesc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'desc');
                break;
            case 3: case "datumPocetkaAsc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'asc');
                break;
            case 4: case "datumPocetkaDesc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'desc');
                break;
            case 5: case "datumIstekaAsc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'asc');
                break;
            case 6: case "datumIstekaDesc":
                $ankete->ankete = $this->data['ankete'] = $sql->getActiveAndFinishedAnketeInfoWithBrojPopunjenihListica($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'desc');
                break;
        }


        if (isset($_GET['ajaxJson'])) {
            //echo json_encode($this->data['ankete']);
            //echo json_encode($activeAnketeInfo);
            if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                echo json_encode($ankete);
            } else {
                echo "false";
            }
        } else {
            //kada prvi put otvaramo stranu
            if ($sql->getRecords()==0) {
                $this->data['message'] = "Trenutno nema ni jedna anketa.";
                $this->data['ankete'] = null;
            }

            $this->data['error'] = $error;
            $this->loadPage("pages/rezultati_ankete_kreator", $this->data);
        };
    }

}

