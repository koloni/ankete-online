<?php

class C_ZabraneAnketa extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(-1);

        $error = "";
        if (isset($_GET["anketa"]) && isset($_GET['stanje'])) {
            $idAnketa = $_GET['anketa'];
            if ($idAnketa != "") {
                if (!Validator::Numeric($idAnketa)) {
                    $error.="<br/>Pogrešan format idAnketa!";
                }
            } else {
                $error.="<br/>idAnketa je obavezno!";
            }

            if ($error == "") {
                $sql = new DbAnkete();
                if ($_GET['stanje'] == '1') {
                    if ($sql->activateAnketa($idAnketa)) {
                        echo 'true';
                    } else {
                        echo 'false';
                    }
                } else if ($_GET['stanje'] == '-1') {
                    //echo TRUE;
                    //echo FALSE;//vraća na klijentu prazan string                   
                    if ($sql->blockAnketa($idAnketa)) {
                        echo 'true';
                    } else {
                        echo 'false';
                    }
                } else if ($_GET['stanje'] == 'obr') {
                    //$sql->deleteNalog($_GET['id']);
                } else {
                    
                }
            }
        } else {

            $strana = 0;
            if (isset($_GET['str'])) {
                //neka provra da li je ceo broj
                $strana = $_GET['str'];
                if ($strana < 0) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['trenutna'] = $strana;

            $limit = 5;
            if (isset($_GET['limit'])) {
                //neka provra da li je ceo broj
                $limit = $_GET['limit'];
                if ($limit < 1 || $limit > 50) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['limit'] = $limit;

            
            $orderby = "nazivAsc";
            if (isset($_GET['orderby'])) {
                $orderby = $_GET['orderby'];
            }
            $this->data['orderby'] = $orderby;

            $kljucneReci = "";
            if (isset($_GET['kljucnaRec'])) {
                $kljucneReci = explode(' ', $_GET['kljucnaRec']);
            }

            $sql = new DbAnkete();

            $ukupnoZapisa = 0;
            if (!empty($_GET['kljucnaRec'])) {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnkete(null, null, $kljucneReci);
            } else {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnkete();
            }

            $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);
            $this->data['pocetniZapis'] = $pocetniZapis = $strana * $limit;
            $this->data['poslednja'] = $brStrana - 1;

            $stdObject = new stdClass();
            $stdObject->ukupnoZapisaPretrage = $ukupnoZapisa;

            switch ($orderby) {
                case 1: case "nazivAsc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'asc');
                    break;
                case 2: case "nazivDesc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'desc');
                    break;
                case 3: case "datumPocetkaAsc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'asc');
                    break;
                case 4: case "datumPocetkaDesc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'desc');
                    break;
                case 5: case "datumIstekaDesc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'asc');
                    break;
                case 6: case "datumIstekaDesc":
                    $stdObject->ankete = $this->data['ankete'] = $sql->getAnketeAllInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'desc');
                    break;
            }

            if (isset($_GET['ajaxJson'])) {
                if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                    echo json_encode($stdObject);
                } else {
                    echo "false";
                }
            } else {
                //kada prvi put otvaramo stranu
                if ($this->data['ukupnoZapisa']==0) {
                    $this->data['message'] = "Trenutno nema ni jedno pitanje.";
                    $this->data['ankete'] = null;
                }

                $this->data['error'] = $error;
                $this->loadPage("pages/zabrane_anketa", $this->data);
            }
        }
    }

}

