<?php

/**
 * Description of C_Unformed
 *
 * @author korisnik
 */
class C_RezultatiAnkete extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(0);
        $error = "";

        if ($error == "") {
            $sql = new DbAnkete();
            $ankete = $sql->getRezultatiAnketeInfo();
            //?????
            if ($sql->arrayedResult >= 1) {                                                
                $this->data['ankete'] = $ankete;                
            } else {
                $this->data['message'] = "Nema rezultata ni za jednu anketu.";
            }
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/rezultati_ankete", $this->data);
    }

}