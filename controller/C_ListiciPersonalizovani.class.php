<?php

class C_ListiciPersonalizovani extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);
        $error = "";

        $idAnketa = null;
        if (isset($_GET['anketa'])) {
            $this->data['idAnketa'] = $idAnketa = $_GET['anketa'];
        }


        if (isset($_GET['str'])) {
            //neka provra da li je ceo broj
            $strana = $_GET['str'];
            if ($strana < 0) {
                header("Location: index.php?p=2");
            }
            $this->data['trenutna'] = $strana;
        } else {
            $this->data['trenutna'] = $strana = 0;
        }

        $this->data['limit'] = $limit = 5;
        if (isset($_GET['limit'])) {
            //neka provra da li je ceo broj
            $limit = $_GET['limit'];
            if ($limit < 1 || $limit > 50) {
                header("Location: index.php?p=2");
            }
        }
        $this->data['limit'] = $limit;

        $orderby = "datumZavrsetkaDesc";
        if (isset($_GET['orderby'])) {
            $orderby = $_GET['orderby'];
        }

        $kljucneReci = "";
        if (isset($_GET['kljucnaRec'])) {
            $kljucneReci = explode(' ', $_GET['kljucnaRec']);
        }

        $ukupnoZapisa = 0;
        $sql = new DbAnkete();

        $filterPretrage = 1;
        if (isset($_GET['filterPretrage']) && ($_GET['filterPretrage'] == 1 || $_GET['filterPretrage'] == 2 || $_GET['filterPretrage'] == 3)) {
            $filterPretrage = $_GET['filterPretrage'];

            // exit($filterPretrage);
        }

        if (!empty($_GET['kljucnaRec'])) {
            //treba doraditi metodu
            $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnetniListiciSearch($idAnketa, 1, $filterPretrage, $kljucneReci);
        } else {
            $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfAnetniListici($idAnketa, 1);
        }

        $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);

        $pocetniZapis = $strana * $limit;

        $this->data['pocetniZapis'] = $pocetniZapis;
        $this->data['poslednja'] = $brStrana - 1;

        $anketeStd = new stdClass();
        $anketeStd->ukupnoZapisaPretrage = $ukupnoZapisa;

        $this->data['orderby'] = $orderby;
        switch ($orderby) {
            case 1: case "firstNameAsc":
                //function getAnketniListiciInfoPersonalizovani($idAnketa = null, $idKreator = null, $isPopunjen = null, $od = 0, $brZapisa = 0, $filterPretrage = 1, $kljucneReci = array(), $orderByColumn = "firstName", $ascOrDesc = "asc") {
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'firstName', 'asc');
                break;
            case 2: case "firstNameDesc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'firstName', 'desc');
                break;
            case 3: case "lastNameAsc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'lastName', 'asc');
                break;
            case 4: case "lastNameDesc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'lastName', 'desc');
                break;
            case 5: case "datumZavrsetkaAsc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'datumZavrsetka', 'asc');
                break;
            case 6: case "datumZavrsetkaDesc":
                $anketeStd->ankete = $this->data['ankete'] = $sql->getAnketniListiciInfoPersonalizovani($idAnketa, $user->idKorisnik, 1, $pocetniZapis, $limit, $filterPretrage, $kljucneReci, 'datumZavrsetka', 'desc');
                break;
        }


        if (isset($_GET['ajaxJson'])) {
            //echo json_encode($this->data['ankete']);
            //echo json_encode($activeAnketeInfo);
            if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                echo json_encode($anketeStd);
            } else {
                echo "false";
            }
        } else {
            //kada prvi put otvaramo stranu
            if ($sql->arrayedResult == 0) {
                $this->data['message'] = "Trenutno nema ni jedna anketa.";
                $this->data['ankete'] = null;
            }


            // var_dump($this->data['ankete']);exit();
            //$this->data['ankete']=$ankete;
            $this->data['error'] = $error;
            $this->loadPage("pages/listici_personalizovani", $this->data);
        };
    }

}

