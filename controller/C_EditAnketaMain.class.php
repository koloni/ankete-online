<?php

class C_EditAnketaMain extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";

        if (isset($_POST["naziv"]) && isset($_POST["datumPocetka"]) && isset($_POST["datumIsteka"])
                && isset($_POST["personalizovana"]) && isset($_POST["brPitanjaPoStrani"]) && isset($_POST["redosledPitanja"])) {

            $naziv = $_POST['naziv'];
            if ($naziv != "") {
                if (!Validator::Text($naziv, 3, 100)) {
                    $error.="<br/>Naziv ankete mora da sadrži minimum 3 karaktera!";
                }
            } else {
                $error.="<br/>Naziv ankete je obavezan!";
            }

            $datumPocetka = $_POST['datumPocetka'];
            if ($datumPocetka != "") {
                if (!Validator::DateOrDateTime($datumPocetka)) {
                    $error.="<br/>Datum početka je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Datum početka je obavezan!";
            }

            $datumIseka = $_POST['datumIsteka'];
            if ($datumIseka != "") {
                if (!Validator::DateOrDateTime($datumIseka)) {
                    $error.="<br/>Datum isteka je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Datum isteka je obavezan!";
            }

            $personalizovana = $_POST['personalizovana'];
            if ($personalizovana == 'true') {
                $personalizovana = true;
            } else if ($personalizovana == 'false') {
                $personalizovana = false;
            } else {
                $error.="<br/>Personalizovana je uneto u pogrešnom formatu!";
            }


            $redosledPitanja = $_POST['redosledPitanja'];

            if ($redosledPitanja != 0 && $redosledPitanja != 1 && $redosledPitanja != 2) {
                $error.="<br/>Redosled pitanja je uneto u pogrešnom formatu!";
            }

            $brPitanjaPoStrani = $_POST['brPitanjaPoStrani'];
            if ($brPitanjaPoStrani != "") {
                if (!Validator::Numeric($brPitanjaPoStrani)) {
                    $error.="<br/>Broj pitanja po strani je unešen u pogrešnom formatu!";
                }
            } else {
                $error.="<br/>Broj pitanja po strani je obavezan!";
            }


            if ($error == "") {
                $sql = new MySQL();

                $anketa = $_SESSION['anketa'];
                if ($sql->Update('Anketa', array('naziv' => $naziv, 'idKreator' => 1, 'datumPocetka' => $datumPocetka, 'datumZavrsetka' => $datumIseka,
                            'isPersonalizovana' => $personalizovana, 'brPitanjaPoStrani' => $brPitanjaPoStrani,
                            'idRedosledPitanja' => $redosledPitanja), array('idAnketa' => $anketa['idAnketa']))) {

                    //header("Location: edit_anketa.php?idAnketa={$anketa['idAnketa']}");
                    header("Location: index.php?p=28&idAnketa={$anketa['idAnketa']}");
                    exit();
                } else {
                    $error = '<br/>' . $sql->lastError;
                    $error = '<br/>' . $sql->lastQuery;
                    $this->data['error'] = $error;
                }
            } else {
                $this->data['error'] = $error;
            }
        }

        if (isset($_GET["idAnketa"])) {
            $idAnketa = $_GET['idAnketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            if ($error == "") {
                /*
                  $sql = new DbAnkete();
                  $pitanje = $sql->getPitanje($idPitanje);
                  $this->data['pitanje'] = $pitanje[0];
                  //ubacujemo pitanje u sesiju da bi mogli kasnije da uporedimo
                  $_SESSION['pitanje'] = serialize($this->data['pitanje']);
                 */
                $sql = new MySQL();
                $this->data['anketa'] = $sql->Select("Anketa", array('idAnketa' => $idAnketa));
                /* <input type="radio" name="redosledPitanja" value="poredu" id="poredu" checked="<?php echo $anketa['redosledPitanja'] == 0 ? 'true' : 'false'; ?>" /> */
                if ($this->data['anketa']['idRedosledPitanja'] == 0) {
                    $this->data['anketa']['idRedosledPitanja'] = array('poredu' => 'selected', 'slucajno' => '', 'rucno' => '');
                } else if ($this->data['anketa']['idRedosledPitanja'] == 1) {
                    $this->data['anketa']['idRedosledPitanja'] = array('poredu' => '', 'slucajno' => '', 'rucno' => 'selected');
                } else if ($this->data['anketa']['idRedosledPitanja'] == 2) {
                    $this->data['anketa']['idRedosledPitanja'] = array('slucajno' => 'selected', 'poredu' => '', 'rucno' => '');
                }

                if ($this->data['anketa']['isPersonalizovana'] == 0) {
                    $this->data['anketa']['isPersonalizovana'] = array('ne' => 'checked', 'da' => '');
                } else {
                    $this->data['anketa']['isPersonalizovana'] = array('ne' => '', 'da' => 'checked');
                }

                //print_r($this->data['anketa']);
                $_SESSION['anketa'] = $this->data['anketa'];
            } else {
                //header("Location: index.php");
            }
        } else {
            //ovo ne prikazivati u produkciji
            $error = "Pitanje nije setovano!";
            // header("Location: index.php");
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/edit_anketa_main", $this->data);
    }

}

