<?php

/**
 * Description of C_IzvestajiZbirni
 *
 * @author korisnik
 */
class PitanjeStatistika {

    public $tekst;
    public $idPitanje;
    public $odgovori;
    public $ukupnoOdgovora;

    function __construct($idPitanje, $tekst, $odgovori) {
        $this->tekst = $tekst;
        $this->id = $idPitanje;
        $this->odgovori = $odgovori;
		
		
		//echo '<pre>';
		//print_r($odgovori); die();
        
		
		for($i=0; $i<count($odgovori);$i++){
		    $this->ukupnoOdgovora+=$odgovori[$i]['brOdgovora'];
		}
		
		/*
		foreach ($odgovori as $o) {
            $this->ukupnoOdgovora+=$o['brOdgovora'];
        }
		*/
		
		/*
		foreach ((array) $odgovori as $o) {
				$this->ukupnoOdgovora+=$o['brOdgovora'];
		}*/
		
    }

    function getProcenatByIndexOdgovora($index) {
        if ($this->ukupnoOdgovora >= 1) {
            return $this->odgovori[$index]['brOdgovora'] * (100 / $this->ukupnoOdgovora);
        }
        return 0;
    }

    function getIdOdgovoraByIndexOdgovora($index) {
        return $this->odgovori[$index]['idOdgovora'];
    }

    function getOdgovori() {
        return $this->odgovori;
    }

    function prikazi() {
        ?>
        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border: 0px dotted black; width: 500px; padding: 0 0 20px 0; margin: 0 auto;">           
            <div style="padding: 0 0 10px 0;"><span style="border: 0px solid red;font-size: 17px; font-weight: 700;"><?php echo $this->tekst; ?></span></div>
            <table style="width: 500px;margin: 0px auto;" border="0px">
                <tbody>
                    <tr style="font-size: 11px;">
                        <td> </td>
                        <td style="text-align: right; font-style: italic;">Br. odgovora</td>
                        <td style="text-align: right; font-style: italic; width: 60px;">Procenat</td>     
                    </tr>
                    <?php
                    for ($i = 0; $i < count($this->odgovori); $i++) {
                        ?>
                        <tr>
                            <td style="font-size: 14px;"><?php echo ucfirst($this->odgovori[$i]['tekst']); ?></td>
                            <td style="text-align: right;"><?php echo $this->odgovori[$i]['brOdgovora']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($this->getProcenatByIndexOdgovora($i), 2, '.', ''); ?>%</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-bottom:6px;"><progress style="width: 100%; height: 11px;" value="<?php echo $this->odgovori[$i]['brOdgovora']; ?>" max="<?php echo $this->ukupnoOdgovora; ?>"></progress></td>                            
                        </tr>
                    <?php } ?>
                </tbody>   
            </table>
        </div>
        <?php
    }

//end class PitanjeStatistika
}

class PitanjeStatistikaWithNestoDrugo extends PitanjeStatistika {
    /* array('tekst', brOdgovora) */

    protected $nestoDrugoInfo;
    protected $odgovoriNestoDrugo;

    public function __construct($idPitanje, $tekst, $odgovori, $nestoDrugoInfo, $odgovoriNestoDrugo) {

        parent::__construct($idPitanje, $tekst, $odgovori);

        $this->nestoDrugoInfo = $nestoDrugoInfo;
        $this->ukupnoOdgovora+=$nestoDrugoInfo['brOdgovora'];
        $this->odgovoriNestoDrugo = $odgovoriNestoDrugo;
    }

    function getProcenatNestoDrugo() {
        if ($this->ukupnoOdgovora >= 1) {
            return $this->nestoDrugoInfo['brOdgovora'] * (100 / $this->ukupnoOdgovora);
        }
        return 0;
    }

    function prikazi() {
        ?>
        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border: 0px dotted black; width: 500px; padding: 0 0 20px 0; margin: 0 auto;">           
            <div style="padding: 0 0 10px 0;"><span style="border: 0px solid red;font-size: 17px; font-weight: 700;"><?php echo $this->tekst; ?></span></div>
            <table style="width: 500px;margin: 0px auto;" border="0px">
                <tbody>
                    <tr style="font-size: 11px;">
                        <td> </td>
                        <td style="text-align: right; font-style: italic;">Br. odgovora</td>
                        <td style="text-align: right; font-style: italic; width: 60px;">Procenat</td>     
                    </tr>
                    <?php
                    for ($i = 0; $i < count($this->odgovori); $i++) {
                        ?>
                        <tr>
                            <td style="font-size: 14px;"><?php echo ucfirst($this->odgovori[$i]['tekst']); ?></td>
                            <td style="text-align: right;"><?php echo $this->odgovori[$i]['brOdgovora']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($this->getProcenatByIndexOdgovora($i), 2, '.', ''); ?>%</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-bottom:6px;"><progress style="width: 100%; height: 11px;" value="<?php echo $this->odgovori[$i]['brOdgovora']; ?>" max="<?php echo $this->ukupnoOdgovora; ?>"></progress></td>                            
                        </tr>                        
                    <?php }//for  ?>

                    <tr>
                        <td style="font-size: 14px;"><?php echo ucfirst($this->nestoDrugoInfo['tekst']); ?></td>
                        <td style="text-align: right;"><?php echo $this->nestoDrugoInfo['brOdgovora']; ?></td>
                        <td style="text-align: right;"><?php echo number_format($this->getProcenatNestoDrugo(), 2, '.', ''); ?>%</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding-bottom:6px;"><progress style="width: 100%; height: 11px;" value="<?php echo $this->nestoDrugoInfo['brOdgovora']; ?>" max="<?php echo $this->ukupnoOdgovora; ?>"></progress></td>                            
                    </tr>                     
                </tbody>   
            </table>
            <div style="padding: 10px 0 0 20px;">
                <?php
                if ($this->nestoDrugoInfo['brOdgovora'] > 0) {
                    echo "Uneti odgovori: ";
                    foreach ($this->odgovoriNestoDrugo as $ond) {                        
                        if ($ond['brOdgovora'] > 1) {
                            echo "<b>{$ond['tekst']}({$ond['brOdgovora']})</b>";
                        } else {
                            echo $ond['tekst'];
                        }
                        echo ", ";
                    }
                }
                ?>
            </div>            
        </div>
        <?php
    }

    //class PitanjeStatistikaWithNestoDrugo 
}

class PitanjeStatistikaTekst extends PitanjeStatistika {

    public function __construct($idPitanje, $tekst, $odgovori) {
        parent::__construct($idPitanje, $tekst, $odgovori);
    }

    function getProcenatNestoDrugo() {
        if ($this->ukupnoOdgovora >= 1) {
            return $this->nestoDrugoInfo[0]->brOdgovora * (100 / $this->ukupnoOdgovora);
        }
        return 0;
    }

    function prikazi() {
        ?>
        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border: 0px dotted black; width: 500px; padding: 0 0 20px 0; margin: 0 auto;">           
            <div style="padding: 0 0 10px 0;"><span style="border: 0px solid red;font-size: 17px; font-weight: 700;"><?php echo $this->tekst; ?></span></div>

            <div style="padding: 10px 0 0 20px;">
                <?php
				
				for ($i=0; $i<count($this->odgovori); $i++) {                    
                    if ($this->odgovori[$i]['brOdgovora'] > 1) {
                        echo "<b>{$this->odgovori[$i]['tekst']}({$this->odgovori[$i]['brOdgovora']})</b>, ";
                    } else {
                        echo $this->odgovori[$i]['tekst'].", ";
                    }
                }
				
				/*Prijavljuje grešku, ne znam zašto, pa sam odustao
				//Warning: Invalid argument supplied for foreach() in 
                foreach ($this->odgovori as $o) {
                    echo " * ";
                    if ($o['brOdgovora'] > 1) {
                        echo "<b>{$o['tekst']}({$o['brOdgovora']})</b>";
                    } else {
                        echo $o['tekst'];
                    }
                }*/
                ?>
            </div>            
        </div>
        <?php
    }

    //class PitanjeStatistikaTekst
}

class PitanjeStatistikaMatrica {

    private $potpitanja;

    function __construct($idPitanje, $tekst, $potpitanja) {
        $this->tekst = $tekst;
        $this->id = $idPitanje;
        $this->potpitanja = $potpitanja;
		/*
		echo '<pre>';
		print_r($potpitanja);
		die();
		*/
		
    }

    public function prikaziProgersBar() {
        
    }

    public function prikazi0() {
        foreach ($this->potpitanja as $pot) {
            $pot->prikazi();
        }
    }

    public function prikazi() {
        $num = "a";
        ?>

        <div style="border-top: 1px dotted black; width: 95%; padding: 30px 0 0 0; margin: 0 auto;"></div>
        <div style="border-top: 0px dotted black; width: 100%; padding: 0 0 30px 0;">
            <div style='padding: 0 0 0 30px;'>
                <span style='border: 0px solid red;font-size: 17px; font-weight: 700;'><?php echo $this->tekst; ?></span>
                <div style='padding: 0 0 0 15px;'>

                    <table border='0px' cellspacing='0' width='720px' class='table_pitanje'>
                        <tr><td></td>
                            <?php
                            $odgovori = $this->potpitanja[0]->getOdgovori();
                            //prikazuje podunjene odgovore (prvi red tabele)                                   
                            foreach ($odgovori as $odg) {
                                ?>                                    
                                <td>
                                    <span style='font-weight:bold; width:100%;'><?php echo $odg['tekst']; ?></span><br/>
                                    <span style='font-style:italic; font-size:12px;'>Br. odg. / Proc.</span>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                        $j = 0; //zbog parnih i neparnih kolona
                        //prikazuje kolonu potpitanja 
                        for ($i = 0; $i < count($this->potpitanja); $i++) {
                            ?>
                            <tr style='color: black; <?php echo ($j++ % 2 == 0 ? 'background-color: lightgrey;' : '') ?>'>                         
                                <td style='text-align:left; font-size:15px;'>
                                    <?php echo $num . ") " . $this->potpitanja[$i]->tekst; ?>
                                </td>
                                <?php
                                $odgovori = $this->potpitanja[$i]->getOdgovori();
                                for ($ii = 0; $ii < count($odgovori); $ii++) {
                                    ?>      
                                    <td>
                                        <?php echo $odgovori[$ii]['brOdgovora'] . " / " . number_format($this->potpitanja[$i]->getProcenatByIndexOdgovora($ii), 2, '.', '') . "%"; ?>
                                    </td>
                                    <!-- može i ovako
                                    <td style="text-align:center;padding: 0px;">
                                        <table border="0px;" style="font-size: 14px; border-spacing:0; border-collapse:collapse; margin: 0 auto;">
                                            <tr>
                                                <td style="text-align: right;">
                                    <?php echo $odgovori[$ii]->brOdgovora; ?>
                                                </td>
                                                <td>/</td>
                                                <td style="text-align: right;">
                                    <?php echo number_format($this->potpitanja[$i]->getProcenatByIndexOdgovora($ii), 2, '.', '') . "%"; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    -->
                                <?php } ?>
                            </tr>
                            <?php
                            $num++;
                        }//foreach 
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }

//end class PitanjeStatistikaMatrica
}


class C_IzvestajZbirni extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(0);
        $error = "";
        if (isset($_GET["anketa"])) {
            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            } else {
                
            }

            $sql = new DbAnkete();
            $this->data['anketaInfo'] = $anketaInfo = $sql->getAnketaInfo($idAnketa);

            $pitanja = $sql->getPitanjaInfo2($idAnketa);			
            $pitanjaStatistika = array();
			
			if (count($pitanja) == count($pitanja, COUNT_RECURSIVE)) 
			{
			  //echo 'array is not multidimensional';
			}
			else
			{
			 // echo 'array is multidimensional';
			 //print_r($pitanja); die();
			 
			  foreach ($pitanja as $p) {

					//Ako je pitanje matrica
					if ($p['idVrstaOdgovora'] == 5 || $p['idVrstaOdgovora'] == 6) {
						$potpitanja = $sql->getPotpitanjaInfo($p['idPitanje']);
						$potpitanjaStatistika = array();

						foreach ($potpitanja as $pot) {
							$odgovori = $sql->getFinishedAlMatricaOdgovoriWithBrojOdgovora($pot['idPotpitanje'], $idAnketa, $p['idPitanje']);
							$potpitanjaStatistika[] = new PitanjeStatistika($pot['idPotpitanje'], $pot['tekst'], $odgovori);
						}
						$pitanjaStatistika[] = new PitanjeStatistikaMatrica($p['idPitanje'], $p['tekst'], $potpitanjaStatistika);
					} else if ($p['idVrstaOdgovora'] == 1 || $p['idVrstaOdgovora'] == 2 || $p['idVrstaOdgovora'] == 3 || $p['idVrstaOdgovora'] == 4) {

						$odgovori = $sql->getFinishedAlOdgovoriWithBrojOdgovora($idAnketa, $p['idPitanje']);
						if ($p['idNestoDrugo']==null || $p['idNestoDrugo']==0) {
							if (count($odgovori) >= 1) {
								$pitanjaStatistika[] = new PitanjeStatistika($p['idPitanje'], $p['tekst'], $odgovori);
							}
						} else {
							$nestoDrugoInfo = $sql->getAlNestoDrugoInfoWithUkupnoOdgovora($idAnketa, $p['idPitanje']);
							//   var_dump($nestoDrugoInfo);
							//   exit();
							$odgovoriNestoDrugo = $sql->getAlNestoDrugoWithBrojOdgovora($idAnketa, $p['idPitanje']);
							$pitanjaStatistika[] = new PitanjeStatistikaWithNestoDrugo($p['idPitanje'], $p['tekst'], $odgovori, $nestoDrugoInfo, $odgovoriNestoDrugo);
						}
					} else if ($p['idVrstaOdgovora'] == 7 || $p['idVrstaOdgovora'] == 8 || $p['idVrstaOdgovora'] == 9) {
						$odgovori = $sql->getAlTekstOdgovoriWithBrojOdgovora($idAnketa, $p['idPitanje']);
						if (count($odgovori) >= 1) {
							$pitanjaStatistika[] = new PitanjeStatistikaTekst($p['idPitanje'], $p['tekst'], $odgovori);
						}
					}
				}
			}

			
      
            $this->data['error'] = $error;
            $this->data['pitanjaStatistika'] = $pitanjaStatistika;

            $this->loadPage('pages/izvestaj_zbirni', $this->data);
            //$this->loadPage("pages/edit_pitanje", $this->data);
        }
    }

}
