<?php

include_once 'classes/PitanjaAnketniListic.class.php';

class C_SacuvajOdgovore extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(0);

        $error = "";
        if (isset($_GET['anketniListic']) && isset($_POST['nextPage'])) {

            //treba proveriti iz sesije da li se podudaraju idAnketnog listica
            $idAnketniListic = $_GET['anketniListic'];
            $strana = $_POST['nextPage'];

            $pitanja = unserialize($_SESSION['pitanja']);
            if ($pitanja == null) {
                //treba promeniti
                header("Loction: index.php");
            }

            foreach ($pitanja->getPitanja() as $p) {
                //$tipPitanja = $p->getIdVrstaOdgovora();
                $sql = new DbAnkete();
                if ($p instanceof PitanjeRadio || $p instanceof PitanjeSelect) {
                    if ($p instanceof PitanjeRadioWithNestoDrugo) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pit" . $p->getIdPitanje();
                        } else {
                            $name = "n_pit" . $p->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            continue;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            continue;
                        }

                        //odgovori[0] -radio 
                        //odgovori[1] -text
                        $idOdgovori = $_POST[$name];


                        //ako je izabrano nestoDrugo, proveri da li tekst prazan
                        if (substr($idOdgovori[0], 0, 2) == "1_" && $idOdgovori[1] == "") {
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno! <br/>Morate popuniti tekst 'Nešto drugo!'";
                            continue;
                        }

                        $brIzabranihOdgovora = $p->getBrojIzabranihOdgovora();

                        $vecIzabranIdOdgovor = false;
                        //ako postoji odgovor, i nije nešto drugo, ažuriraj ga 
                        if ($brIzabranihOdgovora == 1 && substr($idOdgovori[0], 0, 2) != "1_") {

                            if ($p->getIsSetNestoDrugo()) {
                                // $sql->Update("AlOdgovor", array('idOdgovor' => $idOdgovori[0]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                $sql->Delete("AlNestoDrugoOdgovor", array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovori[0]), "AlOdgovor");
                            } else {

                                $odgovori = $p->getOdgovori();

                                //proveri da li je izabrani odgovor isti kao i prethodno izabran
                                foreach ($odgovori as $o) {
                                    if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovori[0]) {
                                        $vecIzabranIdOdgovor = true;
                                        break;
                                    }
                                }
                                if ($vecIzabranIdOdgovor == false) {
                                    if ($sql->Update("AlOdgovor", array('idOdgovor' => $idOdgovori[0]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()))) {
                                        
                                    }
                                }
                            }
                            //ako na pitanje nije odgovarano, a novi odgovor je različito od nestoDrugo
                        } else if ($brIzabranihOdgovora == 0 && substr($idOdgovori[0], 0, 2) != "1_") {

                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovori[0]), "AlOdgovor");

                            //ako na pitanje nije odgovarano, a novi odgovor je nestoDrugo
                            //   } else if ($brIzabranihOdgovora == 0 && $idOdgovori[0] == '0') {
                        } else if ($brIzabranihOdgovora == 0 && substr($idOdgovori[0], 0, 2) == "1_") {

                            //insert nesto drugo, za idOdgovor unosimo nulu
                            //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovori[0], 'drugo' => $idOdgovori[1]), "AlOdgovor");
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovorNestoDrugo' => substr($idOdgovori[0], 2), 'tekst' => $idOdgovori[1]), "AlNestoDrugoOdgovor");

                            //ako je na pitanje odgovarano, a novi odgovor je neštoDrugo
                        } else if ($brIzabranihOdgovora == 1 && substr($idOdgovori[0], 0, 2) == "1_") {
                            if ($p->getIsSetNestoDrugo()) {
                                //ako su tekst odgovori razloči ažuriraj samo teskt
                                if ($p->getNestoDrugoTekst() != $idOdgovori[1]) {
                                    //$sql->Update("AlOdgovor", array('drugo' => $idOdgovori[1]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));                                    
                                    $sql->Update("AlNestoDrugoOdgovor", array('tekst' => $idOdgovori[1]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                }
                            } else {
                                //$sql->Update("AlOdgovor", array('idOdgovor' => $idOdgovori[0], 'drugo' => $idOdgovori[1]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                $sql->Delete("AlOdgovor", array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovorNestoDrugo' => substr($idOdgovori[0], 2), 'tekst' => $idOdgovori[1]), "AlNestoDrugoOdgovor");
                            }
                        }

                        //PitanjeRadio i PitanjeSelect
                    } else {
                        if ($p->getIsObavezno()) {
                            $name = "d_pit" . $p->getIdPitanje();
                        } else {
                            $name = "n_pit" . $p->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            continue;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            continue;
                        }

                        $idOdgovor = $_POST[$name];
                        //ako je select kontrola i nije nista izabrano preskoči pitanje
                        if ($idOdgovor == 0) {
                            continue;
                        }
                        $brIzabranihOdgovora = $p->getBrojIzabranihOdgovora();

                        $vecIzabranIdOdgovor = false;
                        //ako postoji odgovor ažuriraj ga 
                        if ($brIzabranihOdgovora == 1) {
                            $odgovori = $p->getOdgovori();

                            //proveri da li je izabrani odgovor isti kao i prethodno izabran
                            foreach ($odgovori as $o) {
                                if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovor) {
                                    $vecIzabranIdOdgovor = true;
                                    break;
                                }
                            }

                            if ($vecIzabranIdOdgovor == false) {
                                if ($sql->Update("AlOdgovor", array('idOdgovor' => $idOdgovor), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()))) {
                                    
                                }
                            }
                        } else {
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");
                        }
                    }
                } else if ($p instanceof PitanjeCheckBox || $p instanceof PitanjeSelectMultiple) {
                    if ($p instanceof PitanjeCheckBoxWithNestoDrugo) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pit" . $p->getIdPitanje();
                        } else {
                            $name = "n_pit" . $p->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            continue;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            continue;
                        }

                        $odgovoriNoviId = $_POST[$name];

                        //treba neka provera veličine niza
                        //mora da bude niz
                        if (is_array($odgovoriNoviId)) {
                            $odgovoriStari = $p->getOdgovori();


                            $isSetNestoDrugo = false;

                            //ovo je nova vrsta provere da li setovano nesto drugo
                            if (substr($odgovoriNoviId[count($odgovoriNoviId) - 2], 0, 2) == "1_") {
                                //if ($odgovoriNoviId[count($odgovoriNoviId) - 2] == 0) {
                                $isSetNestoDrugo = true;
                                if ($odgovoriNoviId[count($odgovoriNoviId) - 1] == "") {
                                    $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno! <br/>Morate popuniti tekst 'Nešto drugo!'";
                                    continue;
                                }
                            }

                            //ako se setovano nešto drugo niz je veći za dva elementa
                            $odgovoriNoviIdSize = 0;
                            $isSetNestoDrugo ? $odgovoriNoviIdSize = count($odgovoriNoviId) - 2 : $odgovoriNoviIdSize = count($odgovoriNoviId);
                            //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                            foreach ($odgovoriStari as $oStari) {
                                $stariPostoji = false;
                                for ($i = 0; $i < $odgovoriNoviIdSize; $i++) {
                                    if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                        $stariPostoji = true;
                                        
                                        //ako stari postoji označimo nulom njegov id
                                        $odgovoriNoviId[$i] = 0;
                                        break;
                                    }
                                }
                                //ako stari odgovor nije izabran, brišemo ga
                                if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                    $sql->Delete("AlOdgovor", array('idAnketniListic' => $idAnketniListic, 'idOdgovor' => $oStari->getIdOdgovor()));
                                }
                            }

                            //unosimo nove odgovore
                            for ($i = 0; $i < $odgovoriNoviIdSize; $i++) {
                                if ($odgovoriNoviId[$i] > 0) {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $odgovoriNoviId[$i]), "AlOdgovor");
                                }
                            }

                            if ($isSetNestoDrugo) {
                                if ($p->getIsSetNestoDrugo()) {
                                    //ako odgovori različiti updejtuj nestoDrugo
                                    if ($p->getNestoDrugoTekst() != $odgovoriNoviId[count($odgovoriNoviId) - 1]) {
                                        $sql->Update("AlNestoDrugoOdgovor", array('tekst' => $odgovoriNoviId[count($odgovoriNoviId) - 1]), array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                    }
                                } else {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovorNestoDrugo' => substr($odgovoriNoviId[count($odgovoriNoviId) - 2], 2), 'tekst' => $odgovoriNoviId[count($odgovoriNoviId) - 1]), "AlNestoDrugoOdgovor");
                                }
                            } else {
                                //ako je prethodno setovano, obiši ga
                                if ($p->getIsSetNestoDrugo()) {
                                    $sql->Delete('AlNestoDrugoOdgovor', array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje()));
                                }
                            }
                            /*
                              foreach ($odgovoriNoviId as $oNoviId) {
                              if ($oNoviId > 0) {
                              $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $oNoviId), "AlOdgovor");
                              //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");
                              }
                              }
                             */
                        } else {
                            //neka greska checkbox pitanje mora da bude niz                        
                        }
                    } else {
                        if ($p->getIsObavezno()) {
                            $name = "d_pit" . $p->getIdPitanje();
                        } else {
                            $name = "n_pit" . $p->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            continue;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            continue;
                        }

                        $odgovoriNoviId = $_POST[$name];

                        //mora da bude niz
                        if (is_array($odgovoriNoviId)) {
                            $odgovoriStari = $p->getOdgovori();

                            //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                            foreach ($odgovoriStari as $oStari) {
                                $stariPostoji = false;
                                for ($i = 0; $i < count($odgovoriNoviId); $i++) {
                                    if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                        $stariPostoji = true;
                                        $odgovoriNoviId[$i] = 0;
                                        break;
                                    }
                                }
                                //ako stari odgovor nije izabran, brišemo ga
                                if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                    $sql->Delete("AlOdgovor", array('idAnketniListic' => $idAnketniListic, 'idOdgovor' => $oStari->getIdOdgovor()));
                                }
                            }

                            //unosimo nove odgovore
                            foreach ($odgovoriNoviId as $oNoviId) {
                                if ($oNoviId > 0) {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $oNoviId), "AlOdgovor");
                                    //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");                                
                                }
                            }
                        } else {
                            //neka greska checkbox pitanje mora da bude niz                        
                        }
                    }
                } else if ($p instanceof PitanjeMatricaRadio) {
                    $potpitanja = $p->getKolekcijaPotpitanjaMatrica()->getPotpitanja();

                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPotpitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPotpitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                            //ako pitanje nije obavezno, a izabrano je prvo potpitanje, upisaće ga ubazu, zato što se još uvek ne zna da li su ostala potpitanja odgovorena
                            //u suprotnom, da na prvo potpitanje nije odgovoreno, izaćiće se iz petlje, tako da ostala potpitanja neće biti upisana u bazu
                        } else if (!isset($_POST[$name])) {
                            break;
                        }

                        $idOdgovor = $_POST[$name];
                        $brIzabranihOdgovora = $pot->getBrojIzabranihOdgovora();

                        $vecIzabranIdOdgovor = false;
                        //ako postoji odgovor ažuriraj ga 
                        if ($brIzabranihOdgovora == 1) {
                            $odgovori = $pot->getOdgovori();

                            //proveri da li je izabrani odgovor isti kao i prethodno izabran
                            foreach ($odgovori as $o) {
                                if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovor) {
                                    //echo "<br/>Odgovor je isti: " . $idOdgovor;
                                    $vecIzabranIdOdgovor = true;
                                    break;
                                }
                            }
                            //Ako novi odgovor nije isti ko stari, ažiriraj strari novim
                            if ($vecIzabranIdOdgovor == false) {
                                if ($sql->Update("AlMatricaOdgovor", array('idOdgovor' => $idOdgovor), array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje()))) {
                                    
                                }
                            }
                        } else {
                            //ako nije postojalo starog odgovora, unesi novi odgovor
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $idOdgovor), "AlMatricaOdgovor");
                        }
                    }
                } else if ($p instanceof PitanjeMatricaCheckBox) {
                    $potpitanja = $p->getKolekcijaPotpitanjaMatrica()->getPotpitanja();
                    //ako pitanje nije obavezno, a ponunjava se, mora u celosti da se popuni
                    $prazno = false;
                    foreach ($potpitanja as $pot) {
                        //pošto, je početno ime isto razlikuje se samo potpitanje, moglo bi ispred for petlje da se napravi neki pomoćni string
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPotpitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPotpitanje();
                        }

                        //ako je obavezno i prazno učitaj ponovo stranicu
                        if ($p->getIsObavezno() && empty($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            $prazno = true;
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (empty($_POST[$name])) {
                            //ako nije obavezno, a prazno je
                            $prazno = true;
                            break;
                        }
                    }
                    if ($prazno) {
                        continue;
                    }

                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPotpitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPotpitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            break;
                        }

                        $odgovoriNoviId = $_POST[$name];
                        if (is_array($odgovoriNoviId)) {
                            $odgovoriStari = $pot->getOdgovori();

                            //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                            foreach ($odgovoriStari as $oStari) {
                                $stariPostoji = false;
                                for ($i = 0; $i < count($odgovoriNoviId); $i++) {
                                    if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                        $stariPostoji = true;
                                        $odgovoriNoviId[$i] = 0;
                                        break;
                                    }
                                }
                                //ako stari odgovor nije izabran, brišemo ga
                                if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                    $sql->Delete("AlMatricaOdgovor", array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $oStari->getIdOdgovor()));
                                    //echo '<br>delete';
                                    //echo $sql->affected . "<br>";
                                }
                            }

                            //unosimo nove odgovore
                            foreach ($odgovoriNoviId as $oNoviId) {
                                if ($oNoviId > 0) {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'idOdgovor' => $oNoviId), "AlMatricaOdgovor");
                                    //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");                                
                                    // echo '<br>insert';
                                    // echo $sql->lastError;
                                }
                            }
                        } else {
                            //neka greska checkbox pitanje mora da bude niz                        
                        }
                    }//foreach
                } else if ($p instanceof PitanjeMatricaCheckBox2) {
                    $potpitanja = $p->getPotpitanja();

                    //ako pitanje nije obavezno, a ponunjava se, mora u celosti da se popuni
                    $prazno = false;
                    foreach ($potpitanja as $pot) {
                        //pošto, je početno ime isto razlikuje se samo potpitanje, moglo bi ispred for petlje da se napravi neki pomoćni string
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPitanje();
                        }

                        //ako je obavezno i prazno učitaj ponovo stranicu
                        if ($p->getIsObavezno() && empty($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            $prazno = true;
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (empty($_POST[$name])) {
                            //ako nije obavezno, a prazno je
                            $prazno = true;
                            break;
                        }
                    }
                    if ($prazno) {
                        continue;
                    }

                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (!isset($_POST[$name])) {
                            break;
                        }

                        $odgovoriNoviId = $_POST[$name];
                        if (is_array($odgovoriNoviId)) {
                            $odgovoriStari = $pot->getOdgovori();

                            //prolzimo kroz stare odgovore, i ako ne postoje u novim, brišemo ih
                            foreach ($odgovoriStari as $oStari) {
                                $stariPostoji = false;
                                for ($i = 0; $i < count($odgovoriNoviId); $i++) {
                                    if ($odgovoriNoviId[$i] == $oStari->getIdOdgovor() && $oStari->getIsIzabran()) {
                                        $stariPostoji = true;
                                        $odgovoriNoviId[$i] = 0;
                                        break;
                                    }
                                }
                                //ako stari odgovor nije izabran, brišemo ga
                                if ($stariPostoji == false && $oStari->getIsIzabran()) {
                                    $sql->Delete("AlMatricaOdgovor", array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPitanje(), 'idOdgovor' => $oStari->getIdOdgovor()));
                                    echo '<br>delete';
                                    echo $sql->affected . "<br>";
                                }
                            }

                            //unosimo nove odgovore
                            foreach ($odgovoriNoviId as $oNoviId) {
                                if ($oNoviId > 0) {
                                    $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPitanje(), 'idOdgovor' => $oNoviId), "AlMatricaOdgovor");
                                    //$sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPitanje' => $p->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlOdgovor");                                

                                    echo '<br>insert';
                                    echo $sql->lastError;
                                }
                            }
                        } else {
                            //neka greska checkbox pitanje mora da bude niz                        
                        }
                    }//foreach
                } else if ($p instanceof PitanjeMatricaRadio2) {
                    $potpitanja = $p->getPotpitanja();

                    //ako pitanje nije obavezno, a ponunjava se, mora u celosti da se popuni
                    $prazno = false;
                    foreach ($potpitanja as $pot) {

                        //pošto, je početno ime isto razlikuje se samo potpitanje, moglo bi ispred for petlje da se napravi neki pomoćni string
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPitanje();
                        }

                        //ako je obavezno i prazno učitaj ponovo stranicu
                        if ($p->getIsObavezno() && empty($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            $prazno = true;
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (empty($_POST[$name])) {
                            //ako nije obavezno, a prazno je
                            $prazno = true;
                            break;
                        }
                    }
                    if ($prazno) {
                        continue;
                    }

                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPitanje();
                        }

                        if ($p->getIsObavezno() && !isset($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                            //ako pitanje nije obavezno, a izabrano je prvo potpitanje, upisaće ga ubazu, zato što se još uvek ne zna da li su ostala potpitanja odgovorena
                            //u suprotnom, da na prvo potpitanje nije odgovoreno, izaćiće se iz petlje, tako da ostala potpitanja neće biti upisana u bazu
                        } else if (!isset($_POST[$name])) {
                            break;
                        }

                        $idOdgovor = $_POST[$name];
                        $brIzabranihOdgovora = $pot->getBrojIzabranihOdgovora();

                        $vecIzabranIdOdgovor = false;
                        //ako postoji odgovor ažuriraj ga 
                        if ($brIzabranihOdgovora == 1) {
                            $odgovori = $pot->getOdgovori();

                            //proveri da li je izabrani odgovor isti kao i prethodno izabran
                            foreach ($odgovori as $o) {
                                if ($o->getIsIzabran() && $o->getIdOdgovor() == $idOdgovor) {
                                    //echo "<br/>Odgovor je isti: " . $idOdgovor;
                                    $vecIzabranIdOdgovor = true;
                                    break;
                                }
                            }
                            //Ako novi odgovor nije isti ko stari, ažiriraj strari novim
                            if ($vecIzabranIdOdgovor == false) {
                                if ($sql->Update("AlMatricaOdgovor", array('idOdgovor' => $idOdgovor), array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPitanje()))) {
                                    
                                }
                            }
                        } else {
                            //ako nije postojalo starog odgovora, unesi novi odgovor
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPitanje(), 'idOdgovor' => $idOdgovor), "AlMatricaOdgovor");
                            // echo $sql->lastError;
                        }
                    }
                } else if ($p instanceof PitanjeSlobodanUnos) {
                    $potpitanja = $p->getPotpitanja();
                    //  echo "Obavezno: " . $p->getIsObavezno();

                    $prazno = false;
                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPotpitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPotpitanje();
                        }

                        //ako je obavezno i prazno učitaj ponovo stranicu
                        if ($p->getIsObavezno() && empty($_POST[$name])) {
                            //$strana-=1;
                            $error .= "Pitanje: '" . $p->getTekst() . "', je obavezno!";
                            $prazno = true;
                            break;
                            //header("Location: index.php?idAnketniListic={$idAnketniListic}&strana={$strana}");
                            //exit();
                        } else if (empty($_POST[$name])) {
                            //ako nije obavezno, a prazno je
                            $prazno = true;
                            break;
                        }
                    }
                    if ($prazno) {
                        continue;
                    }
                    //ako nije prazno
                    // if (!$prazno) {
                    foreach ($potpitanja as $pot) {
                        if ($p->getIsObavezno()) {
                            $name = "d_pot" . $pot->getIdPotpitanje();
                        } else {
                            $name = "n_pot" . $pot->getIdPotpitanje();
                        }

                        if ($pot->getOdgovor() == "") {
                            $sql->Insert(array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje(), 'tekst' => $_POST[$name]), "AlTekstOdgovor");
                            // echo $sql->lastError;
                        } else if ($pot->getOdgovor() != $_POST[$name]) {
                            $sql->Update("AlTekstOdgovor", array('tekst' => $_POST[$name]), array('idAnketniListic' => $idAnketniListic, 'idPotpitanje' => $pot->getIdPotpitanje()));
                            //echo $sql->lastError;
                        }
                    }
                    //}
                }
            }//endforeach
            // header("Location: index.php?idAnketniListic={$_GET['idAnketniListic']}&strana={$_POST['nextPage']}");
            //exit();
            //videći treba da izmenim
            $anketaInfo = $_SESSION['anketaInfo'];
            $brPitanjaPoStrani = $anketaInfo['brPitanjaPoStrani'];
            $ukupnoPitanja = $anketaInfo['ukupnoPitanja'];
            $brStrana = ceil($ukupnoPitanja / $brPitanjaPoStrani);
            //provira da li je neko predao anketu
            if ($strana == $brStrana) {
                //poziva se predavanje ankete
                header("Location: index.php?p=31&anketniListic={$idAnketniListic}");
                exit();
            }

            header("Location: index.php?p=4&anketniListic={$idAnketniListic}&strana={$strana}");
        }
    }

}

?>
