<?php

include_once 'classes/PitanjeRadio.class.php';
include_once 'classes/PitanjeMatricaRadio.class.php';
include_once 'classes/PitanjeMatricaCheckBox.class.php';
include_once 'classes/PotpitanjeMatrica.class.php';

class C_NewPitanje extends Controller {

    function __construct($idAnketa2 = '') {

        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";
        if (isset($_POST['idAnketa']) && isset($_POST['tekst']) && isset($_POST['idVrstaOdgovora'])) {
            $idAnketa = $_POST['idAnketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            $tekst = $_POST['tekst'];
            if ($tekst == '') {
                $error.= '<br/>Tekst pitanja, ne sme biti prazan';
            }

            $idVrstaOdgovora = $_POST['idVrstaOdgovora'];
            if (!Validator::Numeric($idVrstaOdgovora)) {
                $error.= "<br/>Pogrešan format Vrste odgovora!";
            }

            $isObavezno = 0;
            if (isset($_POST['isObavezno'])) {
                $isObavezno = $_POST['isObavezno'];
                if ($isObavezno != '1') {
                    $error.= "<br/>Greška, pogrešna vrednost obavezno!";
                } else {
                    $isObavezno = 1;
                }
            }

            //provera da li su odgovori unešeni
            if ($idVrstaOdgovora != 7 && $idVrstaOdgovora != 8 && $idVrstaOdgovora != 9) {
                if (isset($_POST['odgovori'])) {
                    $odgovori = $_POST['odgovori'];
                    if (!is_array($odgovori)) {
                        $error.= "<br/>Greška nema odgovora!";
                    } else {
                        foreach ($odgovori as $o) {
                            if ($o == '') {
                                $error.= "<br/>Prazno polje odgovor!";
                            }
                        }
                    }
                } else {
                    $error.= "<br/>Greška, odgovori nisu setovani!";
                }
            }

            //if ($idVrstaOdgovora == 5 || $idVrstaOdgovora == 6 || $idVrstaOdgovora == 7 || $idVrstaOdgovora == 8 || $idVrstaOdgovora == 9) {
            //provera da li su potpitanja unešena
            if ($idVrstaOdgovora == 5 || $idVrstaOdgovora == 6) {

                if (isset($_POST['potpitanja'])) {
                    $potpitanja = $_POST['potpitanja'];
                    if (!is_array($potpitanja)) {
                        $error.= "<br/>Greška nema potpitanja!";
                    } else {
                        foreach ($potpitanja as $p) {
                            if ($p == '') {
                                $error.= "<br/>Prazno polje potpitanja!";
                            }
                        }
                    }
                } else {
                    $error.= "<br/>Greška, potpitanja nisu setovana!";
                }
            }

            if ($error == "") {
                $sql = new DbAnkete();

                //'datumKreiranja' => 'now()' ne radi, treba proveriti zašto, zbog glupe klase MySQL
                //$sql->Insert(array('idPitanje' => '', 'tekst' => $tekst, 'idKreator' => $user->idKorisnik, 'idVrstaOdgovora' => $idVrstaOdgovora, 'datumKreiranja' => 'now()'), 'Pitanje');
                $sql->createPitanje($tekst, $user->idKorisnik, $idVrstaOdgovora, 0);

                $idPitanje = $sql->ExecuteSQL("SELECT idPitanje 
                                  FROM Pitanje 
                                  WHERE idKreator={$user->idKorisnik} 
                                  ORDER BY idPitanje desc
                                  LIMIT 1"
                );

                //unos potpitanja
                if ($idVrstaOdgovora == 5 || $idVrstaOdgovora == 6 || $idVrstaOdgovora == 7 || $idVrstaOdgovora == 8) {
                    if (isset($_POST['potpitanja'])) {
                        $potpitanja = $_POST['potpitanja'];

                        for ($i = 0; $i < count($potpitanja); $i++) {
                            $sql->Insert(array('idPitanje' => $idPitanje['idPitanje'], 'tekst' => $potpitanja[$i]), 'Potpitanje');
                        }
                    }
                }

                //unos potpitanja, odnosno podnaslova, za dugačak tekst
                if ($idVrstaOdgovora == 9) {
                    if (isset($_POST['podnaslov'])) {
                        $podnaslov = $_POST['podnaslov'];

                        $sql->Insert(array('idPitanje' => $idPitanje['idPitanje'], 'tekst' => $podnaslov), 'Potpitanje');
                    }
                }

                //unos odgovora
                if ($idVrstaOdgovora != 7 && $idVrstaOdgovora != 8 && $idVrstaOdgovora != 9) {
                    //unos novih odgovora
                    for ($i = 0; $i < count($odgovori); $i++) {
                        $sql->Insert(array('idPitanje' => $idPitanje['idPitanje'], 'tekst' => $odgovori[$i]), 'Odgovor');
                    }
                }
                    //Ovo je provera ček boks kontrole
                if (isset($_POST['nestoDrugo'])) {
                    if ($_POST['nestoDrugo'] == "1") {

                        //ovde treba da ubacim novi tekst
                        if (isset($_POST['idOdgovorNestoDrugo'])) {
                            //ubacivanje pitanja u anketu, sa nestoDrugo               
                            $sql->Update("Pitanje", array('idNestoDrugo' => $_POST['idOdgovorNestoDrugo']), array('idPitanje' => $idPitanje['idPitanje'], 'idKreator' => $user->idKorisnik));                     
                        }
                    } else {
                        $error.= "<br/>Greška, nešto drugo je setovano u pogrešnom formatu!";
                    }
                } else {
                    
                }

                //ubacivanje pitanja u anketu                
                $sql->Insert(array('isObavezno' => $isObavezno, 'idPitanje' => $idPitanje['idPitanje'], 'idAnketa' => $idAnketa), "AnketaPitanje");

                //kada sačuvamo pitanje moramo ponovo da se vratimo na stranu
                $this->data['vrstaOdgovora'] = $sql->Select('VrstaOdgovora');
                $this->data['idAnketa'] = $idAnketa;
            }//$error==''
        } else if (isset($_GET['idAnketa'])) { //kada se prvi put pokrene strana
            $idAnketa = $_GET['idAnketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            $sql = new MySQL();

            $this->data['vrstaOdgovora'] = $sql->Select('VrstaOdgovora');
            $this->data['odgovorNestoDrugo']=$sql->Select('OdgovorNestoDrugo');
            $this->data['idAnketa'] = $idAnketa;
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/new_pitanje", $this->data);
    }

}

