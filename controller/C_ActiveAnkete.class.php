<?php

/**
 * Description of C_Unformed
 *
 * @author korisnik
 */
class C_ActiveAnkete extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(10);
        $error = "";

        if (isset($_GET['orderby']) || !isset($_GET['orderby'])) {
            //Osvežava se tabela, a ne cela strana

            /*
              <option value="nazivAsc">Nazivu: A - Z</option>
              <option value="nazivDesc">Nazivu: Z - A</option>
              <option value="datumPocetkaAsc">Dat. početka: rasuće</option>
              <option value="datumPocetkaDesc" selected>Dat. početka: opadajuće</option>
              <option value="datumIstekaAsc">Dat. isteka: rasuće</option>
              <option value="datumIstekaDesc">Dat. isteka: opadajuće</option>
             */
            if (isset($_GET['str'])) {
                //neka provra da li je ceo broj
                $strana = $_GET['str'];
                if ($strana < 0) {
                    header("Location: index.php?p=2");
                }
                $this->data['trenutna'] = $strana;
            } else {
                $this->data['trenutna'] = $strana = 0;
            }

            $limit = 10;
            if (isset($_GET['limit'])) {
                //neka provra da li je ceo broj
                $limit = $_GET['limit'];
                if ($limit < 1 || $limit > 50) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['limit'] = $limit;

            $orderby = "datumPocetkaDesc";
            if (isset($_GET['orderby'])) {
                $orderby = $_GET['orderby'];
            }

            $kljucneReci = "";
            if (isset($_GET['kljucnaRec'])) {
                $kljucneReci = explode(' ', $_GET['kljucnaRec']);
            }

            // $brZapisaPoStrani = 2;

            $ukupnoZapisa = 0;
            $sql = new DbAnkete();

            if (!empty($_GET['kljucnaRec'])) {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfActiveAnketaSearch($kljucneReci);
            } else {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfActiveAnketa();
            }

            $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);

            $pocetniZapis = $strana * $limit;

            $this->data['pocetniZapis'] = $pocetniZapis;
            $this->data['poslednja'] = $brStrana - 1;

            $activeAnketeInfo = new stdClass();
            $activeAnketeInfo->ukupnoZapisaPretrage = $ukupnoZapisa;


            $this->data['orderby'] = $orderby;
            switch ($orderby) {
                case 1: case "nazivAsc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('naziv', 'asc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'asc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'asc');
                    }
                    break;
                case 2: case "nazivDesc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('naziv', 'desc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'desc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'naziv', 'desc');
                    }
                    break;
                case 3: case "datumPocetkaAsc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('datumPocetka', 'asc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'asc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'asc');
                    }
                    break;
                case 4: case "datumPocetkaDesc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('datumPocetka', 'desc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'desc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumPocetka', 'desc');
                    }
                    break;
                case 5: case "datumIstekaAsc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('datumZavrsetka', 'asc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'asc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'asc');
                    }
                    break;
                case 6: case "datumIstekaDesc":
                    //$this->data['ankete'] = $sql->getAnketeInfoActive('datumZavrsetka', 'desc');
                    if ($user != null) {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'desc');
                    } else {
                        $activeAnketeInfo->ankete = $this->data['ankete'] = $sql->getActiveAnketeInfo(null, $pocetniZapis, $limit, $kljucneReci, 'datumZavrsetka', 'desc');
                    }
                    break;
            }

            //koristim ako radim ajax
            if (isset($_GET['ajax'])) {
                //ovde moram da dodelim pokazivač na ankete, jer se promenljiva koristi u active_ankete_table.php
                $ankete = $this->data['ankete'];
                include_once 'modules/mod_anketa/active_ankete_table.php';
            } else if (isset($_GET['ajaxJson'])) {
                //echo json_encode($this->data['ankete']);
                //echo json_encode($activeAnketeInfo);
                if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                    echo json_encode($activeAnketeInfo);
                } else {
                    echo "false";
                }
            } else {           
                //kada prvi put otvaramo stranu
                if ($this->data['ukupnoZapisa']==0) {
                    $this->data['message'] = "Trenutno nema ni jedna Anketa.";
                    $this->data['ankete'] = null;
                }

                $this->data['error'] = $error;
                $this->loadPage("pages/active_ankete", $this->data);
            }
        } else {

            /*

              if (isset($_GET['str'])) {
              //neka provra da li je ceo broj
              $this->data['trenutna'] = $strana = $_GET['str'];
              } else {
              $this->data['trenutna'] = $strana = 0;
              }
              $brZapisaPoStrani = 3;

              $sql = new DbAnkete();

              $ukupnoZapisa = $sql->getNumberOfActiveAnketa();
              $ukupnoZapisa = $ukupnoZapisa['brAnketa'];

              $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $brZapisaPoStrani);

              $pocetniZapis = $strana * $brZapisaPoStrani;

              $this->data['pocetniZapis'] = $pocetniZapis;
              $this->data['poslednja'] = $brStrana - 1;





              //$this->data['ankete'] = $sql->getAnketeActive();
              //var_dump($user);
              if ($user != null) {
              $this->data['ankete'] = $sql->getActiveAnketeInfoWithIsPopunjavana($user->idKorisnik, $pocetniZapis, $brZapisaPoStrani, 'datumPocetka', 'desc');
              //var_dump($this->data['ankete']);exit();
              } else {
              $this->data['ankete'] = $sql->getActiveAnketeInfo(null, 'datumPocetka', 'desc');
              //var_dump($this->data['ankete']);exit();
              }

              // $anketa->prikaziWithEditLink();
              if ($sql->arrayedResult == 0) {
              $this->data['message'] = "Trenutno nema ni jedna anketa.";
              $this->data['ankete'] = null;
              }

              $this->data['error'] = $error;
              $this->loadPage("pages/active_ankete", $this->data);
             */
        }
    }

}