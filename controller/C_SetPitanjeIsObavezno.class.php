<?php

class C_SetPitanjeIsObavezno extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";
        if (isset($_GET["anketa"]) && isset($_GET['pitanje']) && isset($_GET['isObavezno'])) {
            $idPitanje = $_GET['pitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format pitanja!";
            }

            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            }

            $isObavezno = $_GET['isObavezno'];
            if ($isObavezno != '1' && $isObavezno != '0') {
                $error = "Greška!";
            }

            $sql = new MySQL();
            $sql->Update("AnketaPitanje", array('isObavezno' => $isObavezno), array('idPitanje' => $idPitanje, 'idAnketa' => $idAnketa));
            if($sql->affected==1){
                echo 'true';
            }else{
                
            }
            
        } else {
            header("Location: index.php");
        }
    }

}

?>
