<?php

/**
 * Description of C_Nalozi
 *
 * @author korisnik
 */
class C_Nalozi extends Controller {

    function __construct() {

        $this->data['user'] = $user = User::checkUserPrivilege(-1);
        //kasnije sam dodao pro
        if (isset($_GET['stanje']) && !isset($_GET['pro'])) {
            $sql = new DbAnkete();
            $korisnici = "";
            if ($_GET['stanje'] == 'akt') {
                $korisnici = $sql->getNaloziAktivirani();

                //$sql->mysql_num_rows();
                //if ($sql->affected >= 1) {
                $this->data['korisnici'] = $korisnici;
                //} else {
                //  $this->data['message'] = "Nema aktivnih naloga.";
                //}

                $this->loadPage("pages/user/aktivirani_nalozi", $this->data);
            } else if ($_GET['stanje'] == 'nea') {
                $korisnici = $sql->getNaloziNeaktivirani();

                //if ($sql->getRecords() >= 1) {
                    $this->data['korisnici'] = $korisnici;
                //} else {
                //    $this->data['message'] = "Nema novopristiglih naloga za aktivaciju.";
                //}

                $this->loadPage("pages/user/neaktivirani_nalozi", $this->data);
            } else if ($_GET['stanje'] == 'blo') {
                $korisnici = $sql->getNaloziBlokirani();
                $this->data['korisnici'] = $korisnici;
                $this->loadPage("pages/user/blokirani_nalozi", $this->data);
            } else {
                
            }
        } else if (isset($_GET['pro']) && isset($_GET['id'])) {
            $sql = new DbAnkete();
            if ($_GET['pro'] == 'akt') {
                $sql->acivateNalog($_GET['id']);
            } else if ($_GET['pro'] == 'blo') {
                $sql->blockNalog($_GET['id']);
            } else if ($_GET['pro'] == 'obr') {
                $sql->deleteNalog($_GET['id']);
            } else {
                
            }
        }
    }

}
