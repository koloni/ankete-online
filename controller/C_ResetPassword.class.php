<?php
/**
 * Description of C_ChangePassword
 *
 * @author korisnik
 */
class C_ResetPassword extends Controller {

    function __construct() {
        $user = User::checkUserPrivilege(10);
        $this->data['user'] = $user;

        $error = "";
        $message = "";
        if (isset($_GET['reset_confirm']) and isset($_GET['email'])) {
            //if (isset($_GET['reset_confirm']) and isset($_GET['token']) and isset($_GET['email'])) {
            //otvoriti stranicu za promenu lozinke
            $email = $_GET['email'];
            if (!Validator::Email($email)) {
                $error.="<br/>Pogrešan format E adrese!";
            }

            $mysql = new MySQL();
            $mysql->ExecuteSQL("SELECT COUNT(*) FROM Korisnik WHERE token ='{$_GET['reset_confirm']}' AND email='{$email}'");
            if ($mysql->getRecords() == 1) {
                $this->data['token'] = $_GET['reset_confirm'];
                $this->loadPage("pages/user/new_password", $this->data);
                exit();
            } else {
                $error = "Zahtev za resetovanje lozinke više nije aktivan. Pošaljite novi zahtev.";
            }
        } else if (isset($_GET['reset_cancel']) and isset($_GET['email'])) {
            //updejt tokena, a može i postavljanje na null
            $mysql = new DbAnkete();
            $token = "NULL";
            $mysql->ExecuteSQL("UPDATE Korisnik SET token={$token} WHERE token='{$_GET['reset_cancel']}'");
            $message = "Zahtev za resetovanje lozinke je poništen.";
        } else if (isset($_POST["email"])) {
            //treba setovati token i datum tokena, i poslati email
            $email = $_POST['email'];
            if (!Validator::Email($email)) {
                $error.="<br/>Pogrešan format E adrese!";
            }

            $mysql = new DbAnkete();
            $mysql->ExecuteSQL("SELECT * FROM Korisnik WHERE email='{$email}'");
            if ($mysql->getRecords() == 1) {
                
            } else {
                $error = "Izbrana E-adresa ne postoji u sistemu!";
            }

            if ($error == "") {
                User::BeginResetPassword($email);
                $message = "Zahtev za resetovanje lozinke je uspešno prosleđen";
            }
        } else if (isset($_POST["token"]) && isset($_POST["password"]) && isset($_POST["passwordRepeated"])) {

            $password = $_POST['password'];
            if (!Validator::Alphanumeric($password, 5)) {
                $error.="<br/>Lozinka mora da sadrži minimum 5 karaktera!";
            }

            $passwordRepeated = $_POST['passwordRepeated'];
            if ($password != $passwordRepeated) {
                $error.="<br/>Lozinka i ponovljena lozinka moraju biti iste!";
            }

            if ($error == "") {
                $password = md5($password);
                $sql = new MySQL();

                $sql->Update("Korisnik", array('password' => $password), array('token' => $_POST["token"]));
                if ($sql->affected == 1) {
                    $token = "NULL";
                    $sql->ExecuteSQL("UPDATE Korisnik SET token={$token} WHERE token='{$_POST["token"]}'");
                    $this->data['message'] = "Lozinka uspešno promenjena.";
                    $this->loadPage("pages/user/login", $this->data);
                    exit();
                    //Ako se updejetuje istim podatkom, tj. istom lozinkom vraća affected 0. Mora da se unese druga lozinka
                } else if ($sql->affected == 0) {
                    //ispraviti nema css classe balona
                    $this->data['error'] = $error.="<br/>Ne možete koristiti istu lozinku!<br/>Unesite novu lozinku'!";
                    $this->data['token'] = $_POST["token"];
                    //učitaj istu stranu
                    $this->loadPage("pages/user/new_password", $this->data);
                    exit();
                } else {
                    $error.="<br/>Došlo je do greške prilikom promene lozinke!";
                }
            }
        }

        $this->data['message'] = $message;
        $this->data['error'] = $error;
        $this->loadPage("pages/user/reset_password", $this->data);
    }

}
