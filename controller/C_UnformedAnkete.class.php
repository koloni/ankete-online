<?php

/**
 * Description of C_Unformed
 *
 * @author korisnik
 */
class C_UnformedAnkete extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);

        $error = "";

        if ($error == "") {
            $sql = new DbAnkete();
            $ankete = $sql->getAnketeUnformedInfo($user->idKorisnik);
            //$ankete = $sql->getAnketeInfoUnformed($user->idKorisnik);
            
            // $anketa->prikaziWithEditLink();\    
            //ovako ne može je čim pokušam nešto da upišem on napravi kopiju $a niza
            /*
              foreach ($ankete as $a) {
              if ($a['redosledPitanja'] == 0) {
              $a['redosledPitanja'] = "po redosledu unosa";
              } else if ($a['redosledPitanja'] == 1) {
              $a['redosledPitanja'] = 'Ručno izabrano';
              } else if ($a['redosledPitanja'] == 2) {
              $a['redosledPitanja'] = 'Slučajno generisan';
              } else {
              //neka greška
              }
              }
             */

            if ($sql->getRecords() >= 1) {
                $this->data['ankete'] = $ankete;
            } else {
                $this->data['message'] = "Nema anketa za uređivanje, napravite novu anketu.";
            }
        }
        // }

        $this->data['error'] = $error;
        $this->loadPage("pages/unformed_ankete", $this->data);
    }

}