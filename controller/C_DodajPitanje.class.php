<?php

class C_DodajPitanje extends Controller {

    function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(4);


        $error = "";
        //dodavanje pitanja
        if (isset($_GET["anketa"]) && isset($_GET['pitanje'])) {
            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format idAnketa!";
            }

            $idPitanje = $_GET['pitanje'];
            if (!Validator::Numeric($idPitanje)) {
                $error = "Pogrešan format pitanja!";
            }

            if ($error == "") {
                $sql = new DbAnkete();
                
               // var_dump($sql->dodajPitanje($idAnketa, $idPitanje));                exit();
                if ($sql->dodajPitanje($idAnketa, $idPitanje)>0) {
                    echo 'true';
                    
                } else {
                    echo 'false';
                }
            } else {
                $this->data['error'] = $error;
                $this->loadPage("pages/user/login", $this->data);
            }
            
            //Prikazuje listu pitanja za dodavanje    
        } else if (isset($_GET["anketa"])) {            
            $idAnketa = $_GET['anketa'];
            if (!Validator::Numeric($idAnketa)) {
                $error = "Pogrešan format ankete!";
            }

            //$sql = new DbAnkete();
            //$pitanja = $sql->getListaPitanjaNotInAnketa($idAnketa);
            //$this->data['pitanja'] = $pitanja;
            $this->data['idAnketa'] = $idAnketa;

            $strana = 0;
            if (isset($_GET['str'])) {
                //neka provra da li je ceo broj
                $strana = $_GET['str'];
                if ($strana < 0) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['trenutna'] = $strana;

            $limit = 10;
            if (isset($_GET['limit'])) {
                //neka provra da li je ceo broj
                $limit = $_GET['limit'];
                if ($limit < 1 || $limit > 50) {
                    header("Location: index.php?p=2");
                }
            }
            $this->data['limit'] = $limit;

            $kljucneReci = "";
            if (isset($_GET['kljucnaRec'])) {
                $kljucneReci = explode(' ', $_GET['kljucnaRec']);
            }

            $orderby = "tekstAsc";
            if (isset($_GET['orderby'])) {
                $orderby = $_GET['orderby'];
            }
            $this->data['orderby'] = $orderby;

            $sql = new DbAnkete();

            $ukupnoZapisa = 0;
            if (!empty($_GET['kljucnaRec'])) {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfPitanjaNotInAnket($idAnketa, 1, null, $kljucneReci);
            } else {
                $this->data['ukupnoZapisa'] = $ukupnoZapisa = $sql->getNumberOfPitanjaNotInAnket($idAnketa, 1);
            }

            $this->data['brStrana'] = $brStrana = ceil($ukupnoZapisa / $limit);
            $this->data['pocetniZapis'] = $pocetniZapis = $strana * $limit;
            $this->data['poslednja'] = $brStrana - 1;

            $stdObject = new stdClass();
            $stdObject->ukupnoZapisaPretrage = $ukupnoZapisa;

            switch ($orderby) {
                case 1: case "tekstAsc":
                    $stdObject->ankete = $this->data['pitanja'] = $sql->getListaPitanjaNotInAnketa($idAnketa, 1, null, $pocetniZapis, $limit, $kljucneReci, 'tekst', 'asc');
                    break;
                case 2: case "tekstDesc":
                    $stdObject->ankete = $this->data['pitanja'] = $sql->getListaPitanjaNotInAnketa($idAnketa, 1, null, $pocetniZapis, $limit, $kljucneReci, 'tekst', 'desc');
                    break;
                case 3: case "datumKreiranjaAsc":
                    $stdObject->ankete = $this->data['pitanja'] = $sql->getListaPitanjaNotInAnketa($idAnketa, 1, null, $pocetniZapis, $limit, $kljucneReci, 'datumKreiranja', 'asc');
                    break;
                case 4: case "datumKreiranjaDesc":
                    $stdObject->ankete = $this->data['pitanja'] = $sql->getListaPitanjaNotInAnketa($idAnketa, 1, null, $pocetniZapis, $limit, $kljucneReci, 'datumKreiranja', 'desc');
                    break;
            }

            if (isset($_GET['ajaxJson'])) {
                if ($sql->getRecords() >= 1) {// && $ankete['naziv']!=null) {
                    echo json_encode($stdObject);
                } else {
                    echo "false";
                }
            } else {
                //kada prvi put otvaramo stranu
                if ($this->data['ukupnoZapisa'] == 0) {
                    $this->data['message'] = "Trenutno nema ni jedno pitanje.";
                    $this->data['ankete'] = null;
                }

                $this->data['error'] = $error;
                //$this->loadPage("pages/zabrane_anketa", $this->data);
                $this->loadPage("pages/dodaj_pitanje", $this->data);
            }
        }

        //__construct()
    }

    //class C_DodajPitanje
}
