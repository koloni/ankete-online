<?php

class UserRequest {

    public function proba() {
        if (isset($_GET['confirm_registration'])) {
            User::ConfirmRegistration($_GET['confirm_registration']);
        }
        if (isset($_POST['btn_confirm_registration'])) {
            $username = $_POST['tb_username'];
            $password = $_POST['tb_password'];
            $email = $_POST['tb_email'];
            if (!Validator::Email($email)) {
                echo "Invalid email address";
            } else {
                User::BeginRegistration($username, $email, $password);
            }
        }


        include_once 'views/user/register.php';        
    }

    public function register() {
        if (isset($_GET['confirm_registration'])) {
            User::ConfirmRegistration($_GET['confirm_registration']);
        }
        if (isset($_POST['btn_confirm_registration'])) {
            $username = $_POST['tb_username'];
            $password = $_POST['tb_password'];
            $email = $_POST['tb_email'];
            if (!Validator::Email($email)) {
                echo "Invalid email address";
            } else {
                User::BeginRegistration($username, $email, $password);
            }
        }
    }

}

?>
