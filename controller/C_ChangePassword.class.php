<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_ChangePassword
 *
 * @author korisnik
 */
class C_ChangePassword extends Controller {

    function __construct() {
        $user = User::checkUserPrivilege(0);
        $this->data['user'] = $user;

        $error = "";
        $message = "";
        if (isset($_POST["username"]) && isset($_POST["passwordCurrent"]) && isset($_POST["passwordNew"]) && isset($_POST["passwordRepeated"])) {
            // Session::SetKey("user", "nikola");
            // return;

            $username = $_POST['username'];
            if (!Validator::Alphanumeric($username)) {
                $error.="<br/>Korisničko ime mora da sadrži minimum 3 karaktera!";
            }

            $passwordCurrent = $_POST['passwordCurrent'];
            if (!Validator::Alphanumeric($passwordCurrent, 5)) {
                $error.="<br/>Lozinka mora da sadrži minimum 5 karaktera!";
            }

            $passwordNew = $_POST['passwordNew'];
            if (!Validator::Alphanumeric($passwordNew, 5)) {
                $error.="<br/>Nova lozinka mora da sadrži minimum 5 karaktera!";
            }

            $passwordRepeated = $_POST['passwordRepeated'];
            if ($passwordNew != $passwordRepeated) {
                $error.="<br/>Nova lozinka i ponovljena lozinka moraju biti iste!";
            }

            if ($passwordNew == $passwordCurrent) {
                $error.="<br/>'Trenutna Lozinka' i 'Nova Lozinka', moraju biti različite!";
            }

            if ($error == "") {
                $passwordNew = md5($passwordNew);
                $passwordCurrent = md5($passwordCurrent);
                $sql = new MySQL();

                $sql->Update("Korisnik", array('password' => $passwordNew), array('username' => $username, 'password' => $passwordCurrent));
                if ($sql->affected == 1) {
                    $message = "Lozinka uspešno promenjena.";
                    //header("Location: login.php");
                    //exit();
                    //Ako se updejetuje istim podatkom vraća affected 0
                } else if ($sql->affected == 0) {
                    $error.="<br/>Pogrešno 'Korisničko ime' i/ili 'Trenutna lozinka'!";
                } else {
                    $error.="<br/>Došlo je do greške prilikom promene lozinke!";
                }
            }
        }

        $this->data['message'] = $message;
        $this->data['error'] = $error;
        $this->loadPage("pages/user/change_password", $this->data);
    }

}
