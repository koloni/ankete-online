<?php

class C_CreateNalog extends Controller {

    public function __construct() {
        $this->data['user'] = $user = User::checkUserPrivilege(-1);
        $this->data['error'] = "";

        if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['gender']) &&
                isset($_POST['birthDate']) && isset($_POST['phone']) && isset($_POST['email']) &&
                isset($_POST['username']) && isset($_POST['password']) &&
                isset($_POST['passwordRepeated']) && isset($_POST['type'])) {

            $this->register();
        } else {
            $this->loadPage("pages/user/create_nalog", $this->data);
        }
    }

    private function register() {
        $error = "";
        $firstName = $_POST['firstName'];
        if ($firstName == "") {
            $error.="<br/>Ime je obavezno!";
        } else {
            if (!Validator::Alphabet($firstName)) {
                //echo $firstName;
                $error.="<br/>Ime mora da sadrži minimum 3 karaktera!";
            }
        }
        $lastName = $_POST['lastName'];
        if ($lastName == "") {
            $error.="<br/>Prezime je obavezno!";
        } else {
            if (!Validator::Alphabet($lastName)) {
                $error.="<br/>Prezime mora da sadrži minimum 3 karaktera!";
            }
        }

        $gender = $_POST['gender'];
        if ($gender == 'male') {
            $gender = 1;
        } elseif ($gender == 'female') {
            $gender = 2;
        } else {
            $gender = 0;
        }

        $birthDate = $_POST['birthDate'];
        if ($birthDate != "") {
            if (!Validator::DateOrDateTime($birthDate)) {
                $error.="<br/>Datum rođenja je unešen u pogrešnom formatu!";
            }
        } else {
            //Odustao od obaveznog datuma rođenja
            //$error.="<br/>Datum rođenja je obavezan!";
        }

        $phone = $_POST['phone'];
        if ($phone != "") {
            if (!Validator::Phone($phone)) {
                $error.="<br/>Pogrešan format telefona";
            }
        } else {
            //$error.="<br/>Telefon je obavezan!";            
        }

        $email = $_POST['email'];
        if (!Validator::Email($email)) {
            $error.="<br/>Pogrešan format email adrese!";
            //echo "Invalid email address";
        }

        $username = $_POST['username'];
        if (!Validator::AlphanumericUTF($username)) {
            $error.="<br/>Korisničko ime mora da sadrži minimum 3 karaktera!";
        }

        $password = $_POST['password'];
        if (!Validator::AlphanumericUTF($password, 5)) {
            $error.="<br/>Lozinka mora da sadrži minimum 5 karaktera!";
        }

        $passwordRepeated = $_POST['passwordRepeated'];
        if ($password != $passwordRepeated) {
            $error.="<br/>Lozinka i ponovljena lozinka moraju da budu iste!";
        }

        $type = $_POST['type'];

        if ($type == 'ispitanik') {
            $type = 0;
        } else if ($type == 'sluzbenik') {
            $type = 1;
        } else if ($type == 'kreator') {
            $type = 2;
        } else {
            $error = "greška tipa naloga!";
            //ne može da se unese admin sa ove forme
            //$type = -1;
        }

        DbBroker::Connect();
        $emailDb = DbBroker::GetRow("SELECT email FROM Korisnik WHERE email='{$email}'");
        // var_dump($emailDb);
        if ($emailDb != "") {
            $error .= "<br/>Izabrana E-adresa '{$email}', je već registrovana!<br/>Molimo izaberite drugu 'E-adresu'";
        }

        $usernameDb = DbBroker::GetRow("SELECT username FROM Korisnik WHERE username='{$username}'");
        if ($usernameDb != "") {
            $error .= "<br/>Izabrano Korisničko ime '{$username}', je već registrovano!<br/>Molimo izaberite drugo 'Korisničko ime'";
        }

        if ($error == "") {
            User::BeginRegistration($username, $password, $firstName, $lastName, $gender, $birthDate, null, null, $phone, $email, $type, 1);
        }
        $this->data['error'] = $error;
        $this->loadPage("pages/user/create_nalog", $this->data);
    }

}

