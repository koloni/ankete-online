<?php

class C_login extends Controller {

    function __construct($error2 = '') {
        $error = $error2;
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            $username = $_POST['username'];
            if (!Validator::AlphanumericUTF($username)) {
                $error.="Korisničko ime mora da sadrži minimum 3 karaktera!<br/>";
            }

            $password = $_POST['password'];
            if (!Validator::AlphanumericUTF($password, 5)) {
                $error.="Lozinka mora da sadrži minimum 5 karaktera!<br/>";
            }

            if ($error == "") {
                if (($u = User::Login($username, $password)) != null) {
                    $u->SetSessions();
                    header("Location: index.php");
                    exit();
                } else {
                    $error.="Pogrešno korisničko ime i/ili lozinka!";
                }
            }
        }

        $this->data['error'] = $error;
        $this->loadPage("pages/user/login", $this->data);
    }

}

?>
