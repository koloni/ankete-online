<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class C_PretragaKorisnika {

    public function __construct() {

        if (!empty($_GET['kljucna_rec']) && !empty($_GET['filterPretrage'])) {

            $kljucneReci = explode(' ', $_GET['kljucna_rec']);

            //$korisnici = null;
            $sql = new DbAnkete();

            $upit = "SELECT * FROM Korisnik WHERE ";
            //ako je filter pretrage frstName ili lastName
            if ($_GET['filterPretrage'] == 1) {
                if (count($kljucneReci) == 1 && $kljucneReci[0] != '') {
                    $upit = $upit . "firstName LIKE '" . $kljucneReci[0] . "%' ";
                } else if (count($kljucneReci) == 2) {
                    $upit = $upit . "firstName LIKE '" . $kljucneReci[0] . "%' AND lastName LIKE '" . $kljucneReci[1] . "%' ";
                } else {
                    exit();
                }
            } else if ($_GET['filterPretrage'] == 2) {
                $upit = $upit . "username LIKE '" . $kljucneReci[0] . "%' ";
            } else if ($_GET['filterPretrage'] == 3) {
                $upit = $upit . "email LIKE '" . $kljucneReci[0] . "%' ";
            } else {
                exit();
            }

            $korisnici = $sql->ExecuteSQL($upit);


            if (is_array($korisnici)) {
                echo json_encode($korisnici);
            } else {
                //pokušao sam da echo '' prazan string i da xmlhttp.responseText; proveravam da li prazan string i nije htelo da radi
                //znam zašto nije htelo da radi, jer meni jSon dodajen <br/>ispred rezultata
                echo 'false';
            }
        }
    }

}

//Odustao, a prešao na vraćanje json-a i štampanje u javasript-u
class C_PretragaKorisnika0 {

    public function __construct() {

        if (!empty($_GET['kljucna_rec'])) {

            $niz = explode(' ', $_GET['kljucna_rec']);

            $korisnici = null;
            $sql = new DbAnkete();
            $upit = "SELECT * FROM Korisnik WHERE ";
            if (count($niz) == 1 && $niz[0] != '') {
                $upit = $upit . "firstName LIKE '" . $niz[0] . "%' ";
                $korisnici = $sql->ExecuteSQL($upit);
            } else if (count($niz) == 2) {
                $upit = $upit . "firstName LIKE '" . $niz[0] . "%' AND lastName LIKE '" . $niz[1] . "%' ";
                $korisnici = $sql->ExecuteSQL($upit);
            } else {
                //echo ' ';
            }

            //ako mi vrati jednodimenzionalni niz baguje
            if (count($korisnici) != count($korisnici, 1)) {
                for ($i = 0; $i < count($korisnici); $i++) {
                    ?><a style='text-decoration:none;' href="izaberiKorisnika(<?php echo "'" . $korisnici[$i]['idKorisnik'] . "', '" . $korisnici[$i]['firstName'] . "', '" . $korisnici[$i]['lastName'] ?>')"><?php echo $korisnici[$i]['firstName'] . " " . $korisnici[$i]['lastName']; ?></a><br/><?php
                }
            } else if (is_array($korisnici)) {
                echo $korisnici['firstName'] . " " . $korisnici['lastName'] . "<br/>";
            } else {
                echo 'Nema rezultata pretrage!';
            }
        }
    }

}