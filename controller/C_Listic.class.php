<?php

/**
 * Description of C_Listic
 *
 * @author korisnik
 */
include_once 'classes/PitanjaAnketniListic.class.php';

class C_Listic extends Controller {

    function __construct() {

        $this->data['user'] = $user = User::checkUserPrivilege(0);

        $error = "";
        //$_GET['idAnketniListic'] = 1;
        if (isset($_GET["anketniListic"]) && isset($_GET['strana'])) {          

            //if($user->type==4){}
            $idAnketniListic = $_GET['anketniListic'];
            if (!Validator::Numeric($idAnketniListic)) {
                $error = "Pogrešan format idAnketnogListica!";
            }

            $strana = $_GET['strana'];
            if (!Validator::Numeric($strana)) {
                $error = "Pogrešan format strane!";
            }

            if ($error == "") {
                $sql = new DbAnkete();
                //$anketaInfo = $sql->getAnketaInfo(1);

                $this->data['idAnketniListic'] = $idAnketniListic;
                $anketaInfo = $_SESSION['anketaInfo'];
                $brPitanjaPoStrani = $anketaInfo['brPitanjaPoStrani'];
                $ukupnoPitanja = $anketaInfo['ukupnoPitanja'];
                $brStrana = ceil($ukupnoPitanja / $brPitanjaPoStrani);

                $pocetnoPitanje = $strana * $brPitanjaPoStrani;
                $this->data['pocetnoPitanje'] = $pocetnoPitanje;
                $this->data['procenat'] = $pocetnoPitanje * (100 / $ukupnoPitanja);
                $this->data['trenutna'] = $strana;
                $this->data['poslednja'] = $brStrana - 1;
                $this->data['anketaInfo'] = $anketaInfo;


                //postavljanje početnog rednog broja trenutne strane
                Pitanje::setRedniBroj($pocetnoPitanje);
                $pitanja = new PitanjaAnketniListic($idAnketniListic, $pocetnoPitanje, $brPitanjaPoStrani);

                $_SESSION['pitanja'] = serialize($pitanja);
                $this->data['pitanja'] = $pitanja;
            }
            $this->loadPage("pages/listic", $this->data);
        } else if (isset($_GET["anketa"])) {

            //provera da li se unose svoji odgovori, ili drugog ispitanika
            $idIspitanik = "";
            if (!empty($_GET["ispitanik"])) {
                //exit();
                Session::SetKey('idIspitanik', $idIspitanik = $_GET["ispitanik"]);
            } else {
                Session::SetKey('idIspitanik', $idIspitanik = $user->idKorisnik);
            }

            //ako otvaramo stranu 0
            $strana = 0;
            //$idAnketniListic = $_GET['idAnketniListic'];
            $idAnketa = $_GET['anketa'];
            // $idAnketa = 8;
            if ($error == "") {
                $sql = new DbAnkete();

                $idAnketniListic = $sql->getAnketniListicId($idAnketa, $idIspitanik);
                //ako anketni listić nije napravljen, napravi ga
                if ($idAnketniListic == false) {
                    $idAnketniListic = $sql->newAnketniListic($idAnketa, $idIspitanik);
                }

                $this->data['idAnketniListic'] = $idAnketniListic;
                $anketaInfo = $sql->getAnketaInfo($idAnketa);
                $_SESSION['anketaInfo'] = $anketaInfo;

                $brPitanjaPoStrani = $anketaInfo['brPitanjaPoStrani'];
                $ukupnoPitanja = $anketaInfo['ukupnoPitanja'];

                $brStrana = ceil($ukupnoPitanja / $brPitanjaPoStrani);

                $pocetnoPitanje = ($strana) * $brPitanjaPoStrani;
                $this->data['pocetnoPitanje'] = $pocetnoPitanje;
                $this->data['procenat'] = ($pocetnoPitanje / $ukupnoPitanja) * 100;

                $this->data['trenutna'] = $strana;
                $this->data['poslednja'] = $brStrana - 1;
                $this->data['anketaInfo'] = $anketaInfo;

                $pitanja = new PitanjaAnketniListic($idAnketniListic, $pocetnoPitanje, $brPitanjaPoStrani);

                //postavljanje početnog rednog broja trenutne strane
                //Pitanje::setRedniBroj($pocetnoPitanje);

                $_SESSION['pitanja'] = serialize($pitanja);
                $this->data['pitanja'] = $pitanja;
            }

            $this->loadPage("pages/listic", $this->data);
        }
    }

}

?>
